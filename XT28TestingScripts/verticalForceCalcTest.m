%test script to check force vertical calculation
clear all, clc

cylinderPos_mm = 0:0.04:0.5

%bx = 0.165;
%by = 0.471;
bxy = 0.4991;
l1 = 1008/1000;
%l2 = 386/1000;
l3 = 1211/1000;
theta2 = 0.3370;
theta3 = 0.2983;

for cylinder_mm = cylinderPos_mm
    
    stroke = 0.82 + cylinder / 1000.0;
    alpha = acos( (bxy^2 + l3^2 - stroke.^2) / (2 * bxy * l3) );
    theta = alpha + theta2 + theta3;
    theta1 = theta - theta3;
    beta1 = acos( (stroke.^2 + bxy^2 - l3^2) ./ (2 * stroke * bxy) );
    
    %gamma = 0;
    %relationship = 0;
    
    if(beta1 < pi / 2 + theta2)
        gamma = pi / 2 - (beta1 - theta2);
        gammacase = 1
    else
        gamma = -(beta1 - (pi / 2 + theta2));
        gammacase = 2
    end
    if (theta1 > pi / 2)
        relationship = l3 * ((sin(gamma) .* cos(theta1 - pi / 2) - cos(gamma) .* sin(theta1 - pi / 2)) ./ (l1 * cos(theta - pi/2)));
        case123 = 1
    else
        if(gamma > 0)
            relationship = l3 * ((sin(gamma) .* cos(pi / 2 - theta1) + cos(gamma) .* sin(pi / 2 - theta1)) ./ (l1 * cos(pi / 2 - theta)));
            case123 = 2
        else
            relationship = l3 * ((sin(gamma) .* cos(pi / 2 - theta1) + cos(gamma) .* sin(pi / 2 - theta1)) ./ (l1 * cos(pi / 2 - theta)));
            %case123 = 3
        end
    end
    %relationship
end

for i = 1:6
    i
end