%low pass filterr d term

load('C:\Users\ladmin\Desktop\Dropbox\thesis\Logg\piddar\brus_d_del_step_phi.mat')
time=0.01:0.01:length(sl_debug_1)*0.01;

%plot(time,sl_debug_1)

 rng default
Fs = 100;
%t = 0:1/Fs:1-1/Fs;
x = sl_debug_1;
N = length(x);
xdft = fft(x);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(x):Fs/2;

plot(freq,(psdx))
grid on
title('Periodogram Using FFT')
xlabel('Frequency (Hz)')
ylabel('Power/Frequency (dB/Hz)')


%%
Ts = 0.01;
f_cutoff = 6;
Tf = (2 * pi )/f_cutoff
alpha = Tf / (Tf + Ts)

   Df(1) = 0;

for k = 1:1:length(sl_debug_1)-1
     Df(k+1) = Df(k) * alpha + (1 - alpha) * sl_debug_1(k+1);
end
%plot(sl_debug_1,'r');hold on
plot(Df);hold on
plot(Phi_deg*10)
plot(phiReference*10)
axis([0 inf -150 150])



%%
fftshift(fft(Df))
 
 Y = fft(Df);
 Pyy = Y.*conj(Y)/251;
f = 1/0.01/251*(0:127);
 plot(f(1:50),Pyy(1:50))
title('Power spectral density')
xlabel('Frequency (Hz)')
%%
f = 1/((2.72-2.668)+1/0.01);
w=2*pi*f;
s = tf('s');

G = 1 / (s /w+ 1);

[a b]=bode(G)

margin(G)


Ts = 0.01;

f_cutoff = 28;
Tf = 1 / (2 * pi * f_cutoff)

alpha = Tf / (Tf + Ts)