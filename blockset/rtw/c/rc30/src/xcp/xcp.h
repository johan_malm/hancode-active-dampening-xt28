/****************************************************************************************
|  Description: Application specifics for the XCP driver header file
|    File Name: xcp.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef XCP_H
#define XCP_H


/****************************************************************************************
* Include files
****************************************************************************************/
#include "xcpBasic.h"                                  /* XCP protocol header          */


/****************************************************************************************
* Macro definitions with plausibility check
****************************************************************************************/
#ifdef USE_XCP_CAN_1
  #ifndef USE_XCP_DRIVER
    #define USE_XCP_DRIVER
  #endif
  #define XCP_CHANNEL_USED CAN_1
  /* XCP is designed for use on ONE channel only */
  #if defined(USE_XCP_CAN_2) || defined(USE_XCP_CAN_3) || defined(USE_XCP_CAN_4)
    #error XCP driver is designed for use one ONE channel only
  #endif
  #define USE_CAN_1
#endif

#ifdef USE_XCP_CAN_2
  #ifndef USE_XCP_DRIVER
    #define USE_XCP_DRIVER
  #endif
  #define XCP_CHANNEL_USED CAN_2
  /* XCP is designed for use on ONE channel only */
  #if defined(USE_XCP_CAN_1) || defined(USE_XCP_CAN_3) || defined(USE_XCP_CAN_4)
    #error XCP driver is designed for use one ONE channel only
  #endif
  #define USE_CAN_2
#endif

#ifdef USE_XCP_CAN_3
  #ifndef USE_XCP_DRIVER
    #define USE_XCP_DRIVER
  #endif
  #define XCP_CHANNEL_USED CAN_3
  /* XCP is designed for use on ONE channel only */
  #if defined(USE_XCP_CAN_1) || defined(USE_XCP_CAN_2) || defined(USE_XCP_CAN_4)
    #error XCP driver is designed for use one ONE channel only
  #endif
  #define USE_CAN_3
#endif

#ifdef USE_XCP_CAN_4
  #ifndef USE_XCP_DRIVER
    #define USE_XCP_DRIVER
  #endif
  #define XCP_CHANNEL_USED CAN_4
  /* XCP is designed for use on ONE channel only */
  #if defined(USE_XCP_CAN_1) || defined(USE_XCP_CAN_2) || defined(USE_XCP_CAN_3)
    #error XCP driver is designed for use one ONE channel only
  #endif
  #define USE_CAN_4
#endif

/***************************************************************************************
* MTA Range Check defines
***************************************************************************************/
#define	ER_MTA_MEM_OUT_OF_RANGE        (0xF500)

/****************************************************************************************
* Function prototypes
****************************************************************************************/
void XcpInitDriver(void);
void XcpEventChannel(uint8 channel);


/****************************************************************************************
* Hook function prototypes
****************************************************************************************/
void                ApplXcpSend(vuint8 len, MEMORY_ROM BYTEPTR msg);
MTABYTEPTR          ApplXcpGetPointer(vuint8 addr_ext, vuint32 addr);
vuint8              ApplXcpUserService(MEMORY_ROM BYTEPTR pCmd);
#if defined(XCP_ENABLE_DAQ_TIMESTAMP)
XcpDaqTimestampType ApplXcpGetTimestamp(void);
#endif
#if defined(XCP_ENABLE_SEED_KEY)
vuint8              ApplXcpGetSeed(vuint8 resourceMask, vuint8 *seed);
vuint8              ApplXcpUnlock(MEMORY_ROM vuint8 *key,vuint8 length);
#endif


#endif /* XCP_H */
/*********************************** end of xcp.h **************************************/

