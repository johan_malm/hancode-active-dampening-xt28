/*----------------------------------------------------------------------------
| File:
|   XCP_CFG.H
|
| Project:
|   XCP on CAN Sample
|
 ----------------------------------------------------------------------------*/

#if defined ( __XCP_CFG_H__ )
#else
#define __XCP_CFG_H__

/*----------------------------------------------------------------------------*/

/* Platform specific includes */

#define XCP_ENABLE_PARAMETER_CHECK

/* Event specific settings */
#define XCP_ENABLE_DAQ_EVENT_INFO
#define kXcpMaxEvent 3

/*----------------------------------------------------------------------------*/
/* XCP parameters */

  /* 8-Bit qualifier */
typedef unsigned char  vuint8;
typedef signed char    vsint8;   

/* 16-Bit qualifier */
typedef unsigned short vuint16;
typedef signed short   vsint16;

/* 32-Bit qualifier */
typedef unsigned long  vuint32; 
typedef signed long    vsint32;

/* Special pointer types */  
#define MTABYTEPTR unsigned char*
#define DAQBYTEPTR unsigned char*

/*----------------------------------------------------------------------------*/
/* XCP protocol parameters */

/* Byte order */
/*#define C_CPUTYPE_BIGENDIAN *//* Motorola */
#define XCP_CPUTYPE_LITTLEENDIAN /* Intel */

/* XCP message length */
#define kXcpMaxCTO     8      /* Maximum CTO Message Lenght */
#define kXcpMaxDTO     8      /* Maximum DTO Message Lenght */

/*----------------------------------------------------------------------------*/
/* Optional GET_SEED and UNLOCK */

/*#define XCP_ENABLE_SEED_KEY*/

/*----------------------------------------------------------------------------*/
/* Optional memory checksum command */

/* Note: The checksum will be calculated in XcpBackground()
#define XCP_ENABLE_CHECKSUM
#define XcpChecksumType        vuint16
#define kXcpChecksumMethod     XCP_CHECKSUM_TYPE_ADD12
*/

/*----------------------------------------------------------------------------*/
/* XCP Data Acquisition Parameters */
                                  
/* Enable data acquisition */
#define XCP_ENABLE_DAQ    
#define XCP_ENABLE_DAQ_PRESCALER
#define XCP_ENABLE_DAQ_HDR_ODT_DAQ // Garrick

/* Memory space reserved for DAQ */
#define kXcpDaqMemSize 32768

/* Optional timestamped data acquisition */
#define XCP_ENABLE_DAQ_TIMESTAMP
typedef vuint16 XcpDaqTimestampType;
#define kXcpDaqTimestampSize                 2
#define kXcpDaqTimestampUnit                 DAQ_TIMESTAMP_UNIT_10MS
#define kXcpDaqTimestampTicksPerUnit         1

/*----------------------------------------------------------------------------*/
/* Implement Calibration Page switching */

/*#define XCP_ENABLE_CALIBRATION_PAGE*/

/*----------------------------------------------------------------------------*/
/* CAN identifiers for XCP.C */
extern vuint32 XCP_CRO_ID;
extern vuint32 XCP_DTO_ID;

/*----------------------------------------------------------------------------*/
#define XCP_ENABLE_USER_COMMAND

#endif

