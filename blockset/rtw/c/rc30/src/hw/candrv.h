/****************************************************************************************
|  Description: CAN driver header file
|    File Name: candrv.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef CAN_H
#define CAN_H

/****************************************************************************************
* Includes
****************************************************************************************/
#include "candrv_src.h"      /* Precompiled functionality */


/****************************************************************************************
* Function prototypes
****************************************************************************************/
void CanDrvInit(void);
extern void CanDrvByteSwap_x(uint8 *b, size_t n);

/* Callbacks and message functions per channel */
#ifdef USE_CAN_1
extern uint8 get_can_1_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
extern void reserve_can_1_receive_slot(uint32 reserve_id_u32);
extern uint8 change_can_1_receive_slot(uint32 old_id_u32, uint32 new_id_u32);
#endif
#ifdef USE_CAN_2
extern void can_2_RxCallback(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
extern uint8 get_can_2_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
extern void reserve_can_2_receive_slot(uint32 reserve_id_u32);
extern uint8 change_can_2_receive_slot(uint32 old_id_u32, uint32 new_id_u32);
#endif
#ifdef USE_CAN_3
extern void can_3_RxCallback(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
extern uint8 get_can_3_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
extern void reserve_can_3_receive_slot(uint32 reserve_id_u32);
extern uint8 change_can_3_receive_slot(uint32 old_id_u32, uint32 new_id_u32);
#endif
#ifdef USE_CAN_4
extern void can_4_RxCallback(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
extern uint8 get_can_4_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
extern void reserve_can_4_receive_slot(uint32 reserve_id_u32);
extern uint8 change_can_4_receive_slot(uint32 old_id_u32, uint32 new_id_u32);
#endif


/****************************************************************************************
* External data declarations
****************************************************************************************/
/* buffers for sending and receiving the CAN data */
#ifdef USE_CAN_1
extern supported_types can_1_send_message;
#endif
#ifdef USE_CAN_2
extern supported_types can_2_send_message;
#endif
#ifdef USE_CAN_3
extern supported_types can_3_send_message;
#endif
#ifdef USE_CAN_4
extern supported_types can_4_send_message;
#endif


#endif /* CAN_H */
/*********************************** end of candrv.h ***********************************/


