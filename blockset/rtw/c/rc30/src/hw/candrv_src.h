/****************************************************************************************
|  Description: CAN driver header file
|    File Name: candrv_src.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef CANDRV_SRC_H
#define CANDRV_SRC_H

/****************************************************************************************
* Include files
****************************************************************************************/
#include "api_lib_basic.h"            /* for Bosch Rexroth C-API  */
#include <stddef.h>                   /* for default types        */

/****************************************************************************************
* Defines
****************************************************************************************/
#define CAN_MAX_DATA_LENGTH          (8)
/* Use these in case you want support for low level commands*/
//#define SYSCOM_ID 0x667
//#define CAN_REPLY_ID 0x668

/****************************************************************************************
* Macro definitions
****************************************************************************************/
/* map ByteSwap function */
#define CanDrvByteSwap(x)        (CanDrvByteSwap_x((unsigned char *) &x, sizeof(x)))

/****************************************************************************************
* Global settings
****************************************************************************************/
char LowLevelCommands_1;
char LowLevelCommands_2;
char LowLevelCommands_3;
char LowLevelCommands_4;

char UseXCPCan_1;
char UseXCPCan_2;
char UseXCPCan_3;
char UseXCPCan_4;

/****************************************************************************************
* Typedefs & Structs
****************************************************************************************/ 
/* an union to easely support all types of data */
typedef union supported_types_def
{
    /* compiler types */
    uint8 ubytes[8];
    sint8 bytes[8];
    uint16 uint16_info[4];
    sint16 int16_info[4];
    uint32 uint32_info[2];
    sint32 int32_info[2];
    /* simulink types (for quick reference) */
    uint8 uint8_T_info[8];
    sint8 int8_T_info[8];
    uint16 uint16_T_info[4];
    sint16 int16_T_info[4];
    uint32 uint32_T_info[2];
    sint32 int32_T_info[2];
    uint8 boolean_T_info[8];
} supported_types;

/* a struct to remember received CAN messages */
typedef struct can_message_receive_def
{
    uint32 id_u32;
    uint8 message_length;
    uint8 new_parameter;
    supported_types received_message;
} can_message_receive;

/****************************************************************************************
* Function prototypes
****************************************************************************************/
/* Utility functions */
void CanDrvByteSwap_x(uint8 *b, size_t n);

/* Callbacks and message functions per channel */
extern void CanDrvRxCallbackCh1(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
uint8 get_can_1_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
void reserve_can_1_receive_slot(uint32 reserve_id_u32);
uint8 change_can_1_receive_slot(uint32 old_id_u32, uint32 new_id_u32);

extern void CanDrvRxCallbackCh2(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
uint8 get_can_2_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
void reserve_can_2_receive_slot(uint32 reserve_id_u32);
uint8 change_can_2_receive_slot(uint32 old_id_u32, uint32 new_id_u32);

extern void CanDrvRxCallbackCh3(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
uint8 get_can_3_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
void reserve_can_3_receive_slot(uint32 reserve_id_u32);
uint8 change_can_3_receive_slot(uint32 old_id_u32, uint32 new_id_u32);

extern void CanDrvRxCallbackCh4(uint8 format_u8, uint32 id_u32, uint8 numBytes_u8, uint8 *data_pau8);
uint8 get_can_4_message(uint32 search_id_u32, supported_types *message, uint8 *messageLength, uint8 *newData);
void reserve_can_4_receive_slot(uint32 reserve_id_u32);
uint8 change_can_4_receive_slot(uint32 old_id_u32, uint32 new_id_u32);

/* Init functions for each channel */                              
void doCanDrvInit1(can_Message_ts* buffer,
					uint16 bufferLength, can_RxDatabox_ts* databox,
					uint16 databoxNum, uint32 baudrate);
void doCanDrvInit2(can_Message_ts* buffer,
					uint16 bufferLength, can_RxDatabox_ts* databox,
					uint16 databoxNum, uint32 baudrate);
void doCanDrvInit3(can_Message_ts* buffer,
					uint16 bufferLength, can_RxDatabox_ts* databox,
					uint16 databoxNum, uint32 baudrate);
void doCanDrvInit4(can_Message_ts* buffer,
					uint16 bufferLength, can_RxDatabox_ts* databox,
					uint16 databoxNum, uint32 baudrate);


#endif /* CANDRV_SRC_H */
/*********************************** end of candrv_src.h ***********************************/
