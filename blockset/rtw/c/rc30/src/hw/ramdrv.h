/****************************************************************************************
|  Description: RAM driver header file
|    File Name: ramdrv.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef RAMDRV_H
#define RAMDRV_H


/****************************************************************************************
* Type definitions
****************************************************************************************/
/* struct to remember all variables */
typedef struct ram_reservation_def
{
  uint16 arrayID;
  uint8 arrayCount;
  uint8 varLenght;
  uint8 *pointer;
} ram_reservation;


/****************************************************************************************
* Function prototypes
****************************************************************************************/
void   reserve_ram(uint16 arrayID, uint8 arrayCount, uint8 varLenght);
void   empty_array(uint8 *pointer, uint8 startNr, uint16 arrayCount, uint8 dataLenght);
uint16 find_ramList_Nr(uint16 arrayID);
uint64 read_value_ram(uint16 arrayID, uint8 nr);
void   write_value_ram(uint16 arrayID, uint8 nr, uint64 value);
void   change_ram_size(uint16 arrayID, uint8 arrayCount);


/****************************************************************************************
* External data declaratons
****************************************************************************************/
extern ram_reservation *ram_reservation_list;


#endif /* RAMDRV_H */
/*********************************** end of ramdrv.h ***********************************/


