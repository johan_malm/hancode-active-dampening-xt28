/****************************************************************************************
|  Description: CAN driver source file
|    File Name: candrv.c
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/


/****************************************************************************************
* Include files
****************************************************************************************/
#include <stddef.h>                                    /* for default types            */
#include <malloc.h>                                    /* for malloc/free              */
#include <string.h>                                    /* for memcpy                   */
#include "SYS_config.h"                                /* for system configuration     */
#include "errorList.h"                                 /* for program error codes      */
#include "api_lib_basic.h"                             /* for Bosch Rexroth C-API      */
#include "candrv.h"                                    /* for CAN driver               */



/****************************************************************************************
* Macro definitions
****************************************************************************************/

#define CAN_DEFAULT_BAUDRATE         (500000)
#ifdef USE_CAN_1
#define CAN_1_TX_BUF_LEN_DU8         (can_1_TX_buffer_Size)
#define CAN_1_NUM_RX_DATABOXES_DU16  (1)
#endif
#ifdef USE_CAN_2
#define CAN_2_TX_BUF_LEN_DU8         (can_2_TX_buffer_Size)
#define CAN_2_NUM_RX_DATABOXES_DU16  (1)
#endif
#ifdef USE_CAN_3
#define CAN_3_TX_BUF_LEN_DU8         (can_3_TX_buffer_Size)
#define CAN_3_NUM_RX_DATABOXES_DU16  (1)
#endif
#ifdef USE_CAN_4
#define CAN_4_TX_BUF_LEN_DU8         (can_4_TX_buffer_Size)
#define CAN_4_NUM_RX_DATABOXES_DU16  (1)
#endif


/****************************************************************************************
* Plausibility checks
****************************************************************************************/
#ifdef USE_CAN_1
# ifndef CAN_1_BAUDRATE
# define CAN_1_BAUDRATE            (CAN_DEFAULT_BAUDRATE)
# endif
#endif
#ifdef USE_CAN_2
# ifndef CAN_2_BAUDRATE
# define CAN_2_BAUDRATE            (CAN_DEFAULT_BAUDRATE)
# endif
#endif
#ifdef USE_CAN_3
# ifndef CAN_3_BAUDRATE
# define CAN_3_BAUDRATE            (CAN_DEFAULT_BAUDRATE)
# endif
#endif
#ifdef USE_CAN_4
# ifndef CAN_4_BAUDRATE
# define CAN_4_BAUDRATE            (CAN_DEFAULT_BAUDRATE)
# endif
#endif

/****************************************************************************************
* Settings based on defines in SYS_config.h
****************************************************************************************/
#ifdef USE_LOW_LEVEL_COMMANDS_1
char LowLevelCommands_1 = 1;
#endif
#ifdef USE_LOW_LEVEL_COMMANDS_2
char LowLevelCommands_2 = 1;
#endif
#ifdef USE_LOW_LEVEL_COMMANDS_3
char LowLevelCommands_3 = 1;
#endif
#ifdef USE_LOW_LEVEL_COMMANDS_4
char LowLevelCommands_4 = 1;
#endif
#ifdef USE_XCP_CAN_1
char UseXCPCan_1 = 1;
#endif
#ifdef USE_XCP_CAN_2
char UseXCPCan_2 = 1;
#endif
#ifdef USE_XCP_CAN_3
char UseXCPCan_3 = 1;
#endif
#ifdef USE_XCP_CAN_4
char UseXCPCan_4 = 1;
#endif

/****************************************************************************************
* Local data declaratons
****************************************************************************************/
/* buffers for transmit queue and reception databoxes */
#ifdef USE_CAN_1
static can_Message_ts   can_1_TxBuf_as[CAN_1_TX_BUF_LEN_DU8];
static can_RxDatabox_ts can_1_RxDataboxes_as[CAN_1_NUM_RX_DATABOXES_DU16];
#endif
#ifdef USE_CAN_2
static can_Message_ts   can_2_TxBuf_as[CAN_2_TX_BUF_LEN_DU8];
static can_RxDatabox_ts can_2_RxDataboxes_as[CAN_2_NUM_RX_DATABOXES_DU16];
#endif
#ifdef USE_CAN_3
static can_Message_ts   can_3_TxBuf_as[CAN_3_TX_BUF_LEN_DU8];
static can_RxDatabox_ts can_3_RxDataboxes_as[CAN_3_NUM_RX_DATABOXES_DU16];
#endif
#ifdef USE_CAN_4
static can_Message_ts   can_4_TxBuf_as[CAN_4_TX_BUF_LEN_DU8];
static can_RxDatabox_ts can_4_RxDataboxes_as[CAN_4_NUM_RX_DATABOXES_DU16];
#endif


/****************************************************************************************
** NAME:           CanDrvInit
** PARAMETER:      none
** RETURN VALUE:   none
** DESCRIPTION:    CAN driver initialization.
**
****************************************************************************************/
void CanDrvInit(void)
{
/* Call an init function for each used channel */
#ifdef USE_CAN_1
  doCanDrvInit1(
    can_1_TxBuf_as,
    CAN_1_TX_BUF_LEN_DU8,
    can_1_RxDataboxes_as,
    CAN_1_NUM_RX_DATABOXES_DU16,
    CAN_1_BAUDRATE);
#endif
#ifdef USE_CAN_2
  doCanDrvInit2(
    can_2_TxBuf_as,
    CAN_2_TX_BUF_LEN_DU8,
    can_2_RxDataboxes_as,
    CAN_2_NUM_RX_DATABOXES_DU16,
    CAN_2_BAUDRATE);
#endif
#ifdef USE_CAN_3
  doCanDrvInit3(
    can_3_TxBuf_as,
    CAN_3_TX_BUF_LEN_DU8,
    can_3_RxDataboxes_as,
    CAN_3_NUM_RX_DATABOXES_DU16,
    CAN_3_BAUDRATE);
#endif
#ifdef USE_CAN_4
  doCanDrvInit4(
    can_4_TxBuf_as,
    CAN_4_TX_BUF_LEN_DU8,
    can_4_RxDataboxes_as,
    CAN_4_NUM_RX_DATABOXES_DU16,
    CAN_4_BAUDRATE);
#endif
} /*** end of CanDrvInit ***/


/*********************************** end of candrv.c ***********************************/
