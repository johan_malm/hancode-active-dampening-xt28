/****************************************************************************************
|  Description: Error list header file
|    File Name: errorList.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef ERRORLIST_H
#define ERRORLIST_H


/****************************************************************************************
* Macro definitions
****************************************************************************************/
/* CAN ID is already in use, parameter = CAN channel */
#define	ER_CAN_DOUBLE_ID	             (0xF000)
/* CAN ID is reservered (XCP 0x665 & 0x666), parameter = CAN channel */
#define	ER_CAN_XCP_ID                  (0xF001	)
/* CAN ID is reservered (low-level commands 0x667 & 0x668), parameter = CAN channel */
#define	ER_CAN_LOW_LEVEL_ID	           (0xF001)
/* Error ID is in API range (0 - 0x7FFF) */
#define	ER_ERROR_API_RANGE	           (0xF100)
/* Error ID is in pre-defined toolbox range (0xF000 - 0xFFFF) */
#define	ER_ERROR_TOOL_RANGE	           (0xF101)
/* Reservating new part of memory failed, parameter 0 = on init, 1 = runtime */
#define	ER_MEM_NO_FREE_SPACE           (0xF200)
/* Array ID not found, parameter 0 = read, 1 = write */
#define	ER_MEM_ID_NOT_FOUND            (0xF201)
/* Attempt to read/write outside of array size (includes reading on row 0), parameter 0 = read, 1 = write */
#define	ER_MEM_OUT_OF_RANGE            (0xF202)
/* Attempt to create a zero sized array */
#define ER_MEM_ZERO_ARRAY              (0xF203)
/* XCP debug error */
#define	ER_XCP_EVENT_ERROR             (0xF300)
/* Default error to be set with low-level command */
#define	ER_DEBUG_DEFAULT               (0xF400)
/* Address out of Range. Incorrect Address used with a setMTA Command */
#define	ER_MTA_MEM_OUT_OF_RANGE        (0xF500)
#endif /* ERRORLIST_H */
/*********************************** end of errorList.h ********************************/

    
