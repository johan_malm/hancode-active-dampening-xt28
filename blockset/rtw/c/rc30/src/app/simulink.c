/****************************************************************************************
|  Description: Interface to Simulink generated model source file
|    File Name: simulink.c
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "simulink.h"
#include "xcp_cfg.h"
#include "xcp_par.h"
#include "SYS_config.h"

/****************************************************************************************
* Global variables declaration
****************************************************************************************/
vuint32 XCP_CRO_ID = XCP_CRO_ID_CONFIG;
vuint32 XCP_DTO_ID = XCP_DTO_ID_CONFIG;
vuint32 AppCtrlTaskTiming = APP_CTRL_TASK_TIMING;

/**
Pieces of code from xcp_par.c to be able to precompile the rest of the file
**/
/* Slave device id */
#if defined ( kXcpStationIdLength )
	V_MEMROM0 MEMORY_ROM vuint8 kXcpStationId[kXcpStationIdLength] = kXcpStationIdString;
#endif

#if defined ( XCP_ENABLE_DAQ_EVENT_INFO )
	V_MEMROM0 vuint8 MEMORY_ROM kXcpEventCycle[] =
	{
		(vuint8)APP_CTRL_TASK_TIMING, (vuint8)APP_CTRL_TASK_TIMING, (vuint8)APP_CTRL_TASK_TIMING															
	};
#endif


/****************************************************************************************
* Plausibility checks
****************************************************************************************/
#if ONESTEPFCN==0
#error Separate output and update functions are not supported.
#endif

#if MT==1
#error Only single tasking is supported.
#endif


/****************************************************************************************
* External function prototypes
****************************************************************************************/
#if defined(INCLUDE_FIRST_TIME_ARG) && INCLUDE_FIRST_TIME_ARG==0
extern void MODEL_INITIALIZE();
#else
extern void MODEL_INITIALIZE(boolean_T firstTime);
#endif
extern void MODEL_TERMINATE(void);
#if NUMST == 1
extern void MODEL_STEP(void);                      /* single instance                  */
#else
extern void MODEL_STEP(int_T tid);                 /* multiple instance                */
#endif


/****************************************************************************************
** NAME:           ModelInit
** PARAMETER:      none
** RETURN VALUE:   none
** DESCRIPTION:    Initializes the Simulink model.
**
****************************************************************************************/
void ModelInit(void)
{
  /* initialize the model */
#if defined(INCLUDE_FIRST_TIME_ARG) && INCLUDE_FIRST_TIME_ARG==0
  MODEL_INITIALIZE();
#else
  MODEL_INITIALIZE(1);
#endif
} /*** end of ModelInit ***/


/****************************************************************************************
** NAME:           ModelStep
** PARAMETER:      none
** RETURN VALUE:   none
** DESCRIPTION:    Runs the Simulink model. It is important that this function is called
**                 with the same period time as the fixed steptime configuration in the
**                 Simulink model itself.
**
****************************************************************************************/
void ModelStep(void)
{
#if NUMST > 1
  /* perform a step of the model */
  MODEL_STEP(0);
#else
  /* perform a step of the model */
  MODEL_STEP();
#endif
} /*** end of ModelStep ***/


/*********************************** end of simulink.c *********************************/

