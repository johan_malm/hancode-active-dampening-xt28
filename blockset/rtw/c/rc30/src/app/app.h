/****************************************************************************************
|  Description: Application header file
|    File Name: app.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef APP_H
#define APP_H


/****************************************************************************************
* Include files
****************************************************************************************/
#include <stddef.h>                                    /* for default types            */
#include "SYS_config.h"                                /* for system configuration     */
#include "errorList.h"                                 /* for program error codes      */
#include "api_lib_basic.h"                             /* for Bosch Rexroth C-API      */
#include "api_lib_eepRC.h"                             /* for Bosch Rexroth EEPROM     */


/****************************************************************************************
* Macro definitions
****************************************************************************************/
/* AppCtrlTask priority which has the highest priority in the system */
#define APP_CTRL_TASK_PRIO       (10)                  
/* Offset within task timing */
#define APP_CTRL_TASK_OFFSET     (0)             
#if defined (USE_LOW_LEVEL_COMMANDS_1) || \
    defined (USE_LOW_LEVEL_COMMANDS_2) || \
    defined (USE_LOW_LEVEL_COMMANDS_3) || \
    defined (USE_LOW_LEVEL_COMMANDS_4)
# ifndef USE_RAM_ARRAY
# define USE_RAM_ARRAY
# endif
#endif


/****************************************************************************************
* Function prototypes
****************************************************************************************/
void sys_main(void);
#if defined (USE_LOW_LEVEL_COMMANDS_1) || \
    defined (USE_LOW_LEVEL_COMMANDS_2) || \
    defined (USE_LOW_LEVEL_COMMANDS_3) || \
    defined (USE_LOW_LEVEL_COMMANDS_4)
void sys_command(uint8 *command);
#endif

#endif /* APP_H */
/*********************************** end of app.h **************************************/


