
/****************************************************************************************
|  Description: Interface to Simulink generated model header file
|    File Name: simulink.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef SIMULINK_H
#define SIMULINK_H


/****************************************************************************************
* Include files
****************************************************************************************/
#include "rtwtypes.h"


/****************************************************************************************
* Function prototypes
****************************************************************************************/
void ModelInit(void);
void ModelStep(void);


#endif /* SIMULINK_H */
/*********************************** end of simulink.h *********************************/
