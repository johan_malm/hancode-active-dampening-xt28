/****************************************************************************************
|  Description: Error handling header file
|    File Name: errorHandling.h
|
|----------------------------------------------------------------------------------------
|                          C O P Y R I G H T
|----------------------------------------------------------------------------------------
|   Copyright 2012 (c) by HAN Automotive     http://www.han.nl     All rights reserved
|
|----------------------------------------------------------------------------------------
|                            L I C E N S E
|----------------------------------------------------------------------------------------
| This file is part of the RC30 Target Matlab/Simulink Blockset environment. For the
| licensing terms, please contact HAN Automotive.
|
| This software has been carefully tested, but is not guaranteed for any particular
| purpose. HAN Automotive does not offer any warranties and does not guarantee the 
| accuracy, adequacy, or completeness of the software and is not responsible for any 
| errors or omissions or the results obtained from use of the software.
| 
****************************************************************************************/
#ifndef ERRORHANDLING_H
#define ERRORHANDLING_H


/****************************************************************************************
* Include files
****************************************************************************************/
#include <stddef.h>                                    /* for default types            */
#include "SYS_config.h"                                /* for system configuration     */
#include "api_lib_basic.h"                             /* for Bosch Rexroth C-API      */


/****************************************************************************************
* Function prototypes
****************************************************************************************/
uint8* getErrorListAddress(void);
void   bufferSync(uint8 action);
bool   releaseBuffer(void);

#endif /* ERRORHANDLING_H */
/*********************************** end of errorHandling.h ****************************/

