

#ifndef _SBTARGET_H
#define _SBTARGET_H











#define SBTARGET_NUM_CSENTRIES    7ul


typedef enum
{
    NO_PROZ,
    TC1775,
    METIS,
    LEDA,
    LEDA_LIGHT,
    TC1762,
    TC1792,
    TC1764,
    TC1767,
    TC1797,
	TC1387
} SBTarget_CPUType_t;


typedef enum
{
    NO_APPL_INTF,
    ED_DEVICE,
    SERAM,
    ETK
} SBTarget_ApplType_t;


typedef enum
{
    NO_DEVICE,
    EXT_RAM,
    METIS_PFLASH,
    METIS_DFLASH,
    LEDA_LIGHT_PFLASH,
    LEDA_LIGHT_DFLASH,
    LEDA_PFLASH,
    LEDA_DFLASH,
    STM58BW016,
    AMD29BDD160,
    AMD29BL162X2,
    AMD29BDD320,
    TC_1762_PFLASH,
    TC_1762_DFLASH,
    TC_1792_PFLASH,
    TC_1792_DFLASH,
    TC_1764_PFLASH,
    TC_1764_DFLASH,
    TC_1767_PFLASH,
    TC_1767_DFLASH,
    TC_1797_PFLASH,
    TC_1797_DFLASH,
    STM58BW032,
    TC_1736_PFLASH,
    TC_1736_DFLASH,
    TC_1387_PFLASH,
    TC_1387_DFLASH
} SBTarget_MemoryType_t;

typedef struct
{
    SBTarget_MemoryType_t   xDeviceType_e;
    uint32                  adStartAddress_u32;
    uint32                  adEndAddress_u32;
}SBTarget_MemoryProperties_t;


typedef struct
{
    uint32  fSysFreq_u32;                      
    SBTarget_CPUType_t xMasterCPUTyp_e;        
    uint8  xMasterCPURev_u8;                   
    SBTarget_CPUType_t xSlaveCPUTyp_e;         
    uint8  xSlaveCPURev_u8;                    
    uint32 xSlaveCPUBaseAddr_u32;              
    SBTarget_ApplType_t xApplTyp_e;            
    SBTarget_MemoryProperties_t idxFlash_s[SBTARGET_NUM_CSENTRIES]; 
} SBTarget_Info_t;


typedef struct
{
    uint32 BusAp_u32;                           
    uint32 BusCon_u32;                          
} CsBuffer_t;

#endif
