

#ifndef _COMPILER_H
#define _COMPILER_H





#ifdef TASKING

#define MTCR __mtcr
#define ISYNC __isync
#define Disable_Interrupts __disable
#define Enable_Interrupts __enable
#endif

#ifdef __GNUC__

#include "intrinsics.h"

#include "gnu_pragma_conf.h"


#define MTCR _mtcr
#define ISYNC _isync
#define Disable_Interrupts _disable
#define Enable_Interrupts _enable
#else

#include "gnu_pragma_default_conf.h"
#endif


#define PARAM_UNUSED(param) if ((param) != 0) {}


#define initValueRAM(val, x)      ( *(x) = ((val) ? (val) : *(x)) )



#define __builtin_types_compatible_p_misra(X,Y) (1)

#endif
