

#ifndef _SBRESET_H
#define _SBRESET_H










#define SBRESET_SWRESETHIST_SIZE 0x08


#define SBRESET_LOOPCOND_TRUE    0xDA

#define SBRESET_LOOPCOND_FALSE   0x00

#define SBRESET_USERID_CALWUP   0xECCECEECul

#define SBRESET_USERID_CANWAKEUP   0xCA11CA11ul

#define SBRESET_USERID_STCWAKEUP   0x270B270Bul

#define SBRESET_PROTRAM_CLEAR   0xA1B2

#define SBRESET_VERSION         0x0006

#define SBRESET_WDT_SHUTOFF     0xED78




typedef void (*SBReset_CallbackFctPtr_t)(void);



typedef struct
{
    uint64 tiStampCurr_u64;                                 
    uint64 tiStampLast_u64;                                    
    uint32 dUserDefined_u32;                                                
    uint32 regLoc_u32;                                  
    uint16 dResetType_u16;                                             
    uint16 dResetTypeCompl_u16;            
    uint8  dSysState_u8[3];                                               
    uint8  dResetGrp_u8;                                           
    uint8  ctReset_u8;                                                   
    uint8  dLoopCond_u8;                                                
    uint16 dComplConfigPtr_u16;           
}SBReset_EnvSWReset_t;



typedef struct
{
    uint16 dBuffer_u16[SBRESET_SWRESETHIST_SIZE];           
    uint8  ctIndex_u8;                                                 
}SBReset_SWResetHist_t;



typedef struct
{
    uint32 tiMinCyc_u32;                                            
    uint8  ctMax_u8;              
}SBReset_LoopCondSWReset_t;


#if ( (MACHINE_TYPE == TC_1796) || (MACHINE_TYPE == TC_1766) || (defined(TC_1762) && (MACHINE_TYPE == TC_1762)) )
typedef struct
{
    const SBReset_CallbackFctPtr_t *dCallbackFctPtrTable_pcpfn; 
    const uint16 *dConfMaskTable_pcu16;            
                                                   
    const SBReset_LoopCondSWReset_t *dLoopCondTable_pcs;    
}SBReset_ConfSWReset_t;
#else
typedef struct
{
    const SBReset_CallbackFctPtr_t *dCallbackFctPtrTable_pcpfn; 
    const uint32 *dConfTable_pcu32;   
    const SBReset_LoopCondSWReset_t *dLoopCondTable_pcs;     
}SBReset_ConfSWReset_t;
#endif


typedef struct
{
    uint16 dVersion_u16;
    uint16 dProtRAMClear_u16;
    uint16 xWDTShutOff_u16;
    uint16 xWDTShutOffCmpl_u16;
}SBReset_AddInfo_t;


#endif
