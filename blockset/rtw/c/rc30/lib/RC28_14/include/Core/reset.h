


#ifndef _RESET_H
#define _RESET_H



#include "sb.h"
#include "memlay.h"



#if ( (MACHINE_TYPE == TC_1796) || (MACHINE_TYPE == TC_1766) )
#include "reset_pub.h"
#include "reset_auto_conf.h"
#else
#include "reset_auto_conf.h"
#include "reset_pub.h"
#endif



#endif
