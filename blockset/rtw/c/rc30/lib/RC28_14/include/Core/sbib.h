

#ifndef _SBIB_H
#define _SBIB_H








#define IB_VERSION_SUBID    0x00              


 

#define IB_FLAG_NONE        0x0000                 
#define IB_FLAG_OTP        (0x8000 << 8)           



#define IB_SB_SUBID         0x10              
#define IB_TP_SUBID         0x20              
#define IB_CB_SUBID         0x30              
#define IB_ASW0_SUBID       0x40              
#define IB_ASW1_SUBID       0x50              
#define IB_DS0_SUBID        0x60              
#define IB_DS1_SUBID        0x70              
#define IB_VDS_SUBID        0x80              
#define IB_CTP_SUBID        0x90              
#define IB_ASW2_SUBID       0xA0              
#define IB_ASW3_SUBID       0xB0              
#define IB_ABS0_SUBID       0xC0              
#define IB_EEC_SUBID        0xD0              
#define IB_CUST_SUBID       0xE0              
#define IB_RL_SUBID         0xF0              
#define IB_AA_SUBID         0xF1              




#define IB_CS0_SUBID        0x00              
#define IB_CS1_SUBID        0x01              
#define IB_CS2_SUBID        0x02              
#define IB_CS3_SUBID        0x03              
#define IB_CS4_SUBID        0x04              
#define IB_CS5_SUBID        0x05              
#define IB_CS6_SUBID        0x06              
#define IB_CS7_SUBID        0x07              
#define IB_CS8_SUBID        0x08              
#define IB_CS9_SUBID        0x09              



#define IB_SB_ID   ((IB_VERSION_SUBID<<24)+IB_SB_SUBID)     
#define IB_TP_ID   ((IB_VERSION_SUBID<<24)+IB_TP_SUBID)     
#define IB_CB_ID   ((IB_VERSION_SUBID<<24)+IB_CB_SUBID)     
#define IB_ASW0_ID ((IB_VERSION_SUBID<<24)+IB_ASW0_SUBID)   
#define IB_ASW1_ID ((IB_VERSION_SUBID<<24)+IB_ASW1_SUBID)   
#define IB_ASW2_ID ((IB_VERSION_SUBID<<24)+IB_ASW2_SUBID)   
#define IB_ASW3_ID ((IB_VERSION_SUBID<<24)+IB_ASW3_SUBID)   
#define IB_ABS0_ID ((IB_VERSION_SUBID<<24)+IB_ABS0_SUBID)   
#define IB_EEC_ID  ((IB_VERSION_SUBID<<24)+IB_EEC_SUBID)    
#define IB_DS0_ID  ((IB_VERSION_SUBID<<24)+IB_DS0_SUBID)    
#define IB_DS1_ID  ((IB_VERSION_SUBID<<24)+IB_DS1_SUBID)    
#define IB_VDS_ID  ((IB_VERSION_SUBID<<24)+IB_VDS_SUBID)    
#define IB_CTP_ID  ((IB_VERSION_SUBID<<24)+IB_CTP_SUBID)    
#define IB_CUST_ID ((IB_VERSION_SUBID<<24)+IB_CUST_SUBID)   
#define IB_RL_ID   ((IB_VERSION_SUBID<<24)+IB_RL_SUBID)     
#define IB_AA_ID   ((IB_VERSION_SUBID<<24)+IB_AA_SUBID)     


#define IB_SB_CS0_ID   (IB_CS0_SUBID | IB_SB_SUBID)  
#define IB_SB_CS1_ID   (IB_CS1_SUBID | IB_SB_SUBID)  
#define IB_SB_CS2_ID   (IB_CS2_SUBID | IB_SB_SUBID)  
#define IB_SB_CS3_ID   (IB_CS3_SUBID | IB_SB_SUBID)  
#define IB_SB_CS4_ID   (IB_CS4_SUBID | IB_SB_SUBID)  
#define IB_SB_CS5_ID   (IB_CS5_SUBID | IB_SB_SUBID)  
#define IB_SB_CS6_ID   (IB_CS6_SUBID | IB_SB_SUBID)  
#define IB_SB_CS7_ID   (IB_CS7_SUBID | IB_SB_SUBID)  
#define IB_SB_CS8_ID   (IB_CS8_SUBID | IB_SB_SUBID)  
#define IB_SB_CS9_ID   (IB_CS9_SUBID | IB_SB_SUBID)  

#define IB_TP_CS0_ID   (IB_CS0_SUBID | IB_TP_SUBID)  
#define IB_TP_CS1_ID   (IB_CS1_SUBID | IB_TP_SUBID)  
#define IB_TP_CS2_ID   (IB_CS2_SUBID | IB_TP_SUBID)  
#define IB_TP_CS3_ID   (IB_CS3_SUBID | IB_TP_SUBID)  
#define IB_TP_CS4_ID   (IB_CS4_SUBID | IB_TP_SUBID)  
#define IB_TP_CS5_ID   (IB_CS5_SUBID | IB_TP_SUBID)  
#define IB_TP_CS6_ID   (IB_CS6_SUBID | IB_TP_SUBID)  
#define IB_TP_CS7_ID   (IB_CS7_SUBID | IB_TP_SUBID)  
#define IB_TP_CS8_ID   (IB_CS8_SUBID | IB_TP_SUBID)  
#define IB_TP_CS9_ID   (IB_CS9_SUBID | IB_TP_SUBID)  

#define IB_CB_CS0_ID   (IB_CS0_SUBID | IB_CB_SUBID)  
#define IB_CB_CS1_ID   (IB_CS1_SUBID | IB_CB_SUBID)  
#define IB_CB_CS2_ID   (IB_CS2_SUBID | IB_CB_SUBID)  
#define IB_CB_CS3_ID   (IB_CS3_SUBID | IB_CB_SUBID)  
#define IB_CB_CS4_ID   (IB_CS4_SUBID | IB_CB_SUBID)  
#define IB_CB_CS5_ID   (IB_CS5_SUBID | IB_CB_SUBID)  
#define IB_CB_CS6_ID   (IB_CS6_SUBID | IB_CB_SUBID)  
#define IB_CB_CS7_ID   (IB_CS7_SUBID | IB_CB_SUBID)  
#define IB_CB_CS8_ID   (IB_CS8_SUBID | IB_CB_SUBID)  
#define IB_CB_CS9_ID   (IB_CS9_SUBID | IB_CB_SUBID)  

#define IB_ASW0_CS0_ID   (IB_CS0_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS1_ID   (IB_CS1_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS2_ID   (IB_CS2_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS3_ID   (IB_CS3_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS4_ID   (IB_CS4_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS5_ID   (IB_CS5_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS6_ID   (IB_CS6_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS7_ID   (IB_CS7_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS8_ID   (IB_CS8_SUBID | IB_ASW0_SUBID)  
#define IB_ASW0_CS9_ID   (IB_CS9_SUBID | IB_ASW0_SUBID)  

#define IB_ASW1_CS0_ID   (IB_CS0_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS1_ID   (IB_CS1_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS2_ID   (IB_CS2_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS3_ID   (IB_CS3_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS4_ID   (IB_CS4_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS5_ID   (IB_CS5_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS6_ID   (IB_CS6_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS7_ID   (IB_CS7_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS8_ID   (IB_CS8_SUBID | IB_ASW1_SUBID)  
#define IB_ASW1_CS9_ID   (IB_CS9_SUBID | IB_ASW1_SUBID)  

#define IB_ASW2_CS0_ID   (IB_CS0_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS1_ID   (IB_CS1_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS2_ID   (IB_CS2_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS3_ID   (IB_CS3_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS4_ID   (IB_CS4_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS5_ID   (IB_CS5_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS6_ID   (IB_CS6_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS7_ID   (IB_CS7_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS8_ID   (IB_CS8_SUBID | IB_ASW2_SUBID)  
#define IB_ASW2_CS9_ID   (IB_CS9_SUBID | IB_ASW2_SUBID)  

#define IB_ASW3_CS0_ID   (IB_CS0_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS1_ID   (IB_CS1_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS2_ID   (IB_CS2_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS3_ID   (IB_CS3_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS4_ID   (IB_CS4_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS5_ID   (IB_CS5_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS6_ID   (IB_CS6_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS7_ID   (IB_CS7_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS8_ID   (IB_CS8_SUBID | IB_ASW3_SUBID)  
#define IB_ASW3_CS9_ID   (IB_CS9_SUBID | IB_ASW3_SUBID)  

#define IB_ABS0_CS0_ID   (IB_CS0_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS1_ID   (IB_CS1_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS2_ID   (IB_CS2_SUBID | IB_ABs0_SUBID)  
#define IB_ABS0_CS3_ID   (IB_CS3_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS4_ID   (IB_CS4_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS5_ID   (IB_CS5_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS6_ID   (IB_CS6_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS7_ID   (IB_CS7_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS8_ID   (IB_CS8_SUBID | IB_ABS0_SUBID)  
#define IB_ABS0_CS9_ID   (IB_CS9_SUBID | IB_ABS0_SUBID)  

#define IB_EEC_CS0_ID    (IB_CS0_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS1_ID    (IB_CS1_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS2_ID    (IB_CS2_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS3_ID    (IB_CS3_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS4_ID    (IB_CS4_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS5_ID    (IB_CS5_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS6_ID    (IB_CS6_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS7_ID    (IB_CS7_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS8_ID    (IB_CS8_SUBID | IB_EEC_SUBID)   
#define IB_EEC_CS9_ID    (IB_CS9_SUBID | IB_EEC_SUBID)   

#define IB_DS0_CS0_ID   (IB_CS0_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS1_ID   (IB_CS1_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS2_ID   (IB_CS2_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS3_ID   (IB_CS3_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS4_ID   (IB_CS4_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS5_ID   (IB_CS5_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS6_ID   (IB_CS6_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS7_ID   (IB_CS7_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS8_ID   (IB_CS8_SUBID | IB_DS0_SUBID)  
#define IB_DS0_CS9_ID   (IB_CS9_SUBID | IB_DS0_SUBID)  

#define IB_DS1_CS0_ID   (IB_CS0_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS1_ID   (IB_CS1_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS2_ID   (IB_CS2_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS3_ID   (IB_CS3_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS4_ID   (IB_CS4_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS5_ID   (IB_CS5_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS6_ID   (IB_CS6_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS7_ID   (IB_CS7_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS8_ID   (IB_CS8_SUBID | IB_DS1_SUBID)  
#define IB_DS1_CS9_ID   (IB_CS9_SUBID | IB_DS1_SUBID)  

#define IB_VDS_CS0_ID   (IB_CS0_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS1_ID   (IB_CS1_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS2_ID   (IB_CS2_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS3_ID   (IB_CS3_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS4_ID   (IB_CS4_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS5_ID   (IB_CS5_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS6_ID   (IB_CS6_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS7_ID   (IB_CS7_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS8_ID   (IB_CS8_SUBID | IB_VDS_SUBID)  
#define IB_VDS_CS9_ID   (IB_CS9_SUBID | IB_VDS_SUBID)  

#define IB_CTP_CS0_ID   (IB_CS0_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS1_ID   (IB_CS1_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS2_ID   (IB_CS2_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS3_ID   (IB_CS3_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS4_ID   (IB_CS4_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS5_ID   (IB_CS5_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS6_ID   (IB_CS6_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS7_ID   (IB_CS7_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS8_ID   (IB_CS8_SUBID | IB_CTP_SUBID)  
#define IB_CTP_CS9_ID   (IB_CS9_SUBID | IB_CTP_SUBID)  

#define IB_CUST_CS0_ID  (IB_CS0_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS1_ID  (IB_CS1_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS2_ID  (IB_CS2_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS3_ID  (IB_CS3_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS4_ID  (IB_CS4_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS5_ID  (IB_CS5_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS6_ID  (IB_CS6_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS7_ID  (IB_CS7_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS8_ID  (IB_CS8_SUBID | IB_CUST_SUBID) 
#define IB_CUST_CS9_ID  (IB_CS9_SUBID | IB_CUST_SUBID) 




#define IB_NO_FLAG_SET           0x00         
#define IB_AREA_EMPTY_ALLOWED    0x01         
#define IB_CHECK_AT_POWERON      0x02         
#define IB_CHECK_AT_RUNTIME      0x04         
#define IB_CONT_ON_ERROR         0x08         
#define IB_CHECK_IN_SB           0x10         



#define IB_CKSUM_STARTVAL          0xFADECAFEUL


#define IB_CKSUM_EXPECTEDVAL       0xCAFEAFFEUL


#define IB_ENDID_VAL          0xDEADBEEFUL


#define IB_ENDID_SIZE          0x04


#define IB_VALID_PATTERN           0xAFAFAFAFUL


#define  IB_VALID8       0xAF


#define  IB_DUMMY_TTNR  IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8
#define  IB_DUMMY_FSWID IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8


#define  IB_DUMMY_SIGNATURE IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, \
                            IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8, IB_VALID8




typedef struct
{
    uint32 dCompRef;                     
    uint32 dCompId;                      
    uint8 dSignatur[128];                
    uint32 dCSAdjust;                    
    uint32 dEndPattern;                  
}IB_Epilog_t;


struct IB_InfoTabId_def
{
    uint8  index;                        
    uint8  id;                           
} __attribute__((packed));
typedef struct IB_InfoTabId_def IB_InfoTabId_t;


struct IB_CSBlkTab_def {
    uint8           dCSBlkID;            
    uint8           numBlkIDLen;         
    bit16           dFlags;              
                                         
                                         
                                         
                                         
    uint32          adCSStart;           
    uint32          adCSEnd;             
    uint32          dCSStartVal;         
    uint32          dCSExpectedVal;      
    uint32          adBlkIDRef;          
    uint32          adBlkID;             
    IB_InfoTabId_t  dCSAlgo;             
    IB_InfoTabId_t  dErrorExitID;        
} __attribute__((packed));
typedef struct IB_CSBlkTab_def IB_CSBlkTab_t, *IB_PCSBlkTab_t;



struct IB_InfoBlk_def
{
    uint32          dBlkID;             
    uint32          dBlkLen;            
    uint32          adNextInfoBlk;      
    uint32          adPattern;          
    uint32          adIntInfoTab;       
    uint32          adExtInfoTab;       
    uint8           numIntInfoTab;      
    uint8           numExtInfoTab;      
    uint8           dTTNr_u8[10];       
    uint8           dFSWId_u8[8];       
    uint16          numCSEntries;       
    IB_InfoTabId_t  dErrorExitID;       
                                        
    uint32          dCSAdjust;          
} __attribute__((packed));
typedef struct IB_InfoBlk_def IB_InfoBlk_t, *IB_PInfoBlk_t;







#endif

