

#ifndef _CPS_1797_H
#define _CPS_1797_H


#ifdef REGDEF_FOR_PCP
    #define _CPS_UL(x) x
#else
    #define _CPS_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[44];       
    volatile uint32 CPU_SBSRC;           
    volatile uint32 RESERVED1[12];       
    volatile uint32 CPU_SRC3;            
    volatile uint32 CPU_SRC2;            
    volatile uint32 CPU_SRC1;            
    volatile uint32 CPU_SRC0;            
} CPS_RegMap_t;




extern CPS_RegMap_t CPS __attribute__ ((asection (".bss.label_only")));





#define CPS_ID_MOD_NUMBER_POS                _CPS_UL(16)
#define CPS_ID_MOD_NUMBER_LEN                _CPS_UL(16)


#define CPS_ID_MOD_REV_POS                   _CPS_UL(0)
#define CPS_ID_MOD_REV_LEN                   _CPS_UL(8)


#define CPS_ID_MOD_TYPE_POS                  _CPS_UL(8)
#define CPS_ID_MOD_TYPE_LEN                  _CPS_UL(8)


#define CPS_ID_MOD_POS                       _CPS_UL(16)
#define CPS_ID_MOD_LEN                       _CPS_UL(16)


#define CPS_ID_MOD_32B_POS                   _CPS_UL(8)
#define CPS_ID_MOD_32B_LEN                   _CPS_UL(8)


#define CPS_ID_MOD_REV_POS                   _CPS_UL(0)
#define CPS_ID_MOD_REV_LEN                   _CPS_UL(8)


#define CPS_CPU_SBSRC_SRPN_POS               _CPS_UL(0)
#define CPS_CPU_SBSRC_SRPN_LEN               _CPS_UL(8)


#define CPS_CPU_SBSRC_TOS_POS                _CPS_UL(10)
#define CPS_CPU_SBSRC_TOS_LEN                _CPS_UL(1)


#define CPS_CPU_SBSRC_SRE_POS                _CPS_UL(12)
#define CPS_CPU_SBSRC_SRE_LEN                _CPS_UL(1)


#define CPS_CPU_SBSRC_SRR_POS                _CPS_UL(13)
#define CPS_CPU_SBSRC_SRR_LEN                _CPS_UL(1)


#define CPS_CPU_SBSRC_CLRR_POS               _CPS_UL(14)
#define CPS_CPU_SBSRC_CLRR_LEN               _CPS_UL(1)


#define CPS_CPU_SBSRC_SETR_POS               _CPS_UL(15)
#define CPS_CPU_SBSRC_SETR_LEN               _CPS_UL(1)


#define CPS_CPU_SRC3_SRPN_POS                _CPS_UL(0)
#define CPS_CPU_SRC3_SRPN_LEN                _CPS_UL(8)


#define CPS_CPU_SRC3_TOS_POS                 _CPS_UL(10)
#define CPS_CPU_SRC3_TOS_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC3_SRE_POS                 _CPS_UL(12)
#define CPS_CPU_SRC3_SRE_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC3_SRR_POS                 _CPS_UL(13)
#define CPS_CPU_SRC3_SRR_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC3_CLRR_POS                _CPS_UL(14)
#define CPS_CPU_SRC3_CLRR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC3_SETR_POS                _CPS_UL(15)
#define CPS_CPU_SRC3_SETR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC2_SRPN_POS                _CPS_UL(0)
#define CPS_CPU_SRC2_SRPN_LEN                _CPS_UL(8)


#define CPS_CPU_SRC2_TOS_POS                 _CPS_UL(10)
#define CPS_CPU_SRC2_TOS_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC2_SRE_POS                 _CPS_UL(12)
#define CPS_CPU_SRC2_SRE_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC2_SRR_POS                 _CPS_UL(13)
#define CPS_CPU_SRC2_SRR_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC2_CLRR_POS                _CPS_UL(14)
#define CPS_CPU_SRC2_CLRR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC2_SETR_POS                _CPS_UL(15)
#define CPS_CPU_SRC2_SETR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC1_SRPN_POS                _CPS_UL(0)
#define CPS_CPU_SRC1_SRPN_LEN                _CPS_UL(8)


#define CPS_CPU_SRC1_TOS_POS                 _CPS_UL(10)
#define CPS_CPU_SRC1_TOS_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC1_SRE_POS                 _CPS_UL(12)
#define CPS_CPU_SRC1_SRE_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC1_SRR_POS                 _CPS_UL(13)
#define CPS_CPU_SRC1_SRR_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC1_CLRR_POS                _CPS_UL(14)
#define CPS_CPU_SRC1_CLRR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC1_SETR_POS                _CPS_UL(15)
#define CPS_CPU_SRC1_SETR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC0_SRPN_POS                _CPS_UL(0)
#define CPS_CPU_SRC0_SRPN_LEN                _CPS_UL(8)


#define CPS_CPU_SRC0_TOS_POS                 _CPS_UL(10)
#define CPS_CPU_SRC0_TOS_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC0_SRE_POS                 _CPS_UL(12)
#define CPS_CPU_SRC0_SRE_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC0_SRR_POS                 _CPS_UL(13)
#define CPS_CPU_SRC0_SRR_LEN                 _CPS_UL(1)


#define CPS_CPU_SRC0_CLRR_POS                _CPS_UL(14)
#define CPS_CPU_SRC0_CLRR_LEN                _CPS_UL(1)


#define CPS_CPU_SRC0_SETR_POS                _CPS_UL(15)
#define CPS_CPU_SRC0_SETR_LEN                _CPS_UL(1)

#endif
