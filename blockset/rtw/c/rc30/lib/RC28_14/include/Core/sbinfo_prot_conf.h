

#ifndef _SBINFO_PROT_CONF_H
#define _SBINFO_PROT_CONF_H







typedef enum
{
    SB_CRC32_ALGO_E,               
    SB_ADD32_ALGO_E,               
    SB_CRC32IFX_ALGO_E,            
    SB_VESION_INFO_E,              
    SB_TARGET_INFO_E,              
    SBRESET_ENV_E,                 
    SBRESET_CONF_E,                
    SBFLASHDO_START_E,             
    SBFLASHDO_SIZE_E,              
    SBTARGET_CLRWPR_START_E,       
    SBTARGET_CLRWPR_SIZE_E,        
    SBTARGET_SETWPR_START_E,       
    SBTARGET_SETWPR_SIZE_E,        
    SBPOTEST_RESULT_E,             
    SBFLASHDO_START_RAM_E,         
    SBRESET_SR_E,                  
    SB_ADD16_ALGO_E,               
    SBRESET_ADDINFO_E,             
    SBRESET_EXTHISTBUF_E,          
    SB_NUM_INT_INFO_E              
}SB_IntInfo_t;


typedef enum
{
    SB_NUM_EXT_INFO_E                   
}SB_ExtInfo_t;


typedef enum
{
    CB_MAIN_E,
    CB_VERSION_INFO_E,
    CB_OFFSET_CB2_E,
    CB_NUMSB_INT_INFO_E
}CB_IntInfo_t;


typedef enum
{
    ASW0_MAIN_E,
    ASW0_VESION_INFO_E,
    ASW0_DUMMY_E,                        
    ASW0_NUMSB_INT_INFO_E
}ASW0_IntInfo_t;

typedef uint32 (SBIB_CsFunc_t) (uint32*, uint32*, uint32);

#endif
