

#ifndef _FADC_1797_H
#define _FADC_1797_H


#ifdef REGDEF_FOR_PCP
    #define _FADC_UL(x) x
#else
    #define _FADC_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 RESERVED0[2];        
    volatile uint32 FDR;                 
    volatile uint32 CRSR;                
    volatile uint32 FMR;                 
    volatile uint32 NCTR;                
    volatile uint32 GCR;                 
    volatile uint32 CFGR[4];             
    volatile uint32 ACR[4];              
    volatile uint32 RCH[4];              
    volatile uint32 RESERVED1[1];        
    volatile uint32 ALR;                 
    volatile uint32 RESERVED2[2];        
    volatile uint32 FCR0;                
    volatile uint32 CRR0;                
    volatile uint32 IRR10;               
    volatile uint32 IRR20;               
    volatile uint32 IRR30;               
    volatile uint32 FRR0;                
    volatile uint32 RESERVED3[2];        
    volatile uint32 FCR1;                
    volatile uint32 CRR1;                
    volatile uint32 IRR11;               
    volatile uint32 RESERVED4[2];        
    volatile uint32 FRR1;                
    volatile uint32 SFRR1;               
    volatile uint32 RESERVED5[1];        
    volatile uint32 FCR2;                
    volatile uint32 CRR2;                
    volatile uint32 IRR12;               
    volatile uint32 IRR22;               
    volatile uint32 IRR32;               
    volatile uint32 FRR2;                
    volatile uint32 RESERVED6[2];        
    volatile uint32 FCR3;                
    volatile uint32 CRR3;                
    volatile uint32 IRR13;               
    volatile uint32 RESERVED7[2];        
    volatile uint32 FRR3;                
    volatile uint32 SFRR3;               
    volatile uint32 RESERVED8[5];        
    volatile uint32 SRC3;                
    volatile uint32 SRC2;                
    volatile uint32 SRC1;                
    volatile uint32 SRC0;                
} FADC_RegMap_t;




extern FADC_RegMap_t FADC __attribute__ ((asection (".bss.label_only")));





#define FADC_ID_MOD_NUMBER_POS               _FADC_UL(16)
#define FADC_ID_MOD_NUMBER_LEN               _FADC_UL(16)


#define FADC_ID_MOD_REV_POS                  _FADC_UL(0)
#define FADC_ID_MOD_REV_LEN                  _FADC_UL(8)


#define FADC_ID_MOD_TYPE_POS                 _FADC_UL(8)
#define FADC_ID_MOD_TYPE_LEN                 _FADC_UL(8)


#define FADC_CLC_DISR_POS                    _FADC_UL(0)
#define FADC_CLC_DISR_LEN                    _FADC_UL(1)


#define FADC_CLC_DISS_POS                    _FADC_UL(1)
#define FADC_CLC_DISS_LEN                    _FADC_UL(1)


#define FADC_CLC_SPEN_POS                    _FADC_UL(2)
#define FADC_CLC_SPEN_LEN                    _FADC_UL(1)


#define FADC_CLC_EDIS_POS                    _FADC_UL(3)
#define FADC_CLC_EDIS_LEN                    _FADC_UL(1)


#define FADC_CLC_SBWE_POS                    _FADC_UL(4)
#define FADC_CLC_SBWE_LEN                    _FADC_UL(1)


#define FADC_CLC_FSOE_POS                    _FADC_UL(5)
#define FADC_CLC_FSOE_LEN                    _FADC_UL(1)


#define FADC_FDR_STEP_POS                    _FADC_UL(0)
#define FADC_FDR_STEP_LEN                    _FADC_UL(10)


#define FADC_FDR_SM_POS                      _FADC_UL(11)
#define FADC_FDR_SM_LEN                      _FADC_UL(1)


#define FADC_FDR_SC_POS                      _FADC_UL(12)
#define FADC_FDR_SC_LEN                      _FADC_UL(2)


#define FADC_FDR_DM_POS                      _FADC_UL(14)
#define FADC_FDR_DM_LEN                      _FADC_UL(2)


#define FADC_FDR_RESULT_POS                  _FADC_UL(16)
#define FADC_FDR_RESULT_LEN                  _FADC_UL(10)


#define FADC_FDR_SUSACK_POS                  _FADC_UL(28)
#define FADC_FDR_SUSACK_LEN                  _FADC_UL(1)


#define FADC_FDR_SUSREQ_POS                  _FADC_UL(29)
#define FADC_FDR_SUSREQ_LEN                  _FADC_UL(1)


#define FADC_FDR_ENHW_POS                    _FADC_UL(30)
#define FADC_FDR_ENHW_LEN                    _FADC_UL(1)


#define FADC_FDR_DISCLK_POS                  _FADC_UL(31)
#define FADC_FDR_DISCLK_LEN                  _FADC_UL(1)


#define FADC_CRSR_CRF0_POS                   _FADC_UL(0)
#define FADC_CRSR_CRF0_LEN                   _FADC_UL(1)


#define FADC_CRSR_CRF1_POS                   _FADC_UL(1)
#define FADC_CRSR_CRF1_LEN                   _FADC_UL(1)


#define FADC_CRSR_CRF2_POS                   _FADC_UL(2)
#define FADC_CRSR_CRF2_LEN                   _FADC_UL(1)


#define FADC_CRSR_CRF3_POS                   _FADC_UL(3)
#define FADC_CRSR_CRF3_LEN                   _FADC_UL(1)


#define FADC_CRSR_BSY0_POS                   _FADC_UL(8)
#define FADC_CRSR_BSY0_LEN                   _FADC_UL(1)


#define FADC_CRSR_BSY1_POS                   _FADC_UL(9)
#define FADC_CRSR_BSY1_LEN                   _FADC_UL(1)


#define FADC_CRSR_BSY2_POS                   _FADC_UL(10)
#define FADC_CRSR_BSY2_LEN                   _FADC_UL(1)


#define FADC_CRSR_BSY3_POS                   _FADC_UL(11)
#define FADC_CRSR_BSY3_LEN                   _FADC_UL(1)


#define FADC_CRSR_IRQ0_POS                   _FADC_UL(16)
#define FADC_CRSR_IRQ0_LEN                   _FADC_UL(1)


#define FADC_CRSR_IRQ1_POS                   _FADC_UL(17)
#define FADC_CRSR_IRQ1_LEN                   _FADC_UL(1)


#define FADC_CRSR_IRQ2_POS                   _FADC_UL(18)
#define FADC_CRSR_IRQ2_LEN                   _FADC_UL(1)


#define FADC_CRSR_IRQ3_POS                   _FADC_UL(19)
#define FADC_CRSR_IRQ3_LEN                   _FADC_UL(1)


#define FADC_CRSR_IRQF0_POS                  _FADC_UL(20)
#define FADC_CRSR_IRQF0_LEN                  _FADC_UL(1)


#define FADC_CRSR_IRQF1_POS                  _FADC_UL(21)
#define FADC_CRSR_IRQF1_LEN                  _FADC_UL(1)


#define FADC_CRSR_IRQF2_POS                  _FADC_UL(22)
#define FADC_CRSR_IRQF2_LEN                  _FADC_UL(1)


#define FADC_CRSR_IRQF3_POS                  _FADC_UL(23)
#define FADC_CRSR_IRQF3_LEN                  _FADC_UL(1)


#define FADC_FMR_RCRF0_POS                   _FADC_UL(0)
#define FADC_FMR_RCRF0_LEN                   _FADC_UL(1)


#define FADC_FMR_RCRF1_POS                   _FADC_UL(1)
#define FADC_FMR_RCRF1_LEN                   _FADC_UL(1)


#define FADC_FMR_RCRF2_POS                   _FADC_UL(2)
#define FADC_FMR_RCRF2_LEN                   _FADC_UL(1)


#define FADC_FMR_RCRF3_POS                   _FADC_UL(3)
#define FADC_FMR_RCRF3_LEN                   _FADC_UL(1)


#define FADC_FMR_SCRF0_POS                   _FADC_UL(8)
#define FADC_FMR_SCRF0_LEN                   _FADC_UL(1)


#define FADC_FMR_SCRF1_POS                   _FADC_UL(9)
#define FADC_FMR_SCRF1_LEN                   _FADC_UL(1)


#define FADC_FMR_SCRF2_POS                   _FADC_UL(10)
#define FADC_FMR_SCRF2_LEN                   _FADC_UL(1)


#define FADC_FMR_SCRF3_POS                   _FADC_UL(11)
#define FADC_FMR_SCRF3_LEN                   _FADC_UL(1)


#define FADC_FMR_RIRQ0_POS                   _FADC_UL(16)
#define FADC_FMR_RIRQ0_LEN                   _FADC_UL(1)


#define FADC_FMR_RIRQ1_POS                   _FADC_UL(17)
#define FADC_FMR_RIRQ1_LEN                   _FADC_UL(1)


#define FADC_FMR_RIRQ2_POS                   _FADC_UL(18)
#define FADC_FMR_RIRQ2_LEN                   _FADC_UL(1)


#define FADC_FMR_RIRQ3_POS                   _FADC_UL(19)
#define FADC_FMR_RIRQ3_LEN                   _FADC_UL(1)


#define FADC_FMR_RIRQF0_POS                  _FADC_UL(20)
#define FADC_FMR_RIRQF0_LEN                  _FADC_UL(1)


#define FADC_FMR_RIRQF1_POS                  _FADC_UL(21)
#define FADC_FMR_RIRQF1_LEN                  _FADC_UL(1)


#define FADC_FMR_RIRQF2_POS                  _FADC_UL(22)
#define FADC_FMR_RIRQF2_LEN                  _FADC_UL(1)


#define FADC_FMR_RIRQF3_POS                  _FADC_UL(23)
#define FADC_FMR_RIRQF3_LEN                  _FADC_UL(1)


#define FADC_FMR_SIRQ0_POS                   _FADC_UL(24)
#define FADC_FMR_SIRQ0_LEN                   _FADC_UL(1)


#define FADC_FMR_SIRQ1_POS                   _FADC_UL(25)
#define FADC_FMR_SIRQ1_LEN                   _FADC_UL(1)


#define FADC_FMR_SIRQ2_POS                   _FADC_UL(26)
#define FADC_FMR_SIRQ2_LEN                   _FADC_UL(1)


#define FADC_FMR_SIRQ3_POS                   _FADC_UL(27)
#define FADC_FMR_SIRQ3_LEN                   _FADC_UL(1)


#define FADC_FMR_SIRQF0_POS                  _FADC_UL(28)
#define FADC_FMR_SIRQF0_LEN                  _FADC_UL(1)


#define FADC_FMR_SIRQF1_POS                  _FADC_UL(29)
#define FADC_FMR_SIRQF1_LEN                  _FADC_UL(1)


#define FADC_FMR_SIRQF2_POS                  _FADC_UL(30)
#define FADC_FMR_SIRQF2_LEN                  _FADC_UL(1)


#define FADC_FMR_SIRQF3_POS                  _FADC_UL(31)
#define FADC_FMR_SIRQF3_LEN                  _FADC_UL(1)


#define FADC_NCTR_EN01_POS                   _FADC_UL(1)
#define FADC_NCTR_EN01_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN02_POS                   _FADC_UL(2)
#define FADC_NCTR_EN02_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN03_POS                   _FADC_UL(3)
#define FADC_NCTR_EN03_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN10_POS                   _FADC_UL(8)
#define FADC_NCTR_EN10_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN12_POS                   _FADC_UL(10)
#define FADC_NCTR_EN12_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN13_POS                   _FADC_UL(11)
#define FADC_NCTR_EN13_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN20_POS                   _FADC_UL(16)
#define FADC_NCTR_EN20_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN21_POS                   _FADC_UL(17)
#define FADC_NCTR_EN21_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN23_POS                   _FADC_UL(19)
#define FADC_NCTR_EN23_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN30_POS                   _FADC_UL(24)
#define FADC_NCTR_EN30_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN31_POS                   _FADC_UL(25)
#define FADC_NCTR_EN31_LEN                   _FADC_UL(1)


#define FADC_NCTR_EN32_POS                   _FADC_UL(26)
#define FADC_NCTR_EN32_LEN                   _FADC_UL(1)


#define FADC_GCR_RCT0_POS                    _FADC_UL(0)
#define FADC_GCR_RCT0_LEN                    _FADC_UL(1)


#define FADC_GCR_RCT1_POS                    _FADC_UL(1)
#define FADC_GCR_RCT1_LEN                    _FADC_UL(1)


#define FADC_GCR_RCT2_POS                    _FADC_UL(2)
#define FADC_GCR_RCT2_LEN                    _FADC_UL(1)


#define FADC_GCR_RCT3_POS                    _FADC_UL(3)
#define FADC_GCR_RCT3_LEN                    _FADC_UL(1)


#define FADC_GCR_RCD_POS                     _FADC_UL(8)
#define FADC_GCR_RCD_LEN                     _FADC_UL(1)


#define FADC_GCR_RSTF0_POS                   _FADC_UL(9)
#define FADC_GCR_RSTF0_LEN                   _FADC_UL(1)


#define FADC_GCR_RSTF1_POS                   _FADC_UL(10)
#define FADC_GCR_RSTF1_LEN                   _FADC_UL(1)


#define FADC_GCR_RSTF2_POS                   _FADC_UL(11)
#define FADC_GCR_RSTF2_LEN                   _FADC_UL(1)


#define FADC_GCR_RSTF3_POS                   _FADC_UL(12)
#define FADC_GCR_RSTF3_LEN                   _FADC_UL(1)


#define FADC_GCR_CRPRIO_POS                  _FADC_UL(16)
#define FADC_GCR_CRPRIO_LEN                  _FADC_UL(2)


#define FADC_GCR_DPAEN_POS                   _FADC_UL(18)
#define FADC_GCR_DPAEN_LEN                   _FADC_UL(1)


#define FADC_GCR_RESWEN_POS                  _FADC_UL(19)
#define FADC_GCR_RESWEN_LEN                  _FADC_UL(1)


#define FADC_GCR_MUXTM_POS                   _FADC_UL(20)
#define FADC_GCR_MUXTM_LEN                   _FADC_UL(1)


#define FADC_GCR_ANON_POS                    _FADC_UL(21)
#define FADC_GCR_ANON_LEN                    _FADC_UL(1)


#define FADC_GCR_CALMODE_POS                 _FADC_UL(24)
#define FADC_GCR_CALMODE_LEN                 _FADC_UL(2)


#define FADC_GCR_CALCH_POS                   _FADC_UL(26)
#define FADC_GCR_CALCH_LEN                   _FADC_UL(2)


#define FADC_CFGR_GSEL_POS                   _FADC_UL(0)
#define FADC_CFGR_GSEL_LEN                   _FADC_UL(3)


#define FADC_CFGR_TSEL_POS                   _FADC_UL(3)
#define FADC_CFGR_TSEL_LEN                   _FADC_UL(3)


#define FADC_CFGR_GM_POS                     _FADC_UL(6)
#define FADC_CFGR_GM_LEN                     _FADC_UL(2)


#define FADC_CFGR_TM_POS                     _FADC_UL(8)
#define FADC_CFGR_TM_LEN                     _FADC_UL(2)


#define FADC_CFGR_CTM_POS                    _FADC_UL(10)
#define FADC_CFGR_CTM_LEN                    _FADC_UL(2)


#define FADC_CFGR_CTF_POS                    _FADC_UL(12)
#define FADC_CFGR_CTF_LEN                    _FADC_UL(3)


#define FADC_CFGR_CTREL_POS                  _FADC_UL(16)
#define FADC_CFGR_CTREL_LEN                  _FADC_UL(8)


#define FADC_CFGR_INP_POS                    _FADC_UL(28)
#define FADC_CFGR_INP_LEN                    _FADC_UL(2)


#define FADC_CFGR_IEN_POS                    _FADC_UL(31)
#define FADC_CFGR_IEN_LEN                    _FADC_UL(1)


#define FADC_ACR_GAIN_POS                    _FADC_UL(0)
#define FADC_ACR_GAIN_LEN                    _FADC_UL(2)


#define FADC_ACR_ENP_POS                     _FADC_UL(2)
#define FADC_ACR_ENP_LEN                     _FADC_UL(1)


#define FADC_ACR_ENN_POS                     _FADC_UL(3)
#define FADC_ACR_ENN_LEN                     _FADC_UL(1)


#define FADC_ACR_CALOFF_POS                  _FADC_UL(8)
#define FADC_ACR_CALOFF_LEN                  _FADC_UL(3)


#define FADC_ACR_CALOFF3_POS                 _FADC_UL(12)
#define FADC_ACR_CALOFF3_LEN                 _FADC_UL(1)


#define FADC_ACR_CALGAIN_POS                 _FADC_UL(13)
#define FADC_ACR_CALGAIN_LEN                 _FADC_UL(2)


#define FADC_RCH_ADRES_POS                   _FADC_UL(0)
#define FADC_RCH_ADRES_LEN                   _FADC_UL(10)


#define FADC_ALR_ALIAS0_POS                  _FADC_UL(0)
#define FADC_ALR_ALIAS0_LEN                  _FADC_UL(2)


#define FADC_ALR_ALIAS1_POS                  _FADC_UL(2)
#define FADC_ALR_ALIAS1_LEN                  _FADC_UL(2)


#define FADC_ALR_ALIAS2_POS                  _FADC_UL(4)
#define FADC_ALR_ALIAS2_LEN                  _FADC_UL(2)


#define FADC_ALR_ALIAS3_POS                  _FADC_UL(6)
#define FADC_ALR_ALIAS3_LEN                  _FADC_UL(2)


#define FADC_FCR0_ADDL_POS                   _FADC_UL(0)
#define FADC_FCR0_ADDL_LEN                   _FADC_UL(3)


#define FADC_FCR0_MAVL_POS                   _FADC_UL(4)
#define FADC_FCR0_MAVL_LEN                   _FADC_UL(2)


#define FADC_FCR0_INSEL_POS                  _FADC_UL(8)
#define FADC_FCR0_INSEL_LEN                  _FADC_UL(3)


#define FADC_FCR0_INP_POS                    _FADC_UL(12)
#define FADC_FCR0_INP_LEN                    _FADC_UL(2)


#define FADC_FCR0_IEN_POS                    _FADC_UL(15)
#define FADC_FCR0_IEN_LEN                    _FADC_UL(1)


#define FADC_CRR0_CR_POS                     _FADC_UL(0)
#define FADC_CRR0_CR_LEN                     _FADC_UL(19)


#define FADC_CRR0_AC_POS                     _FADC_UL(24)
#define FADC_CRR0_AC_LEN                     _FADC_UL(3)


#define FADC_CRR0_MAVS_POS                   _FADC_UL(28)
#define FADC_CRR0_MAVS_LEN                   _FADC_UL(2)


#define FADC_IRR10_IR_POS                    _FADC_UL(0)
#define FADC_IRR10_IR_LEN                    _FADC_UL(13)


#define FADC_IRR20_IR_POS                    _FADC_UL(0)
#define FADC_IRR20_IR_LEN                    _FADC_UL(13)


#define FADC_IRR30_IR_POS                    _FADC_UL(0)
#define FADC_IRR30_IR_LEN                    _FADC_UL(13)


#define FADC_FRR0_FR_POS                     _FADC_UL(0)
#define FADC_FRR0_FR_LEN                     _FADC_UL(15)


#define FADC_FCR1_ADDL_POS                   _FADC_UL(0)
#define FADC_FCR1_ADDL_LEN                   _FADC_UL(3)


#define FADC_FCR1_MAVL_POS                   _FADC_UL(4)
#define FADC_FCR1_MAVL_LEN                   _FADC_UL(2)


#define FADC_FCR1_INSEL_POS                  _FADC_UL(8)
#define FADC_FCR1_INSEL_LEN                  _FADC_UL(3)


#define FADC_FCR1_INP_POS                    _FADC_UL(12)
#define FADC_FCR1_INP_LEN                    _FADC_UL(2)


#define FADC_FCR1_IEN_POS                    _FADC_UL(15)
#define FADC_FCR1_IEN_LEN                    _FADC_UL(1)


#define FADC_CRR1_CR_POS                     _FADC_UL(0)
#define FADC_CRR1_CR_LEN                     _FADC_UL(19)


#define FADC_CRR1_AC_POS                     _FADC_UL(24)
#define FADC_CRR1_AC_LEN                     _FADC_UL(3)


#define FADC_CRR1_MAVS_POS                   _FADC_UL(28)
#define FADC_CRR1_MAVS_LEN                   _FADC_UL(2)


#define FADC_IRR11_IR_POS                    _FADC_UL(0)
#define FADC_IRR11_IR_LEN                    _FADC_UL(18)


#define FADC_FRR1_FR_POS                     _FADC_UL(0)
#define FADC_FRR1_FR_LEN                     _FADC_UL(20)


#define FADC_SFRR1_FR_POS                    _FADC_UL(0)
#define FADC_SFRR1_FR_LEN                    _FADC_UL(15)


#define FADC_FCR2_ADDL_POS                   _FADC_UL(0)
#define FADC_FCR2_ADDL_LEN                   _FADC_UL(3)


#define FADC_FCR2_MAVL_POS                   _FADC_UL(4)
#define FADC_FCR2_MAVL_LEN                   _FADC_UL(2)


#define FADC_FCR2_INSEL_POS                  _FADC_UL(8)
#define FADC_FCR2_INSEL_LEN                  _FADC_UL(3)


#define FADC_FCR2_INP_POS                    _FADC_UL(12)
#define FADC_FCR2_INP_LEN                    _FADC_UL(2)


#define FADC_FCR2_IEN_POS                    _FADC_UL(15)
#define FADC_FCR2_IEN_LEN                    _FADC_UL(1)


#define FADC_CRR2_CR_POS                     _FADC_UL(0)
#define FADC_CRR2_CR_LEN                     _FADC_UL(19)


#define FADC_CRR2_AC_POS                     _FADC_UL(24)
#define FADC_CRR2_AC_LEN                     _FADC_UL(3)


#define FADC_CRR2_MAVS_POS                   _FADC_UL(28)
#define FADC_CRR2_MAVS_LEN                   _FADC_UL(2)


#define FADC_IRR12_IR_POS                    _FADC_UL(0)
#define FADC_IRR12_IR_LEN                    _FADC_UL(13)


#define FADC_IRR22_IR_POS                    _FADC_UL(0)
#define FADC_IRR22_IR_LEN                    _FADC_UL(13)


#define FADC_IRR32_IR_POS                    _FADC_UL(0)
#define FADC_IRR32_IR_LEN                    _FADC_UL(13)


#define FADC_FRR2_FR_POS                     _FADC_UL(0)
#define FADC_FRR2_FR_LEN                     _FADC_UL(15)


#define FADC_FCR3_ADDL_POS                   _FADC_UL(0)
#define FADC_FCR3_ADDL_LEN                   _FADC_UL(3)


#define FADC_FCR3_MAVL_POS                   _FADC_UL(4)
#define FADC_FCR3_MAVL_LEN                   _FADC_UL(2)


#define FADC_FCR3_INSEL_POS                  _FADC_UL(8)
#define FADC_FCR3_INSEL_LEN                  _FADC_UL(3)


#define FADC_FCR3_INP_POS                    _FADC_UL(12)
#define FADC_FCR3_INP_LEN                    _FADC_UL(2)


#define FADC_FCR3_IEN_POS                    _FADC_UL(15)
#define FADC_FCR3_IEN_LEN                    _FADC_UL(1)


#define FADC_CRR3_CR_POS                     _FADC_UL(0)
#define FADC_CRR3_CR_LEN                     _FADC_UL(19)


#define FADC_CRR3_AC_POS                     _FADC_UL(24)
#define FADC_CRR3_AC_LEN                     _FADC_UL(3)


#define FADC_CRR3_MAVS_POS                   _FADC_UL(28)
#define FADC_CRR3_MAVS_LEN                   _FADC_UL(2)


#define FADC_IRR13_IR_POS                    _FADC_UL(0)
#define FADC_IRR13_IR_LEN                    _FADC_UL(18)


#define FADC_FRR3_FR_POS                     _FADC_UL(0)
#define FADC_FRR3_FR_LEN                     _FADC_UL(20)


#define FADC_SFRR3_FR_POS                    _FADC_UL(0)
#define FADC_SFRR3_FR_LEN                    _FADC_UL(15)


#define FADC_SRC3_SRPN_POS                   _FADC_UL(0)
#define FADC_SRC3_SRPN_LEN                   _FADC_UL(8)


#define FADC_SRC3_TOS_POS                    _FADC_UL(10)
#define FADC_SRC3_TOS_LEN                    _FADC_UL(1)


#define FADC_SRC3_SRE_POS                    _FADC_UL(12)
#define FADC_SRC3_SRE_LEN                    _FADC_UL(1)


#define FADC_SRC3_SRR_POS                    _FADC_UL(13)
#define FADC_SRC3_SRR_LEN                    _FADC_UL(1)


#define FADC_SRC3_CLRR_POS                   _FADC_UL(14)
#define FADC_SRC3_CLRR_LEN                   _FADC_UL(1)


#define FADC_SRC3_SETR_POS                   _FADC_UL(15)
#define FADC_SRC3_SETR_LEN                   _FADC_UL(1)


#define FADC_SRC2_SRPN_POS                   _FADC_UL(0)
#define FADC_SRC2_SRPN_LEN                   _FADC_UL(8)


#define FADC_SRC2_TOS_POS                    _FADC_UL(10)
#define FADC_SRC2_TOS_LEN                    _FADC_UL(1)


#define FADC_SRC2_SRE_POS                    _FADC_UL(12)
#define FADC_SRC2_SRE_LEN                    _FADC_UL(1)


#define FADC_SRC2_SRR_POS                    _FADC_UL(13)
#define FADC_SRC2_SRR_LEN                    _FADC_UL(1)


#define FADC_SRC2_CLRR_POS                   _FADC_UL(14)
#define FADC_SRC2_CLRR_LEN                   _FADC_UL(1)


#define FADC_SRC2_SETR_POS                   _FADC_UL(15)
#define FADC_SRC2_SETR_LEN                   _FADC_UL(1)


#define FADC_SRC1_SRPN_POS                   _FADC_UL(0)
#define FADC_SRC1_SRPN_LEN                   _FADC_UL(8)


#define FADC_SRC1_TOS_POS                    _FADC_UL(10)
#define FADC_SRC1_TOS_LEN                    _FADC_UL(1)


#define FADC_SRC1_SRE_POS                    _FADC_UL(12)
#define FADC_SRC1_SRE_LEN                    _FADC_UL(1)


#define FADC_SRC1_SRR_POS                    _FADC_UL(13)
#define FADC_SRC1_SRR_LEN                    _FADC_UL(1)


#define FADC_SRC1_CLRR_POS                   _FADC_UL(14)
#define FADC_SRC1_CLRR_LEN                   _FADC_UL(1)


#define FADC_SRC1_SETR_POS                   _FADC_UL(15)
#define FADC_SRC1_SETR_LEN                   _FADC_UL(1)


#define FADC_SRC0_SRPN_POS                   _FADC_UL(0)
#define FADC_SRC0_SRPN_LEN                   _FADC_UL(8)


#define FADC_SRC0_TOS_POS                    _FADC_UL(10)
#define FADC_SRC0_TOS_LEN                    _FADC_UL(1)


#define FADC_SRC0_SRE_POS                    _FADC_UL(12)
#define FADC_SRC0_SRE_LEN                    _FADC_UL(1)


#define FADC_SRC0_SRR_POS                    _FADC_UL(13)
#define FADC_SRC0_SRR_LEN                    _FADC_UL(1)


#define FADC_SRC0_CLRR_POS                   _FADC_UL(14)
#define FADC_SRC0_CLRR_LEN                   _FADC_UL(1)


#define FADC_SRC0_SETR_POS                   _FADC_UL(15)
#define FADC_SRC0_SETR_LEN                   _FADC_UL(1)

#endif
