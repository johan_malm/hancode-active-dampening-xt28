

#ifndef _MCHK_1797_H
#define _MCHK_1797_H


#ifdef REGDEF_FOR_PCP
    #define _MCHK_UL(x) x
#else
    #define _MCHK_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[1];        
    volatile uint32 IR;                  
    volatile uint32 RR;                  
    volatile uint32 RESERVED1[2];        
    volatile uint32 WR;                  
} MCHK_RegMap_t;




extern MCHK_RegMap_t MCHK __attribute__ ((asection (".bss.label_only")));





#define MCHK_ID_MOD_NUMBER_POS               _MCHK_UL(16)
#define MCHK_ID_MOD_NUMBER_LEN               _MCHK_UL(16)


#define MCHK_ID_MOD_REV_POS                  _MCHK_UL(0)
#define MCHK_ID_MOD_REV_LEN                  _MCHK_UL(8)


#define MCHK_ID_MOD_TYPE_POS                 _MCHK_UL(8)
#define MCHK_ID_MOD_TYPE_LEN                 _MCHK_UL(8)


#define MCHK_ID_MOD_REV_POS                  _MCHK_UL(0)
#define MCHK_ID_MOD_REV_LEN                  _MCHK_UL(8)


#define MCHK_ID_MOD_TYPE_POS                 _MCHK_UL(8)
#define MCHK_ID_MOD_TYPE_LEN                 _MCHK_UL(8)


#define MCHK_ID_MOD_NUMBER_POS               _MCHK_UL(16)
#define MCHK_ID_MOD_NUMBER_LEN               _MCHK_UL(16)


#define MCHK_IR_MCHKIN_POS                   _MCHK_UL(0)
#define MCHK_IR_MCHKIN_LEN                   _MCHK_UL(32)


#define MCHK_RR_MCHKR_POS                    _MCHK_UL(0)
#define MCHK_RR_MCHKR_LEN                    _MCHK_UL(32)


#define MCHK_WR_WO_POS                       _MCHK_UL(0)
#define MCHK_WR_WO_LEN                       _MCHK_UL(32)

#endif
