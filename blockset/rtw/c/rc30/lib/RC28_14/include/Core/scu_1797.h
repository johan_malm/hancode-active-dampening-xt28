

#ifndef _SCU_1797_H
#define _SCU_1797_H


#ifdef REGDEF_FOR_PCP
    #define _SCU_UL(x) x
#else
    #define _SCU_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[1];        
    volatile uint32 OSCCON;              
    volatile uint32 PLLSTAT;             
    volatile uint32 PLLCON0;             
    volatile uint32 PLLCON1;             
    volatile uint32 PLLERAYCTR;          
    volatile uint32 RESERVED1[3];        
    volatile uint32 CCUCON0;             
    volatile uint32 CCUCON1;             
    volatile uint32 FDR;                 
    volatile uint32 EXTCON;              
    volatile uint32 SYSCON;              
    volatile uint32 RESERVED2[3];        
    volatile uint32 RSTSTAT;             
    volatile uint32 RSTCNTCON;           
    volatile uint32 RSTCON;              
    volatile uint32 ARSTDIS;             
    volatile uint32 SWRSTCON;            
    volatile uint32 RESERVED3[3];        
    volatile uint32 ESRCFG0;             
    volatile uint32 ESRCFG1;             
    volatile uint32 ESRCFG2;             
    volatile uint32 RESERVED4[1];        
    volatile uint32 EICR0;               
    volatile uint32 EICR1;               
    volatile uint32 EIFR;                
    volatile uint32 FMR;                 
    volatile uint32 PDRR;                
    volatile uint32 IGCR0;               
    volatile uint32 IGCR1;               
    volatile uint32 RESERVED5[1];        
    volatile uint32 IOCR;                
    volatile uint32 OUT;                 
    volatile uint32 OMR;                 
    volatile uint32 IN;                  
    volatile uint32 PMCSR;               
    volatile uint32 RESERVED6[3];        
    volatile uint32 STSTAT;              
    volatile uint32 STCON;               
    volatile uint32 RESERVED7[2];        
    volatile uint32 PETCR;               
	volatile uint32 PETSR;               
    volatile uint32 RESERVED8[2];        
    volatile uint32 DTSSTAT;             
    volatile uint32 DTSCON;              
    volatile uint32 RESERVED9[2];        
    volatile uint32 WDT_CON0;            
    volatile uint32 WDT_CON1;            
    volatile uint32 WDT_SR;              
    volatile uint32 RESERVED10[1];       
    volatile uint32 EMSR;                
    volatile uint32 RESERVED11[3];       
    volatile uint32 INTSTAT;             
    volatile uint32 INTSET;              
    volatile uint32 INTCLR;              
    volatile uint32 INTDIS;              
    volatile uint32 INTNP;               
    volatile uint32 TRAPSTAT;            
    volatile uint32 TRAPSET;             
    volatile uint32 TRAPCLR;             
    volatile uint32 TRAPDIS;             
    volatile uint32 RESERVED12[3];       
    volatile uint32 CHIPID;              
    volatile uint32 MANID;               
    volatile uint32 RESERVED13[42];      
    volatile uint32 SRC3;                
    volatile uint32 SRC2;                
    volatile uint32 SRC1;                
    volatile uint32 SRC0;                
} SCU_RegMap_t;




extern SCU_RegMap_t SCU __attribute__ ((asection (".zbss.label_only","f=awz")));





#define SCU_WDT_CON0_WDTLCK_POS                  SCU_WDT_CON0_LCK_POS
#define SCU_WDT_CON0_WDTLCK_LEN                  SCU_WDT_CON0_LCK_LEN


#define SCU_WDT_CON0_WDTHPW0_POS                 SCU_WDT_CON0_HPW0_POS
#define SCU_WDT_CON0_WDTHPW0_LEN                 SCU_WDT_CON0_HPW0_LEN


#define SCU_WDT_CON0_WDTHPW1_POS                 SCU_WDT_CON0_HPW1_POS
#define SCU_WDT_CON0_WDTHPW1_LEN                 SCU_WDT_CON0_HPW1_LEN


#define SCU_WDT_CON0_WDTPW_POS                   SCU_WDT_CON0_PW_POS
#define SCU_WDT_CON0_WDTPW_LEN                   SCU_WDT_CON0_PW_LEN


#define SCU_WDT_CON0_WDTREL_POS                  SCU_WDT_CON0_REL_POS
#define SCU_WDT_CON0_WDTREL_LEN                  SCU_WDT_CON0_REL_LEN


#define SCU_WDT_CON1_WDTIR_POS                   SCU_WDT_CON1_IR_POS
#define SCU_WDT_CON1_WDTIR_LEN                   SCU_WDT_CON1_IR_LEN


#define SCU_WDT_CON1_WDTDR_POS                   SCU_WDT_CON1_DR_POS
#define SCU_WDT_CON1_WDTDR_LEN                   SCU_WDT_CON1_DR_LEN


#define SCU_WDT_SR_WDTAE_POS                     SCU_WDT_SR_AE_POS
#define SCU_WDT_SR_WDTAE_LEN                     SCU_WDT_SR_AE_LEN


#define SCU_WDT_SR_WDTOE_POS                     SCU_WDT_SR_OE_POS
#define SCU_WDT_SR_WDTOE_LEN                     SCU_WDT_SR_OE_LEN


#define SCU_WDT_SR_WDTIS_POS                     SCU_WDT_SR_IS_POS
#define SCU_WDT_SR_WDTIS_LEN                     SCU_WDT_SR_IS_LEN


#define SCU_WDT_SR_WDTDS_POS                     SCU_WDT_SR_DS_POS
#define SCU_WDT_SR_WDTDS_LEN                     SCU_WDT_SR_DS_LEN


#define SCU_WDT_SR_WDTTO_POS                     SCU_WDT_SR_TO_POS
#define SCU_WDT_SR_WDTTO_LEN                     SCU_WDT_SR_TO_LEN


#define SCU_WDT_SR_WDTPR_POS                     SCU_WDT_SR_PR_POS
#define SCU_WDT_SR_WDTPR_LEN                     SCU_WDT_SR_PR_LEN


#define SCU_WDT_SR_WDTTIM_POS                    SCU_WDT_SR_TIM_POS
#define SCU_WDT_SR_WDTTIM_LEN                    SCU_WDT_SR_TIM_LEN








#define SCU_ID_MOD_NUMBER_POS                _SCU_UL(16)
#define SCU_ID_MOD_NUMBER_LEN                _SCU_UL(16)


#define SCU_ID_MOD_REV_POS                   _SCU_UL(0)
#define SCU_ID_MOD_REV_LEN                   _SCU_UL(8)


#define SCU_ID_MOD_TYPE_POS                  _SCU_UL(8)
#define SCU_ID_MOD_TYPE_LEN                  _SCU_UL(8)


#define SCU_ID_MODREV_POS                    _SCU_UL(0)
#define SCU_ID_MODREV_LEN                    _SCU_UL(8)


#define SCU_ID_MODTYPE_POS                   _SCU_UL(8)
#define SCU_ID_MODTYPE_LEN                   _SCU_UL(8)


#define SCU_ID_MODNUMBER_POS                 _SCU_UL(16)
#define SCU_ID_MODNUMBER_LEN                 _SCU_UL(16)


#define SCU_OSCCON_PLLLV_POS                 _SCU_UL(1)
#define SCU_OSCCON_PLLLV_LEN                 _SCU_UL(1)


#define SCU_OSCCON_OSCRES_POS                _SCU_UL(2)
#define SCU_OSCCON_OSCRES_LEN                _SCU_UL(1)


#define SCU_OSCCON_GAINSEL_POS               _SCU_UL(3)
#define SCU_OSCCON_GAINSEL_LEN               _SCU_UL(2)


#define SCU_OSCCON_MODE_POS                  _SCU_UL(5)
#define SCU_OSCCON_MODE_LEN                  _SCU_UL(2)


#define SCU_OSCCON_SHBY_POS                  _SCU_UL(7)
#define SCU_OSCCON_SHBY_LEN                  _SCU_UL(1)


#define SCU_OSCCON_PLLHV_POS                 _SCU_UL(8)
#define SCU_OSCCON_PLLHV_LEN                 _SCU_UL(1)


#define SCU_OSCCON_PLLSP_POS                 _SCU_UL(9)
#define SCU_OSCCON_PLLSP_LEN                 _SCU_UL(1)


#define SCU_OSCCON_X1D_POS                   _SCU_UL(10)
#define SCU_OSCCON_X1D_LEN                   _SCU_UL(1)


#define SCU_OSCCON_X1DEN_POS                 _SCU_UL(11)
#define SCU_OSCCON_X1DEN_LEN                 _SCU_UL(1)


#define SCU_OSCCON_OSCVAL_POS                _SCU_UL(16)
#define SCU_OSCCON_OSCVAL_LEN                _SCU_UL(5)


#define SCU_PLLSTAT_VCOBYST_POS              _SCU_UL(0)
#define SCU_PLLSTAT_VCOBYST_LEN              _SCU_UL(1)


#define SCU_PLLSTAT_PWDSTAT_POS              _SCU_UL(1)
#define SCU_PLLSTAT_PWDSTAT_LEN              _SCU_UL(1)


#define SCU_PLLSTAT_VCOLOCK_POS              _SCU_UL(2)
#define SCU_PLLSTAT_VCOLOCK_LEN              _SCU_UL(1)


#define SCU_PLLSTAT_FINDIS_POS               _SCU_UL(3)
#define SCU_PLLSTAT_FINDIS_LEN               _SCU_UL(1)


#define SCU_PLLSTAT_K1RDY_POS                _SCU_UL(4)
#define SCU_PLLSTAT_K1RDY_LEN                _SCU_UL(1)


#define SCU_PLLCON0_VCOBYP_POS               _SCU_UL(0)
#define SCU_PLLCON0_VCOBYP_LEN               _SCU_UL(1)


#define SCU_PLLCON0_VCOPWD_POS               _SCU_UL(1)
#define SCU_PLLCON0_VCOPWD_LEN               _SCU_UL(1)


#define SCU_PLLCON0_SETFINDIS_POS            _SCU_UL(4)
#define SCU_PLLCON0_SETFINDIS_LEN            _SCU_UL(1)


#define SCU_PLLCON0_CLRFINDIS_POS            _SCU_UL(5)
#define SCU_PLLCON0_CLRFINDIS_LEN            _SCU_UL(1)


#define SCU_PLLCON0_OSCDISCDIS_POS           _SCU_UL(6)
#define SCU_PLLCON0_OSCDISCDIS_LEN           _SCU_UL(1)


#define SCU_PLLCON0_NDIV_POS                 _SCU_UL(9)
#define SCU_PLLCON0_NDIV_LEN                 _SCU_UL(7)


#define SCU_PLLCON0_PLLPWD_POS               _SCU_UL(16)
#define SCU_PLLCON0_PLLPWD_LEN               _SCU_UL(1)


#define SCU_PLLCON0_RESLD_POS                _SCU_UL(18)
#define SCU_PLLCON0_RESLD_LEN                _SCU_UL(1)


#define SCU_PLLCON0_PDIV_POS                 _SCU_UL(24)
#define SCU_PLLCON0_PDIV_LEN                 _SCU_UL(4)


#define SCU_PLLCON1_K2DIV_POS                _SCU_UL(0)
#define SCU_PLLCON1_K2DIV_LEN                _SCU_UL(7)


#define SCU_PLLCON1_K1DIV_POS                _SCU_UL(16)
#define SCU_PLLCON1_K1DIV_LEN                _SCU_UL(7)


#define SCU_PLLERAYCTR_KDIV_POS              _SCU_UL(0)
#define SCU_PLLERAYCTR_KDIV_LEN              _SCU_UL(4)


#define SCU_PLLERAYCTR_PDIV_POS              _SCU_UL(4)
#define SCU_PLLERAYCTR_PDIV_LEN              _SCU_UL(3)


#define SCU_PLLERAYCTR_VB_POS                _SCU_UL(7)
#define SCU_PLLERAYCTR_VB_LEN                _SCU_UL(2)


#define SCU_PLLERAYCTR_PWD_POS               _SCU_UL(9)
#define SCU_PLLERAYCTR_PWD_LEN               _SCU_UL(1)


#define SCU_PLLERAYCTR_VCOBYP_POS            _SCU_UL(10)
#define SCU_PLLERAYCTR_VCOBYP_LEN            _SCU_UL(1)


#define SCU_PLLERAYCTR_NDIV_POS              _SCU_UL(11)
#define SCU_PLLERAYCTR_NDIV_LEN              _SCU_UL(5)


#define SCU_PLLERAYCTR_VCOLDRES_POS          _SCU_UL(16)
#define SCU_PLLERAYCTR_VCOLDRES_LEN          _SCU_UL(1)


#define SCU_PLLERAYCTR_VCOLOCK_POS           _SCU_UL(17)
#define SCU_PLLERAYCTR_VCOLOCK_LEN           _SCU_UL(1)


#define SCU_PLLERAYCTR_OSCWDTRES_POS         _SCU_UL(18)
#define SCU_PLLERAYCTR_OSCWDTRES_LEN         _SCU_UL(1)


#define SCU_PLLERAYCTR_PLLV_POS              _SCU_UL(19)
#define SCU_PLLERAYCTR_PLLV_LEN              _SCU_UL(1)


#define SCU_PLLERAYCTR_KDIVRDY_POS           _SCU_UL(20)
#define SCU_PLLERAYCTR_KDIVRDY_LEN           _SCU_UL(1)


#define SCU_PLLERAYCTR_PWDSTAT_POS           _SCU_UL(21)
#define SCU_PLLERAYCTR_PWDSTAT_LEN           _SCU_UL(1)


#define SCU_PLLERAYCTR_FINDIS_POS            _SCU_UL(22)
#define SCU_PLLERAYCTR_FINDIS_LEN            _SCU_UL(1)


#define SCU_PLLERAYCTR_PLLEN_POS             _SCU_UL(23)
#define SCU_PLLERAYCTR_PLLEN_LEN             _SCU_UL(1)


#define SCU_CCUCON0_FPIDIV_POS               _SCU_UL(0)
#define SCU_CCUCON0_FPIDIV_LEN               _SCU_UL(4)


#define SCU_CCUCON0_LMBDIV_POS               _SCU_UL(8)
#define SCU_CCUCON0_LMBDIV_LEN               _SCU_UL(4)


#define SCU_CCUCON0_PCPDIV_POS               _SCU_UL(24)
#define SCU_CCUCON0_PCPDIV_LEN               _SCU_UL(4)


#define SCU_CCUCON0_LCK_POS                  _SCU_UL(31)
#define SCU_CCUCON0_LCK_LEN                  _SCU_UL(1)


#define SCU_CCUCON1_MCDSDIV_POS              _SCU_UL(0)
#define SCU_CCUCON1_MCDSDIV_LEN              _SCU_UL(4)


#define SCU_CCUCON1_REFCLKDIV_POS            _SCU_UL(8)
#define SCU_CCUCON1_REFCLKDIV_LEN            _SCU_UL(4)


#define SCU_CCUCON1_LCK_POS                  _SCU_UL(31)
#define SCU_CCUCON1_LCK_LEN                  _SCU_UL(1)


#define SCU_FDR_STEP_POS                     _SCU_UL(0)
#define SCU_FDR_STEP_LEN                     _SCU_UL(10)


#define SCU_FDR_DM_POS                       _SCU_UL(14)
#define SCU_FDR_DM_LEN                       _SCU_UL(2)


#define SCU_FDR_RESULT_POS                   _SCU_UL(16)
#define SCU_FDR_RESULT_LEN                   _SCU_UL(10)


#define SCU_FDR_DISCLK_POS                   _SCU_UL(31)
#define SCU_FDR_DISCLK_LEN                   _SCU_UL(1)


#define SCU_EXTCON_EN0_POS                   _SCU_UL(0)
#define SCU_EXTCON_EN0_LEN                   _SCU_UL(1)


#define SCU_EXTCON_SEL0_POS                  _SCU_UL(2)
#define SCU_EXTCON_SEL0_LEN                  _SCU_UL(4)


#define SCU_EXTCON_GPTAINSEL_POS             _SCU_UL(6)
#define SCU_EXTCON_GPTAINSEL_LEN             _SCU_UL(1)


#define SCU_EXTCON_EN1_POS                   _SCU_UL(16)
#define SCU_EXTCON_EN1_LEN                   _SCU_UL(1)


#define SCU_EXTCON_NSEL_POS                  _SCU_UL(17)
#define SCU_EXTCON_NSEL_LEN                  _SCU_UL(1)


#define SCU_EXTCON_SEL1_POS                  _SCU_UL(18)
#define SCU_EXTCON_SEL1_LEN                  _SCU_UL(4)


#define SCU_EXTCON_DIV1_POS                  _SCU_UL(24)
#define SCU_EXTCON_DIV1_LEN                  _SCU_UL(8)


#define SCU_SYSCON_GPTAIS_POS                _SCU_UL(2)
#define SCU_SYSCON_GPTAIS_LEN                _SCU_UL(2)


#define SCU_SYSCON_SETLUDIS_POS              _SCU_UL(4)
#define SCU_SYSCON_SETLUDIS_LEN              _SCU_UL(1)


#define SCU_SYSCON_SETEXTBEN_POS             _SCU_UL(5)
#define SCU_SYSCON_SETEXTBEN_LEN             _SCU_UL(1)


#define SCU_RSTSTAT_ESR0_POS                 _SCU_UL(0)
#define SCU_RSTSTAT_ESR0_LEN                 _SCU_UL(1)


#define SCU_RSTSTAT_ESR1_POS                 _SCU_UL(1)
#define SCU_RSTSTAT_ESR1_LEN                 _SCU_UL(1)


#define SCU_RSTSTAT_WDT_POS                  _SCU_UL(3)
#define SCU_RSTSTAT_WDT_LEN                  _SCU_UL(1)


#define SCU_RSTSTAT_SW_POS                   _SCU_UL(4)
#define SCU_RSTSTAT_SW_LEN                   _SCU_UL(1)


#define SCU_RSTSTAT_PORST_POS                _SCU_UL(16)
#define SCU_RSTSTAT_PORST_LEN                _SCU_UL(1)


#define SCU_RSTSTAT_OCDS_POS                 _SCU_UL(17)
#define SCU_RSTSTAT_OCDS_LEN                 _SCU_UL(1)


#define SCU_RSTSTAT_CB0_POS                  _SCU_UL(18)
#define SCU_RSTSTAT_CB0_LEN                  _SCU_UL(1)


#define SCU_RSTSTAT_CB1_POS                  _SCU_UL(19)
#define SCU_RSTSTAT_CB1_LEN                  _SCU_UL(1)


#define SCU_RSTSTAT_CB3_POS                  _SCU_UL(20)
#define SCU_RSTSTAT_CB3_LEN                  _SCU_UL(1)


#define SCU_RSTSTAT_TP_POS                   _SCU_UL(21)
#define SCU_RSTSTAT_TP_LEN                   _SCU_UL(1)


#define SCU_RSTCNTCON_RELSA_POS              _SCU_UL(0)
#define SCU_RSTCNTCON_RELSA_LEN              _SCU_UL(16)


#define SCU_RSTCNTCON_RELD_POS               _SCU_UL(16)
#define SCU_RSTCNTCON_RELD_LEN               _SCU_UL(16)


#define SCU_RSTCON_ESR0_POS                  _SCU_UL(0)
#define SCU_RSTCON_ESR0_LEN                  _SCU_UL(2)


#define SCU_RSTCON_ESR1_POS                  _SCU_UL(2)
#define SCU_RSTCON_ESR1_LEN                  _SCU_UL(2)


#define SCU_RSTCON_WDT_POS                   _SCU_UL(6)
#define SCU_RSTCON_WDT_LEN                   _SCU_UL(2)


#define SCU_RSTCON_SW_POS                    _SCU_UL(8)
#define SCU_RSTCON_SW_LEN                    _SCU_UL(2)


#define SCU_ARSTDIS_STMDIS_POS               _SCU_UL(0)
#define SCU_ARSTDIS_STMDIS_LEN               _SCU_UL(1)


#define SCU_SWRSTCON_SWBOOT_POS              _SCU_UL(0)
#define SCU_SWRSTCON_SWBOOT_LEN              _SCU_UL(1)


#define SCU_SWRSTCON_SWRSTREQ_POS            _SCU_UL(1)
#define SCU_SWRSTCON_SWRSTREQ_LEN            _SCU_UL(1)


#define SCU_SWRSTCON_SWCFG_POS               _SCU_UL(8)
#define SCU_SWRSTCON_SWCFG_LEN               _SCU_UL(8)


#define SCU_ESRCFG0_DFEN_POS                 _SCU_UL(4)
#define SCU_ESRCFG0_DFEN_LEN                 _SCU_UL(1)


#define SCU_ESRCFG0_EDCON_POS                _SCU_UL(7)
#define SCU_ESRCFG0_EDCON_LEN                _SCU_UL(2)


#define SCU_ESRCFG1_DFEN_POS                 _SCU_UL(4)
#define SCU_ESRCFG1_DFEN_LEN                 _SCU_UL(1)


#define SCU_ESRCFG1_EDCON_POS                _SCU_UL(7)
#define SCU_ESRCFG1_EDCON_LEN                _SCU_UL(2)


#define SCU_ESRCFG2_DFEN_POS                 _SCU_UL(4)
#define SCU_ESRCFG2_DFEN_LEN                 _SCU_UL(1)


#define SCU_ESRCFG2_EDCON_POS                _SCU_UL(7)
#define SCU_ESRCFG2_EDCON_LEN                _SCU_UL(2)


#define SCU_EICR0_EXIS0_POS                  _SCU_UL(4)
#define SCU_EICR0_EXIS0_LEN                  _SCU_UL(2)


#define SCU_EICR0_FEN0_POS                   _SCU_UL(8)
#define SCU_EICR0_FEN0_LEN                   _SCU_UL(1)


#define SCU_EICR0_REN0_POS                   _SCU_UL(9)
#define SCU_EICR0_REN0_LEN                   _SCU_UL(1)


#define SCU_EICR0_LDEN0_POS                  _SCU_UL(10)
#define SCU_EICR0_LDEN0_LEN                  _SCU_UL(1)


#define SCU_EICR0_EIEN0_POS                  _SCU_UL(11)
#define SCU_EICR0_EIEN0_LEN                  _SCU_UL(1)


#define SCU_EICR0_INP0_POS                   _SCU_UL(12)
#define SCU_EICR0_INP0_LEN                   _SCU_UL(3)


#define SCU_EICR0_EXIS1_POS                  _SCU_UL(20)
#define SCU_EICR0_EXIS1_LEN                  _SCU_UL(2)


#define SCU_EICR0_FEN1_POS                   _SCU_UL(24)
#define SCU_EICR0_FEN1_LEN                   _SCU_UL(1)


#define SCU_EICR0_REN1_POS                   _SCU_UL(25)
#define SCU_EICR0_REN1_LEN                   _SCU_UL(1)


#define SCU_EICR0_LDEN1_POS                  _SCU_UL(26)
#define SCU_EICR0_LDEN1_LEN                  _SCU_UL(1)


#define SCU_EICR0_EIEN1_POS                  _SCU_UL(27)
#define SCU_EICR0_EIEN1_LEN                  _SCU_UL(1)


#define SCU_EICR0_INP1_POS                   _SCU_UL(28)
#define SCU_EICR0_INP1_LEN                   _SCU_UL(3)


#define SCU_EICR1_EXIS2_POS                  _SCU_UL(4)
#define SCU_EICR1_EXIS2_LEN                  _SCU_UL(2)


#define SCU_EICR1_FEN2_POS                   _SCU_UL(8)
#define SCU_EICR1_FEN2_LEN                   _SCU_UL(1)


#define SCU_EICR1_REN2_POS                   _SCU_UL(9)
#define SCU_EICR1_REN2_LEN                   _SCU_UL(1)


#define SCU_EICR1_LDEN2_POS                  _SCU_UL(10)
#define SCU_EICR1_LDEN2_LEN                  _SCU_UL(1)


#define SCU_EICR1_EIEN2_POS                  _SCU_UL(11)
#define SCU_EICR1_EIEN2_LEN                  _SCU_UL(1)


#define SCU_EICR1_INP2_POS                   _SCU_UL(12)
#define SCU_EICR1_INP2_LEN                   _SCU_UL(3)


#define SCU_EICR1_EXIS3_POS                  _SCU_UL(20)
#define SCU_EICR1_EXIS3_LEN                  _SCU_UL(2)


#define SCU_EICR1_FEN3_POS                   _SCU_UL(24)
#define SCU_EICR1_FEN3_LEN                   _SCU_UL(1)


#define SCU_EICR1_REN3_POS                   _SCU_UL(25)
#define SCU_EICR1_REN3_LEN                   _SCU_UL(1)


#define SCU_EICR1_LDEN3_POS                  _SCU_UL(26)
#define SCU_EICR1_LDEN3_LEN                  _SCU_UL(1)


#define SCU_EICR1_EIEN3_POS                  _SCU_UL(27)
#define SCU_EICR1_EIEN3_LEN                  _SCU_UL(1)


#define SCU_EICR1_INP3_POS                   _SCU_UL(28)
#define SCU_EICR1_INP3_LEN                   _SCU_UL(3)


#define SCU_EIFR_INTF0_POS                   _SCU_UL(0)
#define SCU_EIFR_INTF0_LEN                   _SCU_UL(1)


#define SCU_EIFR_INTF1_POS                   _SCU_UL(1)
#define SCU_EIFR_INTF1_LEN                   _SCU_UL(1)


#define SCU_EIFR_INTF2_POS                   _SCU_UL(2)
#define SCU_EIFR_INTF2_LEN                   _SCU_UL(1)


#define SCU_EIFR_INTF3_POS                   _SCU_UL(3)
#define SCU_EIFR_INTF3_LEN                   _SCU_UL(1)


#define SCU_FMR_FS0_POS                      _SCU_UL(0)
#define SCU_FMR_FS0_LEN                      _SCU_UL(1)


#define SCU_FMR_FS1_POS                      _SCU_UL(1)
#define SCU_FMR_FS1_LEN                      _SCU_UL(1)


#define SCU_FMR_FS2_POS                      _SCU_UL(2)
#define SCU_FMR_FS2_LEN                      _SCU_UL(1)


#define SCU_FMR_FS3_POS                      _SCU_UL(3)
#define SCU_FMR_FS3_LEN                      _SCU_UL(1)


#define SCU_FMR_FC0_POS                      _SCU_UL(16)
#define SCU_FMR_FC0_LEN                      _SCU_UL(1)


#define SCU_FMR_FC1_POS                      _SCU_UL(17)
#define SCU_FMR_FC1_LEN                      _SCU_UL(1)


#define SCU_FMR_FC2_POS                      _SCU_UL(18)
#define SCU_FMR_FC2_LEN                      _SCU_UL(1)


#define SCU_FMR_FC3_POS                      _SCU_UL(19)
#define SCU_FMR_FC3_LEN                      _SCU_UL(1)


#define SCU_PDRR_PDR0_POS                    _SCU_UL(0)
#define SCU_PDRR_PDR0_LEN                    _SCU_UL(1)


#define SCU_PDRR_PDR1_POS                    _SCU_UL(1)
#define SCU_PDRR_PDR1_LEN                    _SCU_UL(1)


#define SCU_PDRR_PDR2_POS                    _SCU_UL(2)
#define SCU_PDRR_PDR2_LEN                    _SCU_UL(1)


#define SCU_PDRR_PDR3_POS                    _SCU_UL(3)
#define SCU_PDRR_PDR3_LEN                    _SCU_UL(1)


#define SCU_IGCR0_IPEN00_POS                 _SCU_UL(0)
#define SCU_IGCR0_IPEN00_LEN                 _SCU_UL(1)


#define SCU_IGCR0_IPEN01_POS                 _SCU_UL(1)
#define SCU_IGCR0_IPEN01_LEN                 _SCU_UL(1)


#define SCU_IGCR0_IPEN02_POS                 _SCU_UL(2)
#define SCU_IGCR0_IPEN02_LEN                 _SCU_UL(1)


#define SCU_IGCR0_IPEN03_POS                 _SCU_UL(3)
#define SCU_IGCR0_IPEN03_LEN                 _SCU_UL(1)


#define SCU_IGCR0_GEEN0_POS                  _SCU_UL(13)
#define SCU_IGCR0_GEEN0_LEN                  _SCU_UL(1)


#define SCU_IGCR0_IGP0_POS                   _SCU_UL(14)
#define SCU_IGCR0_IGP0_LEN                   _SCU_UL(2)


#define SCU_IGCR0_IPEN10_POS                 _SCU_UL(16)
#define SCU_IGCR0_IPEN10_LEN                 _SCU_UL(1)


#define SCU_IGCR0_IPEN11_POS                 _SCU_UL(17)
#define SCU_IGCR0_IPEN11_LEN                 _SCU_UL(1)


#define SCU_IGCR0_IPEN12_POS                 _SCU_UL(18)
#define SCU_IGCR0_IPEN12_LEN                 _SCU_UL(1)


#define SCU_IGCR0_IPEN13_POS                 _SCU_UL(19)
#define SCU_IGCR0_IPEN13_LEN                 _SCU_UL(1)


#define SCU_IGCR0_GEEN1_POS                  _SCU_UL(29)
#define SCU_IGCR0_GEEN1_LEN                  _SCU_UL(1)


#define SCU_IGCR0_IGP1_POS                   _SCU_UL(30)
#define SCU_IGCR0_IGP1_LEN                   _SCU_UL(2)


#define SCU_IGCR1_IPEN20_POS                 _SCU_UL(0)
#define SCU_IGCR1_IPEN20_LEN                 _SCU_UL(1)


#define SCU_IGCR1_IPEN21_POS                 _SCU_UL(1)
#define SCU_IGCR1_IPEN21_LEN                 _SCU_UL(1)


#define SCU_IGCR1_IPEN22_POS                 _SCU_UL(2)
#define SCU_IGCR1_IPEN22_LEN                 _SCU_UL(1)


#define SCU_IGCR1_IPEN23_POS                 _SCU_UL(3)
#define SCU_IGCR1_IPEN23_LEN                 _SCU_UL(1)


#define SCU_IGCR1_GEEN2_POS                  _SCU_UL(13)
#define SCU_IGCR1_GEEN2_LEN                  _SCU_UL(1)


#define SCU_IGCR1_IGP2_POS                   _SCU_UL(14)
#define SCU_IGCR1_IGP2_LEN                   _SCU_UL(2)


#define SCU_IGCR1_IPEN30_POS                 _SCU_UL(16)
#define SCU_IGCR1_IPEN30_LEN                 _SCU_UL(1)


#define SCU_IGCR1_IPEN31_POS                 _SCU_UL(17)
#define SCU_IGCR1_IPEN31_LEN                 _SCU_UL(1)


#define SCU_IGCR1_IPEN32_POS                 _SCU_UL(18)
#define SCU_IGCR1_IPEN32_LEN                 _SCU_UL(1)


#define SCU_IGCR1_IPEN33_POS                 _SCU_UL(19)
#define SCU_IGCR1_IPEN33_LEN                 _SCU_UL(1)


#define SCU_IGCR1_GEEN3_POS                  _SCU_UL(29)
#define SCU_IGCR1_GEEN3_LEN                  _SCU_UL(1)


#define SCU_IGCR1_IGP3_POS                   _SCU_UL(30)
#define SCU_IGCR1_IGP3_LEN                   _SCU_UL(2)


#define SCU_IOCR_PC0_POS                     _SCU_UL(4)
#define SCU_IOCR_PC0_LEN                     _SCU_UL(4)


#define SCU_IOCR_PC1_POS                     _SCU_UL(12)
#define SCU_IOCR_PC1_LEN                     _SCU_UL(4)


#define SCU_OUT_P0_POS                       _SCU_UL(0)
#define SCU_OUT_P0_LEN                       _SCU_UL(1)


#define SCU_OUT_P1_POS                       _SCU_UL(1)
#define SCU_OUT_P1_LEN                       _SCU_UL(1)


#define SCU_OMR_PS0_POS                      _SCU_UL(0)
#define SCU_OMR_PS0_LEN                      _SCU_UL(1)


#define SCU_OMR_PS1_POS                      _SCU_UL(1)
#define SCU_OMR_PS1_LEN                      _SCU_UL(1)


#define SCU_OMR_PR0_POS                      _SCU_UL(16)
#define SCU_OMR_PR0_LEN                      _SCU_UL(1)


#define SCU_OMR_PR1_POS                      _SCU_UL(17)
#define SCU_OMR_PR1_LEN                      _SCU_UL(1)


#define SCU_IN_P0_POS                        _SCU_UL(0)
#define SCU_IN_P0_LEN                        _SCU_UL(1)


#define SCU_IN_P1_POS                        _SCU_UL(1)
#define SCU_IN_P1_LEN                        _SCU_UL(1)


#define SCU_PMCSR_REQSLP_POS                 _SCU_UL(0)
#define SCU_PMCSR_REQSLP_LEN                 _SCU_UL(2)


#define SCU_PMCSR_PMST_POS                   _SCU_UL(8)
#define SCU_PMCSR_PMST_LEN                   _SCU_UL(3)


#define SCU_STSTAT_HWCFG_POS                 _SCU_UL(0)
#define SCU_STSTAT_HWCFG_LEN                 _SCU_UL(8)


#define SCU_STSTAT_Mode_POS                  _SCU_UL(15)
#define SCU_STSTAT_Mode_LEN                  _SCU_UL(1)


#define SCU_STSTAT_FCBAE_POS                 _SCU_UL(16)
#define SCU_STSTAT_FCBAE_LEN                 _SCU_UL(1)


#define SCU_STSTAT_LUDIS_POS                 _SCU_UL(17)
#define SCU_STSTAT_LUDIS_LEN                 _SCU_UL(1)


#define SCU_STSTAT_EXTBEN_POS                _SCU_UL(18)
#define SCU_STSTAT_EXTBEN_LEN                _SCU_UL(1)


#define SCU_STSTAT_TRSTL_POS                 _SCU_UL(19)
#define SCU_STSTAT_TRSTL_LEN                 _SCU_UL(1)


#define SCU_STCON_HWCFG_POS                  _SCU_UL(0)
#define SCU_STCON_HWCFG_LEN                  _SCU_UL(8)


#define SCU_STCON_SFCBAE_POS                 _SCU_UL(13)
#define SCU_STCON_SFCBAE_LEN                 _SCU_UL(1)


#define SCU_STCON_CFCBAE_POS                 _SCU_UL(14)
#define SCU_STCON_CFCBAE_LEN                 _SCU_UL(1)


#define SCU_STCON_STP_POS                    _SCU_UL(15)
#define SCU_STCON_STP_LEN                    _SCU_UL(1)


#define SCU_PETCR_PENLDRAM_POS               _SCU_UL(0)
#define SCU_PETCR_PENLDRAM_LEN               _SCU_UL(1)


#define SCU_PETCR_PENDTAG_POS                _SCU_UL(1)
#define SCU_PETCR_PENDTAG_LEN                _SCU_UL(1)


#define SCU_PETCR_PENSPRAM_POS               _SCU_UL(2)
#define SCU_PETCR_PENSPRAM_LEN               _SCU_UL(1)


#define SCU_PETCR_PENPTAG_POS                _SCU_UL(3)
#define SCU_PETCR_PENPTAG_LEN                _SCU_UL(1)


#define SCU_PETCR_PENPMU_POS                 _SCU_UL(4)
#define SCU_PETCR_PENPMU_LEN                 _SCU_UL(1)


#define SCU_PETCR_PENPRAM_POS                _SCU_UL(5)
#define SCU_PETCR_PENPRAM_LEN                _SCU_UL(1)


#define SCU_PETCR_PENCMEM_POS                _SCU_UL(6)
#define SCU_PETCR_PENCMEM_LEN                _SCU_UL(1)


#define SCU_PETCR_PENCAN_POS                 _SCU_UL(7)
#define SCU_PETCR_PENCAN_LEN                 _SCU_UL(1)


#define SCU_PETCR_PENERAY_POS                _SCU_UL(8)
#define SCU_PETCR_PENERAY_LEN                _SCU_UL(1)


#define SCU_PETSR_PFLLDRAM_POS               _SCU_UL(0)
#define SCU_PETSR_PFLLDRAM_LEN               _SCU_UL(1)


#define SCU_PETSR_PFLDTAG_POS                _SCU_UL(1)
#define SCU_PETSR_PFLDTAG_LEN                _SCU_UL(1)


#define SCU_PETSR_PFLSPRAM_POS               _SCU_UL(2)
#define SCU_PETSR_PFLSPRAM_LEN               _SCU_UL(1)


#define SCU_PETSR_PFLPTAG_POS                _SCU_UL(3)
#define SCU_PETSR_PFLPTAG_LEN                _SCU_UL(1)


#define SCU_PETSR_PFLPMU_POS                 _SCU_UL(4)
#define SCU_PETSR_PFLPMU_LEN                 _SCU_UL(1)


#define SCU_PETSR_PFLPRAM_POS                _SCU_UL(5)
#define SCU_PETSR_PFLPRAM_LEN                _SCU_UL(1)


#define SCU_PETSR_PFLCMEM_POS                _SCU_UL(6)
#define SCU_PETSR_PFLCMEM_LEN                _SCU_UL(1)


#define SCU_PETSR_PFLCAN_POS                 _SCU_UL(7)
#define SCU_PETSR_PFLCAN_LEN                 _SCU_UL(1)


#define SCU_PETSR_PFLERAY_POS                _SCU_UL(8)
#define SCU_PETSR_PFLERAY_LEN                _SCU_UL(1)


#define SCU_DTSSTAT_RESULT_POS               _SCU_UL(0)
#define SCU_DTSSTAT_RESULT_LEN               _SCU_UL(10)


#define SCU_DTSSTAT_RDY_POS                  _SCU_UL(14)
#define SCU_DTSSTAT_RDY_LEN                  _SCU_UL(1)


#define SCU_DTSSTAT_BUSY_POS                 _SCU_UL(15)
#define SCU_DTSSTAT_BUSY_LEN                 _SCU_UL(1)


#define SCU_DTSCON_PWD_POS                   _SCU_UL(0)
#define SCU_DTSCON_PWD_LEN                   _SCU_UL(1)


#define SCU_DTSCON_START_POS                 _SCU_UL(1)
#define SCU_DTSCON_START_LEN                 _SCU_UL(1)


#define SCU_WDT_CON0_ENDINIT_POS             _SCU_UL(0)
#define SCU_WDT_CON0_ENDINIT_LEN             _SCU_UL(1)


#define SCU_WDT_CON0_LCK_POS                 _SCU_UL(1)
#define SCU_WDT_CON0_LCK_LEN                 _SCU_UL(1)


#define SCU_WDT_CON0_HPW0_POS                _SCU_UL(2)
#define SCU_WDT_CON0_HPW0_LEN                _SCU_UL(2)


#define SCU_WDT_CON0_HPW1_POS                _SCU_UL(4)
#define SCU_WDT_CON0_HPW1_LEN                _SCU_UL(4)


#define SCU_WDT_CON0_PW_POS                  _SCU_UL(8)
#define SCU_WDT_CON0_PW_LEN                  _SCU_UL(8)


#define SCU_WDT_CON0_REL_POS                 _SCU_UL(16)
#define SCU_WDT_CON0_REL_LEN                 _SCU_UL(16)


#define SCU_WDT_CON1_CLRIRF_POS              _SCU_UL(0)
#define SCU_WDT_CON1_CLRIRF_LEN              _SCU_UL(1)


#define SCU_WDT_CON1_IR_POS                  _SCU_UL(2)
#define SCU_WDT_CON1_IR_LEN                  _SCU_UL(1)


#define SCU_WDT_CON1_DR_POS                  _SCU_UL(3)
#define SCU_WDT_CON1_DR_LEN                  _SCU_UL(1)


#define SCU_WDT_SR_AE_POS                    _SCU_UL(0)
#define SCU_WDT_SR_AE_LEN                    _SCU_UL(1)


#define SCU_WDT_SR_OE_POS                    _SCU_UL(1)
#define SCU_WDT_SR_OE_LEN                    _SCU_UL(1)


#define SCU_WDT_SR_IS_POS                    _SCU_UL(2)
#define SCU_WDT_SR_IS_LEN                    _SCU_UL(1)


#define SCU_WDT_SR_DS_POS                    _SCU_UL(3)
#define SCU_WDT_SR_DS_LEN                    _SCU_UL(1)


#define SCU_WDT_SR_TO_POS                    _SCU_UL(4)
#define SCU_WDT_SR_TO_LEN                    _SCU_UL(1)


#define SCU_WDT_SR_PR_POS                    _SCU_UL(5)
#define SCU_WDT_SR_PR_LEN                    _SCU_UL(1)


#define SCU_WDT_SR_TIM_POS                   _SCU_UL(16)
#define SCU_WDT_SR_TIM_LEN                   _SCU_UL(16)


#define SCU_EMSR_POL_POS                     _SCU_UL(0)
#define SCU_EMSR_POL_LEN                     _SCU_UL(1)


#define SCU_EMSR_MODE_POS                    _SCU_UL(1)
#define SCU_EMSR_MODE_LEN                    _SCU_UL(1)


#define SCU_EMSR_ENON_POS                    _SCU_UL(2)
#define SCU_EMSR_ENON_LEN                    _SCU_UL(1)


#define SCU_EMSR_EMSF_POS                    _SCU_UL(16)
#define SCU_EMSR_EMSF_LEN                    _SCU_UL(1)


#define SCU_EMSR_EMSFM_POS                   _SCU_UL(24)
#define SCU_EMSR_EMSFM_LEN                   _SCU_UL(2)


#define SCU_INTSTAT_WDTI_POS                 _SCU_UL(0)
#define SCU_INTSTAT_WDTI_LEN                 _SCU_UL(1)


#define SCU_INTSTAT_ERUI0_POS                _SCU_UL(1)
#define SCU_INTSTAT_ERUI0_LEN                _SCU_UL(1)


#define SCU_INTSTAT_ERUI1_POS                _SCU_UL(2)
#define SCU_INTSTAT_ERUI1_LEN                _SCU_UL(1)


#define SCU_INTSTAT_ERUI2_POS                _SCU_UL(3)
#define SCU_INTSTAT_ERUI2_LEN                _SCU_UL(1)


#define SCU_INTSTAT_ERUI3_POS                _SCU_UL(4)
#define SCU_INTSTAT_ERUI3_LEN                _SCU_UL(1)


#define SCU_INTSTAT_FL0I_POS                 _SCU_UL(5)
#define SCU_INTSTAT_FL0I_LEN                 _SCU_UL(1)


#define SCU_INTSTAT_FL1I_POS                 _SCU_UL(6)
#define SCU_INTSTAT_FL1I_LEN                 _SCU_UL(1)


#define SCU_INTSTAT_DTSI_POS                 _SCU_UL(7)
#define SCU_INTSTAT_DTSI_LEN                 _SCU_UL(1)


#define SCU_INTSET_WDTI_POS                  _SCU_UL(0)
#define SCU_INTSET_WDTI_LEN                  _SCU_UL(1)


#define SCU_INTSET_ERUI0_POS                 _SCU_UL(1)
#define SCU_INTSET_ERUI0_LEN                 _SCU_UL(1)


#define SCU_INTSET_ERUI1_POS                 _SCU_UL(2)
#define SCU_INTSET_ERUI1_LEN                 _SCU_UL(1)


#define SCU_INTSET_ERUI2_POS                 _SCU_UL(3)
#define SCU_INTSET_ERUI2_LEN                 _SCU_UL(1)


#define SCU_INTSET_ERUI3_POS                 _SCU_UL(4)
#define SCU_INTSET_ERUI3_LEN                 _SCU_UL(1)


#define SCU_INTSET_FL0I_POS                  _SCU_UL(5)
#define SCU_INTSET_FL0I_LEN                  _SCU_UL(1)


#define SCU_INTSET_FL1I_POS                  _SCU_UL(6)
#define SCU_INTSET_FL1I_LEN                  _SCU_UL(1)


#define SCU_INTSET_DTSI_POS                  _SCU_UL(7)
#define SCU_INTSET_DTSI_LEN                  _SCU_UL(1)


#define SCU_INTCLR_WDTI_POS                  _SCU_UL(0)
#define SCU_INTCLR_WDTI_LEN                  _SCU_UL(1)


#define SCU_INTCLR_ERUI0_POS                 _SCU_UL(1)
#define SCU_INTCLR_ERUI0_LEN                 _SCU_UL(1)


#define SCU_INTCLR_ERUI1_POS                 _SCU_UL(2)
#define SCU_INTCLR_ERUI1_LEN                 _SCU_UL(1)


#define SCU_INTCLR_ERUI2_POS                 _SCU_UL(3)
#define SCU_INTCLR_ERUI2_LEN                 _SCU_UL(1)


#define SCU_INTCLR_ERUI3_POS                 _SCU_UL(4)
#define SCU_INTCLR_ERUI3_LEN                 _SCU_UL(1)


#define SCU_INTCLR_FL0I_POS                  _SCU_UL(5)
#define SCU_INTCLR_FL0I_LEN                  _SCU_UL(1)


#define SCU_INTCLR_FL1I_POS                  _SCU_UL(6)
#define SCU_INTCLR_FL1I_LEN                  _SCU_UL(1)


#define SCU_INTCLR_DTSI_POS                  _SCU_UL(7)
#define SCU_INTCLR_DTSI_LEN                  _SCU_UL(1)


#define SCU_INTDIS_WDTI_POS                  _SCU_UL(0)
#define SCU_INTDIS_WDTI_LEN                  _SCU_UL(1)


#define SCU_INTDIS_ERUI0_POS                 _SCU_UL(1)
#define SCU_INTDIS_ERUI0_LEN                 _SCU_UL(1)


#define SCU_INTDIS_ERUI1_POS                 _SCU_UL(2)
#define SCU_INTDIS_ERUI1_LEN                 _SCU_UL(1)


#define SCU_INTDIS_ERUI2_POS                 _SCU_UL(3)
#define SCU_INTDIS_ERUI2_LEN                 _SCU_UL(1)


#define SCU_INTDIS_ERUI3_POS                 _SCU_UL(4)
#define SCU_INTDIS_ERUI3_LEN                 _SCU_UL(1)


#define SCU_INTDIS_FL0I_POS                  _SCU_UL(5)
#define SCU_INTDIS_FL0I_LEN                  _SCU_UL(1)


#define SCU_INTDIS_FL1I_POS                  _SCU_UL(6)
#define SCU_INTDIS_FL1I_LEN                  _SCU_UL(1)


#define SCU_INTDIS_DTSI_POS                  _SCU_UL(7)
#define SCU_INTDIS_DTSI_LEN                  _SCU_UL(1)


#define SCU_INTNP_WDT_POS                    _SCU_UL(0)
#define SCU_INTNP_WDT_LEN                    _SCU_UL(2)


#define SCU_INTNP_ERU0_POS                   _SCU_UL(2)
#define SCU_INTNP_ERU0_LEN                   _SCU_UL(2)


#define SCU_INTNP_ERU1_POS                   _SCU_UL(4)
#define SCU_INTNP_ERU1_LEN                   _SCU_UL(2)


#define SCU_INTNP_ERU2_POS                   _SCU_UL(6)
#define SCU_INTNP_ERU2_LEN                   _SCU_UL(2)


#define SCU_INTNP_ERU3_POS                   _SCU_UL(8)
#define SCU_INTNP_ERU3_LEN                   _SCU_UL(2)


#define SCU_INTNP_FL0_POS                    _SCU_UL(10)
#define SCU_INTNP_FL0_LEN                    _SCU_UL(2)


#define SCU_INTNP_FL1_POS                    _SCU_UL(12)
#define SCU_INTNP_FL1_LEN                    _SCU_UL(2)


#define SCU_INTNP_DTS_POS                    _SCU_UL(14)
#define SCU_INTNP_DTS_LEN                    _SCU_UL(2)


#define SCU_TRAPSTAT_ESR0T_POS               _SCU_UL(0)
#define SCU_TRAPSTAT_ESR0T_LEN               _SCU_UL(1)


#define SCU_TRAPSTAT_ESR1T_POS               _SCU_UL(1)
#define SCU_TRAPSTAT_ESR1T_LEN               _SCU_UL(1)


#define SCU_TRAPSTAT_WDTT_POS                _SCU_UL(3)
#define SCU_TRAPSTAT_WDTT_LEN                _SCU_UL(1)


#define SCU_TRAPSTAT_PET_POS                 _SCU_UL(4)
#define SCU_TRAPSTAT_PET_LEN                 _SCU_UL(1)


#define SCU_TRAPSTAT_OSCLWDTT_POS            _SCU_UL(5)
#define SCU_TRAPSTAT_OSCLWDTT_LEN            _SCU_UL(1)


#define SCU_TRAPSTAT_OSCHWDTT_POS            _SCU_UL(6)
#define SCU_TRAPSTAT_OSCHWDTT_LEN            _SCU_UL(1)


#define SCU_TRAPSTAT_OSCSPWDTT_POS           _SCU_UL(7)
#define SCU_TRAPSTAT_OSCSPWDTT_LEN           _SCU_UL(1)


#define SCU_TRAPSTAT_SYSVCOLCKT_POS          _SCU_UL(8)
#define SCU_TRAPSTAT_SYSVCOLCKT_LEN          _SCU_UL(1)


#define SCU_TRAPSTAT_ERAYVCOLCKT_POS         _SCU_UL(9)
#define SCU_TRAPSTAT_ERAYVCOLCKT_LEN         _SCU_UL(1)


#define SCU_TRAPSET_ESR0T_POS                _SCU_UL(0)
#define SCU_TRAPSET_ESR0T_LEN                _SCU_UL(1)


#define SCU_TRAPSET_ESR1T_POS                _SCU_UL(1)
#define SCU_TRAPSET_ESR1T_LEN                _SCU_UL(1)


#define SCU_TRAPSET_WDTT_POS                 _SCU_UL(3)
#define SCU_TRAPSET_WDTT_LEN                 _SCU_UL(1)


#define SCU_TRAPSET_PET_POS                  _SCU_UL(4)
#define SCU_TRAPSET_PET_LEN                  _SCU_UL(1)


#define SCU_TRAPSET_OSCLWDTT_POS             _SCU_UL(5)
#define SCU_TRAPSET_OSCLWDTT_LEN             _SCU_UL(1)


#define SCU_TRAPSET_OSCHWDTT_POS             _SCU_UL(6)
#define SCU_TRAPSET_OSCHWDTT_LEN             _SCU_UL(1)


#define SCU_TRAPSET_OSCSPWDTT_POS            _SCU_UL(7)
#define SCU_TRAPSET_OSCSPWDTT_LEN            _SCU_UL(1)


#define SCU_TRAPSET_SYSVCOLCKT_POS           _SCU_UL(8)
#define SCU_TRAPSET_SYSVCOLCKT_LEN           _SCU_UL(1)


#define SCU_TRAPSET_ERAYVCOLCKT_POS          _SCU_UL(9)
#define SCU_TRAPSET_ERAYVCOLCKT_LEN          _SCU_UL(1)


#define SCU_TRAPCLR_ESR0T_POS                _SCU_UL(0)
#define SCU_TRAPCLR_ESR0T_LEN                _SCU_UL(1)


#define SCU_TRAPCLR_ESR1T_POS                _SCU_UL(1)
#define SCU_TRAPCLR_ESR1T_LEN                _SCU_UL(1)


#define SCU_TRAPCLR_WDTT_POS                 _SCU_UL(3)
#define SCU_TRAPCLR_WDTT_LEN                 _SCU_UL(1)


#define SCU_TRAPCLR_PET_POS                  _SCU_UL(4)
#define SCU_TRAPCLR_PET_LEN                  _SCU_UL(1)


#define SCU_TRAPCLR_OSCLWDTT_POS             _SCU_UL(5)
#define SCU_TRAPCLR_OSCLWDTT_LEN             _SCU_UL(1)


#define SCU_TRAPCLR_OSCHWDTT_POS             _SCU_UL(6)
#define SCU_TRAPCLR_OSCHWDTT_LEN             _SCU_UL(1)


#define SCU_TRAPCLR_OSCSPWDTT_POS            _SCU_UL(7)
#define SCU_TRAPCLR_OSCSPWDTT_LEN            _SCU_UL(1)


#define SCU_TRAPCLR_SYSVCOLCKT_POS           _SCU_UL(8)
#define SCU_TRAPCLR_SYSVCOLCKT_LEN           _SCU_UL(1)


#define SCU_TRAPCLR_ERAYVCOLCKT_POS          _SCU_UL(9)
#define SCU_TRAPCLR_ERAYVCOLCKT_LEN          _SCU_UL(1)


#define SCU_TRAPDIS_ESR0T_POS                _SCU_UL(0)
#define SCU_TRAPDIS_ESR0T_LEN                _SCU_UL(1)


#define SCU_TRAPDIS_ESR1T_POS                _SCU_UL(1)
#define SCU_TRAPDIS_ESR1T_LEN                _SCU_UL(1)


#define SCU_TRAPDIS_WDTT_POS                 _SCU_UL(3)
#define SCU_TRAPDIS_WDTT_LEN                 _SCU_UL(1)


#define SCU_TRAPDIS_PET_POS                  _SCU_UL(4)
#define SCU_TRAPDIS_PET_LEN                  _SCU_UL(1)


#define SCU_TRAPDIS_OSCLWDTT_POS             _SCU_UL(5)
#define SCU_TRAPDIS_OSCLWDTT_LEN             _SCU_UL(1)


#define SCU_TRAPDIS_OSCHWDTT_POS             _SCU_UL(6)
#define SCU_TRAPDIS_OSCHWDTT_LEN             _SCU_UL(1)


#define SCU_TRAPDIS_OSCSPWDTT_POS            _SCU_UL(7)
#define SCU_TRAPDIS_OSCSPWDTT_LEN            _SCU_UL(1)


#define SCU_TRAPDIS_SYSVCOLCKT_POS           _SCU_UL(8)
#define SCU_TRAPDIS_SYSVCOLCKT_LEN           _SCU_UL(1)


#define SCU_TRAPDIS_ERAYVCOLCKT_POS          _SCU_UL(9)
#define SCU_TRAPDIS_ERAYVCOLCKT_LEN          _SCU_UL(1)


#define SCU_CHIPID_CHREV_POS                 _SCU_UL(0)
#define SCU_CHIPID_CHREV_LEN                 _SCU_UL(8)


#define SCU_CHIPID_CHID_POS                  _SCU_UL(8)
#define SCU_CHIPID_CHID_LEN                  _SCU_UL(8)


#define SCU_CHIPID_EEA_POS                   _SCU_UL(16)
#define SCU_CHIPID_EEA_LEN                   _SCU_UL(1)


#define SCU_MANID_DEPT_POS                   _SCU_UL(0)
#define SCU_MANID_DEPT_LEN                   _SCU_UL(5)


#define SCU_MANID_MANUF_POS                  _SCU_UL(5)
#define SCU_MANID_MANUF_LEN                  _SCU_UL(11)


#define SCU_SRC3_SRPN_POS                    _SCU_UL(0)
#define SCU_SRC3_SRPN_LEN                    _SCU_UL(8)


#define SCU_SRC3_TOS_POS                     _SCU_UL(10)
#define SCU_SRC3_TOS_LEN                     _SCU_UL(1)


#define SCU_SRC3_SRE_POS                     _SCU_UL(12)
#define SCU_SRC3_SRE_LEN                     _SCU_UL(1)


#define SCU_SRC3_SRR_POS                     _SCU_UL(13)
#define SCU_SRC3_SRR_LEN                     _SCU_UL(1)


#define SCU_SRC3_CLRR_POS                    _SCU_UL(14)
#define SCU_SRC3_CLRR_LEN                    _SCU_UL(1)


#define SCU_SRC3_SETR_POS                    _SCU_UL(15)
#define SCU_SRC3_SETR_LEN                    _SCU_UL(1)


#define SCU_SRC2_SRPN_POS                    _SCU_UL(0)
#define SCU_SRC2_SRPN_LEN                    _SCU_UL(8)


#define SCU_SRC2_TOS_POS                     _SCU_UL(10)
#define SCU_SRC2_TOS_LEN                     _SCU_UL(1)


#define SCU_SRC2_SRE_POS                     _SCU_UL(12)
#define SCU_SRC2_SRE_LEN                     _SCU_UL(1)


#define SCU_SRC2_SRR_POS                     _SCU_UL(13)
#define SCU_SRC2_SRR_LEN                     _SCU_UL(1)


#define SCU_SRC2_CLRR_POS                    _SCU_UL(14)
#define SCU_SRC2_CLRR_LEN                    _SCU_UL(1)


#define SCU_SRC2_SETR_POS                    _SCU_UL(15)
#define SCU_SRC2_SETR_LEN                    _SCU_UL(1)


#define SCU_SRC1_SRPN_POS                    _SCU_UL(0)
#define SCU_SRC1_SRPN_LEN                    _SCU_UL(8)


#define SCU_SRC1_TOS_POS                     _SCU_UL(10)
#define SCU_SRC1_TOS_LEN                     _SCU_UL(1)


#define SCU_SRC1_SRE_POS                     _SCU_UL(12)
#define SCU_SRC1_SRE_LEN                     _SCU_UL(1)


#define SCU_SRC1_SRR_POS                     _SCU_UL(13)
#define SCU_SRC1_SRR_LEN                     _SCU_UL(1)


#define SCU_SRC1_CLRR_POS                    _SCU_UL(14)
#define SCU_SRC1_CLRR_LEN                    _SCU_UL(1)


#define SCU_SRC1_SETR_POS                    _SCU_UL(15)
#define SCU_SRC1_SETR_LEN                    _SCU_UL(1)


#define SCU_SRC0_SRPN_POS                    _SCU_UL(0)
#define SCU_SRC0_SRPN_LEN                    _SCU_UL(8)


#define SCU_SRC0_TOS_POS                     _SCU_UL(10)
#define SCU_SRC0_TOS_LEN                     _SCU_UL(1)


#define SCU_SRC0_SRE_POS                     _SCU_UL(12)
#define SCU_SRC0_SRE_LEN                     _SCU_UL(1)


#define SCU_SRC0_SRR_POS                     _SCU_UL(13)
#define SCU_SRC0_SRR_LEN                     _SCU_UL(1)


#define SCU_SRC0_CLRR_POS                    _SCU_UL(14)
#define SCU_SRC0_CLRR_LEN                    _SCU_UL(1)


#define SCU_SRC0_SETR_POS                    _SCU_UL(15)
#define SCU_SRC0_SETR_LEN                    _SCU_UL(1)

#endif
