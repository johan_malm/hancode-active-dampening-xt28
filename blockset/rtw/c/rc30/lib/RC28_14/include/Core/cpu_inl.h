

#ifndef _CPU_INL_H
#define _CPU_INL_H








extern bool Cpu_stWDTDisabled_b;



CPU_INLINE_PROTOTYPE void   Cpu_UnlockWDTCON0(void);
CPU_INLINE_PROTOTYPE void   Cpu_ModifyWDTCON0(uint32, uint32);
CPU_INLINE_PROTOTYPE uint64 Cpu_GetSysTime(void);
CPU_INLINE_PROTOTYPE void   Cpu_ModifyProtSFR(uint32, uint32, uint32);
CPU_INLINE_PROTOTYPE uint32 Cpu_GetCPUPrio(void);
CPU_INLINE_PROTOTYPE void   Cpu_SetCPUPrio(uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemCopy64(uint64*, const uint64*, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemCopy32(uint32*, const uint32*, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemCopy16(uint16*, const uint16*, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemCopy8(uint8*, const uint8*, uint32);
CPU_INLINE_PROTOTYPE uint32 Cpu_MemCompare64(const uint64*, const uint64*, uint32);
CPU_INLINE_PROTOTYPE uint32 Cpu_MemCompare32(const uint32*, const uint32*, uint32);
CPU_INLINE_PROTOTYPE uint32 Cpu_MemCompare16(const uint16*, const uint16*, uint32);
CPU_INLINE_PROTOTYPE uint32 Cpu_MemCompare8(const uint8*, const uint8*, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemSet64(uint64*, uint64, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemSet32(uint32*, uint32, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemSet16(uint16*, uint32, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_MemSet8(uint8*, uint32, uint32);
CPU_INLINE_PROTOTYPE void   Cpu_SwapBytes16(uint8*);
CPU_INLINE_PROTOTYPE void   Cpu_SwapBytes32(uint8*);
CPU_INLINE_PROTOTYPE void   Cpu_Wait(uint32);
CPU_INLINE_PROTOTYPE uint32 Cpu_GetSysClock(void);





CPU_INLINE void Cpu_UnlockWDTCON0(void)
{
    uint32          xPasswd_u32;
    volatile uint32 xReadBack_u32;

    xPasswd_u32 = SCU.WDT_CON0;        
    xPasswd_u32 = xPasswd_u32 | 0x0F0UL;  

    
    xPasswd_u32 = (SCU.WDT_CON1 & 0x00CUL) | (xPasswd_u32 & 0xFFFFFFF3UL);

    
    xPasswd_u32 = (xPasswd_u32 ^ 0x00000002UL);
    SCU.WDT_CON0 = xPasswd_u32;

    
    
    xReadBack_u32 = SCU.WDT_CON0;
}



CPU_INLINE void Cpu_ModifyWDTCON0(uint32 xValue_u32, uint32 xMask_u32)
{
    uint32          xNewVal_u32;
    volatile uint32 xReadBack_u32;

    
    xNewVal_u32 = SCU.WDT_CON0 & ~xMask_u32;
    xNewVal_u32 = xNewVal_u32 | (xValue_u32 & xMask_u32);

    
    xNewVal_u32 = ((0xF2UL | xNewVal_u32) & (0xFFFFFFF3UL));

    
    SCU.WDT_CON0 = xNewVal_u32;

    
    
    xReadBack_u32 = SCU.WDT_CON0;
}



CPU_INLINE uint64 Cpu_GetSysTime(void)
{
    uint64 tiSys_u64;

    
    Cpu_SuspendInt();
    
    tiSys_u64 = (uint64)STM.TIM0;
    asm volatile ("" ::: "memory");
    tiSys_u64 |= ((uint64)STM.CAP << 32UL);
    Cpu_ResumeInt();

    return (tiSys_u64);
}



CPU_INLINE void Cpu_ModifyProtSFR(uint32 xReg_u32, uint32 xMask_u32, uint32 xVal_u32)
{
    
    Cpu_ResetEndinit();

    
    xReg_u32 = (xReg_u32 &~ xMask_u32) | (xVal_u32 & xMask_u32);

    
    Cpu_SetEndinit();
}



CPU_INLINE uint32 Cpu_GetCPUPrio(void)
{
    uint32 xICR_u32;

    
    
    asm volatile ("mfcr %0, $icr":"=d" (xICR_u32) : );

    
    return (xICR_u32 & 0x000000FFUL);
}



CPU_INLINE void Cpu_SetCPUPrio(uint32 numPrio_u32)
{
    uint32 xTemp_u32;

    
    
    asm volatile ("mfcr %0, $icr":"=d" (xTemp_u32) : );

    
    
    xTemp_u32 = (xTemp_u32 & 0xFFFFFF00UL) | (numPrio_u32 & 0x000000FFUL);

    
    
    asm volatile ("mtcr $icr, %0": :"d" (xTemp_u32):"memory");
    asm volatile ("isync");
}


CPU_INLINE void Cpu_MemCopy64(uint64* xDest_pu64, const uint64* xSrc_pcu64, uint32 numBytes_u32)
{
    uint64 xTemp_u64;
    uint8* ctLoop_pu8;

    if (numBytes_u32 > 7UL)
    {
        ctLoop_pu8 = (uint8*)((numBytes_u32 / 8UL) - 1UL);

        
        asm volatile(
            "                   \n\
        1:                      \n\
            ld.d   %A0,[%2+]8   \n\
            st.d   [%1+]8,%A0   \n\
            loop   %3,1b        \n\
            "
            : "=d"(xTemp_u64), "+a"(xDest_pu64), "+a"(xSrc_pcu64), "+a"(ctLoop_pu8): : "memory");
        
    }

    return;
}


CPU_INLINE void Cpu_MemCopy32(uint32* xDest_pu32, const uint32* xSrc_pcu32, uint32 numBytes_u32)
{
    uint32 xTemp_u32;
    uint8* ctLoop_pu8;

    if (numBytes_u32 > 3UL)
    {
        ctLoop_pu8 = (uint8*)((numBytes_u32 / 4UL) - 1UL);

        
        asm volatile(
            "                   \n\
        1:                      \n\
            ld.w   %0,[%2+]4    \n\
            st.w   [%1+]4,%0    \n\
            loop   %3,1b        \n\
            "
            : "=d"(xTemp_u32), "+a"(xDest_pu32), "+a"(xSrc_pcu32), "+a"(ctLoop_pu8): : "memory");
        
    }

    return;
}


CPU_INLINE void Cpu_MemCopy16(uint16* xDest_pu16, const uint16* xSrc_pcu16, uint32 numBytes_u32)
{
    uint32 xTemp_u32;
    uint8* ctLoop_pu8;

    if (numBytes_u32 > 1UL)
    {
        ctLoop_pu8 = (uint8*)((numBytes_u32 / 2UL) - 1UL);

        
        asm volatile(
            "                   \n\
        1:                      \n\
            ld.hu  %0,[%2+]2    \n\
            st.h   [%1+]2,%0    \n\
            loop   %3,1b        \n\
            "
            : "=d"(xTemp_u32), "+a"(xDest_pu16), "+a"(xSrc_pcu16), "+a"(ctLoop_pu8): : "memory");
        
    }

    return;
}


CPU_INLINE void Cpu_MemCopy8(uint8* xDest_pu8, const uint8* xSrc_pcu8, uint32 numBytes_u32)
{
    uint32 xTemp_u32;
    uint8* ctLoop_pu8;

    if (numBytes_u32 > 0UL)
    {
        ctLoop_pu8 = (uint8*)(numBytes_u32 - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            ld.bu  %0,[%2+]1    \n\
            st.b   [%1+]1,%0    \n\
            loop   %3,1b        \n\
        "
        : "=d"(xTemp_u32), "+a"(xDest_pu8), "+a"(xSrc_pcu8), "+a"(ctLoop_pu8): : "memory");
        
    }

    return;
}


CPU_INLINE uint32 Cpu_MemCompare64(const uint64* xSrc1_pcu64, const uint64* xSrc2_pcu64,
                                   uint32 numBytes_u32)
{
    uint32 stEqual_u32;
    uint64 xTemp1_u64;
    uint64 xTemp2_u64;
    uint8* ctLoop_pu8;

    
    stEqual_u32 = 1UL;

    if (numBytes_u32 > 7UL)
    {
        ctLoop_pu8 = (uint8*)((numBytes_u32 / 8UL) - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            ld.d   %A4,[%1+]8   \n\
            ld.d   %A5,[%2+]8   \n\
            jne    %L4,%L5,2f   \n\
            jne    %H4,%H5,2f   \n\
            nop    # CPU_TC.070 workaround \n\
            nop    # CPU_TC.070 workaround \n\
            loop   %3,1b        \n\
            mov    %0,0         \n\
        2:                      \n\
        "
        : "+d"(stEqual_u32), "+a"(xSrc1_pcu64), "+a"(xSrc2_pcu64),
        
        "+a"(ctLoop_pu8),  "=d"(xTemp1_u64), "=d"(xTemp2_u64): : "memory");
        
    }

    return stEqual_u32;
}


CPU_INLINE uint32 Cpu_MemCompare32(const uint32* xSrc1_pcu32, const uint32* xSrc2_pcu32,
                                   uint32 numBytes_u32)
{
    uint32 stEqual_u32;
    uint32 xTemp1_u32;
    uint32 xTemp2_u32;
    uint8* ctLoop_pu8;

    
    stEqual_u32 = 1UL;

    if (numBytes_u32 > 3UL)
    {
        ctLoop_pu8 = (uint8*)((numBytes_u32 / 4UL) - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            ld.w   %4,[%1+]4    \n\
            ld.w   %5,[%2+]4    \n\
            jne    %4,%5,2f     \n\
            nop    # CPU_TC.070 workaround \n\
            nop    # CPU_TC.070 workaround \n\
            loop   %3,1b        \n\
            mov    %0,0         \n\
        2:                      \n\
        "
        : "+d"(stEqual_u32), "+a"(xSrc1_pcu32), "+a"(xSrc2_pcu32),
        
        "+a"(ctLoop_pu8),  "=d"(xTemp1_u32), "=d"(xTemp2_u32): : "memory");
        
    }

    return stEqual_u32;
}


CPU_INLINE uint32 Cpu_MemCompare16(const uint16* xSrc1_pcu16, const uint16* xSrc2_pcu16,
                                   uint32 numBytes_u32)
{
    uint32 stEqual_u32;
    uint32 xTemp1_u32;
    uint32 xTemp2_u32;
    uint8* ctLoop_pu8;

    
    stEqual_u32 = 1UL;

    if (numBytes_u32 > 1UL)
    {
        ctLoop_pu8 = (uint8*)((numBytes_u32 / 2UL) - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            ld.hu  %4,[%1+]2    \n\
            ld.hu  %5,[%2+]2    \n\
            jne    %4,%5,2f     \n\
            nop    # CPU_TC.070 workaround \n\
            nop    # CPU_TC.070 workaround \n\
            loop   %3,1b        \n\
            mov    %0,0         \n\
        2:                      \n\
        "
        : "+d"(stEqual_u32), "+a"(xSrc1_pcu16), "+a"(xSrc2_pcu16),
        
        "+a"(ctLoop_pu8),  "=d"(xTemp1_u32), "=d"(xTemp2_u32): : "memory");
        
    }

    return stEqual_u32;
}


CPU_INLINE uint32 Cpu_MemCompare8(const uint8* xSrc1_pcu8, const uint8* xSrc2_pcu8,
                                  uint32 numBytes_u32)
{
    uint32 stEqual_u32;
    uint32 xTemp1_u32;
    uint32 xTemp2_u32;
    uint8* ctLoop_pu8;

    
    stEqual_u32 = 1UL;

    if (numBytes_u32 > 0UL)
    {
        ctLoop_pu8 = (uint8*)(numBytes_u32 - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            ld.bu  %4,[%1+]1    \n\
            ld.bu  %5,[%2+]1    \n\
            jne    %4,%5,2f     \n\
            nop    # CPU_TC.070 workaround \n\
            nop    # CPU_TC.070 workaround \n\
            loop   %3,1b        \n\
            mov    %0,0         \n\
        2:                      \n\
        "
        : "+d"(stEqual_u32), "+a"(xSrc1_pcu8), "+a"(xSrc2_pcu8),
        
        "+a"(ctLoop_pu8), "=d"(xTemp1_u32), "=d"(xTemp2_u32): : "memory");
        
    }

    return stEqual_u32;
}


CPU_INLINE void Cpu_MemSet64(uint64* xDest_pu64, uint64 xPattern_u64, uint32 numBytes_u32)
{
    uint8* ct_pu8;

    if (numBytes_u32 > 7UL)
    {
        ct_pu8      = (uint8*)(((numBytes_u32) / 8UL) - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            st.d   [%0+]8,%A2   \n\
            loop   %1,1b        \n\
        "
        : "+a"(xDest_pu64), "+a"(ct_pu8) : "d"(xPattern_u64): "memory");
        
    }

    return;
}


CPU_INLINE void Cpu_MemSet32(uint32* xDest_pu32, uint32 xPattern_u32, uint32 numBytes_u32)
{
    uint8* ct_pu8;

    if (numBytes_u32 > 3UL)
    {
        ct_pu8      = (uint8*)(((numBytes_u32) / 4UL) - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            st.w   [%0+]4,%2    \n\
            loop   %1,1b        \n\
        "
        : "+a"(xDest_pu32), "+a"(ct_pu8) : "d"(xPattern_u32): "memory");
        
    }

    return;
}


CPU_INLINE void Cpu_MemSet16(uint16* xDest_pu16, uint32 xPattern_u32, uint32 numBytes_u32)
{
    uint8* ct_pu8;

    if (numBytes_u32 > 1UL)
    {
        ct_pu8      = (uint8*)(((numBytes_u32) / 2UL) - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            st.h   [%0+]2,%2    \n\
            loop   %1,1b        \n\
        "
        : "+a"(xDest_pu16), "+a"(ct_pu8) : "d"(xPattern_u32): "memory");
        
    }

    return;
}


CPU_INLINE void Cpu_MemSet8(uint8* xDest_pu8, uint32 xPattern_u32, uint32 numBytes_u32)
{
    uint8* ct_pu8;

    if (numBytes_u32 > 0UL)
    {
        ct_pu8      = (uint8*)(numBytes_u32 - 1UL);

        
        asm volatile(
        "                       \n\
        1:                      \n\
            st.b   [%0+]1,%2    \n\
            loop   %1,1b        \n\
        "
        : "+a"(xDest_pu8), "+a"(ct_pu8) :  "d"(xPattern_u32): "memory");
        
    }

    return;
}



CPU_INLINE void Cpu_SwapBytes16(uint8 *xValue_pu8)
{
    uint8 temp_u8;

    
    temp_u8 = xValue_pu8[0];
    xValue_pu8[0] = xValue_pu8[1];
    xValue_pu8[1] = temp_u8;
}




CPU_INLINE void Cpu_SwapBytes32(uint8 *xValue_pu8)
{
    uint8 temp_u8;

    
    temp_u8 = xValue_pu8[0];
    xValue_pu8[0] = xValue_pu8[3];
    xValue_pu8[3] = temp_u8;
    
    temp_u8 = xValue_pu8[1];
    xValue_pu8[1] = xValue_pu8[2];
    xValue_pu8[2] = temp_u8;
}



CPU_INLINE void Cpu_Wait(uint32 numCycles_u32)
{
    uint8* ct_pu8;

    
    ct_pu8      = (uint8*)numCycles_u32;

    
    asm volatile(
    "                       \n\
    1:                      \n\
        nop                 \n\
        loop   %0,1b        \n\
    "
    : "+a"(ct_pu8) : : "memory");

    return;
}



CPU_INLINE uint32 Cpu_GetSysClock(void)
{
    return(MACHINE_TICKS_PER_US * 1000UL);
}


#endif
