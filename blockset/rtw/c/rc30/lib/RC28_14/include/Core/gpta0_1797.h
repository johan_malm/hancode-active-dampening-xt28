

#ifndef _GPTA0_1797_H
#define _GPTA0_1797_H


#ifdef REGDEF_FOR_PCP
    #define _GPTA0_UL(x) x
#else
    #define _GPTA0_UL(x) x##UL
#endif




typedef struct {
    volatile uint32 LTCCTR;        
    volatile uint32 LTCXR;         
} GPTA0_LTC_t;


typedef struct {
    volatile uint32 GTCCTR;        
    volatile uint32 GTCXR;         
} GPTA0_GTC_t;


typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 DBGCTR;              
    volatile uint32 ID;                  
    volatile uint32 FDR;                 
    volatile uint32 SRSC0;               
    volatile uint32 SRSS0;               
    volatile uint32 SRSC1;               
    volatile uint32 SRSS1;               
    volatile uint32 SRSC2;               
    volatile uint32 SRSS2;               
    volatile uint32 SRSC3;               
    volatile uint32 SRSS3;               
    volatile uint32 SRNR;                
    volatile uint32 RESERVED0[1];        
    volatile uint32 MRACTL;              
    volatile uint32 MRADIN;              
    volatile uint32 MRADOUT;             
    volatile uint32 FPCSTAT;             
    volatile uint32 FPCCTR0;             
    volatile uint32 FPCTIM0;             
    volatile uint32 FPCCTR1;             
    volatile uint32 FPCTIM1;             
    volatile uint32 FPCCTR2;             
    volatile uint32 FPCTIM2;             
    volatile uint32 FPCCTR3;             
    volatile uint32 FPCTIM3;             
    volatile uint32 FPCCTR4;             
    volatile uint32 FPCTIM4;             
    volatile uint32 FPCCTR5;             
    volatile uint32 FPCTIM5;             
    volatile uint32 PDLCTR;              
    volatile uint32 RESERVED1[1];        
    volatile uint32 DCMCTR0;             
    volatile uint32 DCMTIM0;             
    volatile uint32 DCMCAV0;             
    volatile uint32 DCMCOV0;             
    volatile uint32 DCMCTR1;             
    volatile uint32 DCMTIM1;             
    volatile uint32 DCMCAV1;             
    volatile uint32 DCMCOV1;             
    volatile uint32 DCMCTR2;             
    volatile uint32 DCMTIM2;             
    volatile uint32 DCMCAV2;             
    volatile uint32 DCMCOV2;             
    volatile uint32 DCMCTR3;             
    volatile uint32 DCMTIM3;             
    volatile uint32 DCMCAV3;             
    volatile uint32 DCMCOV3;             
    volatile uint32 PLLCTR;              
    volatile uint32 PLLMTI;              
    volatile uint32 PLLCNT;              
    volatile uint32 PLLSTP;              
    volatile uint32 PLLREV;              
    volatile uint32 PLLDTR;              
    volatile uint32 CKBCTR;              
    volatile uint32 RESERVED2[1];        
    volatile uint32 GTCTR0;              
    volatile uint32 GTREV0;              
    volatile uint32 GTTIM0;              
    volatile uint32 RESERVED3[1];        
    volatile uint32 GTCTR1;              
    volatile uint32 GTREV1;              
    volatile uint32 GTTIM1;              
    volatile uint32 RESERVED4[1];        
    GPTA0_GTC_t     GTC[32];             
    GPTA0_LTC_t     LTC[64];             
    volatile uint32 EDCTR;               
    volatile uint32 RESERVED5[191];      
    volatile uint32 MMXCTR00;            
    volatile uint32 MMXCTR01;            
    volatile uint32 MMXCTR10;            
    volatile uint32 MMXCTR11;            
    volatile uint32 RESERVED6[22];       
    volatile uint32 SRC37;               
    volatile uint32 SRC36;               
    volatile uint32 SRC35;               
    volatile uint32 SRC34;               
    volatile uint32 SRC33;               
    volatile uint32 SRC32;               
    volatile uint32 SRC31;               
    volatile uint32 SRC30;               
    volatile uint32 SRC29;               
    volatile uint32 SRC28;               
    volatile uint32 SRC27;               
    volatile uint32 SRC26;               
    volatile uint32 SRC25;               
    volatile uint32 SRC24;               
    volatile uint32 SRC23;               
    volatile uint32 SRC22;               
    volatile uint32 SRC21;               
    volatile uint32 SRC20;               
    volatile uint32 SRC19;               
    volatile uint32 SRC18;               
    volatile uint32 SRC17;               
    volatile uint32 SRC16;               
    volatile uint32 SRC15;               
    volatile uint32 SRC14;               
    volatile uint32 SRC13;               
    volatile uint32 SRC12;               
    volatile uint32 SRC11;               
    volatile uint32 SRC10;               
    volatile uint32 SRC09;               
    volatile uint32 SRC08;               
    volatile uint32 SRC07;               
    volatile uint32 SRC06;               
    volatile uint32 SRC05;               
    volatile uint32 SRC04;               
    volatile uint32 SRC03;               
    volatile uint32 SRC02;               
    volatile uint32 SRC01;               
    volatile uint32 SRC00;               
} GPTA0_RegMap_t;




extern GPTA0_RegMap_t GPTA0 __attribute__ ((asection (".zbss.label_only","f=awz")));





#define GPTA0_ID_MOD_NUMBER_POS              _GPTA0_UL(16)
#define GPTA0_ID_MOD_NUMBER_LEN              _GPTA0_UL(16)


#define GPTA0_ID_MOD_REV_POS                 _GPTA0_UL(0)
#define GPTA0_ID_MOD_REV_LEN                 _GPTA0_UL(8)


#define GPTA0_ID_MOD_TYPE_POS                _GPTA0_UL(8)
#define GPTA0_ID_MOD_TYPE_LEN                _GPTA0_UL(8)


#define GPTA0_CLC_DISR_POS                   _GPTA0_UL(0)
#define GPTA0_CLC_DISR_LEN                   _GPTA0_UL(1)


#define GPTA0_CLC_DISS_POS                   _GPTA0_UL(1)
#define GPTA0_CLC_DISS_LEN                   _GPTA0_UL(1)


#define GPTA0_CLC_SPEN_POS                   _GPTA0_UL(2)
#define GPTA0_CLC_SPEN_LEN                   _GPTA0_UL(1)


#define GPTA0_CLC_EDIS_POS                   _GPTA0_UL(3)
#define GPTA0_CLC_EDIS_LEN                   _GPTA0_UL(1)


#define GPTA0_CLC_SBWE_POS                   _GPTA0_UL(4)
#define GPTA0_CLC_SBWE_LEN                   _GPTA0_UL(1)


#define GPTA0_CLC_FSOE_POS                   _GPTA0_UL(5)
#define GPTA0_CLC_FSOE_LEN                   _GPTA0_UL(1)


#define GPTA0_DBGCTR_CLKCNT_POS              _GPTA0_UL(0)
#define GPTA0_DBGCTR_CLKCNT_LEN              _GPTA0_UL(16)


#define GPTA0_DBGCTR_DBGCEN_POS              _GPTA0_UL(31)
#define GPTA0_DBGCTR_DBGCEN_LEN              _GPTA0_UL(1)


#define GPTA0_ID_MOD_REV_POS                 _GPTA0_UL(0)
#define GPTA0_ID_MOD_REV_LEN                 _GPTA0_UL(8)


#define GPTA0_ID_MOD_TYPE_POS                _GPTA0_UL(8)
#define GPTA0_ID_MOD_TYPE_LEN                _GPTA0_UL(8)


#define GPTA0_ID_MOD_NUM_POS                 _GPTA0_UL(16)
#define GPTA0_ID_MOD_NUM_LEN                 _GPTA0_UL(16)


#define GPTA0_FDR_STEP_POS                   _GPTA0_UL(0)
#define GPTA0_FDR_STEP_LEN                   _GPTA0_UL(10)


#define GPTA0_FDR_SM_POS                     _GPTA0_UL(11)
#define GPTA0_FDR_SM_LEN                     _GPTA0_UL(1)


#define GPTA0_FDR_SC_POS                     _GPTA0_UL(12)
#define GPTA0_FDR_SC_LEN                     _GPTA0_UL(2)


#define GPTA0_FDR_DM_POS                     _GPTA0_UL(14)
#define GPTA0_FDR_DM_LEN                     _GPTA0_UL(2)


#define GPTA0_FDR_RESULT_POS                 _GPTA0_UL(16)
#define GPTA0_FDR_RESULT_LEN                 _GPTA0_UL(10)


#define GPTA0_FDR_SUSACK_POS                 _GPTA0_UL(28)
#define GPTA0_FDR_SUSACK_LEN                 _GPTA0_UL(1)


#define GPTA0_FDR_SUSREQ_POS                 _GPTA0_UL(29)
#define GPTA0_FDR_SUSREQ_LEN                 _GPTA0_UL(1)


#define GPTA0_FDR_ENHW_POS                   _GPTA0_UL(30)
#define GPTA0_FDR_ENHW_LEN                   _GPTA0_UL(1)


#define GPTA0_FDR_DISCLK_POS                 _GPTA0_UL(31)
#define GPTA0_FDR_DISCLK_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM00R_POS               _GPTA0_UL(0)
#define GPTA0_SRSC0_DCM00R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM01R_POS               _GPTA0_UL(3)
#define GPTA0_SRSC0_DCM01R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM02R_POS               _GPTA0_UL(6)
#define GPTA0_SRSC0_DCM02R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM03R_POS               _GPTA0_UL(9)
#define GPTA0_SRSC0_DCM03R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM00F_POS               _GPTA0_UL(1)
#define GPTA0_SRSC0_DCM00F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM01F_POS               _GPTA0_UL(4)
#define GPTA0_SRSC0_DCM01F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM02F_POS               _GPTA0_UL(7)
#define GPTA0_SRSC0_DCM02F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM03F_POS               _GPTA0_UL(10)
#define GPTA0_SRSC0_DCM03F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM00C_POS               _GPTA0_UL(2)
#define GPTA0_SRSC0_DCM00C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM01C_POS               _GPTA0_UL(5)
#define GPTA0_SRSC0_DCM01C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM02C_POS               _GPTA0_UL(8)
#define GPTA0_SRSC0_DCM02C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_DCM03C_POS               _GPTA0_UL(11)
#define GPTA0_SRSC0_DCM03C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSC0_PLL_POS                  _GPTA0_UL(12)
#define GPTA0_SRSC0_PLL_LEN                  _GPTA0_UL(1)


#define GPTA0_SRSC0_GT00_POS                 _GPTA0_UL(13)
#define GPTA0_SRSC0_GT00_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC0_GT01_POS                 _GPTA0_UL(14)
#define GPTA0_SRSC0_GT01_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM00R_POS               _GPTA0_UL(0)
#define GPTA0_SRSS0_DCM00R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM01R_POS               _GPTA0_UL(3)
#define GPTA0_SRSS0_DCM01R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM02R_POS               _GPTA0_UL(6)
#define GPTA0_SRSS0_DCM02R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM03R_POS               _GPTA0_UL(9)
#define GPTA0_SRSS0_DCM03R_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM00F_POS               _GPTA0_UL(1)
#define GPTA0_SRSS0_DCM00F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM01F_POS               _GPTA0_UL(4)
#define GPTA0_SRSS0_DCM01F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM02F_POS               _GPTA0_UL(7)
#define GPTA0_SRSS0_DCM02F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM03F_POS               _GPTA0_UL(10)
#define GPTA0_SRSS0_DCM03F_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM00C_POS               _GPTA0_UL(2)
#define GPTA0_SRSS0_DCM00C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM01C_POS               _GPTA0_UL(5)
#define GPTA0_SRSS0_DCM01C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM02C_POS               _GPTA0_UL(8)
#define GPTA0_SRSS0_DCM02C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_DCM03C_POS               _GPTA0_UL(11)
#define GPTA0_SRSS0_DCM03C_LEN               _GPTA0_UL(1)


#define GPTA0_SRSS0_PLL_POS                  _GPTA0_UL(12)
#define GPTA0_SRSS0_PLL_LEN                  _GPTA0_UL(1)


#define GPTA0_SRSS0_GT00_POS                 _GPTA0_UL(13)
#define GPTA0_SRSS0_GT00_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS0_GT01_POS                 _GPTA0_UL(14)
#define GPTA0_SRSS0_GT01_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC0_POS                 _GPTA0_UL(0)
#define GPTA0_SRSC1_GTC0_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC1_POS                 _GPTA0_UL(1)
#define GPTA0_SRSC1_GTC1_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC2_POS                 _GPTA0_UL(2)
#define GPTA0_SRSC1_GTC2_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC3_POS                 _GPTA0_UL(3)
#define GPTA0_SRSC1_GTC3_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC4_POS                 _GPTA0_UL(4)
#define GPTA0_SRSC1_GTC4_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC5_POS                 _GPTA0_UL(5)
#define GPTA0_SRSC1_GTC5_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC6_POS                 _GPTA0_UL(6)
#define GPTA0_SRSC1_GTC6_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC7_POS                 _GPTA0_UL(7)
#define GPTA0_SRSC1_GTC7_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC8_POS                 _GPTA0_UL(8)
#define GPTA0_SRSC1_GTC8_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC9_POS                 _GPTA0_UL(9)
#define GPTA0_SRSC1_GTC9_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC10_POS                _GPTA0_UL(10)
#define GPTA0_SRSC1_GTC10_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC11_POS                _GPTA0_UL(11)
#define GPTA0_SRSC1_GTC11_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC12_POS                _GPTA0_UL(12)
#define GPTA0_SRSC1_GTC12_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC13_POS                _GPTA0_UL(13)
#define GPTA0_SRSC1_GTC13_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC14_POS                _GPTA0_UL(14)
#define GPTA0_SRSC1_GTC14_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC15_POS                _GPTA0_UL(15)
#define GPTA0_SRSC1_GTC15_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC16_POS                _GPTA0_UL(16)
#define GPTA0_SRSC1_GTC16_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC17_POS                _GPTA0_UL(17)
#define GPTA0_SRSC1_GTC17_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC18_POS                _GPTA0_UL(18)
#define GPTA0_SRSC1_GTC18_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC19_POS                _GPTA0_UL(19)
#define GPTA0_SRSC1_GTC19_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC20_POS                _GPTA0_UL(20)
#define GPTA0_SRSC1_GTC20_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC21_POS                _GPTA0_UL(21)
#define GPTA0_SRSC1_GTC21_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC22_POS                _GPTA0_UL(22)
#define GPTA0_SRSC1_GTC22_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC23_POS                _GPTA0_UL(23)
#define GPTA0_SRSC1_GTC23_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC24_POS                _GPTA0_UL(24)
#define GPTA0_SRSC1_GTC24_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC25_POS                _GPTA0_UL(25)
#define GPTA0_SRSC1_GTC25_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC26_POS                _GPTA0_UL(26)
#define GPTA0_SRSC1_GTC26_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC27_POS                _GPTA0_UL(27)
#define GPTA0_SRSC1_GTC27_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC28_POS                _GPTA0_UL(28)
#define GPTA0_SRSC1_GTC28_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC29_POS                _GPTA0_UL(29)
#define GPTA0_SRSC1_GTC29_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC30_POS                _GPTA0_UL(30)
#define GPTA0_SRSC1_GTC30_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC1_GTC31_POS                _GPTA0_UL(31)
#define GPTA0_SRSC1_GTC31_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC0_POS                 _GPTA0_UL(0)
#define GPTA0_SRSS1_GTC0_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC1_POS                 _GPTA0_UL(1)
#define GPTA0_SRSS1_GTC1_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC2_POS                 _GPTA0_UL(2)
#define GPTA0_SRSS1_GTC2_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC3_POS                 _GPTA0_UL(3)
#define GPTA0_SRSS1_GTC3_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC4_POS                 _GPTA0_UL(4)
#define GPTA0_SRSS1_GTC4_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC5_POS                 _GPTA0_UL(5)
#define GPTA0_SRSS1_GTC5_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC6_POS                 _GPTA0_UL(6)
#define GPTA0_SRSS1_GTC6_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC7_POS                 _GPTA0_UL(7)
#define GPTA0_SRSS1_GTC7_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC8_POS                 _GPTA0_UL(8)
#define GPTA0_SRSS1_GTC8_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC9_POS                 _GPTA0_UL(9)
#define GPTA0_SRSS1_GTC9_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC10_POS                _GPTA0_UL(10)
#define GPTA0_SRSS1_GTC10_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC11_POS                _GPTA0_UL(11)
#define GPTA0_SRSS1_GTC11_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC12_POS                _GPTA0_UL(12)
#define GPTA0_SRSS1_GTC12_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC13_POS                _GPTA0_UL(13)
#define GPTA0_SRSS1_GTC13_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC14_POS                _GPTA0_UL(14)
#define GPTA0_SRSS1_GTC14_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC15_POS                _GPTA0_UL(15)
#define GPTA0_SRSS1_GTC15_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC16_POS                _GPTA0_UL(16)
#define GPTA0_SRSS1_GTC16_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC17_POS                _GPTA0_UL(17)
#define GPTA0_SRSS1_GTC17_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC18_POS                _GPTA0_UL(18)
#define GPTA0_SRSS1_GTC18_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC19_POS                _GPTA0_UL(19)
#define GPTA0_SRSS1_GTC19_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC20_POS                _GPTA0_UL(20)
#define GPTA0_SRSS1_GTC20_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC21_POS                _GPTA0_UL(21)
#define GPTA0_SRSS1_GTC21_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC22_POS                _GPTA0_UL(22)
#define GPTA0_SRSS1_GTC22_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC23_POS                _GPTA0_UL(23)
#define GPTA0_SRSS1_GTC23_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC24_POS                _GPTA0_UL(24)
#define GPTA0_SRSS1_GTC24_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC25_POS                _GPTA0_UL(25)
#define GPTA0_SRSS1_GTC25_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC26_POS                _GPTA0_UL(26)
#define GPTA0_SRSS1_GTC26_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC27_POS                _GPTA0_UL(27)
#define GPTA0_SRSS1_GTC27_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC28_POS                _GPTA0_UL(28)
#define GPTA0_SRSS1_GTC28_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC29_POS                _GPTA0_UL(29)
#define GPTA0_SRSS1_GTC29_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC30_POS                _GPTA0_UL(30)
#define GPTA0_SRSS1_GTC30_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS1_GTC31_POS                _GPTA0_UL(31)
#define GPTA0_SRSS1_GTC31_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC0_POS                 _GPTA0_UL(0)
#define GPTA0_SRSC2_LTC0_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC1_POS                 _GPTA0_UL(1)
#define GPTA0_SRSC2_LTC1_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC2_POS                 _GPTA0_UL(2)
#define GPTA0_SRSC2_LTC2_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC3_POS                 _GPTA0_UL(3)
#define GPTA0_SRSC2_LTC3_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC4_POS                 _GPTA0_UL(4)
#define GPTA0_SRSC2_LTC4_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC5_POS                 _GPTA0_UL(5)
#define GPTA0_SRSC2_LTC5_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC6_POS                 _GPTA0_UL(6)
#define GPTA0_SRSC2_LTC6_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC7_POS                 _GPTA0_UL(7)
#define GPTA0_SRSC2_LTC7_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC8_POS                 _GPTA0_UL(8)
#define GPTA0_SRSC2_LTC8_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC9_POS                 _GPTA0_UL(9)
#define GPTA0_SRSC2_LTC9_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC10_POS                _GPTA0_UL(10)
#define GPTA0_SRSC2_LTC10_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC11_POS                _GPTA0_UL(11)
#define GPTA0_SRSC2_LTC11_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC12_POS                _GPTA0_UL(12)
#define GPTA0_SRSC2_LTC12_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC13_POS                _GPTA0_UL(13)
#define GPTA0_SRSC2_LTC13_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC14_POS                _GPTA0_UL(14)
#define GPTA0_SRSC2_LTC14_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC15_POS                _GPTA0_UL(15)
#define GPTA0_SRSC2_LTC15_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC16_POS                _GPTA0_UL(16)
#define GPTA0_SRSC2_LTC16_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC17_POS                _GPTA0_UL(17)
#define GPTA0_SRSC2_LTC17_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC18_POS                _GPTA0_UL(18)
#define GPTA0_SRSC2_LTC18_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC19_POS                _GPTA0_UL(19)
#define GPTA0_SRSC2_LTC19_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC20_POS                _GPTA0_UL(20)
#define GPTA0_SRSC2_LTC20_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC21_POS                _GPTA0_UL(21)
#define GPTA0_SRSC2_LTC21_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC22_POS                _GPTA0_UL(22)
#define GPTA0_SRSC2_LTC22_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC23_POS                _GPTA0_UL(23)
#define GPTA0_SRSC2_LTC23_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC24_POS                _GPTA0_UL(24)
#define GPTA0_SRSC2_LTC24_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC25_POS                _GPTA0_UL(25)
#define GPTA0_SRSC2_LTC25_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC26_POS                _GPTA0_UL(26)
#define GPTA0_SRSC2_LTC26_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC27_POS                _GPTA0_UL(27)
#define GPTA0_SRSC2_LTC27_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC28_POS                _GPTA0_UL(28)
#define GPTA0_SRSC2_LTC28_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC29_POS                _GPTA0_UL(29)
#define GPTA0_SRSC2_LTC29_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC30_POS                _GPTA0_UL(30)
#define GPTA0_SRSC2_LTC30_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC2_LTC31_POS                _GPTA0_UL(31)
#define GPTA0_SRSC2_LTC31_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC0_POS                 _GPTA0_UL(0)
#define GPTA0_SRSS2_LTC0_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC1_POS                 _GPTA0_UL(1)
#define GPTA0_SRSS2_LTC1_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC2_POS                 _GPTA0_UL(2)
#define GPTA0_SRSS2_LTC2_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC3_POS                 _GPTA0_UL(3)
#define GPTA0_SRSS2_LTC3_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC4_POS                 _GPTA0_UL(4)
#define GPTA0_SRSS2_LTC4_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC5_POS                 _GPTA0_UL(5)
#define GPTA0_SRSS2_LTC5_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC6_POS                 _GPTA0_UL(6)
#define GPTA0_SRSS2_LTC6_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC7_POS                 _GPTA0_UL(7)
#define GPTA0_SRSS2_LTC7_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC8_POS                 _GPTA0_UL(8)
#define GPTA0_SRSS2_LTC8_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC9_POS                 _GPTA0_UL(9)
#define GPTA0_SRSS2_LTC9_LEN                 _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC10_POS                _GPTA0_UL(10)
#define GPTA0_SRSS2_LTC10_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC11_POS                _GPTA0_UL(11)
#define GPTA0_SRSS2_LTC11_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC12_POS                _GPTA0_UL(12)
#define GPTA0_SRSS2_LTC12_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC13_POS                _GPTA0_UL(13)
#define GPTA0_SRSS2_LTC13_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC14_POS                _GPTA0_UL(14)
#define GPTA0_SRSS2_LTC14_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC15_POS                _GPTA0_UL(15)
#define GPTA0_SRSS2_LTC15_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC16_POS                _GPTA0_UL(16)
#define GPTA0_SRSS2_LTC16_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC17_POS                _GPTA0_UL(17)
#define GPTA0_SRSS2_LTC17_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC18_POS                _GPTA0_UL(18)
#define GPTA0_SRSS2_LTC18_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC19_POS                _GPTA0_UL(19)
#define GPTA0_SRSS2_LTC19_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC20_POS                _GPTA0_UL(20)
#define GPTA0_SRSS2_LTC20_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC21_POS                _GPTA0_UL(21)
#define GPTA0_SRSS2_LTC21_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC22_POS                _GPTA0_UL(22)
#define GPTA0_SRSS2_LTC22_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC23_POS                _GPTA0_UL(23)
#define GPTA0_SRSS2_LTC23_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC24_POS                _GPTA0_UL(24)
#define GPTA0_SRSS2_LTC24_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC25_POS                _GPTA0_UL(25)
#define GPTA0_SRSS2_LTC25_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC26_POS                _GPTA0_UL(26)
#define GPTA0_SRSS2_LTC26_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC27_POS                _GPTA0_UL(27)
#define GPTA0_SRSS2_LTC27_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC28_POS                _GPTA0_UL(28)
#define GPTA0_SRSS2_LTC28_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC29_POS                _GPTA0_UL(29)
#define GPTA0_SRSS2_LTC29_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC30_POS                _GPTA0_UL(30)
#define GPTA0_SRSS2_LTC30_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS2_LTC31_POS                _GPTA0_UL(31)
#define GPTA0_SRSS2_LTC31_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC32_POS                _GPTA0_UL(0)
#define GPTA0_SRSC3_LTC32_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC33_POS                _GPTA0_UL(1)
#define GPTA0_SRSC3_LTC33_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC34_POS                _GPTA0_UL(2)
#define GPTA0_SRSC3_LTC34_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC35_POS                _GPTA0_UL(3)
#define GPTA0_SRSC3_LTC35_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC36_POS                _GPTA0_UL(4)
#define GPTA0_SRSC3_LTC36_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC37_POS                _GPTA0_UL(5)
#define GPTA0_SRSC3_LTC37_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC38_POS                _GPTA0_UL(6)
#define GPTA0_SRSC3_LTC38_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC39_POS                _GPTA0_UL(7)
#define GPTA0_SRSC3_LTC39_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC40_POS                _GPTA0_UL(8)
#define GPTA0_SRSC3_LTC40_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC41_POS                _GPTA0_UL(9)
#define GPTA0_SRSC3_LTC41_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC42_POS                _GPTA0_UL(10)
#define GPTA0_SRSC3_LTC42_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC43_POS                _GPTA0_UL(11)
#define GPTA0_SRSC3_LTC43_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC44_POS                _GPTA0_UL(12)
#define GPTA0_SRSC3_LTC44_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC45_POS                _GPTA0_UL(13)
#define GPTA0_SRSC3_LTC45_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC46_POS                _GPTA0_UL(14)
#define GPTA0_SRSC3_LTC46_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC47_POS                _GPTA0_UL(15)
#define GPTA0_SRSC3_LTC47_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC48_POS                _GPTA0_UL(16)
#define GPTA0_SRSC3_LTC48_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC49_POS                _GPTA0_UL(17)
#define GPTA0_SRSC3_LTC49_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC50_POS                _GPTA0_UL(18)
#define GPTA0_SRSC3_LTC50_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC51_POS                _GPTA0_UL(19)
#define GPTA0_SRSC3_LTC51_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC52_POS                _GPTA0_UL(20)
#define GPTA0_SRSC3_LTC52_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC53_POS                _GPTA0_UL(21)
#define GPTA0_SRSC3_LTC53_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC54_POS                _GPTA0_UL(22)
#define GPTA0_SRSC3_LTC54_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC55_POS                _GPTA0_UL(23)
#define GPTA0_SRSC3_LTC55_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC56_POS                _GPTA0_UL(24)
#define GPTA0_SRSC3_LTC56_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC57_POS                _GPTA0_UL(25)
#define GPTA0_SRSC3_LTC57_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC58_POS                _GPTA0_UL(26)
#define GPTA0_SRSC3_LTC58_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC59_POS                _GPTA0_UL(27)
#define GPTA0_SRSC3_LTC59_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC60_POS                _GPTA0_UL(28)
#define GPTA0_SRSC3_LTC60_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC61_POS                _GPTA0_UL(29)
#define GPTA0_SRSC3_LTC61_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC62_POS                _GPTA0_UL(30)
#define GPTA0_SRSC3_LTC62_LEN                _GPTA0_UL(1)


#define GPTA0_SRSC3_LTC63_POS                _GPTA0_UL(31)
#define GPTA0_SRSC3_LTC63_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC32_POS                _GPTA0_UL(0)
#define GPTA0_SRSS3_LTC32_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC33_POS                _GPTA0_UL(1)
#define GPTA0_SRSS3_LTC33_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC34_POS                _GPTA0_UL(2)
#define GPTA0_SRSS3_LTC34_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC35_POS                _GPTA0_UL(3)
#define GPTA0_SRSS3_LTC35_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC36_POS                _GPTA0_UL(4)
#define GPTA0_SRSS3_LTC36_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC37_POS                _GPTA0_UL(5)
#define GPTA0_SRSS3_LTC37_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC38_POS                _GPTA0_UL(6)
#define GPTA0_SRSS3_LTC38_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC39_POS                _GPTA0_UL(7)
#define GPTA0_SRSS3_LTC39_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC40_POS                _GPTA0_UL(8)
#define GPTA0_SRSS3_LTC40_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC41_POS                _GPTA0_UL(9)
#define GPTA0_SRSS3_LTC41_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC42_POS                _GPTA0_UL(10)
#define GPTA0_SRSS3_LTC42_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC43_POS                _GPTA0_UL(11)
#define GPTA0_SRSS3_LTC43_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC44_POS                _GPTA0_UL(12)
#define GPTA0_SRSS3_LTC44_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC45_POS                _GPTA0_UL(13)
#define GPTA0_SRSS3_LTC45_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC46_POS                _GPTA0_UL(14)
#define GPTA0_SRSS3_LTC46_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC47_POS                _GPTA0_UL(15)
#define GPTA0_SRSS3_LTC47_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC48_POS                _GPTA0_UL(16)
#define GPTA0_SRSS3_LTC48_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC49_POS                _GPTA0_UL(17)
#define GPTA0_SRSS3_LTC49_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC50_POS                _GPTA0_UL(18)
#define GPTA0_SRSS3_LTC50_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC51_POS                _GPTA0_UL(19)
#define GPTA0_SRSS3_LTC51_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC52_POS                _GPTA0_UL(20)
#define GPTA0_SRSS3_LTC52_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC53_POS                _GPTA0_UL(21)
#define GPTA0_SRSS3_LTC53_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC54_POS                _GPTA0_UL(22)
#define GPTA0_SRSS3_LTC54_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC55_POS                _GPTA0_UL(23)
#define GPTA0_SRSS3_LTC55_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC56_POS                _GPTA0_UL(24)
#define GPTA0_SRSS3_LTC56_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC57_POS                _GPTA0_UL(25)
#define GPTA0_SRSS3_LTC57_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC58_POS                _GPTA0_UL(26)
#define GPTA0_SRSS3_LTC58_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC59_POS                _GPTA0_UL(27)
#define GPTA0_SRSS3_LTC59_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC60_POS                _GPTA0_UL(28)
#define GPTA0_SRSS3_LTC60_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC61_POS                _GPTA0_UL(29)
#define GPTA0_SRSS3_LTC61_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC62_POS                _GPTA0_UL(30)
#define GPTA0_SRSS3_LTC62_LEN                _GPTA0_UL(1)


#define GPTA0_SRSS3_LTC63_POS                _GPTA0_UL(31)
#define GPTA0_SRSS3_LTC63_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC01R_POS                _GPTA0_UL(0)
#define GPTA0_SRNR_GTC01R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC03R_POS                _GPTA0_UL(1)
#define GPTA0_SRNR_GTC03R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC05R_POS                _GPTA0_UL(2)
#define GPTA0_SRNR_GTC05R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC07R_POS                _GPTA0_UL(3)
#define GPTA0_SRNR_GTC07R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC09R_POS                _GPTA0_UL(4)
#define GPTA0_SRNR_GTC09R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC11R_POS                _GPTA0_UL(5)
#define GPTA0_SRNR_GTC11R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC13R_POS                _GPTA0_UL(6)
#define GPTA0_SRNR_GTC13R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC15R_POS                _GPTA0_UL(7)
#define GPTA0_SRNR_GTC15R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC17R_POS                _GPTA0_UL(8)
#define GPTA0_SRNR_GTC17R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC19R_POS                _GPTA0_UL(9)
#define GPTA0_SRNR_GTC19R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC21R_POS                _GPTA0_UL(10)
#define GPTA0_SRNR_GTC21R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC23R_POS                _GPTA0_UL(11)
#define GPTA0_SRNR_GTC23R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC25R_POS                _GPTA0_UL(12)
#define GPTA0_SRNR_GTC25R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC27R_POS                _GPTA0_UL(13)
#define GPTA0_SRNR_GTC27R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC29R_POS                _GPTA0_UL(14)
#define GPTA0_SRNR_GTC29R_LEN                _GPTA0_UL(1)


#define GPTA0_SRNR_GTC31R_POS                _GPTA0_UL(15)
#define GPTA0_SRNR_GTC31R_LEN                _GPTA0_UL(1)


#define GPTA0_MRACTL_MAEN_POS                _GPTA0_UL(0)
#define GPTA0_MRACTL_MAEN_LEN                _GPTA0_UL(1)


#define GPTA0_MRACTL_WCRES_POS               _GPTA0_UL(1)
#define GPTA0_MRACTL_WCRES_LEN               _GPTA0_UL(1)


#define GPTA0_MRACTL_FIFOFULL_POS            _GPTA0_UL(2)
#define GPTA0_MRACTL_FIFOFULL_LEN            _GPTA0_UL(1)


#define GPTA0_MRACTL_FIFOFILLCNT_POS         _GPTA0_UL(8)
#define GPTA0_MRACTL_FIFOFILLCNT_LEN         _GPTA0_UL(6)


#define GPTA0_MRADIN_DATAIN_POS              _GPTA0_UL(0)
#define GPTA0_MRADIN_DATAIN_LEN              _GPTA0_UL(32)


#define GPTA0_MRADOUT_DATAOUT_POS            _GPTA0_UL(0)
#define GPTA0_MRADOUT_DATAOUT_LEN            _GPTA0_UL(32)


#define GPTA0_FPCSTAT_REG0_POS               _GPTA0_UL(0)
#define GPTA0_FPCSTAT_REG0_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_REG1_POS               _GPTA0_UL(1)
#define GPTA0_FPCSTAT_REG1_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_REG2_POS               _GPTA0_UL(2)
#define GPTA0_FPCSTAT_REG2_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_REG3_POS               _GPTA0_UL(3)
#define GPTA0_FPCSTAT_REG3_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_REG4_POS               _GPTA0_UL(4)
#define GPTA0_FPCSTAT_REG4_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_REG5_POS               _GPTA0_UL(5)
#define GPTA0_FPCSTAT_REG5_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_FEG0_POS               _GPTA0_UL(8)
#define GPTA0_FPCSTAT_FEG0_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_FEG1_POS               _GPTA0_UL(9)
#define GPTA0_FPCSTAT_FEG1_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_FEG2_POS               _GPTA0_UL(10)
#define GPTA0_FPCSTAT_FEG2_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_FEG3_POS               _GPTA0_UL(11)
#define GPTA0_FPCSTAT_FEG3_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_FEG4_POS               _GPTA0_UL(12)
#define GPTA0_FPCSTAT_FEG4_LEN               _GPTA0_UL(1)


#define GPTA0_FPCSTAT_FEG5_POS               _GPTA0_UL(13)
#define GPTA0_FPCSTAT_FEG5_LEN               _GPTA0_UL(1)


#define GPTA0_FPCCTR0_CMP_POS                _GPTA0_UL(0)
#define GPTA0_FPCCTR0_CMP_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR0_MOD_POS                _GPTA0_UL(16)
#define GPTA0_FPCCTR0_MOD_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR0_IPS_POS                _GPTA0_UL(19)
#define GPTA0_FPCCTR0_IPS_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR0_CLK_POS                _GPTA0_UL(22)
#define GPTA0_FPCCTR0_CLK_LEN                _GPTA0_UL(2)


#define GPTA0_FPCCTR0_RTG_POS                _GPTA0_UL(24)
#define GPTA0_FPCCTR0_RTG_LEN                _GPTA0_UL(1)


#define GPTA0_FPCTIM0_TIM_POS                _GPTA0_UL(0)
#define GPTA0_FPCTIM0_TIM_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR1_CMP_POS                _GPTA0_UL(0)
#define GPTA0_FPCCTR1_CMP_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR1_MOD_POS                _GPTA0_UL(16)
#define GPTA0_FPCCTR1_MOD_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR1_IPS_POS                _GPTA0_UL(19)
#define GPTA0_FPCCTR1_IPS_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR1_CLK_POS                _GPTA0_UL(22)
#define GPTA0_FPCCTR1_CLK_LEN                _GPTA0_UL(2)


#define GPTA0_FPCCTR1_RTG_POS                _GPTA0_UL(24)
#define GPTA0_FPCCTR1_RTG_LEN                _GPTA0_UL(1)


#define GPTA0_FPCTIM1_TIM_POS                _GPTA0_UL(0)
#define GPTA0_FPCTIM1_TIM_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR2_CMP_POS                _GPTA0_UL(0)
#define GPTA0_FPCCTR2_CMP_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR2_MOD_POS                _GPTA0_UL(16)
#define GPTA0_FPCCTR2_MOD_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR2_IPS_POS                _GPTA0_UL(19)
#define GPTA0_FPCCTR2_IPS_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR2_CLK_POS                _GPTA0_UL(22)
#define GPTA0_FPCCTR2_CLK_LEN                _GPTA0_UL(2)


#define GPTA0_FPCCTR2_RTG_POS                _GPTA0_UL(24)
#define GPTA0_FPCCTR2_RTG_LEN                _GPTA0_UL(1)


#define GPTA0_FPCTIM2_TIM_POS                _GPTA0_UL(0)
#define GPTA0_FPCTIM2_TIM_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR3_CMP_POS                _GPTA0_UL(0)
#define GPTA0_FPCCTR3_CMP_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR3_MOD_POS                _GPTA0_UL(16)
#define GPTA0_FPCCTR3_MOD_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR3_IPS_POS                _GPTA0_UL(19)
#define GPTA0_FPCCTR3_IPS_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR3_CLK_POS                _GPTA0_UL(22)
#define GPTA0_FPCCTR3_CLK_LEN                _GPTA0_UL(2)


#define GPTA0_FPCCTR3_RTG_POS                _GPTA0_UL(24)
#define GPTA0_FPCCTR3_RTG_LEN                _GPTA0_UL(1)


#define GPTA0_FPCTIM3_TIM_POS                _GPTA0_UL(0)
#define GPTA0_FPCTIM3_TIM_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR4_CMP_POS                _GPTA0_UL(0)
#define GPTA0_FPCCTR4_CMP_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR4_MOD_POS                _GPTA0_UL(16)
#define GPTA0_FPCCTR4_MOD_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR4_IPS_POS                _GPTA0_UL(19)
#define GPTA0_FPCCTR4_IPS_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR4_CLK_POS                _GPTA0_UL(22)
#define GPTA0_FPCCTR4_CLK_LEN                _GPTA0_UL(2)


#define GPTA0_FPCCTR4_RTG_POS                _GPTA0_UL(24)
#define GPTA0_FPCCTR4_RTG_LEN                _GPTA0_UL(1)


#define GPTA0_FPCTIM4_TIM_POS                _GPTA0_UL(0)
#define GPTA0_FPCTIM4_TIM_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR5_CMP_POS                _GPTA0_UL(0)
#define GPTA0_FPCCTR5_CMP_LEN                _GPTA0_UL(16)


#define GPTA0_FPCCTR5_MOD_POS                _GPTA0_UL(16)
#define GPTA0_FPCCTR5_MOD_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR5_IPS_POS                _GPTA0_UL(19)
#define GPTA0_FPCCTR5_IPS_LEN                _GPTA0_UL(3)


#define GPTA0_FPCCTR5_CLK_POS                _GPTA0_UL(22)
#define GPTA0_FPCCTR5_CLK_LEN                _GPTA0_UL(2)


#define GPTA0_FPCCTR5_RTG_POS                _GPTA0_UL(24)
#define GPTA0_FPCCTR5_RTG_LEN                _GPTA0_UL(1)


#define GPTA0_FPCTIM5_TIM_POS                _GPTA0_UL(0)
#define GPTA0_FPCTIM5_TIM_LEN                _GPTA0_UL(16)


#define GPTA0_PDLCTR_MUX0_POS                _GPTA0_UL(0)
#define GPTA0_PDLCTR_MUX0_LEN                _GPTA0_UL(1)


#define GPTA0_PDLCTR_TSE0_POS                _GPTA0_UL(1)
#define GPTA0_PDLCTR_TSE0_LEN                _GPTA0_UL(1)


#define GPTA0_PDLCTR_ERR0_POS                _GPTA0_UL(2)
#define GPTA0_PDLCTR_ERR0_LEN                _GPTA0_UL(1)


#define GPTA0_PDLCTR_MUX1_POS                _GPTA0_UL(4)
#define GPTA0_PDLCTR_MUX1_LEN                _GPTA0_UL(1)


#define GPTA0_PDLCTR_TSE1_POS                _GPTA0_UL(5)
#define GPTA0_PDLCTR_TSE1_LEN                _GPTA0_UL(1)


#define GPTA0_PDLCTR_ERR1_POS                _GPTA0_UL(6)
#define GPTA0_PDLCTR_ERR1_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_RCA_POS                _GPTA0_UL(0)
#define GPTA0_DCMCTR0_RCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_OCA_POS                _GPTA0_UL(1)
#define GPTA0_DCMCTR0_OCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_RZE_POS                _GPTA0_UL(2)
#define GPTA0_DCMCTR0_RZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_FZE_POS                _GPTA0_UL(3)
#define GPTA0_DCMCTR0_FZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_RCK_POS                _GPTA0_UL(4)
#define GPTA0_DCMCTR0_RCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_FCK_POS                _GPTA0_UL(5)
#define GPTA0_DCMCTR0_FCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_QCK_POS                _GPTA0_UL(6)
#define GPTA0_DCMCTR0_QCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_RRE_POS                _GPTA0_UL(7)
#define GPTA0_DCMCTR0_RRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_FRE_POS                _GPTA0_UL(8)
#define GPTA0_DCMCTR0_FRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR0_CRE_POS                _GPTA0_UL(9)
#define GPTA0_DCMCTR0_CRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMTIM0_TIM_POS                _GPTA0_UL(0)
#define GPTA0_DCMTIM0_TIM_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCAV0_CAV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCAV0_CAV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCOV0_COV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCOV0_COV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCTR1_RCA_POS                _GPTA0_UL(0)
#define GPTA0_DCMCTR1_RCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_OCA_POS                _GPTA0_UL(1)
#define GPTA0_DCMCTR1_OCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_RZE_POS                _GPTA0_UL(2)
#define GPTA0_DCMCTR1_RZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_FZE_POS                _GPTA0_UL(3)
#define GPTA0_DCMCTR1_FZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_RCK_POS                _GPTA0_UL(4)
#define GPTA0_DCMCTR1_RCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_FCK_POS                _GPTA0_UL(5)
#define GPTA0_DCMCTR1_FCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_QCK_POS                _GPTA0_UL(6)
#define GPTA0_DCMCTR1_QCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_RRE_POS                _GPTA0_UL(7)
#define GPTA0_DCMCTR1_RRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_FRE_POS                _GPTA0_UL(8)
#define GPTA0_DCMCTR1_FRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR1_CRE_POS                _GPTA0_UL(9)
#define GPTA0_DCMCTR1_CRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMTIM1_TIM_POS                _GPTA0_UL(0)
#define GPTA0_DCMTIM1_TIM_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCAV1_CAV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCAV1_CAV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCOV1_COV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCOV1_COV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCTR2_RCA_POS                _GPTA0_UL(0)
#define GPTA0_DCMCTR2_RCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_OCA_POS                _GPTA0_UL(1)
#define GPTA0_DCMCTR2_OCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_RZE_POS                _GPTA0_UL(2)
#define GPTA0_DCMCTR2_RZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_FZE_POS                _GPTA0_UL(3)
#define GPTA0_DCMCTR2_FZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_RCK_POS                _GPTA0_UL(4)
#define GPTA0_DCMCTR2_RCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_FCK_POS                _GPTA0_UL(5)
#define GPTA0_DCMCTR2_FCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_QCK_POS                _GPTA0_UL(6)
#define GPTA0_DCMCTR2_QCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_RRE_POS                _GPTA0_UL(7)
#define GPTA0_DCMCTR2_RRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_FRE_POS                _GPTA0_UL(8)
#define GPTA0_DCMCTR2_FRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR2_CRE_POS                _GPTA0_UL(9)
#define GPTA0_DCMCTR2_CRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMTIM2_TIM_POS                _GPTA0_UL(0)
#define GPTA0_DCMTIM2_TIM_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCAV2_CAV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCAV2_CAV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCOV2_COV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCOV2_COV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCTR3_RCA_POS                _GPTA0_UL(0)
#define GPTA0_DCMCTR3_RCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_OCA_POS                _GPTA0_UL(1)
#define GPTA0_DCMCTR3_OCA_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_RZE_POS                _GPTA0_UL(2)
#define GPTA0_DCMCTR3_RZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_FZE_POS                _GPTA0_UL(3)
#define GPTA0_DCMCTR3_FZE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_RCK_POS                _GPTA0_UL(4)
#define GPTA0_DCMCTR3_RCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_FCK_POS                _GPTA0_UL(5)
#define GPTA0_DCMCTR3_FCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_QCK_POS                _GPTA0_UL(6)
#define GPTA0_DCMCTR3_QCK_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_RRE_POS                _GPTA0_UL(7)
#define GPTA0_DCMCTR3_RRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_FRE_POS                _GPTA0_UL(8)
#define GPTA0_DCMCTR3_FRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMCTR3_CRE_POS                _GPTA0_UL(9)
#define GPTA0_DCMCTR3_CRE_LEN                _GPTA0_UL(1)


#define GPTA0_DCMTIM3_TIM_POS                _GPTA0_UL(0)
#define GPTA0_DCMTIM3_TIM_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCAV3_CAV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCAV3_CAV_LEN                _GPTA0_UL(24)


#define GPTA0_DCMCOV3_COV_POS                _GPTA0_UL(0)
#define GPTA0_DCMCOV3_COV_LEN                _GPTA0_UL(24)


#define GPTA0_PLLCTR_MUX_POS                 _GPTA0_UL(0)
#define GPTA0_PLLCTR_MUX_LEN                 _GPTA0_UL(2)


#define GPTA0_PLLCTR_AEN_POS                 _GPTA0_UL(2)
#define GPTA0_PLLCTR_AEN_LEN                 _GPTA0_UL(1)


#define GPTA0_PLLCTR_PEN_POS                 _GPTA0_UL(3)
#define GPTA0_PLLCTR_PEN_LEN                 _GPTA0_UL(1)


#define GPTA0_PLLCTR_REN_POS                 _GPTA0_UL(4)
#define GPTA0_PLLCTR_REN_LEN                 _GPTA0_UL(1)


#define GPTA0_PLLMTI_MTI_POS                 _GPTA0_UL(0)
#define GPTA0_PLLMTI_MTI_LEN                 _GPTA0_UL(16)


#define GPTA0_PLLCNT_CNT_POS                 _GPTA0_UL(0)
#define GPTA0_PLLCNT_CNT_LEN                 _GPTA0_UL(16)


#define GPTA0_PLLSTP_STP_POS                 _GPTA0_UL(0)
#define GPTA0_PLLSTP_STP_LEN                 _GPTA0_UL(16)


#define GPTA0_PLLREV_REV_POS                 _GPTA0_UL(0)
#define GPTA0_PLLREV_REV_LEN                 _GPTA0_UL(24)


#define GPTA0_PLLDTR_DTR_POS                 _GPTA0_UL(0)
#define GPTA0_PLLDTR_DTR_LEN                 _GPTA0_UL(25)


#define GPTA0_CKBCTR_DFA02_POS               _GPTA0_UL(0)
#define GPTA0_CKBCTR_DFA02_LEN               _GPTA0_UL(4)


#define GPTA0_CKBCTR_DFA04_POS               _GPTA0_UL(4)
#define GPTA0_CKBCTR_DFA04_LEN               _GPTA0_UL(4)


#define GPTA0_CKBCTR_DFA06_POS               _GPTA0_UL(8)
#define GPTA0_CKBCTR_DFA06_LEN               _GPTA0_UL(4)


#define GPTA0_CKBCTR_DFA07_POS               _GPTA0_UL(12)
#define GPTA0_CKBCTR_DFA07_LEN               _GPTA0_UL(4)


#define GPTA0_CKBCTR_DFA03_POS               _GPTA0_UL(16)
#define GPTA0_CKBCTR_DFA03_LEN               _GPTA0_UL(2)


#define GPTA0_CKBCTR_DFALTC_POS              _GPTA0_UL(18)
#define GPTA0_CKBCTR_DFALTC_LEN              _GPTA0_UL(3)


#define GPTA0_GTCTR0_SCO_POS                 _GPTA0_UL(0)
#define GPTA0_GTCTR0_SCO_LEN                 _GPTA0_UL(4)


#define GPTA0_GTCTR0_MUX_POS                 _GPTA0_UL(4)
#define GPTA0_GTCTR0_MUX_LEN                 _GPTA0_UL(3)


#define GPTA0_GTCTR0_REN_POS                 _GPTA0_UL(7)
#define GPTA0_GTCTR0_REN_LEN                 _GPTA0_UL(1)


#define GPTA0_GTREV0_REV_POS                 _GPTA0_UL(0)
#define GPTA0_GTREV0_REV_LEN                 _GPTA0_UL(24)


#define GPTA0_GTTIM0_TIM_POS                 _GPTA0_UL(0)
#define GPTA0_GTTIM0_TIM_LEN                 _GPTA0_UL(24)


#define GPTA0_GTCTR1_SCO_POS                 _GPTA0_UL(0)
#define GPTA0_GTCTR1_SCO_LEN                 _GPTA0_UL(4)


#define GPTA0_GTCTR1_MUX_POS                 _GPTA0_UL(4)
#define GPTA0_GTCTR1_MUX_LEN                 _GPTA0_UL(3)


#define GPTA0_GTCTR1_REN_POS                 _GPTA0_UL(7)
#define GPTA0_GTCTR1_REN_LEN                 _GPTA0_UL(1)


#define GPTA0_GTREV1_REV_POS                 _GPTA0_UL(0)
#define GPTA0_GTREV1_REV_LEN                 _GPTA0_UL(24)


#define GPTA0_GTTIM1_TIM_POS                 _GPTA0_UL(0)
#define GPTA0_GTTIM1_TIM_LEN                 _GPTA0_UL(24)


#define GPTA0_GTCCTR_MOD_POS                 _GPTA0_UL(0)
#define GPTA0_GTCCTR_MOD_LEN                 _GPTA0_UL(2)


#define GPTA0_GTCCTR_OSM_POS                 _GPTA0_UL(2)
#define GPTA0_GTCCTR_OSM_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_REN_POS                 _GPTA0_UL(3)
#define GPTA0_GTCCTR_REN_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_RED_POS                 _GPTA0_UL(4)
#define GPTA0_GTCCTR_RED_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_FED_POS                 _GPTA0_UL(5)
#define GPTA0_GTCCTR_FED_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_NE_POS                  _GPTA0_UL(6)
#define GPTA0_GTCCTR_NE_LEN                  _GPTA0_UL(1)


#define GPTA0_GTCCTR_BYP_POS                 _GPTA0_UL(7)
#define GPTA0_GTCCTR_BYP_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_EOA_POS                 _GPTA0_UL(8)
#define GPTA0_GTCCTR_EOA_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_CEN_POS                 _GPTA0_UL(10)
#define GPTA0_GTCCTR_CEN_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_OCM_POS                 _GPTA0_UL(11)
#define GPTA0_GTCCTR_OCM_LEN                 _GPTA0_UL(3)


#define GPTA0_GTCCTR_OIA_POS                 _GPTA0_UL(14)
#define GPTA0_GTCCTR_OIA_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_OUT_POS                 _GPTA0_UL(15)
#define GPTA0_GTCCTR_OUT_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_GES_POS                 _GPTA0_UL(4)
#define GPTA0_GTCCTR_GES_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_CAC_POS                 _GPTA0_UL(5)
#define GPTA0_GTCCTR_CAC_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCCTR_CAT_POS                 _GPTA0_UL(6)
#define GPTA0_GTCCTR_CAT_LEN                 _GPTA0_UL(1)


#define GPTA0_GTCXR_X_POS                    _GPTA0_UL(0)
#define GPTA0_GTCXR_X_LEN                    _GPTA0_UL(24)


#define GPTA0_LTCCTR_MOD_POS                 _GPTA0_UL(0)
#define GPTA0_LTCCTR_MOD_LEN                 _GPTA0_UL(2)


#define GPTA0_LTCCTR_OSM_POS                 _GPTA0_UL(2)
#define GPTA0_LTCCTR_OSM_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_REN_POS                 _GPTA0_UL(3)
#define GPTA0_LTCCTR_REN_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_RED_POS                 _GPTA0_UL(4)
#define GPTA0_LTCCTR_RED_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_PEN_POS                 _GPTA0_UL(4)
#define GPTA0_LTCCTR_PEN_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_FED_POS                 _GPTA0_UL(5)
#define GPTA0_LTCCTR_FED_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_AIL_POS                 _GPTA0_UL(5)
#define GPTA0_LTCCTR_AIL_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_SLO_POS                 _GPTA0_UL(6)
#define GPTA0_LTCCTR_SLO_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_CUDCLR_POS              _GPTA0_UL(7)
#define GPTA0_LTCCTR_CUDCLR_LEN              _GPTA0_UL(1)


#define GPTA0_LTCCTR_ILM_POS                 _GPTA0_UL(8)
#define GPTA0_LTCCTR_ILM_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_CUD_POS                 _GPTA0_UL(9)
#define GPTA0_LTCCTR_CUD_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_CEN_POS                 _GPTA0_UL(10)
#define GPTA0_LTCCTR_CEN_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_OCM_POS                 _GPTA0_UL(11)
#define GPTA0_LTCCTR_OCM_LEN                 _GPTA0_UL(3)


#define GPTA0_LTCCTR_OIA_POS                 _GPTA0_UL(14)
#define GPTA0_LTCCTR_OIA_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_OUT_POS                 _GPTA0_UL(15)
#define GPTA0_LTCCTR_OUT_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_GBYP_POS                _GPTA0_UL(16)
#define GPTA0_LTCCTR_GBYP_LEN                _GPTA0_UL(1)


#define GPTA0_LTCCTR_BYP_POS                 _GPTA0_UL(6)
#define GPTA0_LTCCTR_BYP_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_EOA_POS                 _GPTA0_UL(7)
#define GPTA0_LTCCTR_EOA_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_SLL_POS                 _GPTA0_UL(9)
#define GPTA0_LTCCTR_SLL_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_SOL_POS                 _GPTA0_UL(4)
#define GPTA0_LTCCTR_SOL_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCCTR_SOH_POS                 _GPTA0_UL(5)
#define GPTA0_LTCCTR_SOH_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCXR_X_POS                    _GPTA0_UL(0)
#define GPTA0_LTCXR_X_LEN                    _GPTA0_UL(16)


#define GPTA0_LTCCTR_BRM_POS                 _GPTA0_UL(0)
#define GPTA0_LTCCTR_BRM_LEN                 _GPTA0_UL(1)


#define GPTA0_LTCXR_XS_POS                   _GPTA0_UL(16)
#define GPTA0_LTCXR_XS_LEN                   _GPTA0_UL(16)


#define GPTA0_EDCTR_GT00RUN_POS              _GPTA0_UL(0)
#define GPTA0_EDCTR_GT00RUN_LEN              _GPTA0_UL(1)


#define GPTA0_EDCTR_GT01RUN_POS              _GPTA0_UL(1)
#define GPTA0_EDCTR_GT01RUN_LEN              _GPTA0_UL(1)


#define GPTA0_EDCTR_GT10RUN_POS              _GPTA0_UL(2)
#define GPTA0_EDCTR_GT10RUN_LEN              _GPTA0_UL(1)


#define GPTA0_EDCTR_GT11RUN_POS              _GPTA0_UL(3)
#define GPTA0_EDCTR_GT11RUN_LEN              _GPTA0_UL(1)


#define GPTA0_EDCTR_G0EN_POS                 _GPTA0_UL(8)
#define GPTA0_EDCTR_G0EN_LEN                 _GPTA0_UL(1)


#define GPTA0_EDCTR_G1EN_POS                 _GPTA0_UL(9)
#define GPTA0_EDCTR_G1EN_LEN                 _GPTA0_UL(1)


#define GPTA0_EDCTR_L2EN_POS                 _GPTA0_UL(10)
#define GPTA0_EDCTR_L2EN_LEN                 _GPTA0_UL(1)


#define GPTA0_MMXCTR00_MUX0_POS              _GPTA0_UL(0)
#define GPTA0_MMXCTR00_MUX0_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX1_POS              _GPTA0_UL(2)
#define GPTA0_MMXCTR00_MUX1_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX2_POS              _GPTA0_UL(4)
#define GPTA0_MMXCTR00_MUX2_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX3_POS              _GPTA0_UL(6)
#define GPTA0_MMXCTR00_MUX3_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX4_POS              _GPTA0_UL(8)
#define GPTA0_MMXCTR00_MUX4_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX5_POS              _GPTA0_UL(10)
#define GPTA0_MMXCTR00_MUX5_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX6_POS              _GPTA0_UL(12)
#define GPTA0_MMXCTR00_MUX6_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX7_POS              _GPTA0_UL(14)
#define GPTA0_MMXCTR00_MUX7_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX8_POS              _GPTA0_UL(16)
#define GPTA0_MMXCTR00_MUX8_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX9_POS              _GPTA0_UL(18)
#define GPTA0_MMXCTR00_MUX9_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX10_POS             _GPTA0_UL(20)
#define GPTA0_MMXCTR00_MUX10_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX11_POS             _GPTA0_UL(22)
#define GPTA0_MMXCTR00_MUX11_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX12_POS             _GPTA0_UL(24)
#define GPTA0_MMXCTR00_MUX12_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX13_POS             _GPTA0_UL(26)
#define GPTA0_MMXCTR00_MUX13_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX14_POS             _GPTA0_UL(28)
#define GPTA0_MMXCTR00_MUX14_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR00_MUX15_POS             _GPTA0_UL(30)
#define GPTA0_MMXCTR00_MUX15_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX0_POS              _GPTA0_UL(0)
#define GPTA0_MMXCTR01_MUX0_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX1_POS              _GPTA0_UL(2)
#define GPTA0_MMXCTR01_MUX1_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX2_POS              _GPTA0_UL(4)
#define GPTA0_MMXCTR01_MUX2_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX3_POS              _GPTA0_UL(6)
#define GPTA0_MMXCTR01_MUX3_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX4_POS              _GPTA0_UL(8)
#define GPTA0_MMXCTR01_MUX4_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX5_POS              _GPTA0_UL(10)
#define GPTA0_MMXCTR01_MUX5_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX6_POS              _GPTA0_UL(12)
#define GPTA0_MMXCTR01_MUX6_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX7_POS              _GPTA0_UL(14)
#define GPTA0_MMXCTR01_MUX7_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX8_POS              _GPTA0_UL(16)
#define GPTA0_MMXCTR01_MUX8_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX9_POS              _GPTA0_UL(18)
#define GPTA0_MMXCTR01_MUX9_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX10_POS             _GPTA0_UL(20)
#define GPTA0_MMXCTR01_MUX10_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX11_POS             _GPTA0_UL(22)
#define GPTA0_MMXCTR01_MUX11_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX12_POS             _GPTA0_UL(24)
#define GPTA0_MMXCTR01_MUX12_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX13_POS             _GPTA0_UL(26)
#define GPTA0_MMXCTR01_MUX13_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX14_POS             _GPTA0_UL(28)
#define GPTA0_MMXCTR01_MUX14_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR01_MUX15_POS             _GPTA0_UL(30)
#define GPTA0_MMXCTR01_MUX15_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX0_POS              _GPTA0_UL(0)
#define GPTA0_MMXCTR10_MUX0_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX1_POS              _GPTA0_UL(2)
#define GPTA0_MMXCTR10_MUX1_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX2_POS              _GPTA0_UL(4)
#define GPTA0_MMXCTR10_MUX2_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX3_POS              _GPTA0_UL(6)
#define GPTA0_MMXCTR10_MUX3_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX4_POS              _GPTA0_UL(8)
#define GPTA0_MMXCTR10_MUX4_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX5_POS              _GPTA0_UL(10)
#define GPTA0_MMXCTR10_MUX5_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX6_POS              _GPTA0_UL(12)
#define GPTA0_MMXCTR10_MUX6_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX7_POS              _GPTA0_UL(14)
#define GPTA0_MMXCTR10_MUX7_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX8_POS              _GPTA0_UL(16)
#define GPTA0_MMXCTR10_MUX8_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX9_POS              _GPTA0_UL(18)
#define GPTA0_MMXCTR10_MUX9_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX10_POS             _GPTA0_UL(20)
#define GPTA0_MMXCTR10_MUX10_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX11_POS             _GPTA0_UL(22)
#define GPTA0_MMXCTR10_MUX11_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX12_POS             _GPTA0_UL(24)
#define GPTA0_MMXCTR10_MUX12_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX13_POS             _GPTA0_UL(26)
#define GPTA0_MMXCTR10_MUX13_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX14_POS             _GPTA0_UL(28)
#define GPTA0_MMXCTR10_MUX14_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR10_MUX15_POS             _GPTA0_UL(30)
#define GPTA0_MMXCTR10_MUX15_LEN             _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX0_POS              _GPTA0_UL(0)
#define GPTA0_MMXCTR11_MUX0_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX1_POS              _GPTA0_UL(2)
#define GPTA0_MMXCTR11_MUX1_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX2_POS              _GPTA0_UL(4)
#define GPTA0_MMXCTR11_MUX2_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX3_POS              _GPTA0_UL(6)
#define GPTA0_MMXCTR11_MUX3_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX4_POS              _GPTA0_UL(8)
#define GPTA0_MMXCTR11_MUX4_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX5_POS              _GPTA0_UL(10)
#define GPTA0_MMXCTR11_MUX5_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX6_POS              _GPTA0_UL(12)
#define GPTA0_MMXCTR11_MUX6_LEN              _GPTA0_UL(2)


#define GPTA0_MMXCTR11_MUX7_POS              _GPTA0_UL(14)
#define GPTA0_MMXCTR11_MUX7_LEN              _GPTA0_UL(2)


#define GPTA0_SRC37_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC37_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC37_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC37_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC37_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC37_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC37_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC37_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC37_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC37_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC37_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC37_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC36_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC36_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC36_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC36_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC36_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC36_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC36_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC36_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC36_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC36_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC36_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC36_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC35_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC35_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC35_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC35_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC35_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC35_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC35_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC35_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC35_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC35_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC35_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC35_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC34_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC34_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC34_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC34_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC34_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC34_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC34_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC34_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC34_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC34_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC34_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC34_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC33_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC33_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC33_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC33_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC33_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC33_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC33_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC33_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC33_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC33_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC33_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC33_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC32_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC32_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC32_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC32_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC32_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC32_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC32_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC32_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC32_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC32_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC32_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC32_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC31_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC31_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC31_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC31_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC31_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC31_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC31_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC31_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC31_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC31_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC31_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC31_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC30_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC30_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC30_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC30_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC30_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC30_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC30_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC30_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC30_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC30_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC30_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC30_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC29_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC29_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC29_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC29_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC29_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC29_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC29_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC29_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC29_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC29_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC29_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC29_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC28_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC28_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC28_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC28_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC28_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC28_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC28_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC28_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC28_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC28_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC28_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC28_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC27_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC27_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC27_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC27_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC27_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC27_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC27_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC27_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC27_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC27_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC27_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC27_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC26_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC26_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC26_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC26_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC26_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC26_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC26_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC26_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC26_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC26_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC26_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC26_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC25_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC25_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC25_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC25_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC25_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC25_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC25_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC25_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC25_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC25_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC25_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC25_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC24_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC24_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC24_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC24_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC24_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC24_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC24_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC24_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC24_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC24_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC24_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC24_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC23_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC23_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC23_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC23_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC23_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC23_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC23_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC23_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC23_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC23_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC23_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC23_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC22_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC22_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC22_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC22_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC22_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC22_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC22_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC22_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC22_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC22_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC22_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC22_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC21_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC21_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC21_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC21_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC21_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC21_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC21_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC21_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC21_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC21_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC21_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC21_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC20_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC20_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC20_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC20_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC20_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC20_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC20_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC20_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC20_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC20_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC20_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC20_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC19_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC19_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC19_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC19_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC19_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC19_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC19_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC19_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC19_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC19_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC19_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC19_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC18_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC18_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC18_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC18_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC18_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC18_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC18_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC18_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC18_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC18_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC18_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC18_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC17_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC17_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC17_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC17_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC17_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC17_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC17_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC17_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC17_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC17_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC17_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC17_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC16_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC16_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC16_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC16_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC16_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC16_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC16_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC16_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC16_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC16_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC16_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC16_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC15_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC15_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC15_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC15_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC15_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC15_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC15_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC15_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC15_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC15_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC15_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC15_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC14_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC14_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC14_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC14_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC14_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC14_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC14_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC14_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC14_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC14_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC14_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC14_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC13_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC13_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC13_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC13_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC13_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC13_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC13_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC13_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC13_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC13_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC13_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC13_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC12_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC12_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC12_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC12_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC12_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC12_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC12_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC12_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC12_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC12_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC12_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC12_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC11_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC11_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC11_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC11_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC11_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC11_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC11_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC11_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC11_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC11_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC11_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC11_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC10_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC10_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC10_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC10_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC10_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC10_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC10_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC10_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC10_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC10_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC10_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC10_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC09_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC09_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC09_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC09_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC09_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC09_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC09_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC09_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC09_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC09_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC09_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC09_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC08_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC08_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC08_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC08_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC08_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC08_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC08_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC08_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC08_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC08_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC08_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC08_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC07_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC07_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC07_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC07_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC07_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC07_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC07_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC07_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC07_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC07_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC07_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC07_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC06_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC06_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC06_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC06_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC06_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC06_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC06_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC06_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC06_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC06_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC06_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC06_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC05_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC05_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC05_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC05_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC05_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC05_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC05_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC05_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC05_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC05_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC05_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC05_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC04_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC04_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC04_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC04_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC04_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC04_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC04_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC04_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC04_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC04_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC04_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC04_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC03_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC03_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC03_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC03_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC03_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC03_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC03_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC03_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC03_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC03_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC03_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC03_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC02_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC02_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC02_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC02_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC02_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC02_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC02_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC02_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC02_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC02_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC02_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC02_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC01_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC01_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC01_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC01_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC01_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC01_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC01_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC01_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC01_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC01_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC01_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC01_SETR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC00_SRPN_POS                 _GPTA0_UL(0)
#define GPTA0_SRC00_SRPN_LEN                 _GPTA0_UL(8)


#define GPTA0_SRC00_TOS_POS                  _GPTA0_UL(10)
#define GPTA0_SRC00_TOS_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC00_SRE_POS                  _GPTA0_UL(12)
#define GPTA0_SRC00_SRE_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC00_SRR_POS                  _GPTA0_UL(13)
#define GPTA0_SRC00_SRR_LEN                  _GPTA0_UL(1)


#define GPTA0_SRC00_CLRR_POS                 _GPTA0_UL(14)
#define GPTA0_SRC00_CLRR_LEN                 _GPTA0_UL(1)


#define GPTA0_SRC00_SETR_POS                 _GPTA0_UL(15)
#define GPTA0_SRC00_SETR_LEN                 _GPTA0_UL(1)

#endif
