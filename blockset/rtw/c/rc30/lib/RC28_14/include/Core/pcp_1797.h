

#ifndef _PCP_1797_H
#define _PCP_1797_H


#ifdef REGDEF_FOR_PCP
    #define _PCP_UL(x) x
#else
    #define _PCP_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 RESERVED0[1];        
    volatile uint32 ID;                  
    volatile uint32 RESERVED1[1];        
    volatile uint32 CS;                  
    volatile uint32 ES;                  
    volatile uint32 RESERVED2[2];        
    volatile uint32 ICR;                 
    volatile uint32 ITR;                 
    volatile uint32 ICON;                
    volatile uint32 SSR;                 
    volatile uint32 RESERVED3[40];       
    volatile uint32 SRC11;               
    volatile uint32 SRC10;               
    volatile uint32 SRC9;                
    volatile uint32 SRC8;                
    volatile uint32 SRC7;                
    volatile uint32 SRC6;                
    volatile uint32 SRC5;                
    volatile uint32 SRC4;                
    volatile uint32 SRC3;                
    volatile uint32 SRC2;                
    volatile uint32 SRC1;                
    volatile uint32 SRC0;                
} PCP_RegMap_t;




extern PCP_RegMap_t PCP __attribute__ ((asection (".bss.label_only")));






#define PCP_R7_IEN_POS_NDT          5
#define PCP_R7_IEN_POS              _PCP_UL(5)
#define PCP_R7_IEN_LEN              _PCP_UL(1)


#define PCP_R7_CEN_POS_NDT          6
#define PCP_R7_CEN_POS              _PCP_UL(6)
#define PCP_R7_CEN_LEN              _PCP_UL(1)


#define PCP_R7_DPTR_POS_NDT         8
#define PCP_R7_DPTR_POS             _PCP_UL(8)
#define PCP_R7_DPTR_LEN             _PCP_UL(8)


#define PCP_R7_PC_POS               _PCP_UL(16)
#define PCP_R7_PC_LEN               _PCP_UL(16)



#define PCP_R6_CNT1_POS             _PCP_UL(0)
#define PCP_R6_CNT1_LEN             _PCP_UL(12)


#define PCP_R6_TOS_POS              _PCP_UL(14)
#define PCP_R6_TOS_LEN              _PCP_UL(2)


#define PCP_R6_SRPN_POS             _PCP_UL(16)
#define PCP_R6_SRPN_LEN             _PCP_UL(8)


#define PCP_R6_CPPN_POS_NDT         24
#define PCP_R6_CPPN_POS             _PCP_UL(24)
#define PCP_R6_CPPN_LEN             _PCP_UL(8)
#define PCP_R6_CPPN_MSK             (((_PCP_UL(1) << PCP_R6_CPPN_LEN) - _PCP_UL(1)) << PCP_R6_CPPN_POS)






#define PCP_ICR_ARBCYC_POS             PCP_ICR_PARBCYC_POS
#define PCP_ICR_ARBCYC_LEN             PCP_ICR_PARBCYC_LEN






#define PCP_ID_MOD_NUMBER_POS                _PCP_UL(16)
#define PCP_ID_MOD_NUMBER_LEN                _PCP_UL(16)


#define PCP_ID_MOD_REV_POS                   _PCP_UL(0)
#define PCP_ID_MOD_REV_LEN                   _PCP_UL(8)


#define PCP_ID_MOD_TYPE_POS                  _PCP_UL(8)
#define PCP_ID_MOD_TYPE_LEN                  _PCP_UL(8)


#define PCP_CLC_PCGDIS_POS                   _PCP_UL(15)
#define PCP_CLC_PCGDIS_LEN                   _PCP_UL(1)


#define PCP_ID_REVNUM_POS                    _PCP_UL(0)
#define PCP_ID_REVNUM_LEN                    _PCP_UL(8)


#define PCP_ID_ID32BIT_POS                   _PCP_UL(8)
#define PCP_ID_ID32BIT_LEN                   _PCP_UL(8)


#define PCP_ID_MODNUM_POS                    _PCP_UL(16)
#define PCP_ID_MODNUM_LEN                    _PCP_UL(16)


#define PCP_CS_EN_POS                        _PCP_UL(0)
#define PCP_CS_EN_LEN                        _PCP_UL(1)


#define PCP_CS_RST_POS                       _PCP_UL(1)
#define PCP_CS_RST_LEN                       _PCP_UL(1)


#define PCP_CS_RS_POS                        _PCP_UL(2)
#define PCP_CS_RS_LEN                        _PCP_UL(1)


#define PCP_CS_RCB_POS                       _PCP_UL(4)
#define PCP_CS_RCB_LEN                       _PCP_UL(1)


#define PCP_CS_EIE_POS                       _PCP_UL(5)
#define PCP_CS_EIE_LEN                       _PCP_UL(1)


#define PCP_CS_CS_POS                        _PCP_UL(6)
#define PCP_CS_CS_LEN                        _PCP_UL(2)


#define PCP_CS_PPE_POS                       _PCP_UL(8)
#define PCP_CS_PPE_LEN                       _PCP_UL(1)


#define PCP_CS_PPS_POS                       _PCP_UL(9)
#define PCP_CS_PPS_LEN                       _PCP_UL(7)


#define PCP_CS_CWE_POS                       _PCP_UL(16)
#define PCP_CS_CWE_LEN                       _PCP_UL(1)


#define PCP_CS_CWT_POS                       _PCP_UL(17)
#define PCP_CS_CWT_LEN                       _PCP_UL(7)


#define PCP_CS_ESR_POS                       _PCP_UL(24)
#define PCP_CS_ESR_LEN                       _PCP_UL(8)


#define PCP_ES_BER_POS                       _PCP_UL(0)
#define PCP_ES_BER_LEN                       _PCP_UL(1)


#define PCP_ES_IOP_POS                       _PCP_UL(1)
#define PCP_ES_IOP_LEN                       _PCP_UL(1)


#define PCP_ES_DCR_POS                       _PCP_UL(2)
#define PCP_ES_DCR_LEN                       _PCP_UL(1)


#define PCP_ES_IAE_POS                       _PCP_UL(3)
#define PCP_ES_IAE_LEN                       _PCP_UL(1)


#define PCP_ES_DBE_POS                       _PCP_UL(4)
#define PCP_ES_DBE_LEN                       _PCP_UL(1)


#define PCP_ES_ME_POS                        _PCP_UL(5)
#define PCP_ES_ME_LEN                        _PCP_UL(1)


#define PCP_ES_CWD_POS                       _PCP_UL(6)
#define PCP_ES_CWD_LEN                       _PCP_UL(1)


#define PCP_ES_PPC_POS                       _PCP_UL(7)
#define PCP_ES_PPC_LEN                       _PCP_UL(1)


#define PCP_ES_EPN_POS                       _PCP_UL(8)
#define PCP_ES_EPN_LEN                       _PCP_UL(8)


#define PCP_ES_EPC_POS                       _PCP_UL(16)
#define PCP_ES_EPC_LEN                       _PCP_UL(16)


#define PCP_ICR_CPPN_POS                     _PCP_UL(0)
#define PCP_ICR_CPPN_LEN                     _PCP_UL(8)


#define PCP_ICR_IE_POS                       _PCP_UL(8)
#define PCP_ICR_IE_LEN                       _PCP_UL(1)


#define PCP_ICR_PIPN_POS                     _PCP_UL(16)
#define PCP_ICR_PIPN_LEN                     _PCP_UL(8)


#define PCP_ICR_PARBCYC_POS                  _PCP_UL(24)
#define PCP_ICR_PARBCYC_LEN                  _PCP_UL(2)


#define PCP_ICR_PONECYC_POS                  _PCP_UL(26)
#define PCP_ICR_PONECYC_LEN                  _PCP_UL(1)


#define PCP_ITR_ITP_POS                      _PCP_UL(0)
#define PCP_ITR_ITP_LEN                      _PCP_UL(8)


#define PCP_ITR_ITL_POS                      _PCP_UL(16)
#define PCP_ITR_ITL_LEN                      _PCP_UL(4)


#define PCP_ICON_P0T_POS                     _PCP_UL(0)
#define PCP_ICON_P0T_LEN                     _PCP_UL(2)


#define PCP_ICON_P1T_POS                     _PCP_UL(2)
#define PCP_ICON_P1T_LEN                     _PCP_UL(2)


#define PCP_ICON_P2T_POS                     _PCP_UL(4)
#define PCP_ICON_P2T_LEN                     _PCP_UL(2)


#define PCP_ICON_P3T_POS                     _PCP_UL(6)
#define PCP_ICON_P3T_LEN                     _PCP_UL(2)


#define PCP_ICON_IP0E_POS                    _PCP_UL(8)
#define PCP_ICON_IP0E_LEN                    _PCP_UL(1)


#define PCP_ICON_IP1E_POS                    _PCP_UL(9)
#define PCP_ICON_IP1E_LEN                    _PCP_UL(1)


#define PCP_ICON_IP2E_POS                    _PCP_UL(10)
#define PCP_ICON_IP2E_LEN                    _PCP_UL(1)


#define PCP_ICON_IP3E_POS                    _PCP_UL(11)
#define PCP_ICON_IP3E_LEN                    _PCP_UL(1)


#define PCP_SSR_SSRN_POS                     _PCP_UL(0)
#define PCP_SSR_SSRN_LEN                     _PCP_UL(8)


#define PCP_SSR_STOS_POS                     _PCP_UL(8)
#define PCP_SSR_STOS_LEN                     _PCP_UL(2)


#define PCP_SSR_ST_POS                       _PCP_UL(15)
#define PCP_SSR_ST_LEN                       _PCP_UL(1)


#define PCP_SSR_SCHN_POS                     _PCP_UL(16)
#define PCP_SSR_SCHN_LEN                     _PCP_UL(8)


#define PCP_SRC11_SRPN_POS                   _PCP_UL(0)
#define PCP_SRC11_SRPN_LEN                   _PCP_UL(8)


#define PCP_SRC11_TOS_POS                    _PCP_UL(10)
#define PCP_SRC11_TOS_LEN                    _PCP_UL(2)


#define PCP_SRC11_SRE_POS                    _PCP_UL(12)
#define PCP_SRC11_SRE_LEN                    _PCP_UL(1)


#define PCP_SRC11_SRR_POS                    _PCP_UL(13)
#define PCP_SRC11_SRR_LEN                    _PCP_UL(1)


#define PCP_SRC11_SRCN_POS                   _PCP_UL(16)
#define PCP_SRC11_SRCN_LEN                   _PCP_UL(8)


#define PCP_SRC11_RRQ_POS                    _PCP_UL(28)
#define PCP_SRC11_RRQ_LEN                    _PCP_UL(1)


#define PCP_SRC10_SRPN_POS                   _PCP_UL(0)
#define PCP_SRC10_SRPN_LEN                   _PCP_UL(8)


#define PCP_SRC10_TOS_POS                    _PCP_UL(10)
#define PCP_SRC10_TOS_LEN                    _PCP_UL(2)


#define PCP_SRC10_SRE_POS                    _PCP_UL(12)
#define PCP_SRC10_SRE_LEN                    _PCP_UL(1)


#define PCP_SRC10_SRR_POS                    _PCP_UL(13)
#define PCP_SRC10_SRR_LEN                    _PCP_UL(1)


#define PCP_SRC10_SRCN_POS                   _PCP_UL(16)
#define PCP_SRC10_SRCN_LEN                   _PCP_UL(8)


#define PCP_SRC10_RRQ_POS                    _PCP_UL(28)
#define PCP_SRC10_RRQ_LEN                    _PCP_UL(1)


#define PCP_SRC9_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC9_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC9_TOS_POS                     _PCP_UL(10)
#define PCP_SRC9_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC9_SRE_POS                     _PCP_UL(12)
#define PCP_SRC9_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC9_SRR_POS                     _PCP_UL(13)
#define PCP_SRC9_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC9_SRCN_POS                    _PCP_UL(16)
#define PCP_SRC9_SRCN_LEN                    _PCP_UL(8)


#define PCP_SRC9_RRQ_POS                     _PCP_UL(28)
#define PCP_SRC9_RRQ_LEN                     _PCP_UL(1)


#define PCP_SRC8_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC8_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC8_TOS_POS                     _PCP_UL(10)
#define PCP_SRC8_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC8_SRE_POS                     _PCP_UL(12)
#define PCP_SRC8_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC8_SRR_POS                     _PCP_UL(13)
#define PCP_SRC8_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC7_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC7_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC7_TOS_POS                     _PCP_UL(10)
#define PCP_SRC7_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC7_SRE_POS                     _PCP_UL(12)
#define PCP_SRC7_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC7_SRR_POS                     _PCP_UL(13)
#define PCP_SRC7_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC6_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC6_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC6_TOS_POS                     _PCP_UL(10)
#define PCP_SRC6_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC6_SRE_POS                     _PCP_UL(12)
#define PCP_SRC6_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC6_SRR_POS                     _PCP_UL(13)
#define PCP_SRC6_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC5_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC5_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC5_TOS_POS                     _PCP_UL(10)
#define PCP_SRC5_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC5_SRE_POS                     _PCP_UL(12)
#define PCP_SRC5_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC5_SRR_POS                     _PCP_UL(13)
#define PCP_SRC5_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC4_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC4_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC4_TOS_POS                     _PCP_UL(10)
#define PCP_SRC4_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC4_SRE_POS                     _PCP_UL(12)
#define PCP_SRC4_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC4_SRR_POS                     _PCP_UL(13)
#define PCP_SRC4_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC3_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC3_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC3_TOS_POS                     _PCP_UL(10)
#define PCP_SRC3_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC3_SRE_POS                     _PCP_UL(12)
#define PCP_SRC3_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC3_SRR_POS                     _PCP_UL(13)
#define PCP_SRC3_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC2_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC2_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC2_TOS_POS                     _PCP_UL(10)
#define PCP_SRC2_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC2_SRE_POS                     _PCP_UL(12)
#define PCP_SRC2_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC2_SRR_POS                     _PCP_UL(13)
#define PCP_SRC2_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC1_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC1_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC1_TOS_POS                     _PCP_UL(10)
#define PCP_SRC1_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC1_SRE_POS                     _PCP_UL(12)
#define PCP_SRC1_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC1_SRR_POS                     _PCP_UL(13)
#define PCP_SRC1_SRR_LEN                     _PCP_UL(1)


#define PCP_SRC0_SRPN_POS                    _PCP_UL(0)
#define PCP_SRC0_SRPN_LEN                    _PCP_UL(8)


#define PCP_SRC0_TOS_POS                     _PCP_UL(10)
#define PCP_SRC0_TOS_LEN                     _PCP_UL(2)


#define PCP_SRC0_SRE_POS                     _PCP_UL(12)
#define PCP_SRC0_SRE_LEN                     _PCP_UL(1)


#define PCP_SRC0_SRR_POS                     _PCP_UL(13)
#define PCP_SRC0_SRR_LEN                     _PCP_UL(1)

#endif
