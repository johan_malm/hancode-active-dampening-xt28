

#ifndef _MEMLAY_AUTO_CONF_H
#define _MEMLAY_AUTO_CONF_H
#ifndef _LIBRARYABILITY_H




#include "memlay_pub.h"



#define MEMLAY_TPROT_CFG_REF ((void*)(uint32)&Tprot_Config_cs)




#ifdef LINK_RUN
   #define _MEMLAY_UL(x) x
#else
   #define _MEMLAY_UL(x) x##UL
#endif


#define MEMLAY_SIZE_OF_EPILOG       _MEMLAY_UL(0x90)

#define MEMLAY_DCACHE_REG           _MEMLAY_UL(0x680)
#define MEMLAY_ICACHE_REG           _MEMLAY_UL(0x283)
#define MEMLAY_CACHE_INIT()         MemLay_CacheInit()


#define MEMLAY_EDRAM_START          _MEMLAY_UL(0xAFF00000)
#define MEMLAY_EDRAM_END            _MEMLAY_UL(0xAFF7FFFF)

#define MEMLAY_DS0_LINK_OFFSET   _MEMLAY_UL(0x0)


#define MEMLAY_DATASET_LINK_OFFSET _MEMLAY_UL(0x0)

#define MEMLAY_CB_START             _MEMLAY_UL(0x80010000)
#define MEMLAY_CB_END               _MEMLAY_UL(0x80013F03)


#define MEMLAY_ASW0_START           _MEMLAY_UL(0x80020000)
#define MEMLAY_ASW0_END             _MEMLAY_UL(0x802FFFFF)


#define MEMLAY_DS0_START            _MEMLAY_UL(0x80300000)
#define MEMLAY_DS0_END              _MEMLAY_UL(0x803FFFFF)


#define MEMLAY_TPROT_START          _MEMLAY_UL(0x80014000)
#define MEMLAY_TPROT_END            _MEMLAY_UL(0x80017FFF)


#define MEMLAY_SB_START             _MEMLAY_UL(0x80018000)
#define MEMLAY_SB_END               _MEMLAY_UL(0x8001FFFF)



#define MEMLAY_ICACHE_START         _MEMLAY_UL(0xC0008000)
#define MEMLAY_ICACHE_END           _MEMLAY_UL(0xC0009FFF)


#define MEMLAY_PMU_OLDA_START       _MEMLAY_UL(0xAFE70000)
#define MEMLAY_PMU_OLDA_END         _MEMLAY_UL(0xAFE77FFF)


#define MEMLAY_PMU_OVRAM_START      _MEMLAY_UL(0xAFE80000)
#define MEMLAY_PMU_OVRAM_END        _MEMLAY_UL(0xAFE81FFF)


#define MEMLAY_PMI_SPRAM_START      _MEMLAY_UL(0xC0000000)
#define MEMLAY_PMI_SPRAM_END        _MEMLAY_UL(0xC0007FFF)


#define MEMLAY_DMI_RAM_START        _MEMLAY_UL(0xD0000000)
#define MEMLAY_DMI_RAM_END          _MEMLAY_UL(0xD0019FFF)


#define MEMLAY_PCP_PRAM_START       _MEMLAY_UL(0xF0050000)
#define MEMLAY_PCP_PRAM_END         _MEMLAY_UL(0xF0051FFF)


#define MEMLAY_PCP_PCODE_START      _MEMLAY_UL(0xF0060000)
#define MEMLAY_PCP_PCODE_END        _MEMLAY_UL(0xF0063FFF)

#define MEMLAY_ENVRAM_MAXSIZE       _MEMLAY_UL(0x800)
#define MEMLAY_NUM_PROT_RAM_INFO    _MEMLAY_UL(3)
#define MEMLAY_NUM_RAM_INFO         _MEMLAY_UL(5)
#define MEMLAY_NUM_CSA              _MEMLAY_UL(0x50)


#ifdef LINK_RUN
#define MEMLAY_LINK_LOCATE_PROLOG

#define MEMLAY_LINK_LOCATE_EPILOG\
    _. = ASSERT (MEMLAY_OFFSET_NONCACHED == 0x20000000,\
                 "MEMLAY_OFFSET_NONCACHED must be 0x20000000. Do not overwrite this in locate.inv!") ;
#endif


#ifndef LINK_RUN

MEMLAY_USE_ASW_NONCACHED(extern void MemLay_CacheInit(void));

__PRAGMA_USE__eos__50ms_1s__constant__s32__START
extern const MemLay_RamInfo_t MemLay_ProtRamInfo_cas[MEMLAY_NUM_PROT_RAM_INFO];
extern const MemLay_RamInfo_t MemLay_RamInfo_cas[MEMLAY_NUM_RAM_INFO];
__PRAGMA_USE__eos__50ms_1s__constant__s32__END


extern uint32 MemLay_CSA_u32[MEMLAY_NUM_CSA * 16UL] \
    __attribute__ ((asection(".userCSA_a64")));
#endif



#endif

#endif
