

#ifndef _LBCU_1797_H
#define _LBCU_1797_H


#ifdef REGDEF_FOR_PCP
    #define _LBCU_UL(x) x
#else
    #define _LBCU_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[5];        
    volatile uint32 LEATT;               
    volatile uint32 LEADDR;              
    volatile uint32 LEDATL;              
    volatile uint32 LEDATH;              
    volatile uint32 RESERVED1[51];       
    volatile uint32 SRC;                 
} LBCU_RegMap_t;




extern LBCU_RegMap_t LBCU __attribute__ ((asection (".bss.label_only")));





#define LBCU_ID_MOD_NUMBER_POS               _LBCU_UL(16)
#define LBCU_ID_MOD_NUMBER_LEN               _LBCU_UL(16)


#define LBCU_ID_MOD_REV_POS                  _LBCU_UL(0)
#define LBCU_ID_MOD_REV_LEN                  _LBCU_UL(8)


#define LBCU_ID_MOD_TYPE_POS                 _LBCU_UL(8)
#define LBCU_ID_MOD_TYPE_LEN                 _LBCU_UL(8)


#define LBCU_ID_MOD_REV_POS                  _LBCU_UL(0)
#define LBCU_ID_MOD_REV_LEN                  _LBCU_UL(8)


#define LBCU_ID_MOD_TYPE_POS                 _LBCU_UL(8)
#define LBCU_ID_MOD_TYPE_LEN                 _LBCU_UL(8)


#define LBCU_ID_MOD_NUMBER_POS               _LBCU_UL(16)
#define LBCU_ID_MOD_NUMBER_LEN               _LBCU_UL(16)


#define LBCU_LEATT_LEC_POS                   _LBCU_UL(0)
#define LBCU_LEATT_LEC_LEN                   _LBCU_UL(1)


#define LBCU_LEATT_FPITAG_POS                _LBCU_UL(4)
#define LBCU_LEATT_FPITAG_LEN                _LBCU_UL(4)


#define LBCU_LEATT_NOS_POS                   _LBCU_UL(14)
#define LBCU_LEATT_NOS_LEN                   _LBCU_UL(1)


#define LBCU_LEATT_LOC_POS                   _LBCU_UL(15)
#define LBCU_LEATT_LOC_LEN                   _LBCU_UL(1)


#define LBCU_LEATT_ACK_POS                   _LBCU_UL(16)
#define LBCU_LEATT_ACK_LEN                   _LBCU_UL(3)


#define LBCU_LEATT_UIS_POS                   _LBCU_UL(19)
#define LBCU_LEATT_UIS_LEN                   _LBCU_UL(1)


#define LBCU_LEATT_SVM_POS                   _LBCU_UL(21)
#define LBCU_LEATT_SVM_LEN                   _LBCU_UL(1)


#define LBCU_LEATT_WR_POS                    _LBCU_UL(22)
#define LBCU_LEATT_WR_LEN                    _LBCU_UL(1)


#define LBCU_LEATT_RD_POS                    _LBCU_UL(23)
#define LBCU_LEATT_RD_LEN                    _LBCU_UL(1)


#define LBCU_LEATT_TAG_POS                   _LBCU_UL(24)
#define LBCU_LEATT_TAG_LEN                   _LBCU_UL(3)


#define LBCU_LEATT_OPC_POS                   _LBCU_UL(28)
#define LBCU_LEATT_OPC_LEN                   _LBCU_UL(4)


#define LBCU_LEADDR_LEADDR_POS               _LBCU_UL(0)
#define LBCU_LEADDR_LEADDR_LEN               _LBCU_UL(32)


#define LBCU_LEDATL_LEDAT_31_0_POS           _LBCU_UL(0)
#define LBCU_LEDATL_LEDAT_31_0_LEN           _LBCU_UL(32)


#define LBCU_LEDATH_LEDAT_63_32_POS          _LBCU_UL(0)
#define LBCU_LEDATH_LEDAT_63_32_LEN          _LBCU_UL(32)


#define LBCU_SRC_SRPN_POS                    _LBCU_UL(0)
#define LBCU_SRC_SRPN_LEN                    _LBCU_UL(8)


#define LBCU_SRC_TOS_POS                     _LBCU_UL(10)
#define LBCU_SRC_TOS_LEN                     _LBCU_UL(2)


#define LBCU_SRC_SRE_POS                     _LBCU_UL(12)
#define LBCU_SRC_SRE_LEN                     _LBCU_UL(1)


#define LBCU_SRC_SRR_POS                     _LBCU_UL(13)
#define LBCU_SRC_SRR_LEN                     _LBCU_UL(1)


#define LBCU_SRC_CLRR_POS                    _LBCU_UL(14)
#define LBCU_SRC_CLRR_LEN                    _LBCU_UL(1)


#define LBCU_SRC_SETR_POS                    _LBCU_UL(15)
#define LBCU_SRC_SETR_LEN                    _LBCU_UL(1)

#endif
