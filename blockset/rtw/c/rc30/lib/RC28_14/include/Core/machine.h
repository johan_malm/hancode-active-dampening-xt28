

#ifndef _MACHINE_H
#define _MACHINE_H








#ifdef LINK_RUN
  #define DEF_CONST(x) x
#else
  #define DEF_CONST(x) x##UL
#endif



#define TC_1796                        2
#define TC_1766                        3
#define TC_1767                        4
#define TC_1797                        5
#define TC_1724                        8
#define TC_1792                        21
#define TC_1764                        31




#ifndef DECLARE_MACHINE_TYPE_EXTERNAL
#define MACHINE_TYPE                   TC_1797
#define MACHINE_SUBTYPE                TC_1797

#endif



#define MACHINE_CHID_TC_1796           0x8A
#define MACHINE_CHID_TC_1766           0x8B
#define MACHINE_CHID_TC_1767           0x90
#define MACHINE_CHID_TC_1797           0x91
#define MACHINE_CHID_TC_1792           0x8A
#define MACHINE_CHID_TC_1764           0x8B




#define LITTLE_ENDIAN




#if (MACHINE_TYPE == TC_1796)






#define MEM_ROM_PFLASH_START        DEF_CONST(0x80000000)
#define MEM_ROM_PFLASH_END          DEF_CONST(0x801FFFFF)


#define MEM_ROM_DFLASH1_START       DEF_CONST(0x8FE00000)
#define MEM_ROM_DFLASH1_END         DEF_CONST(0x8FE0FFFF)
#define MEM_ROM_DFLASH2_START       DEF_CONST(0x8FE10000)
#define MEM_ROM_DFLASH2_END         DEF_CONST(0x8FE1FFFF)

#if (MACHINE_SUBTYPE == TC_1792)





#define MEM_RAM_PCPCODE_START       DEF_CONST(0xF0060000)
#define MEM_RAM_PCPCODE_END         DEF_CONST(0xF0063FFF)

#define MEM_RAM_PCPDATA_START       DEF_CONST(0xF0050000)
#define MEM_RAM_PCPDATA_END         DEF_CONST(0xF0051FFF)





#define MEM_RAM_DMUSRAM_START       DEF_CONST(0xC0000000)
#define MEM_RAM_DMUSRAM_END         DEF_CONST(0xC0003FFF)

#define MEM_RAM_DMUSBRAM_START      DEF_CONST(0xC03FC000)
#define MEM_RAM_DMUSBRAM_END        DEF_CONST(0xC03FFFFF)





#define MEM_RAM_DMILDRAM_START      DEF_CONST(0xD0000000)
#define MEM_RAM_DMILDRAM_END        DEF_CONST(0xD000DFFF)











#define MEM_RAM_DMIDPRAM_START      DEF_CONST(0xD000E000)
#define MEM_RAM_DMIDPRAM_END        DEF_CONST(0xD000FFFF)





#define MEM_RAM_PMISPRAM_START      DEF_CONST(0xD4000000)
#define MEM_RAM_PMISPRAM_END        DEF_CONST(0xD4005FFF)





#define MEM_RAM_PMUOVRAM_START      DEF_CONST(0x00000000)
#define MEM_RAM_PMUOVRAM_END        DEF_CONST(0x00000000)

#else





#define MEM_RAM_PCPCODE_START       DEF_CONST(0xF0060000)
#define MEM_RAM_PCPCODE_END         DEF_CONST(0xF0067FFF)

#define MEM_RAM_PCPDATA_START       DEF_CONST(0xF0050000)
#define MEM_RAM_PCPDATA_END         DEF_CONST(0xF0053FFF)





#define MEM_RAM_DMUSRAM_START       DEF_CONST(0xC0000000)
#define MEM_RAM_DMUSRAM_END         DEF_CONST(0xC000FFFF)

#define MEM_RAM_DMUSBRAM_START      DEF_CONST(0xC03FC000)
#define MEM_RAM_DMUSBRAM_END        DEF_CONST(0xC03FFFFF)





#define MEM_RAM_DMILDRAM_START      DEF_CONST(0xD0000000)
#define MEM_RAM_DMILDRAM_END        DEF_CONST(0xD000DFFF)

#define MEM_RAM_DMIDPRAM_START      DEF_CONST(0xD000E000)
#define MEM_RAM_DMIDPRAM_END        DEF_CONST(0xD000FFFF)





#define MEM_RAM_PMISPRAM_START      DEF_CONST(0xD4000000)
#define MEM_RAM_PMISPRAM_END        DEF_CONST(0xD400BFFF)





#define MEM_RAM_PMUOVRAM_START      DEF_CONST(0x00000000)
#define MEM_RAM_PMUOVRAM_END        DEF_CONST(0x00000000)


#endif





#define MEM_RAM_EXTERN_START        DEF_CONST(0x82000000)


#define MEM_ROM_EXTERN_START        DEF_CONST(0x80800000)

#ifdef LINK_RUN








  ADC0       = 0xF0100400;
  ADC1       = 0xF0100600;

  ASC0       = 0xF0000A00;
  ASC1       = 0xF0000B00;

  CAN        = 0xF0004000;

  CBS        = 0xF0000400;

  CORE       = 0xF7E1FD00;

  CPS        = 0xF7E0FF00;

  CSFR       = 0xF7E1FE00;

  DBCU       = 0xF87FFA00;

  DMA        = 0xF0003C00;

  DMI        = 0xF87FFC00;

  DMU        = 0xF8010100;

  EBU        = 0xF8000000;

  FADC       = 0xF0100300;

  FLASH      = 0xF8002000;

  GPR        = 0xF7E1FF00;

  GPTA0      = 0xF0001800;
  GPTA1      = 0xF0002000;

  LFI        = 0xF87FFF00;
#if (MACHINE_SUBTYPE != TC_1792)

  LTCA2      = 0xF0002800;
#endif

  MCHK       = 0xF010C200;

  MLI0       = 0xF010C000;
  MLI1       = 0xF010C100;

  MPR        = 0xF7E1C000;

  MSC0       = 0xF0000800;
#if (MACHINE_SUBTYPE != TC_1792)
  MSC1       = 0xF0000900;
#endif

  P0         = 0xF0000C00;
  P1         = 0xF0000D00;
  P10        = 0xF0001600;
  P2         = 0xF0000E00;
  P3         = 0xF0000F00;
  P4         = 0xF0001000;
  P5         = 0xF0001100;
  P6         = 0xF0001200;
  P7         = 0xF0001300;
  P8         = 0xF0001400;
  P9         = 0xF0001500;

  PBCU       = 0xF87FFE00;

  PCP        = 0xF0043F00;

  PMI        = 0xF87FFD00;

  PMU        = 0xF8000500;

  RBCU       = 0xF0100000;

  SBCU       = 0xF0000100;

  SCU        = 0xF0000000;

  SSC0       = 0xF0100100;
  SSC1       = 0xF0100200;

  STM        = 0xF0000200;


  SC_INT_ADR                = (CPS + 0xFC);
  STM_BASE_ADR              = STM;
  SCU_BASE_ADR              = SCU;
  ALARM_SRC_ADR             = (STM + 0xF8);
  TIMETABLE_SRC_ADR         = (STM + 0xFC);
  SYSTEM_TIME_REGISTER_TC   = (STM + 0x10);

#endif

#elif (MACHINE_TYPE == TC_1766)

#if (MACHINE_SUBTYPE == TC_1764)





#define MEM_ROM_PFLASH_START        DEF_CONST(0x80000000)
#define MEM_ROM_PFLASH_END          DEF_CONST(0x800FFFFF)

#define MEM_ROM_DFLASH1_START       DEF_CONST(0x8FE00000)
#define MEM_ROM_DFLASH1_END         DEF_CONST(0x8FE01FFF)
#define MEM_ROM_DFLASH2_START       DEF_CONST(0x8FE10000)
#define MEM_ROM_DFLASH2_END         DEF_CONST(0x8FE11FFF)





#define MEM_RAM_PCPCODE_START       DEF_CONST(0xF0060000)
#define MEM_RAM_PCPCODE_END         DEF_CONST(0xF0061FFF)

#define MEM_RAM_PCPDATA_START       DEF_CONST(0xF0050000)
#define MEM_RAM_PCPDATA_END         DEF_CONST(0xF00507FF)





#define MEM_RAM_DMUSRAM_START       DEF_CONST(0x00000000)
#define MEM_RAM_DMUSRAM_END         DEF_CONST(0x00000000)

#define MEM_RAM_DMUSBRAM_START      DEF_CONST(0x00000000)
#define MEM_RAM_DMUSBRAM_END        DEF_CONST(0x00000000)





#define MEM_RAM_DMILDRAM_START      DEF_CONST(0xD0000000)
#define MEM_RAM_DMILDRAM_END        DEF_CONST(0xD0007FFF)

#define MEM_RAM_DMIDPRAM_START      DEF_CONST(0x00000000)
#define MEM_RAM_DMIDPRAM_END        DEF_CONST(0x00000000)





#define MEM_RAM_PMISPRAM_START      DEF_CONST(0xD4000000)
#define MEM_RAM_PMISPRAM_END        DEF_CONST(0xD4001FFF)





#define MEM_RAM_PMUOVRAM_START      DEF_CONST(0xC0000000)
#define MEM_RAM_PMUOVRAM_END        DEF_CONST(0xC0001FFF)

#else





#define MEM_ROM_PFLASH_START        DEF_CONST(0x80000000)
#define MEM_ROM_PFLASH_END          DEF_CONST(0x80177FFF)

#define MEM_ROM_DFLASH1_START       DEF_CONST(0x8FE00000)
#define MEM_ROM_DFLASH1_END         DEF_CONST(0x8FE03FFF)
#define MEM_ROM_DFLASH2_START       DEF_CONST(0x8FE10000)
#define MEM_ROM_DFLASH2_END         DEF_CONST(0x8FE13FFF)





#define MEM_RAM_PCPCODE_START       DEF_CONST(0xF0060000)
#define MEM_RAM_PCPCODE_END         DEF_CONST(0xF0062FFF)

#define MEM_RAM_PCPDATA_START       DEF_CONST(0xF0050000)
#define MEM_RAM_PCPDATA_END         DEF_CONST(0xF0051FFF)





#define MEM_RAM_DMUSRAM_START       DEF_CONST(0x00000000)
#define MEM_RAM_DMUSRAM_END         DEF_CONST(0x00000000)

#define MEM_RAM_DMUSBRAM_START      DEF_CONST(0x00000000)
#define MEM_RAM_DMUSBRAM_END        DEF_CONST(0x00000000)





#define MEM_RAM_DMILDRAM_START      DEF_CONST(0xD0000000)
#define MEM_RAM_DMILDRAM_END        DEF_CONST(0xD000DFFF)

#define MEM_RAM_DMIDPRAM_START      DEF_CONST(0x00000000)
#define MEM_RAM_DMIDPRAM_END        DEF_CONST(0x00000000)





#define MEM_RAM_PMISPRAM_START      DEF_CONST(0xD4000000)
#define MEM_RAM_PMISPRAM_END        DEF_CONST(0xD4003FFF)





#define MEM_RAM_PMUOVRAM_START      DEF_CONST(0xC0000000)
#define MEM_RAM_PMUOVRAM_END        DEF_CONST(0xC0001FFF)


#endif





#define MEM_RAM_EXTERN_START        DEF_CONST(0x00000000)


#define MEM_ROM_EXTERN_START        DEF_CONST(0x00000000)


#define MEM_RAM_EMULATION_START     DEF_CONST(0x8FF20000)
#define MEM_RAM_EMULATION_END       DEF_CONST(0x8FF5FFFF)

#ifdef LINK_RUN








  ADC0       = 0xF0100400;

  ASC0       = 0xF0000A00;
  ASC1       = 0xF0000B00;

  CAN        = 0xF0004000;

  CBS        = 0xF0000400;

  CORE       = 0xF7E1FD00;

  CPS        = 0xF7E0FF00;

  CSFR       = 0xF7E1FE00;

  DMA        = 0xF0003C00;

  DMI        = 0xF87FFC00;

  FADC       = 0xF0100300;

  FLASH      = 0xF8002000;

  GPR        = 0xF7E1FF00;

  GPTA0      = 0xF0001800;

  LBCU       = 0xF87FFE00;

  LFI        = 0xF87FFF00;

  MCHK       = 0xF010C200;

  MLI0       = 0xF010C000;












  MLI1       = 0xF010C100;

  MPR        = 0xF7E1C000;

  MSC0       = 0xF0000800;

  P0         = 0xF0000C00;
  P1         = 0xF0000D00;
  P2         = 0xF0000E00;
  P3         = 0xF0000F00;
  P4         = 0xF0001000;
  P5         = 0xF0001100;

  PCP        = 0xF0043F00;

  PMI        = 0xF87FFD00;

  PMU        = 0xF8000500;

  SBCU       = 0xF0000100;

  SCU        = 0xF0000000;

  SSC0       = 0xF0100100;
#if (MACHINE_SUBTYPE != TC_1764)
  SSC1       = 0xF0100200;
#endif

  STM        = 0xF0000200;


  SC_INT_ADR                = (CPS + 0xFC);
  STM_BASE_ADR              = STM;
  SCU_BASE_ADR              = SCU;
  ALARM_SRC_ADR             = (STM + 0xF8);
  TIMETABLE_SRC_ADR         = (STM + 0xFC);
  SYSTEM_TIME_REGISTER_TC   = STM + 0x10;

#endif


#elif (MACHINE_TYPE == TC_1767)






#define MEM_ROM_PFLASH_START        DEF_CONST(0x80000000)
#define MEM_ROM_PFLASH_END          DEF_CONST(0x801FFFFF)


#define MEM_ROM_DFLASH1_START       DEF_CONST(0x8FE00000)
#define MEM_ROM_DFLASH1_END         DEF_CONST(0x8FE07FFF)
#define MEM_ROM_DFLASH2_START       DEF_CONST(0x8FE10000)
#define MEM_ROM_DFLASH2_END         DEF_CONST(0x8FE17FFF)

#ifdef LINK_RUN








  ADC        = 0xF0101000;
  ADC0       = 0xF0101000;
  ADC1       = 0xF0101400;

  ASC        = 0xF0000A00;
  ASC0       = 0xF0000A00;
  ASC1       = 0xF0000B00;

  CAN        = 0xF0004000;

  CBS        = 0xF0000464;

  CORE       = 0xF7E1FD00;

  CPS          = 0xF7E0FF08;
  CPS_CPU_SRC0 = 0xF7E0FFFC;

  CPU        = 0xF7E19004;

  CSFR       = 0xF7E1FE00;

  DMA        = 0xF0003C00;

  DMI        = 0xF87FFC08;

  EBU        = 0xF8000000;

  FADC       = 0xF0100400;

  FLASH0     = 0xF8002008;

  FPU        = 0xF7E1A000;

  GPR        = 0xF7E1FF00;

  GPTA0      = 0xF0001800;

  LBCU       = 0xF87FFE08;

  LFI        = 0xF87FFF08;

  LTCA2      = 0xF0002808;

  MCHK       = 0xF010C208;

  MLI0        = 0xF010C008;

  MMU        = 0xF7E18000;

  MPR        = 0xF7E1C000;

  MSC0        = 0xF0000800;

  OVC        = 0xF87FFB20;

  P0         = 0xF0000C00;
  P1         = 0xF0000D00;
  P2         = 0xF0000E00;
  P3         = 0xF0000F00;
  P4         = 0xF0001000;
  P5         = 0xF0001100;
  P6         = 0xF0001200;
  P7         = 0xF0001300;
  P8         = 0xF0001400;
  P9         = 0xF0001500;

  PCP        = 0xF0043F00;

  PMI        = 0xF87FFD08;

  PMU        = 0xF8000508;

  SBCU       = 0xF0000108;

  SCU             = 0xF0000508;
  SCU_WDT_ERCOSEK = 0xF00005F0 - 0x20;


  SSC        = 0xF0100100;
  SSC0       = 0xF0100100;
  SSC1       = 0xF0100200;

  STM        = 0xF0000200;
  STM_TIM0   = 0xF0000210;
  STM_SRC1   = 0xF00002F8;
  STM_SRC0   = 0xF00002FC;


  SC_INT_ADR                = CPS_CPU_SRC0;
  SCU_BASE_ADR              = SCU_WDT_ERCOSEK;
  STM_BASE_ADR              = STM;
  ALARM_SRC_ADR             = STM_SRC1;
  TIMETABLE_SRC_ADR         = STM_SRC0;
  SYSTEM_TIME_REGISTER_TC   = STM_TIM0;

#endif


#elif (MACHINE_TYPE == TC_1797)






#define MEM_ROM_PFLASH0_START       DEF_CONST(0x80000000)
#define MEM_ROM_PFLASH0_END         DEF_CONST(0x801FFFFF)
#define MEM_ROM_PFLASH1_START       DEF_CONST(0x80200000)
#define MEM_ROM_PFLASH1_END         DEF_CONST(0x803FFFFF)
#define MEM_ROM_PFLASH_START        MEM_ROM_PFLASH0_START
#define MEM_ROM_PFLASH_END          MEM_ROM_PFLASH1_END


#define MEM_ROM_DFLASH1_START       DEF_CONST(0x8FE00000)
#define MEM_ROM_DFLASH1_END         DEF_CONST(0x8FE07FFF)
#define MEM_ROM_DFLASH2_START       DEF_CONST(0x8FE10000)
#define MEM_ROM_DFLASH2_END         DEF_CONST(0x8FE17FFF)


#ifdef LINK_RUN








  ADC        = 0xF0101000;
  ADC0       = 0xF0101000;
  ADC1       = 0xF0101400;
  ADC2       = 0xF0101800;

  ASC        = 0xF0000A00;
  ASC0       = 0xF0000A00;
  ASC1       = 0xF0000B00;

  CAN        = 0xF0004000;

  CBS        = 0xF0000464;

  CORE       = 0xF7E1FD00;

  CPS        = 0xF7E0FF08;
  CPS_CPU_SRC0 = 0xF7E0FFFC;

  CPU        = 0xF7E19004;

  CSFR       = 0xF7E1FE00;

  DMA        = 0xF0003C00;

  DMI        = 0xF87FFC08;

  EBU        = 0xF8000000;

  ERAY       = 0xF0010000;

  FADC       = 0xF0100400;

  FLASH      = 0xF8002008;
  FLASH0     = 0xF8002008;
  FLASH1     = 0xF8004008;

  FPU        = 0xF7E1A000;

  GPR        = 0xF7E1FF00;

  GPTA0      = 0xF0001800;
  GPTA1      = 0xF0002000;

  LBCU       = 0xF87FFE08;

  LFI        = 0xF87FFF08;

  LTCA2      = 0xF0002808;

  MCHK       = 0xF010C208;

  MLI        = 0xF010C008;
  MLI0       = 0xF010C008;
  MLI1       = 0xF010C108;

  MMU        = 0xF7E18000;

  MPR        = 0xF7E1C000;

  MSC        = 0xF0000800;
  MSC0       = 0xF0000800;
  MSC1       = 0xF0000900;

  OVC        = 0xF87FFB20;

  P0         = 0xF0000C00;
  P1         = 0xF0000D00;
  P2         = 0xF0000E00;
  P3         = 0xF0000F00;
  P4         = 0xF0001000;
  P5         = 0xF0001100;
  P6         = 0xF0001200;
  P7         = 0xF0001300;
  P8         = 0xF0001400;
  P9         = 0xF0001500;
  P10        = 0xF0001600;
  P11        = 0xF0001700;
  P12        = 0xF0300000;
  P13        = 0xF0300100;
  P14        = 0xF0300200;
  P15        = 0xF0300300;
  P16        = 0xF0300400;

  PCP        = 0xF0043F00;

  PMI        = 0xF87FFD08;

  PMU        = 0xF8000508;

  SBCU       = 0xF0000108;

  SCU        = 0xF0000508;
  SCU_WDT_ERCOSEK = 0xF00005F0 - 0x20;


  SSC        = 0xF0100100;
  SSC0       = 0xF0100100;
  SSC1       = 0xF0100200;

  STM        = 0xF0000200;
  STM_TIM0   = 0xF0000210;
  STM_SRC1   = 0xF00002F8;
  STM_SRC0   = 0xF00002FC;


  SC_INT_ADR                = CPS_CPU_SRC0;
  SCU_BASE_ADR              = SCU_WDT_ERCOSEK;
  STM_BASE_ADR              = STM;
  ALARM_SRC_ADR             = STM_SRC1;
  TIMETABLE_SRC_ADR         = STM_SRC0;
  SYSTEM_TIME_REGISTER_TC   = STM_TIM0;

#endif

#else
#error "UNSUPPORTED MACHINE_TYPE!"


#endif


#define MACHINE_TICKS_PER_US           DEF_CONST(90)



#define MACHINE_SYSCLOCK_MHZ           DEF_CONST(90)



#define MACHINE_CPUCLOCK_MHZ           DEF_CONST(180)






#endif
