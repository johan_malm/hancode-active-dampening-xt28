
#ifndef _RESET_AUTO_CONF_H
#define _RESET_AUTO_CONF_H







#define RESET_NUMBER_SOFTRESET  0ul


#define SWRESET_WAKEUP_E  0x05

#define RESET_NUM_ASW    42

#define RESET_HWRIDOFFSET   0x0000
#define RESET_TRAPIDOFFSET  0x1000
#define RESET_SBSWRIDOFFSET 0x2000
#define RESET_ASWSWRIDOFFSET 0x3000


#define RESET_HISTBUFVIS_MSK 0x80

#define RESET_DSMVIS_MSK 0x60

#define RESET_DSM_MAXVISIBILITIES   0x03ul
#define RESET_DSM_VISIBLE     0x00
#define RESET_DSM_LOCKED      0x01
#define RESET_DSM_SUPPRESSED  0x02

#define RESET_FLMARGINREADDEF 1




enum
{
    SWRESET_GRP_POWERON_E,
    SWRESET_GRP_HWRESET_E,
    SWRESET_GRP_WDT_E,
    SWRESET_GRP_WAKEUP_E,
    SWRESET_GRP_TRAP_E,
    SWRESET_GRP_SB_E,
    SWRESET_GRP_CB_E,
    SWRESET_SOFTRESETGRP_E,
    SWRESET_GRP_DUMMY_01,
    SWRESET_GRP_DUMMY_02,
    SWRESET_GRP_DUMMY_03,
    SWRESET_GRP_NUM_E 
};


enum
{
    SWRESET_BRH_GRP_E = SWRESET_GRP_NUM_E,
    RESET_EEP_GRP_E,
    RESET_EXECON_GRP_E,
    RESET_SWRESET_ASW_01,
    RESET_SWRESET_OS_01,
    SWRESET_PCP_GRP_E,
    SWRESET_SYCGRP_E,
    RESET_HWEMONGRP_E,
    RESET_ERRINTRGRP_E,
    RESET_SWRESET_TPROT,
    SWRESET_UNSUPPORTED_CPU_E,
    RESET_ADCI_E,
    RESET_DMA_E,
    RESET_FLASH_E,
    SWRESET_BRH_MO_GRP_E,
    RESET_SWRESETGRP_NUM_ASW_E
};



enum
{
    SWRESET_POWERON_E = RESET_HWRIDOFFSET,    
    SWRESET_POWERON_WDT_E,    
    SWRESET_POWERON_KL15_E,    
    SWRESET_HW_E,    
    SWRESET_WDT_E,    
    SWRESET_NUM_HWR_E    
};


enum
{
    TRAP_MMU_VAF_E = RESET_TRAPIDOFFSET,    
    TRAP_MMU_VAP_E,    
    TRAP_INTPROT_PRIV_E,    
    TRAP_INTPROT_MPR_E,    
    TRAP_INTPROT_MPW_E,    
    TRAP_INTPROT_MPX_E,    
    TRAP_INTPROT_MPP_E,    
    TRAP_INTPROT_MPN_E,    
    TRAP_INTPROT_GRWP_E,    
    TRAP_INSTRERR_IOPC_E,    
    TRAP_INSTRERR_UOPC_E,    
    TRAP_INSTRERR_OPD_E,    
    TRAP_INSTRERR_ALN_E,    
    TRAP_INSTRERR_MEM_E,    
    TRAP_CONTMANA_FCD_E,    
    TRAP_CONTMANA_CDO_E,    
    TRAP_CONTMANA_CDU_E,    
    TRAP_CONTMANA_FCU_E,    
    TRAP_CONTMANA_CSU_E,    
    TRAP_CONTMANA_CTYP_E,    
    TRAP_CONTMANA_NEST_E,    
    TRAP_SYSBUSERR_PSE_E,    
    TRAP_SYSBUSERR_DSE_E,    
    TRAP_SYSBUSERR_DAE_E,    
    TRAP_SYSBUSERR_CAE_E,    
    TRAP_SYSBUSERR_PIE_E,    
    TRAP_SYSBUSERR_DIE_E,    
    TRAP_ASSTRAP_OVF_E,    
    TRAP_ASSTRAP_SOVF_E,    
    TRAP_SYSCALL_SYS_E,    
    TRAP_NMI_ESR0_E,    
    TRAP_NMI_ESR1_E,    
    TRAP_NMI_RES0_E,    
    TRAP_NMI_WDT_E,    
    TRAP_NMI_PE_E,    
    TRAP_NMI_OSCLWD_E,    
    TRAP_NMI_OSCHWD_E,    
    TRAP_NMI_OSCSPWD_E,    
    TRAP_NMI_SYSVCOLCK_E,    
    TRAP_NMI_ERAYVCOLCKT,    
    TRAP_NMI_FLOT_E,    
    TRAP_NUM_E    
};


enum
{
    SWRESET_POWERON_SIMU_E = RESET_SBSWRIDOFFSET,
    SWRESET_HRESET_SIMU_E,
    SWRESET_RB_PROG_E,
    SWRESET_SOFTRESET_5VUNDERVOLTAGE_E,
    SWRESET_SOFTRESET_POSTDRV2PREDRV_E,
    SWRESET_CBPROG_E,
    SWRESET_CBCPU_E,
    SWRESET_SBDUMMY_1_E,
    SWRESET_SBDUMMY_2_E,
    SWRESET_SBDUMMY_3_E,
    SWRESET_SBDUMMY_4_E,
    SWRESET_SBDUMMY_5_E,
    SWRESET_SBDUMMY_6_E,
    SWRESET_SBDUMMY_7_E,
    SWRESET_SBDUMMY_8_E 
};


enum
{
    SWRESET_BRH_API_E = RESET_ASWSWRIDOFFSET,
    SWRESET_BRH_APPL_E,
    SWRST_EEPBANDGAP_E,
    SWRST_EEPNODEBUGGER_E,
    SWRST_EEPDELENVRAM_E,
    SWRST_WRITE_ERRORS_SECTORCHANGE_E,
    SWRST_EEPACTFIRSTINIT_E,
    SWRST_EXECON_FAULTYSTATE_E,
    RESET_SWRESET_ILLEGAL_OPCODE,
    RESET_SWRESET_ILLEGAL_RETURN_TO_MAIN,
    RESET_SWRESET_INTERRUPTLOCK_EXPECTED,
    RESET_USERSTACKOVERFLOW_DETECTED,
    SWRESET_PCP_ERROR_E,
    SWRESET_CALWAKEUP_E,
    SWRST_HWEMONDEFAULT_E,
    RESET_ERRINTR_E,
    RESET_SWRESET_SWOVER_DONE,
    SWRESET_CORE_ENV_E,
    SWRST_ADCI_ERROR_E,
    RESET_DMA_ERROR_E,
    SWRST_FLASHCONFIG_E,
    SWRESET_BRH_MO_19_E,
    SWRESET_BRH_MO_20_E,
    SWRESET_BRH_MO_0_E,
    SWRESET_BRH_MO_1_E,
    SWRESET_BRH_MO_2_E,
    SWRESET_BRH_MO_3_E,
    SWRESET_BRH_MO_4_E,
    SWRESET_BRH_MO_5_E,
    SWRESET_BRH_MO_6_E,
    SWRESET_BRH_MO_7_E,
    SWRESET_BRH_MO_8_E,
    SWRESET_BRH_MO_9_E,
    SWRESET_BRH_MO_10_E,
    SWRESET_BRH_MO_11_E,
    SWRESET_BRH_MO_12_E,
    SWRESET_BRH_MO_13_E,
    SWRESET_BRH_MO_14_E,
    SWRESET_BRH_MO_15_E,
    SWRESET_BRH_MO_16_E,
    SWRESET_BRH_MO_17_E,
    SWRESET_BRH_MO_18_E 
};


enum
{
    RESET_DEFAULT_LOOPCOND,
    RESET_NO_LOOPCOND,
    RESET_SOFTRST_NOLOOP 
};








#endif
