

#ifndef _PCP_AUTO_CONF_H
#define _PCP_AUTO_CONF_H
#ifndef _LIBRARYABILITY_H







#define PCP_LOAD_PRIO                                                   0x01    
#define PCP_MON_PRIO                                                    0x02    
#define PWMOUT_P_PDOH_1_PRIO                                            0x03    
#define PWMOUT_P_PDOH_10_PRIO                                           0x04    
#define PWMOUT_P_POH_11_PRIO                                            0x05    
#define PWMOUT_P_POH_13_PRIO                                            0x06    
#define PWMOUT_P_PDOH_9_PRIO                                            0x07    
#define PWMOUT_P_PDOH_12_PRIO                                           0x08    
#define PWMOUT_P_PDOH_11_PRIO                                           0x09    
#define PWMIN_P_DFI_05_PRIO                                             0x0A    
#define PWMIN_P_DFI_01_PRIO                                             0x0B    
#define PWMIN_P_DFI_02_PRIO                                             0x0C    
#define PWMIN_P_DFI_04_PRIO                                             0x0D    
#define PWMIN_P_DFI_03_PRIO                                             0x0E    
#define PWMIN_P_DFI_09_PRIO                                             0x0F    
#define PWMIN_P_DFI_10_PRIO                                             0x10    
#define PWMIN_P_DFI_06_PRIO                                             0x11    
#define PWMIN_P_DFI_08_PRIO                                             0x12    
#define PWMIN_P_DFI_07_PRIO                                             0x13    
#define SPI_PCPDATATRANSSSC0_PRIO                                       0x14    




#define PCP_LOAD_CPPN                                                   0x01    
#define PCP_MON_CPPN                                                    0x14    
#define PWMOUT_P_PDOH_1_CPPN                                            0x14    
#define PWMOUT_P_PDOH_10_CPPN                                           0x14    
#define PWMOUT_P_POH_11_CPPN                                            0x14    
#define PWMOUT_P_POH_13_CPPN                                            0x14    
#define PWMOUT_P_PDOH_9_CPPN                                            0x14    
#define PWMOUT_P_PDOH_12_CPPN                                           0x14    
#define PWMOUT_P_PDOH_11_CPPN                                           0x14    
#define PWMIN_P_DFI_05_CPPN                                             0x14    
#define PWMIN_P_DFI_01_CPPN                                             0x14    
#define PWMIN_P_DFI_02_CPPN                                             0x14    
#define PWMIN_P_DFI_04_CPPN                                             0x14    
#define PWMIN_P_DFI_03_CPPN                                             0x14    
#define PWMIN_P_DFI_09_CPPN                                             0x14    
#define PWMIN_P_DFI_10_CPPN                                             0x14    
#define PWMIN_P_DFI_06_CPPN                                             0x14    
#define PWMIN_P_DFI_08_CPPN                                             0x14    
#define PWMIN_P_DFI_07_CPPN                                             0x14    
#define SPI_PCPDATATRANSSSC0_CPPN                                       0x14    




#define PCP_LOAD_DPTR                                                   0xFF    
#define PCP_MON_DPTR                                                    0x0A    
#define PWMOUT_P_PDOH_1_DPTR                                            0x0A    
#define PWMOUT_P_PDOH_10_DPTR                                           0x0C    
#define PWMOUT_P_POH_11_DPTR                                            0x08    
#define PWMOUT_P_POH_13_DPTR                                            0x09    
#define PWMOUT_P_PDOH_9_DPTR                                            0x0B    
#define PWMOUT_P_PDOH_12_DPTR                                           0x0E    
#define PWMOUT_P_PDOH_11_DPTR                                           0x0D    
#define PWMIN_P_DFI_05_DPTR                                             0x0C    
#define PWMIN_P_DFI_01_DPTR                                             0x08    
#define PWMIN_P_DFI_02_DPTR                                             0x09    
#define PWMIN_P_DFI_04_DPTR                                             0x0B    
#define PWMIN_P_DFI_03_DPTR                                             0x0A    
#define PWMIN_P_DFI_09_DPTR                                             0x0E    
#define PWMIN_P_DFI_10_DPTR                                             0x0F    
#define PWMIN_P_DFI_06_DPTR                                             0x0D    
#define PWMIN_P_DFI_08_DPTR                                             0x09    
#define PWMIN_P_DFI_07_DPTR                                             0x08    
#define SPI_PCPDATATRANSSSC0_DPTR                                       0x0A    




#define PCP_LOAD_LTCY                                                   1000000
#define PCP_MON_LTCY                                                    20000
#define PWMOUT_P_PDOH_1_LTCY                                            50
#define PWMOUT_P_PDOH_10_LTCY                                           50
#define PWMOUT_P_POH_11_LTCY                                            50
#define PWMOUT_P_POH_13_LTCY                                            50
#define PWMOUT_P_PDOH_9_LTCY                                            50
#define PWMOUT_P_PDOH_12_LTCY                                           50
#define PWMOUT_P_PDOH_11_LTCY                                           50
#define PWMIN_P_DFI_05_LTCY                                             42
#define PWMIN_P_DFI_01_LTCY                                             42
#define PWMIN_P_DFI_02_LTCY                                             42
#define PWMIN_P_DFI_04_LTCY                                             42
#define PWMIN_P_DFI_03_LTCY                                             42
#define PWMIN_P_DFI_09_LTCY                                             25
#define PWMIN_P_DFI_10_LTCY                                             25
#define PWMIN_P_DFI_06_LTCY                                             25
#define PWMIN_P_DFI_08_LTCY                                             17
#define PWMIN_P_DFI_07_LTCY                                             17
#define SPI_PCPDATATRANSSSC0_LTCY                                       15




#define PCP_NUM_ARBTR_CYCLES                                            0x03UL






typedef struct
{
    volatile uint32 xR7_u32;
    volatile uint32 xR6_u32;
    volatile uint32 xR5_u32;
    volatile uint32 xR4_u32;
    volatile uint32 xR3_u32;
    volatile uint32 xR2_u32;
    volatile uint32 xR1_u32;
    volatile uint32 xR0_u32;
} Pcp_Csa_t;


typedef struct
{
                   uint32    xStuffIt0[8];
    volatile       Pcp_Csa_t Pcp_Load;
    volatile       Pcp_Csa_t Pcp_Mon;
    volatile       Pcp_Csa_t PwmOut_P_PDOH_1;
    volatile       Pcp_Csa_t PwmOut_P_PDOH_10;
    volatile       Pcp_Csa_t PwmOut_P_POH_11;
    volatile       Pcp_Csa_t PwmOut_P_POH_13;
    volatile       Pcp_Csa_t PwmOut_P_PDOH_9;
    volatile       Pcp_Csa_t PwmOut_P_PDOH_12;
    volatile       Pcp_Csa_t PwmOut_P_PDOH_11;
    volatile       Pcp_Csa_t Pwmin_P_DFI_05;
    volatile       Pcp_Csa_t Pwmin_P_DFI_01;
    volatile       Pcp_Csa_t Pwmin_P_DFI_02;
    volatile       Pcp_Csa_t Pwmin_P_DFI_04;
    volatile       Pcp_Csa_t Pwmin_P_DFI_03;
    volatile       Pcp_Csa_t Pwmin_P_DFI_09;
    volatile       Pcp_Csa_t Pwmin_P_DFI_10;
    volatile       Pcp_Csa_t Pwmin_P_DFI_06;
    volatile       Pcp_Csa_t Pwmin_P_DFI_08;
    volatile       Pcp_Csa_t Pwmin_P_DFI_07;
    volatile       Pcp_Csa_t Spi_PcpDataTransSsc0;
                   uint32    xStuffIt1[344];
    volatile       uint32    Pwmin_P_DFI_01_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_01_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_01_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_01_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_01_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_01_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_01_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_01_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_01_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_01_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_01_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_01_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_01_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_01_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_01_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_01_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_01_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_01_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_01_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_01_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_POH_11_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_POH_11_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_POH_11_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_POH_11_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_POH_11_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_POH_11_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_POH_11_PwmOut_Gt0Adr;
    volatile       uint32    Pwmin_P_DFI_07_GtcLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_07_GtcMaxGt0Value;
    volatile       uint32    Pwmin_P_DFI_07_GtcMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_07_GtcSumPrd;
    volatile       uint32    Pwmin_P_DFI_07_GtcCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_07_GtcCntAvrg;
    volatile       uint32    Pwmin_P_DFI_07_GtcEnblGap;
    volatile       uint32    Pwmin_P_DFI_07_GtcGapLen;
    volatile       uint32    Pwmin_P_DFI_07_GtcOldPer;
    volatile       uint32*   Pwmin_P_DFI_07_GtcLocBufBaseAdr;
    volatile       uint32*   Pwmin_P_DFI_07_GtcCaptCell1Adr;
    volatile       uint32*   Pwmin_P_DFI_07_GtcCaptCell2Adr;
    volatile       void*     Pwmin_P_DFI_07_GtcHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_07_GtcIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_07_GtcTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_07_GtcTimerDestAdr;
                   uint32    xStuffIt2[4];
    volatile       uint32    Pwmin_P_DFI_02_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_02_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_02_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_02_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_02_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_02_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_02_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_02_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_02_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_02_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_02_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_02_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_02_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_02_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_02_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_02_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_02_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_02_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_02_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_02_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_POH_13_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_POH_13_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_POH_13_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_POH_13_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_POH_13_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_POH_13_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_POH_13_PwmOut_Gt0Adr;
    volatile       uint32    Pwmin_P_DFI_08_GtcLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_08_GtcMaxGt0Value;
    volatile       uint32    Pwmin_P_DFI_08_GtcMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_08_GtcSumPrd;
    volatile       uint32    Pwmin_P_DFI_08_GtcCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_08_GtcCntAvrg;
    volatile       uint32    Pwmin_P_DFI_08_GtcEnblGap;
    volatile       uint32    Pwmin_P_DFI_08_GtcGapLen;
    volatile       uint32    Pwmin_P_DFI_08_GtcOldPer;
    volatile       uint32*   Pwmin_P_DFI_08_GtcLocBufBaseAdr;
    volatile       uint32*   Pwmin_P_DFI_08_GtcCaptCell1Adr;
    volatile       uint32*   Pwmin_P_DFI_08_GtcCaptCell2Adr;
    volatile       void*     Pwmin_P_DFI_08_GtcHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_08_GtcIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_08_GtcTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_08_GtcTimerDestAdr;
                   uint32    xStuffIt3[4];
    volatile       uint32    Pwmin_P_DFI_03_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_03_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_03_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_03_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_03_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_03_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_03_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_03_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_03_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_03_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_03_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_03_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_03_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_03_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_03_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_03_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_03_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_03_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_03_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_03_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_PDOH_1_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_PDOH_1_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_PDOH_1_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_PDOH_1_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_PDOH_1_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_PDOH_1_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_PDOH_1_PwmOut_Gt0Adr;
    volatile       uint32    Spi_PcpDataTransSsc0_numSeqLen_u32;
    volatile       uint32    Spi_PcpDataTransSsc0_adTxSrc_u32;
    volatile       uint32*   Spi_PcpDataTransSsc0_adTxDest_pu32;
    volatile       uint32*   Spi_PcpDataTransSsc0_adRxSrc_pu32;
    volatile       uint32    Spi_PcpDataTransSsc0_adRxDest_u32;
    volatile       uint32*   Spi_PcpDataTransSsc0_adQHSrn_pu32;
    volatile       uint32*   Spi_PcpDataTransSsc0_adSscCon_pu32;
    volatile       uint32*   Spi_PcpDataTransSsc0_adLtcCtr_pu32;
    volatile       uint32    Spi_PcpDataTransSsc0_xBsyXr_u32;
    volatile       uint32    Spi_PcpDataTransSsc0_xInactXr_u32;
    volatile       uint32    Spi_PcpDataTransSsc0_xSelSlso_u32;
    volatile       uint32    Spi_PcpDataTransSsc0_xDeSelSlso_u32;
    volatile       uint32    Spi_PcpDataTransSsc0_xOrigSsoc_u32;
    volatile       uint32    Pcp_Mon_xQABuf_a[2];
    volatile       uint32*   Pcp_Mon_adQABuf_pa;
    volatile       uint32    Pcp_Mon_xConst1_u32;
    volatile       uint32    Pcp_Mon_xConst2_u32;
    volatile       uint32    Pcp_Mon_xConst3_u32;
    volatile       uint32    Pcp_Mon_xBuf1_u32;
    volatile       uint32    Pcp_Mon_xConst4_u32;
    volatile       uint32    Pcp_Mon_regR6Safe_u32;
    volatile       void*     Pcp_Mon_ptrData_s;
                   uint32    xStuffIt4[5];
    volatile       uint32    Pwmin_P_DFI_04_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_04_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_04_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_04_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_04_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_04_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_04_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_04_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_04_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_04_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_04_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_04_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_04_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_04_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_04_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_04_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_04_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_04_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_04_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_04_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_PDOH_9_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_PDOH_9_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_PDOH_9_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_PDOH_9_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_PDOH_9_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_PDOH_9_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_PDOH_9_PwmOut_Gt0Adr;
                   uint32    xStuffIt5[28];
    volatile       uint32    Pwmin_P_DFI_05_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_05_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_05_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_05_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_05_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_05_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_05_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_05_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_05_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_05_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_05_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_05_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_05_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_05_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_05_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_05_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_05_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_05_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_05_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_05_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_PDOH_10_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_PDOH_10_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_PDOH_10_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_PDOH_10_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_PDOH_10_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_PDOH_10_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_PDOH_10_PwmOut_Gt0Adr;
                   uint32    xStuffIt6[28];
    volatile       uint32    Pwmin_P_DFI_06_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_06_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_06_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_06_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_06_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_06_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_06_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_06_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_06_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_06_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_06_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_06_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_06_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_06_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_06_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_06_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_06_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_06_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_06_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_06_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_PDOH_11_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_PDOH_11_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_PDOH_11_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_PDOH_11_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_PDOH_11_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_PDOH_11_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_PDOH_11_PwmOut_Gt0Adr;
                   uint32    xStuffIt7[28];
    volatile       uint32    Pwmin_P_DFI_09_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_09_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_09_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_09_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_09_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_09_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_09_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_09_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_09_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_09_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_09_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_09_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_09_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_09_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_09_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_09_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_09_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_09_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_09_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_09_DcmTimerDestAdr;
    volatile       uint32    PwmOut_P_PDOH_12_PwmOut_LocBuf[2];
    volatile       uint32    PwmOut_P_PDOH_12_PwmOut_MaxUint24;
    volatile       void*     PwmOut_P_PDOH_12_PwmOut_LocBufAdr;
    volatile       void*     PwmOut_P_PDOH_12_PwmOut_CpuBufAdr;
    volatile       uint32    PwmOut_P_PDOH_12_PwmOut_CurLtcy;
    volatile       uint32    PwmOut_P_PDOH_12_PwmOut_MaxLtcy;
    volatile       uint32*   PwmOut_P_PDOH_12_PwmOut_Gt0Adr;
                   uint32    xStuffIt8[28];
    volatile       uint32    Pwmin_P_DFI_10_DcmLocBuf[9];
    volatile       uint32    Pwmin_P_DFI_10_DcmMaxDcmValue;
    volatile       uint32    Pwmin_P_DFI_10_DcmSrscCompEvBp;
    volatile       uint32    Pwmin_P_DFI_10_DcmSrscEdgeEvBp;
    volatile       uint32    Pwmin_P_DFI_10_DcmMaxSumPrd;
    volatile       uint32    Pwmin_P_DFI_10_DcmSumPrd;
    volatile       uint32    Pwmin_P_DFI_10_DcmCntSumPrd;
    volatile       uint32    Pwmin_P_DFI_10_DcmCntAvrg;
    volatile       uint32    Pwmin_P_DFI_10_DcmEnblGap;
    volatile       uint32    Pwmin_P_DFI_10_DcmGapLen;
    volatile       uint32    Pwmin_P_DFI_10_DcmOldPer;
    volatile       uint32    Pwmin_P_DFI_10_DcmOldPerCopy;
    volatile       uint32    Pwmin_P_DFI_10_DcmNewPerCopy;
    volatile       uint32*   Pwmin_P_DFI_10_DcmSrscAdr;
    volatile       uint32*   Pwmin_P_DFI_10_DcmLocBufBaseAdr;
    volatile       void*     Pwmin_P_DFI_10_DcmCavCellAdr;
    volatile       void*     Pwmin_P_DFI_10_DcmHandOverBufAdr;
    volatile       uint32*   Pwmin_P_DFI_10_DcmIsrTriggerAdr;
    volatile       uint32*   Pwmin_P_DFI_10_DcmTimerSrcAdr;
    volatile       uint32*   Pwmin_P_DFI_10_DcmTimerDestAdr;
} Pcp_Pram_t;




extern volatile Pcp_Pram_t Pcp_Pram_s __attribute__ ((asection (".pcpdata","f=awp")));

extern void Pcp_DmyFuncPcp_Load(void) __attribute__ ((asection(".text.discard","f=ax")));
extern void Pcp_DmyFuncPcp_Mon(void) __attribute__ ((asection(".text.discard","f=ax")));
extern void Pcp_DmyFuncPwmOut_Gtc2(void) __attribute__ ((asection(".text.discard","f=ax")));
extern void Pcp_DmyFuncSpi_PcpDataTrans(void) __attribute__ ((asection(".text.discard","f=ax")));
extern void Pcp_DmyFuncpwm_in_dcmirq(void) __attribute__ ((asection(".text.discard","f=ax")));
extern void Pcp_DmyFuncpwm_in_gtcirq(void) __attribute__ ((asection(".text.discard","f=ax")));



#endif


#endif
