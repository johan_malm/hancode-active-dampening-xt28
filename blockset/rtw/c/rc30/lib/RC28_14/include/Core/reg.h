

#ifndef _REG_H
#define _REG_H






#define _REG(mod,reg) (mod.reg)


#define _POS(mod,bit) (mod##_##bit##_POS)


#define _LEN(mod,bit) (mod##_##bit##_LEN)


#define _MSK(mod,bit) ((uint32)(((1ull << (_LEN(mod,bit))) - 1ull) << (_POS(mod,bit))))



#define Reg_SetBit(mod,reg,bit) _REG_SET_BIT_STD(mod,reg,bit)



#define Reg_ClrBit(mod,reg,bit) _REG_CLR_BIT_STD(mod,reg,bit)



#define Reg_PutBit(mod,reg,bit,val) _REG_PUT_BIT_STD(mod,reg,bit,val)



#define Reg_GetBit(mod,reg,bit)                                   \
            ( (bool)((_REG(mod,reg) & _MSK(mod,bit)) != 0ul ) )



#define Reg_SetBitField(mod, reg, bitfield, val)                                        \
        ( _REG(mod,reg) =   ( _REG(mod,reg) & ~_MSK(mod,bitfield) )                     \
                          | ( ((val) << _POS(mod,bitfield)) & _MSK(mod,bitfield) ) )



#define Reg_GetBitField(mod, reg, bitfield)                                 \
            ((_REG(mod,reg) & _MSK(mod,bitfield)) >> _POS(mod,bitfield))



#define Reg_Mask(mod,reg,bit) _MSK(mod,bit)



#define Reg_Shift(mod,reg,bit) _POS(mod,bit)



#define _REG_SET_BIT_STD(mod, reg, bit)                                     \
            ( _REG(mod,reg) |= _MSK(mod,bit) )



#define _REG_CLR_BIT_STD(mod, reg, bit)                                     \
            ( _REG(mod,reg) &= ~_MSK(mod,bit) )



#define _REG_PUT_BIT_STD(mod, reg, bit, val)                                \
            ( _REG(mod,reg) = ((val) != 0ul) ?                              \
                              (_REG(mod,reg) | _MSK(mod,bit)) :              \
                              (_REG(mod,reg) & ~_MSK(mod,bit)))





#if (MACHINE_TYPE == TC_1796)

#define REG_ADC_H          "adc_1796.h"
#define REG_ASC_H          "asc_1796.h"
#define REG_CAN_H          "can_1796.h"
#define REG_CBS_H          "cbs_1796.h"
#define REG_CORE_H         "core_1796.h"
#define REG_CPS_H          "cps_1796.h"
#define REG_CPU_H          "core_env.h"
#define REG_CSFR_H         "csfr_1796.h"
#define REG_DBCU_H         "dbcu_1796.h"
#define REG_DMA_H          "dma_1796.h"
#define REG_DMI_H          "dmi_1796.h"
#define REG_DMU_H          "dmu_1796.h"
#define REG_EBU_H          "ebu_1796.h"
#define REG_EMEM_H         "emem_1796.h"
#define REG_ERAY_H         "core_env.h"
#define REG_FADC_H         "fadc_1796.h"
#define REG_FLASH_H        "flash_1796.h"
#define REG_FPU_H          "core_env.h"
#define REG_GENERAL_H      "general_1796.h"
#define REG_GPR_H          "gpr_1796.h"
#define REG_GPTA0_H        "gpta0_1796.h"
#define REG_GPTA1_H        "gpta1_1796.h"
#define REG_LBCU_H         "core_env.h"
#define REG_LFI_H          "lfi_1796.h"
#define REG_LTCA2_H        "ltca2_1796.h"
#define REG_MCHK_H         "mchk_1796.h"
#define REG_MLI_H          "mli_1796.h"
#define REG_MMU_H          "core_env.h"
#define REG_MPR_H          "mpr_1796.h"
#define REG_MSC_H          "msc_1796.h"
#define REG_OVC_H          "core_env.h"
#define REG_PBCU_H         "pbcu_1796.h"
#define REG_PCP_H          "pcp_1796.h"
#define REG_PMI_H          "pmi_1796.h"
#define REG_PMU_H          "pmu_1796.h"
#define REG_PORT_H         "port_1796.h"
#define REG_RBCU_H         "rbcu_1796.h"
#define REG_REGS_H         "regs_1796.h"
#define REG_SBCU_H         "sbcu_1796.h"
#define REG_SCU_H          "scu_1796.h"
#define REG_SSC_H          "ssc_1796.h"
#define REG_STM_H          "stm_1796.h"





#elif (MACHINE_TYPE == TC_1766)

#define REG_ADC_H          "adc_1766.h"
#define REG_ASC_H          "asc_1766.h"
#define REG_CAN_H          "can_1766.h"
#define REG_CBS_H          "cbs_1766.h"
#define REG_CORE_H         "core_1766.h"
#define REG_CPS_H          "cps_1766.h"
#define REG_CPU_H          "core_env.h"
#define REG_CSFR_H         "csfr_1766.h"
#define REG_DBCU_H         "core_env.h"
#define REG_DMA_H          "dma_1766.h"
#define REG_DMI_H          "dmi_1766.h"
#define REG_DMU_H          "core_env.h"
#define REG_EBU_H          "core_env.h"
#define REG_EMEM_H         "core_env.h"
#define REG_ERAY_H         "core_env.h"
#define REG_FADC_H         "fadc_1766.h"
#define REG_FLASH_H        "flash_1766.h"
#define REG_FPU_H          "core_env.h"
#define REG_GENERAL_H      "general_1766.h"
#define REG_GPR_H          "gpr_1766.h"
#define REG_GPTA0_H        "gpta0_1766.h"
#define REG_GPTA1_H        "core_env.h"
#define REG_LBCU_H         "lbcu_1766.h"
#define REG_LFI_H          "lfi_1766.h"
#define REG_LTCA2_H        "core_env.h"
#define REG_MCHK_H         "mchk_1766.h"
#define REG_MLI_H          "mli_1766.h"
#define REG_MMU_H          "core_env.h"
#define REG_MPR_H          "mpr_1766.h"
#define REG_MSC_H          "msc_1766.h"
#define REG_OVC_H          "core_env.h"
#define REG_PBCU_H         "core_env.h"
#define REG_PCP_H          "pcp_1766.h"
#define REG_PMI_H          "pmi_1766.h"
#define REG_PMU_H          "pmu_1766.h"
#define REG_PORT_H         "port_1766.h"
#define REG_RBCU_H         "core_env.h"
#define REG_REGS_H         "regs_1766.h"
#define REG_SBCU_H         "sbcu_1766.h"
#define REG_SCU_H          "scu_1766.h"
#define REG_SSC_H          "ssc_1766.h"
#define REG_STM_H          "stm_1766.h"






#elif ((defined TC_1762) && (MACHINE_TYPE == TC_1762))

#define REG_ADC_H          "adc_1762.h"
#define REG_ASC_H          "asc_1762.h"
#define REG_CAN_H          "can_1762.h"
#define REG_CBS_H          "cbs_1762.h"
#define REG_CORE_H         "core_1762.h"
#define REG_CPS_H          "cps_1762.h"
#define REG_CPU_H          "core_env.h"
#define REG_CSFR_H         "csfr_1762.h"
#define REG_DBCU_H         "core_env.h"
#define REG_DMA_H          "dma_1762.h"
#define REG_DMI_H          "dmi_1762.h"
#define REG_DMU_H          "core_env.h"
#define REG_EBU_H          "core_env.h"
#define REG_EMEM_H         "core_env.h"
#define REG_ERAY_H         "core_env.h"
#define REG_FADC_H         "fadc_1762.h"
#define REG_FLASH_H        "flash_1762.h"
#define REG_FPU_H          "core_env.h"
#define REG_GENERAL_H      "general_1762.h"
#define REG_GPR_H          "gpr_1762.h"
#define REG_GPTA0_H        "gpta0_1762.h"
#define REG_GPTA1_H        "core_env.h"
#define REG_LBCU_H         "core_env.h"
#define REG_LFI_H          "lfi_1762.h"
#define REG_LTCA2_H        "core_env.h"
#define REG_MCHK_H         "mchk_1762.h"
#define REG_MLI_H          "mli_1762.h"
#define REG_MMU_H          "core_env.h"
#define REG_MPR_H          "mpr_1762.h"
#define REG_MSC_H          "msc_1762.h"
#define REG_OVC_H          "core_env.h"
#define REG_PBCU_H         "core_env.h"
#define REG_PCP_H          "core_env.h"
#define REG_PMI_H          "pmi_1762.h"
#define REG_PMU_H          "pmu_1762.h"
#define REG_PORT_H         "port_1762.h"
#define REG_RBCU_H         "core_env.h"
#define REG_REGS_H         "regs_1762.h"
#define REG_SBCU_H         "sbcu_1762.h"
#define REG_SCU_H          "scu_1762.h"
#define REG_SSC_H          "ssc_1762.h"
#define REG_STM_H          "stm_1762.h"





#elif (MACHINE_TYPE == TC_1767)

#define REG_ADC_H          "adc_1767.h"
#define REG_ASC_H          "asc_1767.h"
#define REG_CAN_H          "can_1767.h"
#define REG_CBS_H          "cbs_1767.h"
#define REG_CORE_H         "core_1767.h"
#define REG_CPS_H          "cps_1767.h"
#define REG_CPU_H          "cpu_1767.h"
#define REG_CSFR_H         "csfr_1767.h"
#define REG_DBCU_H         "core_env.h"
#define REG_DMA_H          "dma_1767.h"
#define REG_DMI_H          "dmi_1767.h"
#define REG_DMU_H          "core_env.h"
#define REG_EBU_H          "core_env.h"
#define REG_EMEM_H         "core_env.h"
#define REG_ERAY_H         "core_env.h"
#define REG_FADC_H         "fadc_1767.h"
#define REG_FLASH_H        "flash_1767.h"
#define REG_FPU_H          "fpu_1767.h"
#define REG_GENERAL_H      "general_1767.h"
#define REG_GPR_H          "gpr_1767.h"
#define REG_GPTA0_H        "gpta0_1767.h"
#define REG_GPTA1_H        "core_env.h"
#define REG_LBCU_H         "lbcu_1767.h"
#define REG_LFI_H          "lfi_1767.h"
#define REG_LTCA2_H        "ltca2_1767.h"
#define REG_MCHK_H         "mchk_1767.h"
#define REG_MLI_H          "mli_1767.h"
#define REG_MMU_H          "mmu_1767.h"
#define REG_MPR_H          "mpr_1767.h"
#define REG_MSC_H          "msc_1767.h"
#define REG_OVC_H          "ovc_1767.h"
#define REG_PBCU_H         "core_env.h"
#define REG_PCP_H          "pcp_1767.h"
#define REG_PMI_H          "pmi_1767.h"
#define REG_PMU_H          "pmu_1767.h"
#define REG_PORT_H         "port_1767.h"
#define REG_RBCU_H         "core_env.h"
#define REG_REGS_H         "regs_1767.h"
#define REG_SBCU_H         "sbcu_1767.h"
#define REG_SCU_H          "scu_1767.h"
#define REG_SSC_H          "ssc_1767.h"
#define REG_STM_H          "stm_1767.h"



#elif (MACHINE_TYPE == TC_1797)

#define REG_ADC_H          "adc_1797.h"
#define REG_ASC_H          "asc_1797.h"
#define REG_CAN_H          "can_1797.h"
#define REG_CBS_H          "cbs_1797.h"
#define REG_CORE_H         "core_1797.h"
#define REG_CPS_H          "cps_1797.h"
#define REG_CPU_H          "cpu_1797.h"
#define REG_CSFR_H         "csfr_1797.h"
#define REG_DBCU_H         "core_env.h"
#define REG_DMA_H          "dma_1797.h"
#define REG_DMI_H          "dmi_1797.h"
#define REG_DMU_H          "core_env.h"
#define REG_EBU_H          "ebu_1797.h"
#define REG_EMEM_H         "core_env.h"
#define REG_ERAY_H         "eray_1797.h"
#define REG_FADC_H         "fadc_1797.h"
#define REG_FLASH_H        "flash_1797.h"
#define REG_FPU_H          "fpu_1797.h"
#define REG_GENERAL_H      "general_1797.h"
#define REG_GPR_H          "gpr_1797.h"
#define REG_GPTA0_H        "gpta0_1797.h"
#define REG_GPTA1_H        "gpta1_1797.h"
#define REG_LBCU_H         "lbcu_1797.h"
#define REG_LFI_H          "lfi_1797.h"
#define REG_LTCA2_H        "ltca2_1797.h"
#define REG_MCHK_H         "mchk_1797.h"
#define REG_MLI_H          "mli_1797.h"
#define REG_MMU_H          "mmu_1797.h"
#define REG_MPR_H          "mpr_1797.h"
#define REG_MSC_H          "msc_1797.h"
#define REG_OVC_H          "ovc_1797.h"
#define REG_PBCU_H         "core_env.h"
#define REG_PCP_H          "pcp_1797.h"
#define REG_PMI_H          "pmi_1797.h"
#define REG_PMU_H          "pmu_1797.h"
#define REG_PORT_H         "port_1797.h"
#define REG_RBCU_H         "core_env.h"
#define REG_REGS_H         "regs_1797.h"
#define REG_SBCU_H         "sbcu_1797.h"
#define REG_SCU_H          "scu_1797.h"
#define REG_SSC_H          "ssc_1797.h"
#define REG_STM_H          "stm_1797.h"

#endif

#endif
