

#ifndef _ASC_1797_H
#define _ASC_1797_H


#ifdef REGDEF_FOR_PCP
    #define _ASC_UL(x) x
#else
    #define _ASC_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 PISEL;               
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[1];        
    volatile uint32 CON;                 
    volatile uint32 BG;                  
    volatile uint32 FDV;                 
    volatile uint32 RESERVED1[1];        
    volatile uint32 TBUF;                
    volatile uint32 RBUF;                
    volatile uint32 RESERVED2[10];       
    volatile uint32 WHBCON;              
    volatile uint32 RESERVED3[39];       
    volatile uint32 TSRC;                
    volatile uint32 RSRC;                
    volatile uint32 ESRC;                
    volatile uint32 TBSRC;               
} ASC_RegMap_t;




extern ASC_RegMap_t ASC0 __attribute__ ((asection (".zbss.label_only","f=awz")));
extern ASC_RegMap_t ASC1 __attribute__ ((asection (".zbss.label_only","f=awz")));
extern ASC_RegMap_t ASC[2] __attribute__ ((asection (".zbss.label_only","f=awz")));





#define ASC_ID_MOD_NUMBER_POS                _ASC_UL(16)
#define ASC_ID_MOD_NUMBER_LEN                _ASC_UL(16)


#define ASC_ID_MOD_REV_POS                   _ASC_UL(0)
#define ASC_ID_MOD_REV_LEN                   _ASC_UL(8)


#define ASC_ID_MOD_TYPE_POS                  _ASC_UL(8)
#define ASC_ID_MOD_TYPE_LEN                  _ASC_UL(8)


#define ASC0_ID_MOD_NUMBER_POS               _ASC_UL(16)
#define ASC0_ID_MOD_NUMBER_LEN               _ASC_UL(16)


#define ASC0_ID_MOD_REV_POS                  _ASC_UL(0)
#define ASC0_ID_MOD_REV_LEN                  _ASC_UL(8)


#define ASC0_ID_MOD_TYPE_POS                 _ASC_UL(8)
#define ASC0_ID_MOD_TYPE_LEN                 _ASC_UL(8)


#define ASC1_ID_MOD_NUMBER_POS               _ASC_UL(16)
#define ASC1_ID_MOD_NUMBER_LEN               _ASC_UL(16)


#define ASC1_ID_MOD_REV_POS                  _ASC_UL(0)
#define ASC1_ID_MOD_REV_LEN                  _ASC_UL(8)


#define ASC1_ID_MOD_TYPE_POS                 _ASC_UL(8)
#define ASC1_ID_MOD_TYPE_LEN                 _ASC_UL(8)


#define ASC_CLC_DISR_POS                     _ASC_UL(0)
#define ASC_CLC_DISR_LEN                     _ASC_UL(1)


#define ASC_CLC_DISS_POS                     _ASC_UL(1)
#define ASC_CLC_DISS_LEN                     _ASC_UL(1)


#define ASC_CLC_SPEN_POS                     _ASC_UL(2)
#define ASC_CLC_SPEN_LEN                     _ASC_UL(1)


#define ASC_CLC_EDIS_POS                     _ASC_UL(3)
#define ASC_CLC_EDIS_LEN                     _ASC_UL(1)


#define ASC_CLC_SBWE_POS                     _ASC_UL(4)
#define ASC_CLC_SBWE_LEN                     _ASC_UL(1)


#define ASC_CLC_FSOE_POS                     _ASC_UL(5)
#define ASC_CLC_FSOE_LEN                     _ASC_UL(1)


#define ASC_CLC_RMC_POS                      _ASC_UL(8)
#define ASC_CLC_RMC_LEN                      _ASC_UL(8)


#define ASC_PISEL_RIS_POS                    _ASC_UL(0)
#define ASC_PISEL_RIS_LEN                    _ASC_UL(1)


#define ASC_ID_MODREV_POS                    _ASC_UL(0)
#define ASC_ID_MODREV_LEN                    _ASC_UL(8)


#define ASC_ID_MODNUM_POS                    _ASC_UL(8)
#define ASC_ID_MODNUM_LEN                    _ASC_UL(8)


#define ASC_CON_M_POS                        _ASC_UL(0)
#define ASC_CON_M_LEN                        _ASC_UL(3)


#define ASC_CON_STP_POS                      _ASC_UL(3)
#define ASC_CON_STP_LEN                      _ASC_UL(1)


#define ASC_CON_REN_POS                      _ASC_UL(4)
#define ASC_CON_REN_LEN                      _ASC_UL(1)


#define ASC_CON_PEN_POS                      _ASC_UL(5)
#define ASC_CON_PEN_LEN                      _ASC_UL(1)


#define ASC_CON_FEN_POS                      _ASC_UL(6)
#define ASC_CON_FEN_LEN                      _ASC_UL(1)


#define ASC_CON_OEN_POS                      _ASC_UL(7)
#define ASC_CON_OEN_LEN                      _ASC_UL(1)


#define ASC_CON_PE_POS                       _ASC_UL(8)
#define ASC_CON_PE_LEN                       _ASC_UL(1)


#define ASC_CON_FE_POS                       _ASC_UL(9)
#define ASC_CON_FE_LEN                       _ASC_UL(1)


#define ASC_CON_OE_POS                       _ASC_UL(10)
#define ASC_CON_OE_LEN                       _ASC_UL(1)


#define ASC_CON_FDE_POS                      _ASC_UL(11)
#define ASC_CON_FDE_LEN                      _ASC_UL(1)


#define ASC_CON_ODD_POS                      _ASC_UL(12)
#define ASC_CON_ODD_LEN                      _ASC_UL(1)


#define ASC_CON_BRS_POS                      _ASC_UL(13)
#define ASC_CON_BRS_LEN                      _ASC_UL(1)


#define ASC_CON_LB_POS                       _ASC_UL(14)
#define ASC_CON_LB_LEN                       _ASC_UL(1)


#define ASC_CON_R_POS                        _ASC_UL(15)
#define ASC_CON_R_LEN                        _ASC_UL(1)


#define ASC_BG_BR_VALUE_POS                  _ASC_UL(0)
#define ASC_BG_BR_VALUE_LEN                  _ASC_UL(13)


#define ASC_FDV_FD_VALUE_POS                 _ASC_UL(0)
#define ASC_FDV_FD_VALUE_LEN                 _ASC_UL(9)


#define ASC_TBUF_TD_VALUE_POS                _ASC_UL(0)
#define ASC_TBUF_TD_VALUE_LEN                _ASC_UL(9)


#define ASC_RBUF_RD_VALUE_POS                _ASC_UL(0)
#define ASC_RBUF_RD_VALUE_LEN                _ASC_UL(9)


#define ASC_WHBCON_CLRREN_POS                _ASC_UL(4)
#define ASC_WHBCON_CLRREN_LEN                _ASC_UL(1)


#define ASC_WHBCON_SETREN_POS                _ASC_UL(5)
#define ASC_WHBCON_SETREN_LEN                _ASC_UL(1)


#define ASC_WHBCON_CLRPE_POS                 _ASC_UL(8)
#define ASC_WHBCON_CLRPE_LEN                 _ASC_UL(1)


#define ASC_WHBCON_CLRFE_POS                 _ASC_UL(9)
#define ASC_WHBCON_CLRFE_LEN                 _ASC_UL(1)


#define ASC_WHBCON_CLROE_POS                 _ASC_UL(10)
#define ASC_WHBCON_CLROE_LEN                 _ASC_UL(1)


#define ASC_WHBCON_SETPE_POS                 _ASC_UL(11)
#define ASC_WHBCON_SETPE_LEN                 _ASC_UL(1)


#define ASC_WHBCON_SETFE_POS                 _ASC_UL(12)
#define ASC_WHBCON_SETFE_LEN                 _ASC_UL(1)


#define ASC_WHBCON_SETOE_POS                 _ASC_UL(13)
#define ASC_WHBCON_SETOE_LEN                 _ASC_UL(1)


#define ASC_TSRC_SRPN_POS                    _ASC_UL(0)
#define ASC_TSRC_SRPN_LEN                    _ASC_UL(8)


#define ASC_TSRC_TOS_POS                     _ASC_UL(10)
#define ASC_TSRC_TOS_LEN                     _ASC_UL(1)


#define ASC_TSRC_SRE_POS                     _ASC_UL(12)
#define ASC_TSRC_SRE_LEN                     _ASC_UL(1)


#define ASC_TSRC_SRR_POS                     _ASC_UL(13)
#define ASC_TSRC_SRR_LEN                     _ASC_UL(1)


#define ASC_TSRC_CLRR_POS                    _ASC_UL(14)
#define ASC_TSRC_CLRR_LEN                    _ASC_UL(1)


#define ASC_TSRC_SETR_POS                    _ASC_UL(15)
#define ASC_TSRC_SETR_LEN                    _ASC_UL(1)


#define ASC_RSRC_SRPN_POS                    _ASC_UL(0)
#define ASC_RSRC_SRPN_LEN                    _ASC_UL(8)


#define ASC_RSRC_TOS_POS                     _ASC_UL(10)
#define ASC_RSRC_TOS_LEN                     _ASC_UL(1)


#define ASC_RSRC_SRE_POS                     _ASC_UL(12)
#define ASC_RSRC_SRE_LEN                     _ASC_UL(1)


#define ASC_RSRC_SRR_POS                     _ASC_UL(13)
#define ASC_RSRC_SRR_LEN                     _ASC_UL(1)


#define ASC_RSRC_CLRR_POS                    _ASC_UL(14)
#define ASC_RSRC_CLRR_LEN                    _ASC_UL(1)


#define ASC_RSRC_SETR_POS                    _ASC_UL(15)
#define ASC_RSRC_SETR_LEN                    _ASC_UL(1)


#define ASC_ESRC_SRPN_POS                    _ASC_UL(0)
#define ASC_ESRC_SRPN_LEN                    _ASC_UL(8)


#define ASC_ESRC_TOS_POS                     _ASC_UL(10)
#define ASC_ESRC_TOS_LEN                     _ASC_UL(1)


#define ASC_ESRC_SRE_POS                     _ASC_UL(12)
#define ASC_ESRC_SRE_LEN                     _ASC_UL(1)


#define ASC_ESRC_SRR_POS                     _ASC_UL(13)
#define ASC_ESRC_SRR_LEN                     _ASC_UL(1)


#define ASC_ESRC_CLRR_POS                    _ASC_UL(14)
#define ASC_ESRC_CLRR_LEN                    _ASC_UL(1)


#define ASC_ESRC_SETR_POS                    _ASC_UL(15)
#define ASC_ESRC_SETR_LEN                    _ASC_UL(1)


#define ASC_TBSRC_SRPN_POS                   _ASC_UL(0)
#define ASC_TBSRC_SRPN_LEN                   _ASC_UL(8)


#define ASC_TBSRC_TOS_POS                    _ASC_UL(10)
#define ASC_TBSRC_TOS_LEN                    _ASC_UL(1)


#define ASC_TBSRC_SRE_POS                    _ASC_UL(12)
#define ASC_TBSRC_SRE_LEN                    _ASC_UL(1)


#define ASC_TBSRC_SRR_POS                    _ASC_UL(13)
#define ASC_TBSRC_SRR_LEN                    _ASC_UL(1)


#define ASC_TBSRC_CLRR_POS                   _ASC_UL(14)
#define ASC_TBSRC_CLRR_LEN                   _ASC_UL(1)


#define ASC_TBSRC_SETR_POS                   _ASC_UL(15)
#define ASC_TBSRC_SETR_LEN                   _ASC_UL(1)


#define ASC0_CLC_DISR_POS                    _ASC_UL(0)
#define ASC0_CLC_DISR_LEN                    _ASC_UL(1)


#define ASC0_CLC_DISS_POS                    _ASC_UL(1)
#define ASC0_CLC_DISS_LEN                    _ASC_UL(1)


#define ASC0_CLC_SPEN_POS                    _ASC_UL(2)
#define ASC0_CLC_SPEN_LEN                    _ASC_UL(1)


#define ASC0_CLC_EDIS_POS                    _ASC_UL(3)
#define ASC0_CLC_EDIS_LEN                    _ASC_UL(1)


#define ASC0_CLC_SBWE_POS                    _ASC_UL(4)
#define ASC0_CLC_SBWE_LEN                    _ASC_UL(1)


#define ASC0_CLC_FSOE_POS                    _ASC_UL(5)
#define ASC0_CLC_FSOE_LEN                    _ASC_UL(1)


#define ASC0_CLC_RMC_POS                     _ASC_UL(8)
#define ASC0_CLC_RMC_LEN                     _ASC_UL(8)


#define ASC0_PISEL_RIS_POS                   _ASC_UL(0)
#define ASC0_PISEL_RIS_LEN                   _ASC_UL(1)


#define ASC0_ID_MODREV_POS                   _ASC_UL(0)
#define ASC0_ID_MODREV_LEN                   _ASC_UL(8)


#define ASC0_ID_MODNUM_POS                   _ASC_UL(8)
#define ASC0_ID_MODNUM_LEN                   _ASC_UL(8)


#define ASC0_CON_M_POS                       _ASC_UL(0)
#define ASC0_CON_M_LEN                       _ASC_UL(3)


#define ASC0_CON_STP_POS                     _ASC_UL(3)
#define ASC0_CON_STP_LEN                     _ASC_UL(1)


#define ASC0_CON_REN_POS                     _ASC_UL(4)
#define ASC0_CON_REN_LEN                     _ASC_UL(1)


#define ASC0_CON_PEN_POS                     _ASC_UL(5)
#define ASC0_CON_PEN_LEN                     _ASC_UL(1)


#define ASC0_CON_FEN_POS                     _ASC_UL(6)
#define ASC0_CON_FEN_LEN                     _ASC_UL(1)


#define ASC0_CON_OEN_POS                     _ASC_UL(7)
#define ASC0_CON_OEN_LEN                     _ASC_UL(1)


#define ASC0_CON_PE_POS                      _ASC_UL(8)
#define ASC0_CON_PE_LEN                      _ASC_UL(1)


#define ASC0_CON_FE_POS                      _ASC_UL(9)
#define ASC0_CON_FE_LEN                      _ASC_UL(1)


#define ASC0_CON_OE_POS                      _ASC_UL(10)
#define ASC0_CON_OE_LEN                      _ASC_UL(1)


#define ASC0_CON_FDE_POS                     _ASC_UL(11)
#define ASC0_CON_FDE_LEN                     _ASC_UL(1)


#define ASC0_CON_ODD_POS                     _ASC_UL(12)
#define ASC0_CON_ODD_LEN                     _ASC_UL(1)


#define ASC0_CON_BRS_POS                     _ASC_UL(13)
#define ASC0_CON_BRS_LEN                     _ASC_UL(1)


#define ASC0_CON_LB_POS                      _ASC_UL(14)
#define ASC0_CON_LB_LEN                      _ASC_UL(1)


#define ASC0_CON_R_POS                       _ASC_UL(15)
#define ASC0_CON_R_LEN                       _ASC_UL(1)


#define ASC0_BG_BR_VALUE_POS                 _ASC_UL(0)
#define ASC0_BG_BR_VALUE_LEN                 _ASC_UL(13)


#define ASC0_FDV_FD_VALUE_POS                _ASC_UL(0)
#define ASC0_FDV_FD_VALUE_LEN                _ASC_UL(9)


#define ASC0_TBUF_TD_VALUE_POS               _ASC_UL(0)
#define ASC0_TBUF_TD_VALUE_LEN               _ASC_UL(9)


#define ASC0_RBUF_RD_VALUE_POS               _ASC_UL(0)
#define ASC0_RBUF_RD_VALUE_LEN               _ASC_UL(9)


#define ASC0_WHBCON_CLRREN_POS               _ASC_UL(4)
#define ASC0_WHBCON_CLRREN_LEN               _ASC_UL(1)


#define ASC0_WHBCON_SETREN_POS               _ASC_UL(5)
#define ASC0_WHBCON_SETREN_LEN               _ASC_UL(1)


#define ASC0_WHBCON_CLRPE_POS                _ASC_UL(8)
#define ASC0_WHBCON_CLRPE_LEN                _ASC_UL(1)


#define ASC0_WHBCON_CLRFE_POS                _ASC_UL(9)
#define ASC0_WHBCON_CLRFE_LEN                _ASC_UL(1)


#define ASC0_WHBCON_CLROE_POS                _ASC_UL(10)
#define ASC0_WHBCON_CLROE_LEN                _ASC_UL(1)


#define ASC0_WHBCON_SETPE_POS                _ASC_UL(11)
#define ASC0_WHBCON_SETPE_LEN                _ASC_UL(1)


#define ASC0_WHBCON_SETFE_POS                _ASC_UL(12)
#define ASC0_WHBCON_SETFE_LEN                _ASC_UL(1)


#define ASC0_WHBCON_SETOE_POS                _ASC_UL(13)
#define ASC0_WHBCON_SETOE_LEN                _ASC_UL(1)


#define ASC0_TSRC_SRPN_POS                   _ASC_UL(0)
#define ASC0_TSRC_SRPN_LEN                   _ASC_UL(8)


#define ASC0_TSRC_TOS_POS                    _ASC_UL(10)
#define ASC0_TSRC_TOS_LEN                    _ASC_UL(1)


#define ASC0_TSRC_SRE_POS                    _ASC_UL(12)
#define ASC0_TSRC_SRE_LEN                    _ASC_UL(1)


#define ASC0_TSRC_SRR_POS                    _ASC_UL(13)
#define ASC0_TSRC_SRR_LEN                    _ASC_UL(1)


#define ASC0_TSRC_CLRR_POS                   _ASC_UL(14)
#define ASC0_TSRC_CLRR_LEN                   _ASC_UL(1)


#define ASC0_TSRC_SETR_POS                   _ASC_UL(15)
#define ASC0_TSRC_SETR_LEN                   _ASC_UL(1)


#define ASC0_RSRC_SRPN_POS                   _ASC_UL(0)
#define ASC0_RSRC_SRPN_LEN                   _ASC_UL(8)


#define ASC0_RSRC_TOS_POS                    _ASC_UL(10)
#define ASC0_RSRC_TOS_LEN                    _ASC_UL(1)


#define ASC0_RSRC_SRE_POS                    _ASC_UL(12)
#define ASC0_RSRC_SRE_LEN                    _ASC_UL(1)


#define ASC0_RSRC_SRR_POS                    _ASC_UL(13)
#define ASC0_RSRC_SRR_LEN                    _ASC_UL(1)


#define ASC0_RSRC_CLRR_POS                   _ASC_UL(14)
#define ASC0_RSRC_CLRR_LEN                   _ASC_UL(1)


#define ASC0_RSRC_SETR_POS                   _ASC_UL(15)
#define ASC0_RSRC_SETR_LEN                   _ASC_UL(1)


#define ASC0_ESRC_SRPN_POS                   _ASC_UL(0)
#define ASC0_ESRC_SRPN_LEN                   _ASC_UL(8)


#define ASC0_ESRC_TOS_POS                    _ASC_UL(10)
#define ASC0_ESRC_TOS_LEN                    _ASC_UL(1)


#define ASC0_ESRC_SRE_POS                    _ASC_UL(12)
#define ASC0_ESRC_SRE_LEN                    _ASC_UL(1)


#define ASC0_ESRC_SRR_POS                    _ASC_UL(13)
#define ASC0_ESRC_SRR_LEN                    _ASC_UL(1)


#define ASC0_ESRC_CLRR_POS                   _ASC_UL(14)
#define ASC0_ESRC_CLRR_LEN                   _ASC_UL(1)


#define ASC0_ESRC_SETR_POS                   _ASC_UL(15)
#define ASC0_ESRC_SETR_LEN                   _ASC_UL(1)


#define ASC0_TBSRC_SRPN_POS                  _ASC_UL(0)
#define ASC0_TBSRC_SRPN_LEN                  _ASC_UL(8)


#define ASC0_TBSRC_TOS_POS                   _ASC_UL(10)
#define ASC0_TBSRC_TOS_LEN                   _ASC_UL(1)


#define ASC0_TBSRC_SRE_POS                   _ASC_UL(12)
#define ASC0_TBSRC_SRE_LEN                   _ASC_UL(1)


#define ASC0_TBSRC_SRR_POS                   _ASC_UL(13)
#define ASC0_TBSRC_SRR_LEN                   _ASC_UL(1)


#define ASC0_TBSRC_CLRR_POS                  _ASC_UL(14)
#define ASC0_TBSRC_CLRR_LEN                  _ASC_UL(1)


#define ASC0_TBSRC_SETR_POS                  _ASC_UL(15)
#define ASC0_TBSRC_SETR_LEN                  _ASC_UL(1)


#define ASC1_PISEL_RIS_POS                   _ASC_UL(0)
#define ASC1_PISEL_RIS_LEN                   _ASC_UL(1)


#define ASC1_ID_MODREV_POS                   _ASC_UL(0)
#define ASC1_ID_MODREV_LEN                   _ASC_UL(8)


#define ASC1_ID_MODNUM_POS                   _ASC_UL(8)
#define ASC1_ID_MODNUM_LEN                   _ASC_UL(8)


#define ASC1_CON_M_POS                       _ASC_UL(0)
#define ASC1_CON_M_LEN                       _ASC_UL(3)


#define ASC1_CON_STP_POS                     _ASC_UL(3)
#define ASC1_CON_STP_LEN                     _ASC_UL(1)


#define ASC1_CON_REN_POS                     _ASC_UL(4)
#define ASC1_CON_REN_LEN                     _ASC_UL(1)


#define ASC1_CON_PEN_POS                     _ASC_UL(5)
#define ASC1_CON_PEN_LEN                     _ASC_UL(1)


#define ASC1_CON_FEN_POS                     _ASC_UL(6)
#define ASC1_CON_FEN_LEN                     _ASC_UL(1)


#define ASC1_CON_OEN_POS                     _ASC_UL(7)
#define ASC1_CON_OEN_LEN                     _ASC_UL(1)


#define ASC1_CON_PE_POS                      _ASC_UL(8)
#define ASC1_CON_PE_LEN                      _ASC_UL(1)


#define ASC1_CON_FE_POS                      _ASC_UL(9)
#define ASC1_CON_FE_LEN                      _ASC_UL(1)


#define ASC1_CON_OE_POS                      _ASC_UL(10)
#define ASC1_CON_OE_LEN                      _ASC_UL(1)


#define ASC1_CON_FDE_POS                     _ASC_UL(11)
#define ASC1_CON_FDE_LEN                     _ASC_UL(1)


#define ASC1_CON_ODD_POS                     _ASC_UL(12)
#define ASC1_CON_ODD_LEN                     _ASC_UL(1)


#define ASC1_CON_BRS_POS                     _ASC_UL(13)
#define ASC1_CON_BRS_LEN                     _ASC_UL(1)


#define ASC1_CON_LB_POS                      _ASC_UL(14)
#define ASC1_CON_LB_LEN                      _ASC_UL(1)


#define ASC1_CON_R_POS                       _ASC_UL(15)
#define ASC1_CON_R_LEN                       _ASC_UL(1)


#define ASC1_BG_BR_VALUE_POS                 _ASC_UL(0)
#define ASC1_BG_BR_VALUE_LEN                 _ASC_UL(13)


#define ASC1_FDV_FD_VALUE_POS                _ASC_UL(0)
#define ASC1_FDV_FD_VALUE_LEN                _ASC_UL(9)


#define ASC1_TBUF_TD_VALUE_POS               _ASC_UL(0)
#define ASC1_TBUF_TD_VALUE_LEN               _ASC_UL(9)


#define ASC1_RBUF_RD_VALUE_POS               _ASC_UL(0)
#define ASC1_RBUF_RD_VALUE_LEN               _ASC_UL(9)


#define ASC1_WHBCON_CLRREN_POS               _ASC_UL(4)
#define ASC1_WHBCON_CLRREN_LEN               _ASC_UL(1)


#define ASC1_WHBCON_SETREN_POS               _ASC_UL(5)
#define ASC1_WHBCON_SETREN_LEN               _ASC_UL(1)


#define ASC1_WHBCON_CLRPE_POS                _ASC_UL(8)
#define ASC1_WHBCON_CLRPE_LEN                _ASC_UL(1)


#define ASC1_WHBCON_CLRFE_POS                _ASC_UL(9)
#define ASC1_WHBCON_CLRFE_LEN                _ASC_UL(1)


#define ASC1_WHBCON_CLROE_POS                _ASC_UL(10)
#define ASC1_WHBCON_CLROE_LEN                _ASC_UL(1)


#define ASC1_WHBCON_SETPE_POS                _ASC_UL(11)
#define ASC1_WHBCON_SETPE_LEN                _ASC_UL(1)


#define ASC1_WHBCON_SETFE_POS                _ASC_UL(12)
#define ASC1_WHBCON_SETFE_LEN                _ASC_UL(1)


#define ASC1_WHBCON_SETOE_POS                _ASC_UL(13)
#define ASC1_WHBCON_SETOE_LEN                _ASC_UL(1)


#define ASC1_TSRC_SRPN_POS                   _ASC_UL(0)
#define ASC1_TSRC_SRPN_LEN                   _ASC_UL(8)


#define ASC1_TSRC_TOS_POS                    _ASC_UL(10)
#define ASC1_TSRC_TOS_LEN                    _ASC_UL(1)


#define ASC1_TSRC_SRE_POS                    _ASC_UL(12)
#define ASC1_TSRC_SRE_LEN                    _ASC_UL(1)


#define ASC1_TSRC_SRR_POS                    _ASC_UL(13)
#define ASC1_TSRC_SRR_LEN                    _ASC_UL(1)


#define ASC1_TSRC_CLRR_POS                   _ASC_UL(14)
#define ASC1_TSRC_CLRR_LEN                   _ASC_UL(1)


#define ASC1_TSRC_SETR_POS                   _ASC_UL(15)
#define ASC1_TSRC_SETR_LEN                   _ASC_UL(1)


#define ASC1_RSRC_SRPN_POS                   _ASC_UL(0)
#define ASC1_RSRC_SRPN_LEN                   _ASC_UL(8)


#define ASC1_RSRC_TOS_POS                    _ASC_UL(10)
#define ASC1_RSRC_TOS_LEN                    _ASC_UL(1)


#define ASC1_RSRC_SRE_POS                    _ASC_UL(12)
#define ASC1_RSRC_SRE_LEN                    _ASC_UL(1)


#define ASC1_RSRC_SRR_POS                    _ASC_UL(13)
#define ASC1_RSRC_SRR_LEN                    _ASC_UL(1)


#define ASC1_RSRC_CLRR_POS                   _ASC_UL(14)
#define ASC1_RSRC_CLRR_LEN                   _ASC_UL(1)


#define ASC1_RSRC_SETR_POS                   _ASC_UL(15)
#define ASC1_RSRC_SETR_LEN                   _ASC_UL(1)


#define ASC1_ESRC_SRPN_POS                   _ASC_UL(0)
#define ASC1_ESRC_SRPN_LEN                   _ASC_UL(8)


#define ASC1_ESRC_TOS_POS                    _ASC_UL(10)
#define ASC1_ESRC_TOS_LEN                    _ASC_UL(1)


#define ASC1_ESRC_SRE_POS                    _ASC_UL(12)
#define ASC1_ESRC_SRE_LEN                    _ASC_UL(1)


#define ASC1_ESRC_SRR_POS                    _ASC_UL(13)
#define ASC1_ESRC_SRR_LEN                    _ASC_UL(1)


#define ASC1_ESRC_CLRR_POS                   _ASC_UL(14)
#define ASC1_ESRC_CLRR_LEN                   _ASC_UL(1)


#define ASC1_ESRC_SETR_POS                   _ASC_UL(15)
#define ASC1_ESRC_SETR_LEN                   _ASC_UL(1)


#define ASC1_TBSRC_SRPN_POS                  _ASC_UL(0)
#define ASC1_TBSRC_SRPN_LEN                  _ASC_UL(8)


#define ASC1_TBSRC_TOS_POS                   _ASC_UL(10)
#define ASC1_TBSRC_TOS_LEN                   _ASC_UL(1)


#define ASC1_TBSRC_SRE_POS                   _ASC_UL(12)
#define ASC1_TBSRC_SRE_LEN                   _ASC_UL(1)


#define ASC1_TBSRC_SRR_POS                   _ASC_UL(13)
#define ASC1_TBSRC_SRR_LEN                   _ASC_UL(1)


#define ASC1_TBSRC_CLRR_POS                  _ASC_UL(14)
#define ASC1_TBSRC_CLRR_LEN                  _ASC_UL(1)


#define ASC1_TBSRC_SETR_POS                  _ASC_UL(15)
#define ASC1_TBSRC_SETR_LEN                  _ASC_UL(1)

#endif
