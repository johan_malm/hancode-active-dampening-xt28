

#ifndef _STM_1797_H
#define _STM_1797_H


#ifdef REGDEF_FOR_PCP
    #define _STM_UL(x) x
#else
    #define _STM_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 RESERVED0[1];        
    volatile uint32 ID;                  
    volatile uint32 RESERVED1[1];        
    volatile uint32 TIM0;                
    volatile uint32 TIM1;                
    volatile uint32 TIM2;                
    volatile uint32 TIM3;                
    volatile uint32 TIM4;                
    volatile uint32 TIM5;                
    volatile uint32 TIM6;                
    volatile uint32 CAP;                 
    volatile uint32 CMP0;                
    volatile uint32 CMP1;                
    volatile uint32 CMCON;               
    volatile uint32 ICR;                 
    volatile uint32 ISRR;                
    volatile uint32 RESERVED2[45];       
    volatile uint32 SRC1;                
    volatile uint32 SRC0;                
} STM_RegMap_t;




extern STM_RegMap_t STM __attribute__ ((asection (".zbss.label_only","f=awz")));





#define STM_ID_MOD_NUMBER_POS                _STM_UL(16)
#define STM_ID_MOD_NUMBER_LEN                _STM_UL(16)


#define STM_ID_MOD_REV_POS                   _STM_UL(0)
#define STM_ID_MOD_REV_LEN                   _STM_UL(8)


#define STM_ID_MOD_TYPE_POS                  _STM_UL(8)
#define STM_ID_MOD_TYPE_LEN                  _STM_UL(8)


#define STM_CLC_DISR_POS                     _STM_UL(0)
#define STM_CLC_DISR_LEN                     _STM_UL(1)


#define STM_CLC_DISS_POS                     _STM_UL(1)
#define STM_CLC_DISS_LEN                     _STM_UL(1)


#define STM_CLC_SPEN_POS                     _STM_UL(2)
#define STM_CLC_SPEN_LEN                     _STM_UL(1)


#define STM_CLC_EDIS_POS                     _STM_UL(3)
#define STM_CLC_EDIS_LEN                     _STM_UL(1)


#define STM_CLC_SBWE_POS                     _STM_UL(4)
#define STM_CLC_SBWE_LEN                     _STM_UL(1)


#define STM_CLC_FSOE_POS                     _STM_UL(5)
#define STM_CLC_FSOE_LEN                     _STM_UL(1)


#define STM_CLC_RMC_POS                      _STM_UL(8)
#define STM_CLC_RMC_LEN                      _STM_UL(3)


#define STM_ID_MODREV_POS                    _STM_UL(0)
#define STM_ID_MODREV_LEN                    _STM_UL(8)


#define STM_ID_MODTYPE_POS                   _STM_UL(8)
#define STM_ID_MODTYPE_LEN                   _STM_UL(8)


#define STM_ID_MODNUM_POS                    _STM_UL(16)
#define STM_ID_MODNUM_LEN                    _STM_UL(16)


#define STM_TIM0_STM_31_0_POS                _STM_UL(0)
#define STM_TIM0_STM_31_0_LEN                _STM_UL(32)


#define STM_TIM1_STM_35_4_POS                _STM_UL(0)
#define STM_TIM1_STM_35_4_LEN                _STM_UL(32)


#define STM_TIM2_STM_39_8_POS                _STM_UL(0)
#define STM_TIM2_STM_39_8_LEN                _STM_UL(32)


#define STM_TIM3_STM_43_12_POS               _STM_UL(0)
#define STM_TIM3_STM_43_12_LEN               _STM_UL(32)


#define STM_TIM4_STM_47_16_POS               _STM_UL(0)
#define STM_TIM4_STM_47_16_LEN               _STM_UL(32)


#define STM_TIM5_STM_51_20_POS               _STM_UL(0)
#define STM_TIM5_STM_51_20_LEN               _STM_UL(32)


#define STM_TIM6_STM_55_32_POS               _STM_UL(0)
#define STM_TIM6_STM_55_32_LEN               _STM_UL(24)


#define STM_CAP_STM_55_32_POS                _STM_UL(0)
#define STM_CAP_STM_55_32_LEN                _STM_UL(24)


#define STM_CMP0_CMPVAL_POS                  _STM_UL(0)
#define STM_CMP0_CMPVAL_LEN                  _STM_UL(32)


#define STM_CMP1_CMPVAL_POS                  _STM_UL(0)
#define STM_CMP1_CMPVAL_LEN                  _STM_UL(32)


#define STM_CMCON_MSIZE0_POS                 _STM_UL(0)
#define STM_CMCON_MSIZE0_LEN                 _STM_UL(5)


#define STM_CMCON_MSTART0_POS                _STM_UL(8)
#define STM_CMCON_MSTART0_LEN                _STM_UL(5)


#define STM_CMCON_MSIZE1_POS                 _STM_UL(16)
#define STM_CMCON_MSIZE1_LEN                 _STM_UL(5)


#define STM_CMCON_MSTART1_POS                _STM_UL(24)
#define STM_CMCON_MSTART1_LEN                _STM_UL(5)


#define STM_ICR_CMP0EN_POS                   _STM_UL(0)
#define STM_ICR_CMP0EN_LEN                   _STM_UL(1)


#define STM_ICR_CMP0IR_POS                   _STM_UL(1)
#define STM_ICR_CMP0IR_LEN                   _STM_UL(1)


#define STM_ICR_CMP0OS_POS                   _STM_UL(2)
#define STM_ICR_CMP0OS_LEN                   _STM_UL(1)


#define STM_ICR_CMP1EN_POS                   _STM_UL(4)
#define STM_ICR_CMP1EN_LEN                   _STM_UL(1)


#define STM_ICR_CMP1IR_POS                   _STM_UL(5)
#define STM_ICR_CMP1IR_LEN                   _STM_UL(1)


#define STM_ICR_CMP1OS_POS                   _STM_UL(6)
#define STM_ICR_CMP1OS_LEN                   _STM_UL(1)


#define STM_ISRR_CMP0IRR_POS                 _STM_UL(0)
#define STM_ISRR_CMP0IRR_LEN                 _STM_UL(1)


#define STM_ISRR_CMP0IRS_POS                 _STM_UL(1)
#define STM_ISRR_CMP0IRS_LEN                 _STM_UL(1)


#define STM_ISRR_CMP1IRR_POS                 _STM_UL(2)
#define STM_ISRR_CMP1IRR_LEN                 _STM_UL(1)


#define STM_ISRR_CMP1IRS_POS                 _STM_UL(3)
#define STM_ISRR_CMP1IRS_LEN                 _STM_UL(1)


#define STM_SRC1_SRPN_POS                    _STM_UL(0)
#define STM_SRC1_SRPN_LEN                    _STM_UL(8)


#define STM_SRC1_TOS_POS                     _STM_UL(10)
#define STM_SRC1_TOS_LEN                     _STM_UL(1)


#define STM_SRC1_SRE_POS                     _STM_UL(12)
#define STM_SRC1_SRE_LEN                     _STM_UL(1)


#define STM_SRC1_SRR_POS                     _STM_UL(13)
#define STM_SRC1_SRR_LEN                     _STM_UL(1)


#define STM_SRC1_CLRR_POS                    _STM_UL(14)
#define STM_SRC1_CLRR_LEN                    _STM_UL(1)


#define STM_SRC1_SETR_POS                    _STM_UL(15)
#define STM_SRC1_SETR_LEN                    _STM_UL(1)


#define STM_SRC0_SRPN_POS                    _STM_UL(0)
#define STM_SRC0_SRPN_LEN                    _STM_UL(8)


#define STM_SRC0_TOS_POS                     _STM_UL(10)
#define STM_SRC0_TOS_LEN                     _STM_UL(1)


#define STM_SRC0_SRE_POS                     _STM_UL(12)
#define STM_SRC0_SRE_LEN                     _STM_UL(1)


#define STM_SRC0_SRR_POS                     _STM_UL(13)
#define STM_SRC0_SRR_LEN                     _STM_UL(1)


#define STM_SRC0_CLRR_POS                    _STM_UL(14)
#define STM_SRC0_CLRR_LEN                    _STM_UL(1)


#define STM_SRC0_SETR_POS                    _STM_UL(15)
#define STM_SRC0_SETR_LEN                    _STM_UL(1)

#endif
