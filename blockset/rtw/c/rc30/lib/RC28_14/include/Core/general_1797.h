

#ifndef _GENERAL_1797_H
#define _GENERAL_1797_H


#ifdef REGDEF_FOR_PCP
    #define _GENERAL_UL(x) x
#else
    #define _GENERAL_UL(x) x##UL
#endif


#define DISR         _GENERAL_UL(0x00000001)      
#define DISS         _GENERAL_UL(0x00000002)      
#define SPEN         _GENERAL_UL(0x00000004)      
#define EDIS         _GENERAL_UL(0x00000008)      
#define SBWE         _GENERAL_UL(0x00000010)      
#define FSOE         _GENERAL_UL(0x00000020)      
#define RMC_MSK      _GENERAL_UL(0x0000FF00)      
#define RMC_1        _GENERAL_UL(0x00000100)      
#define RMC_2        _GENERAL_UL(0x00000200)      
#define RMC_4        _GENERAL_UL(0x00000400)      
#define RMC_8        _GENERAL_UL(0x00000800)      
#define RMC_16       _GENERAL_UL(0x00001000)      
#define RMC_32       _GENERAL_UL(0x00002000)      
#define RMC_64       _GENERAL_UL(0x00004000)      
#define RMC_128      _GENERAL_UL(0x00008000)      
#define SMC_MSK      _GENERAL_UL(0x00FF0000)      


#define REV_MSK      _GENERAL_UL(0x000000FF)      
#define MOD_MSK      _GENERAL_UL(0x0000FF00)      


#define SRPN_MSK     _GENERAL_UL(0x000000FF)      
#define TOS_MSK      _GENERAL_UL(0x00000C00)      
#define TOS_00       _GENERAL_UL(0x00000000)      
#define TOS_01       _GENERAL_UL(0x00000400)      
#define SRE          _GENERAL_UL(0x00001000)      
#define SRE_POS      _GENERAL_UL(12)              
#define SRR          _GENERAL_UL(0x00002000)      
#define SRR_POS      _GENERAL_UL(13)              
#define CLRR         _GENERAL_UL(0x00004000)      
#define CLRR_POS     _GENERAL_UL(14)              
#define SETR         _GENERAL_UL(0x00008000)      
#define SETR_POS     _GENERAL_UL(15)              


#endif  
