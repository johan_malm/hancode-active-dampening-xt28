

#ifndef _LTCA2_1797_H
#define _LTCA2_1797_H


#ifdef REGDEF_FOR_PCP
    #define _LTCA2_UL(x) x
#else
    #define _LTCA2_UL(x) x##UL
#endif




typedef struct {
    volatile uint32 LTCCTR;        
    volatile uint32 LTCXR;         
} LTCA2_LTC_t;


typedef struct {
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[5];        
    volatile uint32 SRSC2;               
    volatile uint32 SRSS2;               
    volatile uint32 RESERVED1[4];        
    volatile uint32 MRACTL;              
    volatile uint32 MRADIN;              
    volatile uint32 MRADOUT;             
    volatile uint32 RESERVED2[111];      
    LTCA2_LTC_t     LTC[32];             
    volatile uint32 RESERVED3[312];      
    volatile uint32 SRC07;               
    volatile uint32 SRC06;               
    volatile uint32 SRC05;               
    volatile uint32 SRC04;               
    volatile uint32 SRC03;               
    volatile uint32 SRC02;               
    volatile uint32 SRC01;               
    volatile uint32 SRC00;               
} LTCA2_RegMap_t;




extern LTCA2_RegMap_t LTCA2 __attribute__ ((asection (".zbss.label_only","f=awz")));





#define LTCA2_ID_MOD_NUMBER_POS              _LTCA2_UL(16)
#define LTCA2_ID_MOD_NUMBER_LEN              _LTCA2_UL(16)


#define LTCA2_ID_MOD_REV_POS                 _LTCA2_UL(0)
#define LTCA2_ID_MOD_REV_LEN                 _LTCA2_UL(8)


#define LTCA2_ID_MOD_TYPE_POS                _LTCA2_UL(8)
#define LTCA2_ID_MOD_TYPE_LEN                _LTCA2_UL(8)


#define LTCA2_ID_MOD_REV_POS                 _LTCA2_UL(0)
#define LTCA2_ID_MOD_REV_LEN                 _LTCA2_UL(8)


#define LTCA2_ID_MOD_TYPE_POS                _LTCA2_UL(8)
#define LTCA2_ID_MOD_TYPE_LEN                _LTCA2_UL(8)


#define LTCA2_ID_MOD_NUM_POS                 _LTCA2_UL(16)
#define LTCA2_ID_MOD_NUM_LEN                 _LTCA2_UL(16)


#define LTCA2_SRSC2_LTC0_POS                 _LTCA2_UL(0)
#define LTCA2_SRSC2_LTC0_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC1_POS                 _LTCA2_UL(1)
#define LTCA2_SRSC2_LTC1_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC2_POS                 _LTCA2_UL(2)
#define LTCA2_SRSC2_LTC2_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC3_POS                 _LTCA2_UL(3)
#define LTCA2_SRSC2_LTC3_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC4_POS                 _LTCA2_UL(4)
#define LTCA2_SRSC2_LTC4_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC5_POS                 _LTCA2_UL(5)
#define LTCA2_SRSC2_LTC5_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC6_POS                 _LTCA2_UL(6)
#define LTCA2_SRSC2_LTC6_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC7_POS                 _LTCA2_UL(7)
#define LTCA2_SRSC2_LTC7_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC8_POS                 _LTCA2_UL(8)
#define LTCA2_SRSC2_LTC8_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC9_POS                 _LTCA2_UL(9)
#define LTCA2_SRSC2_LTC9_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC10_POS                _LTCA2_UL(10)
#define LTCA2_SRSC2_LTC10_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC11_POS                _LTCA2_UL(11)
#define LTCA2_SRSC2_LTC11_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC12_POS                _LTCA2_UL(12)
#define LTCA2_SRSC2_LTC12_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC13_POS                _LTCA2_UL(13)
#define LTCA2_SRSC2_LTC13_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC14_POS                _LTCA2_UL(14)
#define LTCA2_SRSC2_LTC14_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC15_POS                _LTCA2_UL(15)
#define LTCA2_SRSC2_LTC15_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC16_POS                _LTCA2_UL(16)
#define LTCA2_SRSC2_LTC16_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC17_POS                _LTCA2_UL(17)
#define LTCA2_SRSC2_LTC17_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC18_POS                _LTCA2_UL(18)
#define LTCA2_SRSC2_LTC18_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC19_POS                _LTCA2_UL(19)
#define LTCA2_SRSC2_LTC19_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC20_POS                _LTCA2_UL(20)
#define LTCA2_SRSC2_LTC20_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC21_POS                _LTCA2_UL(21)
#define LTCA2_SRSC2_LTC21_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC22_POS                _LTCA2_UL(22)
#define LTCA2_SRSC2_LTC22_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC23_POS                _LTCA2_UL(23)
#define LTCA2_SRSC2_LTC23_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC24_POS                _LTCA2_UL(24)
#define LTCA2_SRSC2_LTC24_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC25_POS                _LTCA2_UL(25)
#define LTCA2_SRSC2_LTC25_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC26_POS                _LTCA2_UL(26)
#define LTCA2_SRSC2_LTC26_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC27_POS                _LTCA2_UL(27)
#define LTCA2_SRSC2_LTC27_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC28_POS                _LTCA2_UL(28)
#define LTCA2_SRSC2_LTC28_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC29_POS                _LTCA2_UL(29)
#define LTCA2_SRSC2_LTC29_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC30_POS                _LTCA2_UL(30)
#define LTCA2_SRSC2_LTC30_LEN                _LTCA2_UL(1)


#define LTCA2_SRSC2_LTC31_POS                _LTCA2_UL(31)
#define LTCA2_SRSC2_LTC31_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC0_POS                 _LTCA2_UL(0)
#define LTCA2_SRSS2_LTC0_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC1_POS                 _LTCA2_UL(1)
#define LTCA2_SRSS2_LTC1_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC2_POS                 _LTCA2_UL(2)
#define LTCA2_SRSS2_LTC2_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC3_POS                 _LTCA2_UL(3)
#define LTCA2_SRSS2_LTC3_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC4_POS                 _LTCA2_UL(4)
#define LTCA2_SRSS2_LTC4_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC5_POS                 _LTCA2_UL(5)
#define LTCA2_SRSS2_LTC5_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC6_POS                 _LTCA2_UL(6)
#define LTCA2_SRSS2_LTC6_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC7_POS                 _LTCA2_UL(7)
#define LTCA2_SRSS2_LTC7_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC8_POS                 _LTCA2_UL(8)
#define LTCA2_SRSS2_LTC8_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC9_POS                 _LTCA2_UL(9)
#define LTCA2_SRSS2_LTC9_LEN                 _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC10_POS                _LTCA2_UL(10)
#define LTCA2_SRSS2_LTC10_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC11_POS                _LTCA2_UL(11)
#define LTCA2_SRSS2_LTC11_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC12_POS                _LTCA2_UL(12)
#define LTCA2_SRSS2_LTC12_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC13_POS                _LTCA2_UL(13)
#define LTCA2_SRSS2_LTC13_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC14_POS                _LTCA2_UL(14)
#define LTCA2_SRSS2_LTC14_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC15_POS                _LTCA2_UL(15)
#define LTCA2_SRSS2_LTC15_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC16_POS                _LTCA2_UL(16)
#define LTCA2_SRSS2_LTC16_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC17_POS                _LTCA2_UL(17)
#define LTCA2_SRSS2_LTC17_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC18_POS                _LTCA2_UL(18)
#define LTCA2_SRSS2_LTC18_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC19_POS                _LTCA2_UL(19)
#define LTCA2_SRSS2_LTC19_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC20_POS                _LTCA2_UL(20)
#define LTCA2_SRSS2_LTC20_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC21_POS                _LTCA2_UL(21)
#define LTCA2_SRSS2_LTC21_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC22_POS                _LTCA2_UL(22)
#define LTCA2_SRSS2_LTC22_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC23_POS                _LTCA2_UL(23)
#define LTCA2_SRSS2_LTC23_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC24_POS                _LTCA2_UL(24)
#define LTCA2_SRSS2_LTC24_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC25_POS                _LTCA2_UL(25)
#define LTCA2_SRSS2_LTC25_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC26_POS                _LTCA2_UL(26)
#define LTCA2_SRSS2_LTC26_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC27_POS                _LTCA2_UL(27)
#define LTCA2_SRSS2_LTC27_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC28_POS                _LTCA2_UL(28)
#define LTCA2_SRSS2_LTC28_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC29_POS                _LTCA2_UL(29)
#define LTCA2_SRSS2_LTC29_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC30_POS                _LTCA2_UL(30)
#define LTCA2_SRSS2_LTC30_LEN                _LTCA2_UL(1)


#define LTCA2_SRSS2_LTC31_POS                _LTCA2_UL(31)
#define LTCA2_SRSS2_LTC31_LEN                _LTCA2_UL(1)


#define LTCA2_MRACTL_MAEN_POS                _LTCA2_UL(0)
#define LTCA2_MRACTL_MAEN_LEN                _LTCA2_UL(1)


#define LTCA2_MRACTL_WCRES_POS               _LTCA2_UL(1)
#define LTCA2_MRACTL_WCRES_LEN               _LTCA2_UL(1)


#define LTCA2_MRACTL_FIFOFULL_POS            _LTCA2_UL(2)
#define LTCA2_MRACTL_FIFOFULL_LEN            _LTCA2_UL(1)


#define LTCA2_MRACTL_FIFOFILLCNT_POS         _LTCA2_UL(8)
#define LTCA2_MRACTL_FIFOFILLCNT_LEN         _LTCA2_UL(6)


#define LTCA2_MRADIN_DATAIN_POS              _LTCA2_UL(0)
#define LTCA2_MRADIN_DATAIN_LEN              _LTCA2_UL(32)


#define LTCA2_MRADOUT_DATAOUT_POS            _LTCA2_UL(0)
#define LTCA2_MRADOUT_DATAOUT_LEN            _LTCA2_UL(32)


#define LTCA2_LTCCTR_MOD_POS                 _LTCA2_UL(0)
#define LTCA2_LTCCTR_MOD_LEN                 _LTCA2_UL(2)


#define LTCA2_LTCCTR_OSM_POS                 _LTCA2_UL(2)
#define LTCA2_LTCCTR_OSM_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_REN_POS                 _LTCA2_UL(3)
#define LTCA2_LTCCTR_REN_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_RED_POS                 _LTCA2_UL(4)
#define LTCA2_LTCCTR_RED_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_PEN_POS                 _LTCA2_UL(4)
#define LTCA2_LTCCTR_PEN_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_FED_POS                 _LTCA2_UL(5)
#define LTCA2_LTCCTR_FED_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_AIL_POS                 _LTCA2_UL(5)
#define LTCA2_LTCCTR_AIL_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_SLO_POS                 _LTCA2_UL(6)
#define LTCA2_LTCCTR_SLO_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_CUDCLR_POS              _LTCA2_UL(7)
#define LTCA2_LTCCTR_CUDCLR_LEN              _LTCA2_UL(1)


#define LTCA2_LTCCTR_ILM_POS                 _LTCA2_UL(8)
#define LTCA2_LTCCTR_ILM_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_CUD_POS                 _LTCA2_UL(9)
#define LTCA2_LTCCTR_CUD_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_CEN_POS                 _LTCA2_UL(10)
#define LTCA2_LTCCTR_CEN_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_OCM_POS                 _LTCA2_UL(11)
#define LTCA2_LTCCTR_OCM_LEN                 _LTCA2_UL(3)


#define LTCA2_LTCCTR_OIA_POS                 _LTCA2_UL(14)
#define LTCA2_LTCCTR_OIA_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_OUT_POS                 _LTCA2_UL(15)
#define LTCA2_LTCCTR_OUT_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_GBYP_POS                _LTCA2_UL(16)
#define LTCA2_LTCCTR_GBYP_LEN                _LTCA2_UL(1)


#define LTCA2_LTCCTR_BYP_POS                 _LTCA2_UL(6)
#define LTCA2_LTCCTR_BYP_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_EOA_POS                 _LTCA2_UL(7)
#define LTCA2_LTCCTR_EOA_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_SLL_POS                 _LTCA2_UL(9)
#define LTCA2_LTCCTR_SLL_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_SOL_POS                 _LTCA2_UL(4)
#define LTCA2_LTCCTR_SOL_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCCTR_SOH_POS                 _LTCA2_UL(5)
#define LTCA2_LTCCTR_SOH_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCXR_X_POS                    _LTCA2_UL(0)
#define LTCA2_LTCXR_X_LEN                    _LTCA2_UL(16)


#define LTCA2_LTCCTR_BRM_POS                 _LTCA2_UL(0)
#define LTCA2_LTCCTR_BRM_LEN                 _LTCA2_UL(1)


#define LTCA2_LTCXR_XS_POS                   _LTCA2_UL(16)
#define LTCA2_LTCXR_XS_LEN                   _LTCA2_UL(16)


#define LTCA2_SRC07_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC07_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC07_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC07_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC07_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC07_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC07_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC07_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC07_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC07_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC07_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC07_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC06_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC06_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC06_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC06_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC06_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC06_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC06_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC06_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC06_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC06_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC06_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC06_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC05_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC05_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC05_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC05_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC05_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC05_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC05_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC05_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC05_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC05_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC05_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC05_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC04_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC04_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC04_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC04_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC04_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC04_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC04_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC04_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC04_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC04_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC04_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC04_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC03_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC03_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC03_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC03_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC03_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC03_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC03_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC03_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC03_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC03_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC03_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC03_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC02_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC02_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC02_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC02_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC02_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC02_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC02_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC02_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC02_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC02_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC02_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC02_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC01_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC01_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC01_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC01_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC01_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC01_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC01_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC01_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC01_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC01_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC01_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC01_SETR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC00_SRPN_POS                 _LTCA2_UL(0)
#define LTCA2_SRC00_SRPN_LEN                 _LTCA2_UL(8)


#define LTCA2_SRC00_TOS_POS                  _LTCA2_UL(10)
#define LTCA2_SRC00_TOS_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC00_SRE_POS                  _LTCA2_UL(12)
#define LTCA2_SRC00_SRE_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC00_SRR_POS                  _LTCA2_UL(13)
#define LTCA2_SRC00_SRR_LEN                  _LTCA2_UL(1)


#define LTCA2_SRC00_CLRR_POS                 _LTCA2_UL(14)
#define LTCA2_SRC00_CLRR_LEN                 _LTCA2_UL(1)


#define LTCA2_SRC00_SETR_POS                 _LTCA2_UL(15)
#define LTCA2_SRC00_SETR_LEN                 _LTCA2_UL(1)

#endif
