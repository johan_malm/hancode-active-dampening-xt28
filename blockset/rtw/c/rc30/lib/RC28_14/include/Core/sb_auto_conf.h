

#ifndef _SB_AUTO_CONF_H
#define _SB_AUTO_CONF_H

 


#define INTERN   0
#define EXTERN   1


#define SB_BOOT_TYPE INTERN


#define SB_RAM_FIX_START 0xC0000000


#define SB_RAM_FIX_END 0xC000013F


#define SB_RAM_TEMP_START 0xD0000800


#define SB_RAM_TEMP_END 0xD00047FF

#endif
