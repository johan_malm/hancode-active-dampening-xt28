

#ifndef _SBVERSION_H
#define _SBVERSION_H








typedef struct
{
    uint8 dRes0[16];
    uint8 dVersion_id[2];
    uint8 dRes1;
    uint8 dRevision_id[2];
    uint8 dRes2;
    uint8 dBugfix_id[2];
}SB_Version_t;









#endif
