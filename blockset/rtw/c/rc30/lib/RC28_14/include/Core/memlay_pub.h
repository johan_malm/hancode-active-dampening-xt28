

#ifndef _MEMLAY_PUB_H
#define _MEMLAY_PUB_H








#define MEMLAY_OFFSET_NONCACHED 0x20000000

#ifndef LINK_RUN












#define MEMLAY_USE_CONST_NONCACHED(typ, var)    typ var __attribute__ ((asection(".rodata.noncached","f=a")))

#define MEMLAY_USE_CONST_ABS(typ, var)          typ var __attribute__ ((asection(".zrodata","f=az")))

#define MEMLAY_USE_CONST_NEAR(typ, var)         typ var __attribute__ ((asection(".sdata.rodata","f=as")))


#define MEMLAY_USE_ASW_NONCACHED(fct)               fct __attribute__ ((asection(".text.noncached","f=ax")))

#define MEMLAY_USE_ASW_0(fct)                       fct __attribute__ ((asection(".text.asw0","f=ax")))

#define MEMLAY_USE_ASW_1(fct)                       fct __attribute__ ((asection(".text.asw1","f=ax")))

#define MEMLAY_USE_ASW_2(fct)                       fct __attribute__ ((asection(".text.asw2","f=ax")))


#define MEMLAY_USE_RAM_0(typ, var)              typ var __attribute__ ((asection(".bss.ram0","f=aw")))

#define MEMLAY_USE_RAM_0_ABS(typ, var)          typ var __attribute__ ((asection(".zbss.ram0","f=awz")))

#define MEMLAY_USE_RAM_0_BIT(typ, var)          typ var __attribute__ ((asection(".bbss.ram0","f=awbz")))


#define MEMLAY_USE_RAM_1(typ, var)              typ var __attribute__ ((asection(".bss.ram1","f=aw")))

#define MEMLAY_USE_RAM_1_ABS(typ, var)          typ var __attribute__ ((asection(".zbss.ram1","f=awz")))

#define MEMLAY_USE_RAM_1_BIT(typ, var)          typ var __attribute__ ((asection(".bbss.ram1","f=awbz")))


#define MEMLAY_USE_RAM_2(typ, var)              typ var __attribute__ ((asection(".bss.ram2","f=aw")))

#define MEMLAY_USE_RAM_NEAR(typ, var)           typ var __attribute__ ((asection(".sbss","f=aws")))


#define MEMLAY_USE_RAM_RESTRICTED(typ, var)     typ var __attribute__ ((asection(".bss.ram0.restricted","f=aw")))
#define MEMLAY_USE_RAM_RESTRICTED_A(typ, var, align) \
                                                typ var MEMLAY_ATTRIBUTE_A(.bss.ram0.restricted.a##align, f=aw, a=align)


#define MEMLAY_USE_PROTRAM(typ, var)            typ var __attribute__ ((asection(".bss.protram","f=aw")))
#define MEMLAY_USE_PROTRAM_A(typ, var, align)   typ var MEMLAY_ATTRIBUTE_A(.bss.protram.a##align, f=aw, a=align)

#define MEMLAY_USE_PROTRAM_ABS(typ, var)        typ var __attribute__ ((asection(".zbss.protram","f=awz")))
#define MEMLAY_USE_PROTRAM_ABS_A(typ, var, align) \
                                                typ var MEMLAY_ATTRIBUTE_A(.zbss.protram.a##align, f=awz, a=align)

#define MEMLAY_USE_PROTRAM_BIT(typ, var)        typ var __attribute__ ((asection(".bbss.protram","f=awbz")))


#define MEMLAY_USE_ENVRAM(typ, var)             typ var __attribute__ ((asection(".bss.envram","f=aw")))
#define MEMLAY_USE_ENVRAM_A(typ, var, align)    typ var MEMLAY_ATTRIBUTE_A(.bss.envram.a##align, f=aw, a=align)


#define MEMLAY_USE_SPRAM_CODE(fct)                  fct __attribute__ ((asection(".text.spram","f=ax")))



#define MEMLAY_USE_SPRAM(typ, var)              typ var __attribute__ ((asection(".bss.spram","f=aw")))


#define MEMLAY_USE_FORCE_INLINE(fct)            static inline fct __attribute__ ((always_inline)); \
                                                static inline fct



#define MEMLAY_USE_DPRAM_A1024(typ, var)        typ var __attribute__ ((asection(".bss.dpram_a1024","f=aw","a=32")))
#define MEMLAY_USE_DPRAM_A512(typ, var)         typ var __attribute__ ((asection(".bss.dpram_a512","f=aw","a=32")))
#define MEMLAY_USE_DPRAM_A256(typ, var)         typ var __attribute__ ((asection(".bss.dpram_a256","f=aw","a=32")))
#define MEMLAY_USE_DPRAM_A128(typ, var)         typ var __attribute__ ((asection(".bss.dpram_a128","f=aw","a=32")))
#define MEMLAY_USE_DPRAM_A64(typ, var)          typ var __attribute__ ((asection(".bss.dpram_a64","f=aw","a=32")))
#define MEMLAY_USE_DPRAM_A32(typ, var)          typ var __attribute__ ((asection(".bss.dpram_a32","f=aw","a=32")))
#define MEMLAY_USE_DPRAM_A16(typ, var)          typ var __attribute__ ((asection(".bss.dpram_a16","f=aw","a=16")))
#define MEMLAY_USE_DPRAM_A8(typ, var)           typ var __attribute__ ((asection(".bss.dpram.a8","f=aw","a=8")))
#define MEMLAY_USE_DPRAM_A4(typ, var)           typ var __attribute__ ((asection(".bss.dpram.a4","f=aw","a=4")))
#define MEMLAY_USE_DPRAM_A2(typ, var)           typ var __attribute__ ((asection(".bss.dpram.a2","f=aw","a=2")))
#define MEMLAY_USE_DPRAM_A1(typ, var)           typ var __attribute__ ((asection(".bss.dpram.a1","f=aw","a=1")))

#define MEMLAY_USE_DPRAM(typ, var)              typ var __attribute__ ((asection(".bss.dpram","f=aw")))

#define MEMLAY_USE_MPRAM(typ, var)              typ var __attribute__ ((asection(".bss.mpram","f=aw")))
#define MEMLAY_USE_MPRAM_A(typ, var, align)     typ var MEMLAY_ATTRIBUTE_A(.bss.mpram.a##align, f=aw, a=align)

#define MEMLAY_USE_LABEL(typ, var)              typ var __attribute__ ((asection(".label_only")))


#define MEMLAY_USE_MO_RAM_CYCCHECK(typ, var)     typ var __attribute__ ((asection(".bss.Mo_RamCyclicCheck","f=aw")))
#define MEMLAY_USE_MO_RAM_CYCCHECK_A(typ, var, align) \
                                                 typ var MEMLAY_ATTRIBUTE_A(.bss.Mo_RamCyclicCheck.a##align, f=aw, a=align)
#define MEMLAY_USE_MO_RAM_CPL(typ, var)          typ var __attribute__ ((asection(".bss.Mo_RamCpl","f=aw")))
#define MEMLAY_USE_MO_RAM_CPL_A(typ, var, align) typ var MEMLAY_ATTRIBUTE_A(.bss.Mo_RamCpl.a##align, f=aw, a=align)
#define MEMLAY_USE_MO_RAM_COM(typ, var)          typ var __attribute__ ((asection(".bss.Mo_RamCom","f=aw")))
#define MEMLAY_USE_MO_RAM_COM_A(typ, var, align) typ var MEMLAY_ATTRIBUTE_A(.bss.Mo_RamCom.a##align, f=aw, a=align)
#define MEMLAY_USE_MO_RAM_IT(typ, var)           typ var __attribute__ ((asection(".bss.Mo_RamIt","f=aw")))
#define MEMLAY_USE_MO_RAM_IT_A(typ, var, align)  typ var MEMLAY_ATTRIBUTE_A(.bss.Mo_RamIt.a##align, f=aw, a=align)
#define MEMLAY_USE_MO_RAM_TMP(typ, var)          typ var __attribute__ ((asection(".bss.Mo_RamTmp","f=aw")))
#define MEMLAY_USE_MO_RAM_TMP_A(typ, var, align) typ var MEMLAY_ATTRIBUTE_A(.bss.Mo_RamTmp.a##align, f=aw, a=align)
#define MEMLAY_USE_EARLY_CLEARED_RAM(typ, var)   typ var __attribute__ ((asection(".bss.earlycleared","f=aw")))
#define MEMLAY_USE_MO_CHCCODE(fct)                   fct __attribute__ ((asection(".Mo_ChCCode","f=ax")))
#define MEMLAY_USE_MO_CODE(fct)                      fct __attribute__ ((asection(".Mo_Code","f=ax")))

#define MEMLAY_USE_MOF_RAM(typ, var)            typ var __attribute__ ((asection(".bss.MoF_Ram","f=aw")))
#define MEMLAY_USE_MOF_CPLRAM(typ, var)         typ var __attribute__ ((asection(".bss.MoF_CplRam","f=aw")))
#define MEMLAY_USE_MOF_COMRAM(typ, var)         typ var __attribute__ ((asection(".bss.MoF_ComRam","f=aw")))
#define MEMLAY_USE_MOI_RAM(typ, var)            typ var __attribute__ ((asection(".bss.MoI_Ram","f=aw")))
#define MEMLAY_USE_MOF_DIABITRAM(typ, var)      typ var __attribute__ ((asection(".bbss.MoF_DiaBitRam","f=awb")))
#define MEMLAY_USE_MOF_TMPRAM(typ, var)         typ var __attribute__ ((asection(".bss.MoF_TmpRam","f=aw")))
#define MEMLAY_USE_MOF_NVRAM(typ, var)          typ var __attribute__ ((asection(".bss.MoF_NvRam","f=aw")))


#define MEMLAY_ATTRIBUTE_A(SECTION, FLAG, ALIGN) __attribute__ ((asection(#SECTION, #FLAG, #ALIGN)))



#define MEMLAY_AD_INVALID 0xFFFFFFFFUL

#define MEMLAY_RAM_INIT MemLay_RamInit



typedef struct
{
    uint32* Start_pu32;
    uint32  numData_u32;
}MemLay_RamInfo_t;


typedef struct
{
    uint32  adFlashStart_u32;
    uint32  adFlashEnd_u32;
    uint32  LinkOffset_u32;
    uint32  OCRamOffset_u32;
}MemLay_MapInfo_t;


extern const uint32 MemLay_CompIdDs0_u32     __attribute__ ((asection(".label_only","f=a")));
extern const uint32 MemLay_CompRefDs0_u32    __attribute__ ((asection(".ds0_compref","f=a")));
extern const uint32 MemLay_CompRefDs0Etk_u32 __attribute__ ((asection(".label_only","f=a")));
extern       uint32 MemLay_CompRefEtk_u32    __attribute__ ((asection(".bss.distab_write","f=aw")));


__PRAGMA_USE__CODE__eos__NormalSpeed__START
extern void   MemLay_RamInit(void);
extern uint32 MemLay_GetIntInfo(uint32 BlkId_u32, uint32 Index_u32);
extern uint32 MemLay_CRCA_8_32(const uint8 *adData_pcu8, const uint8 *adEnd_pcu8,
                               uint32 CRCValue_u32);
extern uint32 MemLay_CRCA_32_32(const uint32 *adData_pcu32, const uint32 *adEnd_pcu32,
                                uint32 CRCValue_u32);
extern uint32 MemLay_Translate24to32(uint32 adInput24_u32);
__PRAGMA_USE__CODE__eos__NormalSpeed__END



#if ((MACHINE_TYPE == TC_1766) || (MACHINE_TYPE == TC_1796))
__PRAGMA_USE__CODE__eos__NormalSpeed__START
extern uint32 MemLay_TranslateToSpb(const uint32 adCpuVar_cu32);
__PRAGMA_USE__CODE__eos__NormalSpeed__END
#elif ((MACHINE_TYPE == TC_1767) || (MACHINE_TYPE == TC_1797))


static inline uint32 MemLay_TranslateToSpb(const uint32 adCpuVar_cu32)
{
    
    return (adCpuVar_cu32);
}
#else
#error "Machine type not supported."
#endif



#if (MACHINE_SUBTYPE == TC_1796)
__PRAGMA_USE__CODE__eos__NormalSpeed__START
extern uint32 MemLay_TranslateToRpb(const uint32 adCpuVar_cu32);
__PRAGMA_USE__CODE__eos__NormalSpeed__END
#else


static inline uint32 MemLay_TranslateToRpb(const uint32 adCpuVar_cu32)
{
    return (MemLay_TranslateToSpb(adCpuVar_cu32));
}
#endif



static inline uint32 MemLay_TranslateToNonCached(uint32 adVar_u32)
{
    
    if ((adVar_u32 >> 28) == 0x8)
    {
        adVar_u32 += MEMLAY_OFFSET_NONCACHED;
    }

    return adVar_u32;
}



static inline uint32 MemLay_TranslateToCached(uint32 adVar_u32)
{
    
    if ((adVar_u32 >> 28) == 0xA)
    {
        adVar_u32 -= MEMLAY_OFFSET_NONCACHED;
    }

    return adVar_u32;
}


#endif

#endif
