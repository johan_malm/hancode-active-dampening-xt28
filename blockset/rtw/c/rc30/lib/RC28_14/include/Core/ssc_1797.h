

#ifndef _SSC_1797_H
#define _SSC_1797_H


#ifdef REGDEF_FOR_PCP
    #define _SSC_UL(x) x
#else
    #define _SSC_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 PISEL;               
    volatile uint32 ID;                  
    volatile uint32 FDR;                 
    volatile uint32 CON;                 
    volatile uint32 BR;                  
    volatile uint32 SSOC;                
    volatile uint32 SSOTC;               
    volatile uint32 TB;                  
    volatile uint32 RB;                  
    volatile uint32 STAT;                
    volatile uint32 EFM;                 
    volatile uint32 RESERVED0[49];       
    volatile uint32 TSRC;                
    volatile uint32 RSRC;                
    volatile uint32 ESRC;                
} SSC_RegMap_t;




extern SSC_RegMap_t SSC0 __attribute__ ((asection (".bss.label_only")));
extern SSC_RegMap_t SSC1 __attribute__ ((asection (".bss.label_only")));
extern SSC_RegMap_t SSC[2] __attribute__ ((asection (".bss.label_only")));






#define SSCX_CON_TO_BR_OFS              _SSC_UL(4)
#define SSCX_CON_TO_SSOC_OFS            _SSC_UL(8)
#define SSCX_CON_TO_SSOTC_OFS           _SSC_UL(12)
#define SSCX_CON_TO_TB_OFS              _SSC_UL(16)
#define SSCX_CON_TO_RB_OFS              _SSC_UL(20)
#define SSCX_CON_TO_STAT_OFS            _SSC_UL(24)





#define SSCX_CON_EN_POS                 _SSC_UL(15)
#define SSCX_CON_EN_LEN                 _SSC_UL(1)


#define SSCX_CON_BM_POS                 _SSC_UL(0)
#define SSCX_CON_BM_LEN                 _SSC_UL(4)


#define SSCX_STAT_BSY_POS               _SSC_UL(12)
#define SSCX_STAT_BSY_LEN               _SSC_UL(1)


#define SSCX_PISEL_MRIS_POS             _SSC_UL(0)
#define SSCX_PISEL_MRIS_LEN             _SSC_UL(1)





#define SSC_ID_MOD_NUMBER_POS                _SSC_UL(16)
#define SSC_ID_MOD_NUMBER_LEN                _SSC_UL(16)


#define SSC_ID_MOD_REV_POS                   _SSC_UL(0)
#define SSC_ID_MOD_REV_LEN                   _SSC_UL(8)


#define SSC_ID_MOD_TYPE_POS                  _SSC_UL(8)
#define SSC_ID_MOD_TYPE_LEN                  _SSC_UL(8)


#define SSC0_ID_MOD_NUMBER_POS               _SSC_UL(16)
#define SSC0_ID_MOD_NUMBER_LEN               _SSC_UL(16)


#define SSC0_ID_MOD_REV_POS                  _SSC_UL(0)
#define SSC0_ID_MOD_REV_LEN                  _SSC_UL(8)


#define SSC0_ID_MOD_TYPE_POS                 _SSC_UL(8)
#define SSC0_ID_MOD_TYPE_LEN                 _SSC_UL(8)


#define SSC1_ID_MOD_NUMBER_POS               _SSC_UL(16)
#define SSC1_ID_MOD_NUMBER_LEN               _SSC_UL(16)


#define SSC1_ID_MOD_REV_POS                  _SSC_UL(0)
#define SSC1_ID_MOD_REV_LEN                  _SSC_UL(8)


#define SSC1_ID_MOD_TYPE_POS                 _SSC_UL(8)
#define SSC1_ID_MOD_TYPE_LEN                 _SSC_UL(8)


#define SSC_CLC_DISR_POS                     _SSC_UL(0)
#define SSC_CLC_DISR_LEN                     _SSC_UL(1)


#define SSC_CLC_DISS_POS                     _SSC_UL(1)
#define SSC_CLC_DISS_LEN                     _SSC_UL(1)


#define SSC_CLC_SPEN_POS                     _SSC_UL(2)
#define SSC_CLC_SPEN_LEN                     _SSC_UL(1)


#define SSC_CLC_EDIS_POS                     _SSC_UL(3)
#define SSC_CLC_EDIS_LEN                     _SSC_UL(1)


#define SSC_CLC_SBWE_POS                     _SSC_UL(4)
#define SSC_CLC_SBWE_LEN                     _SSC_UL(1)


#define SSC_CLC_FSOE_POS                     _SSC_UL(5)
#define SSC_CLC_FSOE_LEN                     _SSC_UL(1)


#define SSC_PISEL_MRIS_POS                   _SSC_UL(0)
#define SSC_PISEL_MRIS_LEN                   _SSC_UL(1)


#define SSC_PISEL_SRIS_POS                   _SSC_UL(1)
#define SSC_PISEL_SRIS_LEN                   _SSC_UL(1)


#define SSC_PISEL_SCIS_POS                   _SSC_UL(2)
#define SSC_PISEL_SCIS_LEN                   _SSC_UL(1)


#define SSC_PISEL_SLSIS_POS                  _SSC_UL(3)
#define SSC_PISEL_SLSIS_LEN                  _SSC_UL(3)


#define SSC_PISEL_STIP_POS                   _SSC_UL(8)
#define SSC_PISEL_STIP_LEN                   _SSC_UL(1)


#define SSC_ID_MODREV_POS                    _SSC_UL(0)
#define SSC_ID_MODREV_LEN                    _SSC_UL(8)


#define SSC_ID_MODNUM_POS                    _SSC_UL(8)
#define SSC_ID_MODNUM_LEN                    _SSC_UL(8)


#define SSC_FDR_STEP_POS                     _SSC_UL(0)
#define SSC_FDR_STEP_LEN                     _SSC_UL(10)


#define SSC_FDR_SM_POS                       _SSC_UL(11)
#define SSC_FDR_SM_LEN                       _SSC_UL(1)


#define SSC_FDR_SC_POS                       _SSC_UL(12)
#define SSC_FDR_SC_LEN                       _SSC_UL(2)


#define SSC_FDR_DM_POS                       _SSC_UL(14)
#define SSC_FDR_DM_LEN                       _SSC_UL(2)


#define SSC_FDR_RESULT_POS                   _SSC_UL(16)
#define SSC_FDR_RESULT_LEN                   _SSC_UL(10)


#define SSC_FDR_SUSACK_POS                   _SSC_UL(28)
#define SSC_FDR_SUSACK_LEN                   _SSC_UL(1)


#define SSC_FDR_SUSREQ_POS                   _SSC_UL(29)
#define SSC_FDR_SUSREQ_LEN                   _SSC_UL(1)


#define SSC_FDR_ENHW_POS                     _SSC_UL(30)
#define SSC_FDR_ENHW_LEN                     _SSC_UL(1)


#define SSC_FDR_DISCLK_POS                   _SSC_UL(31)
#define SSC_FDR_DISCLK_LEN                   _SSC_UL(1)


#define SSC_CON_BM_POS                       _SSC_UL(0)
#define SSC_CON_BM_LEN                       _SSC_UL(4)


#define SSC_CON_HB_POS                       _SSC_UL(4)
#define SSC_CON_HB_LEN                       _SSC_UL(1)


#define SSC_CON_PH_POS                       _SSC_UL(5)
#define SSC_CON_PH_LEN                       _SSC_UL(1)


#define SSC_CON_PO_POS                       _SSC_UL(6)
#define SSC_CON_PO_LEN                       _SSC_UL(1)


#define SSC_CON_LB_POS                       _SSC_UL(7)
#define SSC_CON_LB_LEN                       _SSC_UL(1)


#define SSC_CON_TEN_POS                      _SSC_UL(8)
#define SSC_CON_TEN_LEN                      _SSC_UL(1)


#define SSC_CON_REN_POS                      _SSC_UL(9)
#define SSC_CON_REN_LEN                      _SSC_UL(1)


#define SSC_CON_PEN_POS                      _SSC_UL(10)
#define SSC_CON_PEN_LEN                      _SSC_UL(1)


#define SSC_CON_BEN_POS                      _SSC_UL(11)
#define SSC_CON_BEN_LEN                      _SSC_UL(1)


#define SSC_CON_AREN_POS                     _SSC_UL(12)
#define SSC_CON_AREN_LEN                     _SSC_UL(1)


#define SSC_CON_MS_POS                       _SSC_UL(14)
#define SSC_CON_MS_LEN                       _SSC_UL(1)


#define SSC_CON_EN_POS                       _SSC_UL(15)
#define SSC_CON_EN_LEN                       _SSC_UL(1)


#define SSC_BR_BR_VALUE_POS                  _SSC_UL(0)
#define SSC_BR_BR_VALUE_LEN                  _SSC_UL(16)


#define SSC_SSOC_AOL0_POS                    _SSC_UL(0)
#define SSC_SSOC_AOL0_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL1_POS                    _SSC_UL(1)
#define SSC_SSOC_AOL1_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL2_POS                    _SSC_UL(2)
#define SSC_SSOC_AOL2_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL3_POS                    _SSC_UL(3)
#define SSC_SSOC_AOL3_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL4_POS                    _SSC_UL(4)
#define SSC_SSOC_AOL4_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL5_POS                    _SSC_UL(5)
#define SSC_SSOC_AOL5_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL6_POS                    _SSC_UL(6)
#define SSC_SSOC_AOL6_LEN                    _SSC_UL(1)


#define SSC_SSOC_AOL7_POS                    _SSC_UL(7)
#define SSC_SSOC_AOL7_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN0_POS                    _SSC_UL(8)
#define SSC_SSOC_OEN0_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN1_POS                    _SSC_UL(9)
#define SSC_SSOC_OEN1_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN2_POS                    _SSC_UL(10)
#define SSC_SSOC_OEN2_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN3_POS                    _SSC_UL(11)
#define SSC_SSOC_OEN3_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN4_POS                    _SSC_UL(12)
#define SSC_SSOC_OEN4_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN5_POS                    _SSC_UL(13)
#define SSC_SSOC_OEN5_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN6_POS                    _SSC_UL(14)
#define SSC_SSOC_OEN6_LEN                    _SSC_UL(1)


#define SSC_SSOC_OEN7_POS                    _SSC_UL(15)
#define SSC_SSOC_OEN7_LEN                    _SSC_UL(1)


#define SSC_SSOTC_LEAD_POS                   _SSC_UL(0)
#define SSC_SSOTC_LEAD_LEN                   _SSC_UL(2)


#define SSC_SSOTC_TRAIL_POS                  _SSC_UL(2)
#define SSC_SSOTC_TRAIL_LEN                  _SSC_UL(2)


#define SSC_SSOTC_INACT_POS                  _SSC_UL(4)
#define SSC_SSOTC_INACT_LEN                  _SSC_UL(2)


#define SSC_SSOTC_SLSO7MOD_POS               _SSC_UL(8)
#define SSC_SSOTC_SLSO7MOD_LEN               _SSC_UL(1)


#define SSC_TB_TB_VALUE_POS                  _SSC_UL(0)
#define SSC_TB_TB_VALUE_LEN                  _SSC_UL(16)


#define SSC_RB_RB_VALUE_POS                  _SSC_UL(0)
#define SSC_RB_RB_VALUE_LEN                  _SSC_UL(16)


#define SSC_STAT_BC_POS                      _SSC_UL(0)
#define SSC_STAT_BC_LEN                      _SSC_UL(4)


#define SSC_STAT_TE_POS                      _SSC_UL(8)
#define SSC_STAT_TE_LEN                      _SSC_UL(1)


#define SSC_STAT_RE_POS                      _SSC_UL(9)
#define SSC_STAT_RE_LEN                      _SSC_UL(1)


#define SSC_STAT_PE_POS                      _SSC_UL(10)
#define SSC_STAT_PE_LEN                      _SSC_UL(1)


#define SSC_STAT_BE_POS                      _SSC_UL(11)
#define SSC_STAT_BE_LEN                      _SSC_UL(1)


#define SSC_STAT_BSY_POS                     _SSC_UL(12)
#define SSC_STAT_BSY_LEN                     _SSC_UL(1)


#define SSC_EFM_CLRTE_POS                    _SSC_UL(8)
#define SSC_EFM_CLRTE_LEN                    _SSC_UL(1)


#define SSC_EFM_CLRRE_POS                    _SSC_UL(9)
#define SSC_EFM_CLRRE_LEN                    _SSC_UL(1)


#define SSC_EFM_CLRPE_POS                    _SSC_UL(10)
#define SSC_EFM_CLRPE_LEN                    _SSC_UL(1)


#define SSC_EFM_CLRBE_POS                    _SSC_UL(11)
#define SSC_EFM_CLRBE_LEN                    _SSC_UL(1)


#define SSC_EFM_SETTE_POS                    _SSC_UL(12)
#define SSC_EFM_SETTE_LEN                    _SSC_UL(1)


#define SSC_EFM_SETRE_POS                    _SSC_UL(13)
#define SSC_EFM_SETRE_LEN                    _SSC_UL(1)


#define SSC_EFM_SETPE_POS                    _SSC_UL(14)
#define SSC_EFM_SETPE_LEN                    _SSC_UL(1)


#define SSC_EFM_SETBE_POS                    _SSC_UL(15)
#define SSC_EFM_SETBE_LEN                    _SSC_UL(1)


#define SSC_TSRC_SRPN_POS                    _SSC_UL(0)
#define SSC_TSRC_SRPN_LEN                    _SSC_UL(8)


#define SSC_TSRC_TOS_POS                     _SSC_UL(10)
#define SSC_TSRC_TOS_LEN                     _SSC_UL(1)


#define SSC_TSRC_SRE_POS                     _SSC_UL(12)
#define SSC_TSRC_SRE_LEN                     _SSC_UL(1)


#define SSC_TSRC_SRR_POS                     _SSC_UL(13)
#define SSC_TSRC_SRR_LEN                     _SSC_UL(1)


#define SSC_TSRC_CLRR_POS                    _SSC_UL(14)
#define SSC_TSRC_CLRR_LEN                    _SSC_UL(1)


#define SSC_TSRC_SETR_POS                    _SSC_UL(15)
#define SSC_TSRC_SETR_LEN                    _SSC_UL(1)


#define SSC_RSRC_SRPN_POS                    _SSC_UL(0)
#define SSC_RSRC_SRPN_LEN                    _SSC_UL(8)


#define SSC_RSRC_TOS_POS                     _SSC_UL(10)
#define SSC_RSRC_TOS_LEN                     _SSC_UL(1)


#define SSC_RSRC_SRE_POS                     _SSC_UL(12)
#define SSC_RSRC_SRE_LEN                     _SSC_UL(1)


#define SSC_RSRC_SRR_POS                     _SSC_UL(13)
#define SSC_RSRC_SRR_LEN                     _SSC_UL(1)


#define SSC_RSRC_CLRR_POS                    _SSC_UL(14)
#define SSC_RSRC_CLRR_LEN                    _SSC_UL(1)


#define SSC_RSRC_SETR_POS                    _SSC_UL(15)
#define SSC_RSRC_SETR_LEN                    _SSC_UL(1)


#define SSC_ESRC_SRPN_POS                    _SSC_UL(0)
#define SSC_ESRC_SRPN_LEN                    _SSC_UL(8)


#define SSC_ESRC_TOS_POS                     _SSC_UL(10)
#define SSC_ESRC_TOS_LEN                     _SSC_UL(1)


#define SSC_ESRC_SRE_POS                     _SSC_UL(12)
#define SSC_ESRC_SRE_LEN                     _SSC_UL(1)


#define SSC_ESRC_SRR_POS                     _SSC_UL(13)
#define SSC_ESRC_SRR_LEN                     _SSC_UL(1)


#define SSC_ESRC_CLRR_POS                    _SSC_UL(14)
#define SSC_ESRC_CLRR_LEN                    _SSC_UL(1)


#define SSC_ESRC_SETR_POS                    _SSC_UL(15)
#define SSC_ESRC_SETR_LEN                    _SSC_UL(1)


#define SSC0_CLC_DISR_POS                    _SSC_UL(0)
#define SSC0_CLC_DISR_LEN                    _SSC_UL(1)


#define SSC0_CLC_DISS_POS                    _SSC_UL(1)
#define SSC0_CLC_DISS_LEN                    _SSC_UL(1)


#define SSC0_CLC_SPEN_POS                    _SSC_UL(2)
#define SSC0_CLC_SPEN_LEN                    _SSC_UL(1)


#define SSC0_CLC_EDIS_POS                    _SSC_UL(3)
#define SSC0_CLC_EDIS_LEN                    _SSC_UL(1)


#define SSC0_CLC_SBWE_POS                    _SSC_UL(4)
#define SSC0_CLC_SBWE_LEN                    _SSC_UL(1)


#define SSC0_CLC_FSOE_POS                    _SSC_UL(5)
#define SSC0_CLC_FSOE_LEN                    _SSC_UL(1)


#define SSC0_PISEL_MRIS_POS                  _SSC_UL(0)
#define SSC0_PISEL_MRIS_LEN                  _SSC_UL(1)


#define SSC0_PISEL_SRIS_POS                  _SSC_UL(1)
#define SSC0_PISEL_SRIS_LEN                  _SSC_UL(1)


#define SSC0_PISEL_SCIS_POS                  _SSC_UL(2)
#define SSC0_PISEL_SCIS_LEN                  _SSC_UL(1)


#define SSC0_PISEL_SLSIS_POS                 _SSC_UL(3)
#define SSC0_PISEL_SLSIS_LEN                 _SSC_UL(3)


#define SSC0_PISEL_STIP_POS                  _SSC_UL(8)
#define SSC0_PISEL_STIP_LEN                  _SSC_UL(1)


#define SSC0_ID_MODREV_POS                   _SSC_UL(0)
#define SSC0_ID_MODREV_LEN                   _SSC_UL(8)


#define SSC0_ID_MODNUM_POS                   _SSC_UL(8)
#define SSC0_ID_MODNUM_LEN                   _SSC_UL(8)


#define SSC0_FDR_STEP_POS                    _SSC_UL(0)
#define SSC0_FDR_STEP_LEN                    _SSC_UL(10)


#define SSC0_FDR_SM_POS                      _SSC_UL(11)
#define SSC0_FDR_SM_LEN                      _SSC_UL(1)


#define SSC0_FDR_SC_POS                      _SSC_UL(12)
#define SSC0_FDR_SC_LEN                      _SSC_UL(2)


#define SSC0_FDR_DM_POS                      _SSC_UL(14)
#define SSC0_FDR_DM_LEN                      _SSC_UL(2)


#define SSC0_FDR_RESULT_POS                  _SSC_UL(16)
#define SSC0_FDR_RESULT_LEN                  _SSC_UL(10)


#define SSC0_FDR_SUSACK_POS                  _SSC_UL(28)
#define SSC0_FDR_SUSACK_LEN                  _SSC_UL(1)


#define SSC0_FDR_SUSREQ_POS                  _SSC_UL(29)
#define SSC0_FDR_SUSREQ_LEN                  _SSC_UL(1)


#define SSC0_FDR_ENHW_POS                    _SSC_UL(30)
#define SSC0_FDR_ENHW_LEN                    _SSC_UL(1)


#define SSC0_FDR_DISCLK_POS                  _SSC_UL(31)
#define SSC0_FDR_DISCLK_LEN                  _SSC_UL(1)


#define SSC0_CON_BM_POS                      _SSC_UL(0)
#define SSC0_CON_BM_LEN                      _SSC_UL(4)


#define SSC0_CON_HB_POS                      _SSC_UL(4)
#define SSC0_CON_HB_LEN                      _SSC_UL(1)


#define SSC0_CON_PH_POS                      _SSC_UL(5)
#define SSC0_CON_PH_LEN                      _SSC_UL(1)


#define SSC0_CON_PO_POS                      _SSC_UL(6)
#define SSC0_CON_PO_LEN                      _SSC_UL(1)


#define SSC0_CON_LB_POS                      _SSC_UL(7)
#define SSC0_CON_LB_LEN                      _SSC_UL(1)


#define SSC0_CON_TEN_POS                     _SSC_UL(8)
#define SSC0_CON_TEN_LEN                     _SSC_UL(1)


#define SSC0_CON_REN_POS                     _SSC_UL(9)
#define SSC0_CON_REN_LEN                     _SSC_UL(1)


#define SSC0_CON_PEN_POS                     _SSC_UL(10)
#define SSC0_CON_PEN_LEN                     _SSC_UL(1)


#define SSC0_CON_BEN_POS                     _SSC_UL(11)
#define SSC0_CON_BEN_LEN                     _SSC_UL(1)


#define SSC0_CON_AREN_POS                    _SSC_UL(12)
#define SSC0_CON_AREN_LEN                    _SSC_UL(1)


#define SSC0_CON_MS_POS                      _SSC_UL(14)
#define SSC0_CON_MS_LEN                      _SSC_UL(1)


#define SSC0_CON_EN_POS                      _SSC_UL(15)
#define SSC0_CON_EN_LEN                      _SSC_UL(1)


#define SSC0_BR_BR_VALUE_POS                 _SSC_UL(0)
#define SSC0_BR_BR_VALUE_LEN                 _SSC_UL(16)


#define SSC0_SSOC_AOL0_POS                   _SSC_UL(0)
#define SSC0_SSOC_AOL0_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL1_POS                   _SSC_UL(1)
#define SSC0_SSOC_AOL1_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL2_POS                   _SSC_UL(2)
#define SSC0_SSOC_AOL2_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL3_POS                   _SSC_UL(3)
#define SSC0_SSOC_AOL3_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL4_POS                   _SSC_UL(4)
#define SSC0_SSOC_AOL4_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL5_POS                   _SSC_UL(5)
#define SSC0_SSOC_AOL5_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL6_POS                   _SSC_UL(6)
#define SSC0_SSOC_AOL6_LEN                   _SSC_UL(1)


#define SSC0_SSOC_AOL7_POS                   _SSC_UL(7)
#define SSC0_SSOC_AOL7_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN0_POS                   _SSC_UL(8)
#define SSC0_SSOC_OEN0_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN1_POS                   _SSC_UL(9)
#define SSC0_SSOC_OEN1_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN2_POS                   _SSC_UL(10)
#define SSC0_SSOC_OEN2_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN3_POS                   _SSC_UL(11)
#define SSC0_SSOC_OEN3_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN4_POS                   _SSC_UL(12)
#define SSC0_SSOC_OEN4_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN5_POS                   _SSC_UL(13)
#define SSC0_SSOC_OEN5_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN6_POS                   _SSC_UL(14)
#define SSC0_SSOC_OEN6_LEN                   _SSC_UL(1)


#define SSC0_SSOC_OEN7_POS                   _SSC_UL(15)
#define SSC0_SSOC_OEN7_LEN                   _SSC_UL(1)


#define SSC0_SSOTC_LEAD_POS                  _SSC_UL(0)
#define SSC0_SSOTC_LEAD_LEN                  _SSC_UL(2)


#define SSC0_SSOTC_TRAIL_POS                 _SSC_UL(2)
#define SSC0_SSOTC_TRAIL_LEN                 _SSC_UL(2)


#define SSC0_SSOTC_INACT_POS                 _SSC_UL(4)
#define SSC0_SSOTC_INACT_LEN                 _SSC_UL(2)


#define SSC0_SSOTC_SLSO7MOD_POS              _SSC_UL(8)
#define SSC0_SSOTC_SLSO7MOD_LEN              _SSC_UL(1)


#define SSC0_TB_TB_VALUE_POS                 _SSC_UL(0)
#define SSC0_TB_TB_VALUE_LEN                 _SSC_UL(16)


#define SSC0_RB_RB_VALUE_POS                 _SSC_UL(0)
#define SSC0_RB_RB_VALUE_LEN                 _SSC_UL(16)


#define SSC0_STAT_BC_POS                     _SSC_UL(0)
#define SSC0_STAT_BC_LEN                     _SSC_UL(4)


#define SSC0_STAT_TE_POS                     _SSC_UL(8)
#define SSC0_STAT_TE_LEN                     _SSC_UL(1)


#define SSC0_STAT_RE_POS                     _SSC_UL(9)
#define SSC0_STAT_RE_LEN                     _SSC_UL(1)


#define SSC0_STAT_PE_POS                     _SSC_UL(10)
#define SSC0_STAT_PE_LEN                     _SSC_UL(1)


#define SSC0_STAT_BE_POS                     _SSC_UL(11)
#define SSC0_STAT_BE_LEN                     _SSC_UL(1)


#define SSC0_STAT_BSY_POS                    _SSC_UL(12)
#define SSC0_STAT_BSY_LEN                    _SSC_UL(1)


#define SSC0_EFM_CLRTE_POS                   _SSC_UL(8)
#define SSC0_EFM_CLRTE_LEN                   _SSC_UL(1)


#define SSC0_EFM_CLRRE_POS                   _SSC_UL(9)
#define SSC0_EFM_CLRRE_LEN                   _SSC_UL(1)


#define SSC0_EFM_CLRPE_POS                   _SSC_UL(10)
#define SSC0_EFM_CLRPE_LEN                   _SSC_UL(1)


#define SSC0_EFM_CLRBE_POS                   _SSC_UL(11)
#define SSC0_EFM_CLRBE_LEN                   _SSC_UL(1)


#define SSC0_EFM_SETTE_POS                   _SSC_UL(12)
#define SSC0_EFM_SETTE_LEN                   _SSC_UL(1)


#define SSC0_EFM_SETRE_POS                   _SSC_UL(13)
#define SSC0_EFM_SETRE_LEN                   _SSC_UL(1)


#define SSC0_EFM_SETPE_POS                   _SSC_UL(14)
#define SSC0_EFM_SETPE_LEN                   _SSC_UL(1)


#define SSC0_EFM_SETBE_POS                   _SSC_UL(15)
#define SSC0_EFM_SETBE_LEN                   _SSC_UL(1)


#define SSC0_TSRC_SRPN_POS                   _SSC_UL(0)
#define SSC0_TSRC_SRPN_LEN                   _SSC_UL(8)


#define SSC0_TSRC_TOS_POS                    _SSC_UL(10)
#define SSC0_TSRC_TOS_LEN                    _SSC_UL(1)


#define SSC0_TSRC_SRE_POS                    _SSC_UL(12)
#define SSC0_TSRC_SRE_LEN                    _SSC_UL(1)


#define SSC0_TSRC_SRR_POS                    _SSC_UL(13)
#define SSC0_TSRC_SRR_LEN                    _SSC_UL(1)


#define SSC0_TSRC_CLRR_POS                   _SSC_UL(14)
#define SSC0_TSRC_CLRR_LEN                   _SSC_UL(1)


#define SSC0_TSRC_SETR_POS                   _SSC_UL(15)
#define SSC0_TSRC_SETR_LEN                   _SSC_UL(1)


#define SSC0_RSRC_SRPN_POS                   _SSC_UL(0)
#define SSC0_RSRC_SRPN_LEN                   _SSC_UL(8)


#define SSC0_RSRC_TOS_POS                    _SSC_UL(10)
#define SSC0_RSRC_TOS_LEN                    _SSC_UL(1)


#define SSC0_RSRC_SRE_POS                    _SSC_UL(12)
#define SSC0_RSRC_SRE_LEN                    _SSC_UL(1)


#define SSC0_RSRC_SRR_POS                    _SSC_UL(13)
#define SSC0_RSRC_SRR_LEN                    _SSC_UL(1)


#define SSC0_RSRC_CLRR_POS                   _SSC_UL(14)
#define SSC0_RSRC_CLRR_LEN                   _SSC_UL(1)


#define SSC0_RSRC_SETR_POS                   _SSC_UL(15)
#define SSC0_RSRC_SETR_LEN                   _SSC_UL(1)


#define SSC0_ESRC_SRPN_POS                   _SSC_UL(0)
#define SSC0_ESRC_SRPN_LEN                   _SSC_UL(8)


#define SSC0_ESRC_TOS_POS                    _SSC_UL(10)
#define SSC0_ESRC_TOS_LEN                    _SSC_UL(1)


#define SSC0_ESRC_SRE_POS                    _SSC_UL(12)
#define SSC0_ESRC_SRE_LEN                    _SSC_UL(1)


#define SSC0_ESRC_SRR_POS                    _SSC_UL(13)
#define SSC0_ESRC_SRR_LEN                    _SSC_UL(1)


#define SSC0_ESRC_CLRR_POS                   _SSC_UL(14)
#define SSC0_ESRC_CLRR_LEN                   _SSC_UL(1)


#define SSC0_ESRC_SETR_POS                   _SSC_UL(15)
#define SSC0_ESRC_SETR_LEN                   _SSC_UL(1)


#define SSC1_CLC_DISR_POS                    _SSC_UL(0)
#define SSC1_CLC_DISR_LEN                    _SSC_UL(1)


#define SSC1_CLC_DISS_POS                    _SSC_UL(1)
#define SSC1_CLC_DISS_LEN                    _SSC_UL(1)


#define SSC1_CLC_SPEN_POS                    _SSC_UL(2)
#define SSC1_CLC_SPEN_LEN                    _SSC_UL(1)


#define SSC1_CLC_EDIS_POS                    _SSC_UL(3)
#define SSC1_CLC_EDIS_LEN                    _SSC_UL(1)


#define SSC1_CLC_SBWE_POS                    _SSC_UL(4)
#define SSC1_CLC_SBWE_LEN                    _SSC_UL(1)


#define SSC1_CLC_FSOE_POS                    _SSC_UL(5)
#define SSC1_CLC_FSOE_LEN                    _SSC_UL(1)


#define SSC1_PISEL_MRIS_POS                  _SSC_UL(0)
#define SSC1_PISEL_MRIS_LEN                  _SSC_UL(1)


#define SSC1_PISEL_SRIS_POS                  _SSC_UL(1)
#define SSC1_PISEL_SRIS_LEN                  _SSC_UL(1)


#define SSC1_PISEL_SCIS_POS                  _SSC_UL(2)
#define SSC1_PISEL_SCIS_LEN                  _SSC_UL(1)


#define SSC1_PISEL_SLSIS_POS                 _SSC_UL(3)
#define SSC1_PISEL_SLSIS_LEN                 _SSC_UL(3)


#define SSC1_PISEL_STIP_POS                  _SSC_UL(8)
#define SSC1_PISEL_STIP_LEN                  _SSC_UL(1)


#define SSC1_ID_MODREV_POS                   _SSC_UL(0)
#define SSC1_ID_MODREV_LEN                   _SSC_UL(8)


#define SSC1_ID_MODNUM_POS                   _SSC_UL(8)
#define SSC1_ID_MODNUM_LEN                   _SSC_UL(8)


#define SSC1_FDR_STEP_POS                    _SSC_UL(0)
#define SSC1_FDR_STEP_LEN                    _SSC_UL(10)


#define SSC1_FDR_SM_POS                      _SSC_UL(11)
#define SSC1_FDR_SM_LEN                      _SSC_UL(1)


#define SSC1_FDR_SC_POS                      _SSC_UL(12)
#define SSC1_FDR_SC_LEN                      _SSC_UL(2)


#define SSC1_FDR_DM_POS                      _SSC_UL(14)
#define SSC1_FDR_DM_LEN                      _SSC_UL(2)


#define SSC1_FDR_RESULT_POS                  _SSC_UL(16)
#define SSC1_FDR_RESULT_LEN                  _SSC_UL(10)


#define SSC1_FDR_SUSACK_POS                  _SSC_UL(28)
#define SSC1_FDR_SUSACK_LEN                  _SSC_UL(1)


#define SSC1_FDR_SUSREQ_POS                  _SSC_UL(29)
#define SSC1_FDR_SUSREQ_LEN                  _SSC_UL(1)


#define SSC1_FDR_ENHW_POS                    _SSC_UL(30)
#define SSC1_FDR_ENHW_LEN                    _SSC_UL(1)


#define SSC1_FDR_DISCLK_POS                  _SSC_UL(31)
#define SSC1_FDR_DISCLK_LEN                  _SSC_UL(1)


#define SSC1_CON_BM_POS                      _SSC_UL(0)
#define SSC1_CON_BM_LEN                      _SSC_UL(4)


#define SSC1_CON_HB_POS                      _SSC_UL(4)
#define SSC1_CON_HB_LEN                      _SSC_UL(1)


#define SSC1_CON_PH_POS                      _SSC_UL(5)
#define SSC1_CON_PH_LEN                      _SSC_UL(1)


#define SSC1_CON_PO_POS                      _SSC_UL(6)
#define SSC1_CON_PO_LEN                      _SSC_UL(1)


#define SSC1_CON_LB_POS                      _SSC_UL(7)
#define SSC1_CON_LB_LEN                      _SSC_UL(1)


#define SSC1_CON_TEN_POS                     _SSC_UL(8)
#define SSC1_CON_TEN_LEN                     _SSC_UL(1)


#define SSC1_CON_REN_POS                     _SSC_UL(9)
#define SSC1_CON_REN_LEN                     _SSC_UL(1)


#define SSC1_CON_PEN_POS                     _SSC_UL(10)
#define SSC1_CON_PEN_LEN                     _SSC_UL(1)


#define SSC1_CON_BEN_POS                     _SSC_UL(11)
#define SSC1_CON_BEN_LEN                     _SSC_UL(1)


#define SSC1_CON_AREN_POS                    _SSC_UL(12)
#define SSC1_CON_AREN_LEN                    _SSC_UL(1)


#define SSC1_CON_MS_POS                      _SSC_UL(14)
#define SSC1_CON_MS_LEN                      _SSC_UL(1)


#define SSC1_CON_EN_POS                      _SSC_UL(15)
#define SSC1_CON_EN_LEN                      _SSC_UL(1)


#define SSC1_BR_BR_VALUE_POS                 _SSC_UL(0)
#define SSC1_BR_BR_VALUE_LEN                 _SSC_UL(16)


#define SSC1_SSOC_AOL0_POS                   _SSC_UL(0)
#define SSC1_SSOC_AOL0_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL1_POS                   _SSC_UL(1)
#define SSC1_SSOC_AOL1_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL2_POS                   _SSC_UL(2)
#define SSC1_SSOC_AOL2_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL3_POS                   _SSC_UL(3)
#define SSC1_SSOC_AOL3_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL4_POS                   _SSC_UL(4)
#define SSC1_SSOC_AOL4_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL5_POS                   _SSC_UL(5)
#define SSC1_SSOC_AOL5_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL6_POS                   _SSC_UL(6)
#define SSC1_SSOC_AOL6_LEN                   _SSC_UL(1)


#define SSC1_SSOC_AOL7_POS                   _SSC_UL(7)
#define SSC1_SSOC_AOL7_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN0_POS                   _SSC_UL(8)
#define SSC1_SSOC_OEN0_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN1_POS                   _SSC_UL(9)
#define SSC1_SSOC_OEN1_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN2_POS                   _SSC_UL(10)
#define SSC1_SSOC_OEN2_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN3_POS                   _SSC_UL(11)
#define SSC1_SSOC_OEN3_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN4_POS                   _SSC_UL(12)
#define SSC1_SSOC_OEN4_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN5_POS                   _SSC_UL(13)
#define SSC1_SSOC_OEN5_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN6_POS                   _SSC_UL(14)
#define SSC1_SSOC_OEN6_LEN                   _SSC_UL(1)


#define SSC1_SSOC_OEN7_POS                   _SSC_UL(15)
#define SSC1_SSOC_OEN7_LEN                   _SSC_UL(1)


#define SSC1_SSOTC_LEAD_POS                  _SSC_UL(0)
#define SSC1_SSOTC_LEAD_LEN                  _SSC_UL(2)


#define SSC1_SSOTC_TRAIL_POS                 _SSC_UL(2)
#define SSC1_SSOTC_TRAIL_LEN                 _SSC_UL(2)


#define SSC1_SSOTC_INACT_POS                 _SSC_UL(4)
#define SSC1_SSOTC_INACT_LEN                 _SSC_UL(2)


#define SSC1_SSOTC_SLSO7MOD_POS              _SSC_UL(8)
#define SSC1_SSOTC_SLSO7MOD_LEN              _SSC_UL(1)


#define SSC1_TB_TB_VALUE_POS                 _SSC_UL(0)
#define SSC1_TB_TB_VALUE_LEN                 _SSC_UL(16)


#define SSC1_RB_RB_VALUE_POS                 _SSC_UL(0)
#define SSC1_RB_RB_VALUE_LEN                 _SSC_UL(16)


#define SSC1_STAT_BC_POS                     _SSC_UL(0)
#define SSC1_STAT_BC_LEN                     _SSC_UL(4)


#define SSC1_STAT_TE_POS                     _SSC_UL(8)
#define SSC1_STAT_TE_LEN                     _SSC_UL(1)


#define SSC1_STAT_RE_POS                     _SSC_UL(9)
#define SSC1_STAT_RE_LEN                     _SSC_UL(1)


#define SSC1_STAT_PE_POS                     _SSC_UL(10)
#define SSC1_STAT_PE_LEN                     _SSC_UL(1)


#define SSC1_STAT_BE_POS                     _SSC_UL(11)
#define SSC1_STAT_BE_LEN                     _SSC_UL(1)


#define SSC1_STAT_BSY_POS                    _SSC_UL(12)
#define SSC1_STAT_BSY_LEN                    _SSC_UL(1)


#define SSC1_EFM_CLRTE_POS                   _SSC_UL(8)
#define SSC1_EFM_CLRTE_LEN                   _SSC_UL(1)


#define SSC1_EFM_CLRRE_POS                   _SSC_UL(9)
#define SSC1_EFM_CLRRE_LEN                   _SSC_UL(1)


#define SSC1_EFM_CLRPE_POS                   _SSC_UL(10)
#define SSC1_EFM_CLRPE_LEN                   _SSC_UL(1)


#define SSC1_EFM_CLRBE_POS                   _SSC_UL(11)
#define SSC1_EFM_CLRBE_LEN                   _SSC_UL(1)


#define SSC1_EFM_SETTE_POS                   _SSC_UL(12)
#define SSC1_EFM_SETTE_LEN                   _SSC_UL(1)


#define SSC1_EFM_SETRE_POS                   _SSC_UL(13)
#define SSC1_EFM_SETRE_LEN                   _SSC_UL(1)


#define SSC1_EFM_SETPE_POS                   _SSC_UL(14)
#define SSC1_EFM_SETPE_LEN                   _SSC_UL(1)


#define SSC1_EFM_SETBE_POS                   _SSC_UL(15)
#define SSC1_EFM_SETBE_LEN                   _SSC_UL(1)


#define SSC1_TSRC_SRPN_POS                   _SSC_UL(0)
#define SSC1_TSRC_SRPN_LEN                   _SSC_UL(8)


#define SSC1_TSRC_TOS_POS                    _SSC_UL(10)
#define SSC1_TSRC_TOS_LEN                    _SSC_UL(1)


#define SSC1_TSRC_SRE_POS                    _SSC_UL(12)
#define SSC1_TSRC_SRE_LEN                    _SSC_UL(1)


#define SSC1_TSRC_SRR_POS                    _SSC_UL(13)
#define SSC1_TSRC_SRR_LEN                    _SSC_UL(1)


#define SSC1_TSRC_CLRR_POS                   _SSC_UL(14)
#define SSC1_TSRC_CLRR_LEN                   _SSC_UL(1)


#define SSC1_TSRC_SETR_POS                   _SSC_UL(15)
#define SSC1_TSRC_SETR_LEN                   _SSC_UL(1)


#define SSC1_RSRC_SRPN_POS                   _SSC_UL(0)
#define SSC1_RSRC_SRPN_LEN                   _SSC_UL(8)


#define SSC1_RSRC_TOS_POS                    _SSC_UL(10)
#define SSC1_RSRC_TOS_LEN                    _SSC_UL(1)


#define SSC1_RSRC_SRE_POS                    _SSC_UL(12)
#define SSC1_RSRC_SRE_LEN                    _SSC_UL(1)


#define SSC1_RSRC_SRR_POS                    _SSC_UL(13)
#define SSC1_RSRC_SRR_LEN                    _SSC_UL(1)


#define SSC1_RSRC_CLRR_POS                   _SSC_UL(14)
#define SSC1_RSRC_CLRR_LEN                   _SSC_UL(1)


#define SSC1_RSRC_SETR_POS                   _SSC_UL(15)
#define SSC1_RSRC_SETR_LEN                   _SSC_UL(1)


#define SSC1_ESRC_SRPN_POS                   _SSC_UL(0)
#define SSC1_ESRC_SRPN_LEN                   _SSC_UL(8)


#define SSC1_ESRC_TOS_POS                    _SSC_UL(10)
#define SSC1_ESRC_TOS_LEN                    _SSC_UL(1)


#define SSC1_ESRC_SRE_POS                    _SSC_UL(12)
#define SSC1_ESRC_SRE_LEN                    _SSC_UL(1)


#define SSC1_ESRC_SRR_POS                    _SSC_UL(13)
#define SSC1_ESRC_SRR_LEN                    _SSC_UL(1)


#define SSC1_ESRC_CLRR_POS                   _SSC_UL(14)
#define SSC1_ESRC_CLRR_LEN                   _SSC_UL(1)


#define SSC1_ESRC_SETR_POS                   _SSC_UL(15)
#define SSC1_ESRC_SETR_LEN                   _SSC_UL(1)

#endif
