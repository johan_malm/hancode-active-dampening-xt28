

#ifndef _MSC_1797_H
#define _MSC_1797_H


#ifdef REGDEF_FOR_PCP
    #define _MSC_UL(x) x
#else
    #define _MSC_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 CLC;                 
    volatile uint32 RESERVED0[1];        
    volatile uint32 ID;                  
    volatile uint32 FDR;                 
    volatile uint32 USR;                 
    volatile uint32 DSC;                 
    volatile uint32 DSS;                 
    volatile uint32 DD;                  
    volatile uint32 DC;                  
    volatile uint32 DSDSL;               
    volatile uint32 DSDSH;               
    volatile uint32 ESR;                 
    volatile uint32 UD[4];               
    volatile uint32 ICR;                 
    volatile uint32 ISR;                 
    volatile uint32 ISC;                 
    volatile uint32 OCR;                 
    volatile uint32 RESERVED1[42];       
    volatile uint32 SRC1;                
    volatile uint32 SRC0;                
} MSC_RegMap_t;




extern MSC_RegMap_t MSC0 __attribute__ ((asection (".zbss.label_only","f=awz")));
extern MSC_RegMap_t MSC1 __attribute__ ((asection (".zbss.label_only","f=awz")));
extern MSC_RegMap_t MSC[2] __attribute__ ((asection (".zbss.label_only","f=awz")));





#define MSC_ID_MOD_NUMBER_POS                _MSC_UL(16)
#define MSC_ID_MOD_NUMBER_LEN                _MSC_UL(16)


#define MSC_ID_MOD_REV_POS                   _MSC_UL(0)
#define MSC_ID_MOD_REV_LEN                   _MSC_UL(8)


#define MSC_ID_MOD_TYPE_POS                  _MSC_UL(8)
#define MSC_ID_MOD_TYPE_LEN                  _MSC_UL(8)


#define MSC0_ID_MOD_NUMBER_POS               _MSC_UL(16)
#define MSC0_ID_MOD_NUMBER_LEN               _MSC_UL(16)


#define MSC0_ID_MOD_REV_POS                  _MSC_UL(0)
#define MSC0_ID_MOD_REV_LEN                  _MSC_UL(8)


#define MSC0_ID_MOD_TYPE_POS                 _MSC_UL(8)
#define MSC0_ID_MOD_TYPE_LEN                 _MSC_UL(8)


#define MSC1_ID_MOD_NUMBER_POS               _MSC_UL(16)
#define MSC1_ID_MOD_NUMBER_LEN               _MSC_UL(16)


#define MSC1_ID_MOD_REV_POS                  _MSC_UL(0)
#define MSC1_ID_MOD_REV_LEN                  _MSC_UL(8)


#define MSC1_ID_MOD_TYPE_POS                 _MSC_UL(8)
#define MSC1_ID_MOD_TYPE_LEN                 _MSC_UL(8)


#define MSC_CLC_DISR_POS                     _MSC_UL(0)
#define MSC_CLC_DISR_LEN                     _MSC_UL(1)


#define MSC_CLC_DISS_POS                     _MSC_UL(1)
#define MSC_CLC_DISS_LEN                     _MSC_UL(1)


#define MSC_CLC_SPEN_POS                     _MSC_UL(2)
#define MSC_CLC_SPEN_LEN                     _MSC_UL(1)


#define MSC_CLC_EDIS_POS                     _MSC_UL(3)
#define MSC_CLC_EDIS_LEN                     _MSC_UL(1)


#define MSC_CLC_SBWE_POS                     _MSC_UL(4)
#define MSC_CLC_SBWE_LEN                     _MSC_UL(1)


#define MSC_CLC_FSOE_POS                     _MSC_UL(5)
#define MSC_CLC_FSOE_LEN                     _MSC_UL(1)


#define MSC_ID_MODREV_POS                    _MSC_UL(0)
#define MSC_ID_MODREV_LEN                    _MSC_UL(8)


#define MSC_ID_MODTYPE_POS                   _MSC_UL(8)
#define MSC_ID_MODTYPE_LEN                   _MSC_UL(8)


#define MSC_ID_MODNUM_POS                    _MSC_UL(16)
#define MSC_ID_MODNUM_LEN                    _MSC_UL(16)


#define MSC_FDR_STEP_POS                     _MSC_UL(0)
#define MSC_FDR_STEP_LEN                     _MSC_UL(10)


#define MSC_FDR_SM_POS                       _MSC_UL(11)
#define MSC_FDR_SM_LEN                       _MSC_UL(1)


#define MSC_FDR_SC_POS                       _MSC_UL(12)
#define MSC_FDR_SC_LEN                       _MSC_UL(2)


#define MSC_FDR_DM_POS                       _MSC_UL(14)
#define MSC_FDR_DM_LEN                       _MSC_UL(2)


#define MSC_FDR_RESULT_POS                   _MSC_UL(16)
#define MSC_FDR_RESULT_LEN                   _MSC_UL(10)


#define MSC_FDR_SUSACK_POS                   _MSC_UL(28)
#define MSC_FDR_SUSACK_LEN                   _MSC_UL(1)


#define MSC_FDR_SUSREQ_POS                   _MSC_UL(29)
#define MSC_FDR_SUSREQ_LEN                   _MSC_UL(1)


#define MSC_FDR_ENHW_POS                     _MSC_UL(30)
#define MSC_FDR_ENHW_LEN                     _MSC_UL(1)


#define MSC_FDR_DISCLK_POS                   _MSC_UL(31)
#define MSC_FDR_DISCLK_LEN                   _MSC_UL(1)


#define MSC_USR_UFT_POS                      _MSC_UL(0)
#define MSC_USR_UFT_LEN                      _MSC_UL(1)


#define MSC_USR_URR_POS                      _MSC_UL(1)
#define MSC_USR_URR_LEN                      _MSC_UL(3)


#define MSC_USR_PCTR_POS                     _MSC_UL(4)
#define MSC_USR_PCTR_LEN                     _MSC_UL(1)


#define MSC_USR_UC_POS                       _MSC_UL(16)
#define MSC_USR_UC_LEN                       _MSC_UL(5)


#define MSC_DSC_TM_POS                       _MSC_UL(0)
#define MSC_DSC_TM_LEN                       _MSC_UL(1)


#define MSC_DSC_CP_POS                       _MSC_UL(1)
#define MSC_DSC_CP_LEN                       _MSC_UL(1)


#define MSC_DSC_DP_POS                       _MSC_UL(2)
#define MSC_DSC_DP_LEN                       _MSC_UL(1)


#define MSC_DSC_NDBL_POS                     _MSC_UL(3)
#define MSC_DSC_NDBL_LEN                     _MSC_UL(5)


#define MSC_DSC_NDBH_POS                     _MSC_UL(8)
#define MSC_DSC_NDBH_LEN                     _MSC_UL(5)


#define MSC_DSC_ENSELL_POS                   _MSC_UL(13)
#define MSC_DSC_ENSELL_LEN                   _MSC_UL(1)


#define MSC_DSC_ENSELH_POS                   _MSC_UL(14)
#define MSC_DSC_ENSELH_LEN                   _MSC_UL(1)


#define MSC_DSC_DSDIS_POS                    _MSC_UL(15)
#define MSC_DSC_DSDIS_LEN                    _MSC_UL(1)


#define MSC_DSC_NBC_POS                      _MSC_UL(16)
#define MSC_DSC_NBC_LEN                      _MSC_UL(6)


#define MSC_DSC_PPD_POS                      _MSC_UL(24)
#define MSC_DSC_PPD_LEN                      _MSC_UL(5)


#define MSC_DSS_PFC_POS                      _MSC_UL(0)
#define MSC_DSS_PFC_LEN                      _MSC_UL(4)


#define MSC_DSS_NPTF_POS                     _MSC_UL(8)
#define MSC_DSS_NPTF_LEN                     _MSC_UL(4)


#define MSC_DSS_DC_POS                       _MSC_UL(16)
#define MSC_DSS_DC_LEN                       _MSC_UL(7)


#define MSC_DSS_DFA_POS                      _MSC_UL(24)
#define MSC_DSS_DFA_LEN                      _MSC_UL(1)


#define MSC_DSS_CFA_POS                      _MSC_UL(25)
#define MSC_DSS_CFA_LEN                      _MSC_UL(1)


#define MSC_DD_DDL_POS                       _MSC_UL(0)
#define MSC_DD_DDL_LEN                       _MSC_UL(16)


#define MSC_DD_DDH_POS                       _MSC_UL(16)
#define MSC_DD_DDH_LEN                       _MSC_UL(16)


#define MSC_DC_DCL_POS                       _MSC_UL(0)
#define MSC_DC_DCL_LEN                       _MSC_UL(16)


#define MSC_DC_DCH_POS                       _MSC_UL(16)
#define MSC_DC_DCH_LEN                       _MSC_UL(16)


#define MSC_DSDSL_SL0_POS                    _MSC_UL(0)
#define MSC_DSDSL_SL0_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL1_POS                    _MSC_UL(2)
#define MSC_DSDSL_SL1_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL2_POS                    _MSC_UL(4)
#define MSC_DSDSL_SL2_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL3_POS                    _MSC_UL(6)
#define MSC_DSDSL_SL3_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL4_POS                    _MSC_UL(8)
#define MSC_DSDSL_SL4_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL5_POS                    _MSC_UL(10)
#define MSC_DSDSL_SL5_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL6_POS                    _MSC_UL(12)
#define MSC_DSDSL_SL6_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL7_POS                    _MSC_UL(14)
#define MSC_DSDSL_SL7_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL8_POS                    _MSC_UL(16)
#define MSC_DSDSL_SL8_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL9_POS                    _MSC_UL(18)
#define MSC_DSDSL_SL9_LEN                    _MSC_UL(2)


#define MSC_DSDSL_SL10_POS                   _MSC_UL(20)
#define MSC_DSDSL_SL10_LEN                   _MSC_UL(2)


#define MSC_DSDSL_SL11_POS                   _MSC_UL(22)
#define MSC_DSDSL_SL11_LEN                   _MSC_UL(2)


#define MSC_DSDSL_SL12_POS                   _MSC_UL(24)
#define MSC_DSDSL_SL12_LEN                   _MSC_UL(2)


#define MSC_DSDSL_SL13_POS                   _MSC_UL(26)
#define MSC_DSDSL_SL13_LEN                   _MSC_UL(2)


#define MSC_DSDSL_SL14_POS                   _MSC_UL(28)
#define MSC_DSDSL_SL14_LEN                   _MSC_UL(2)


#define MSC_DSDSL_SL15_POS                   _MSC_UL(30)
#define MSC_DSDSL_SL15_LEN                   _MSC_UL(2)


#define MSC_DSDSH_SH0_POS                    _MSC_UL(0)
#define MSC_DSDSH_SH0_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH1_POS                    _MSC_UL(2)
#define MSC_DSDSH_SH1_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH2_POS                    _MSC_UL(4)
#define MSC_DSDSH_SH2_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH3_POS                    _MSC_UL(6)
#define MSC_DSDSH_SH3_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH4_POS                    _MSC_UL(8)
#define MSC_DSDSH_SH4_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH5_POS                    _MSC_UL(10)
#define MSC_DSDSH_SH5_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH6_POS                    _MSC_UL(12)
#define MSC_DSDSH_SH6_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH7_POS                    _MSC_UL(14)
#define MSC_DSDSH_SH7_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH8_POS                    _MSC_UL(16)
#define MSC_DSDSH_SH8_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH9_POS                    _MSC_UL(18)
#define MSC_DSDSH_SH9_LEN                    _MSC_UL(2)


#define MSC_DSDSH_SH10_POS                   _MSC_UL(20)
#define MSC_DSDSH_SH10_LEN                   _MSC_UL(2)


#define MSC_DSDSH_SH11_POS                   _MSC_UL(22)
#define MSC_DSDSH_SH11_LEN                   _MSC_UL(2)


#define MSC_DSDSH_SH12_POS                   _MSC_UL(24)
#define MSC_DSDSH_SH12_LEN                   _MSC_UL(2)


#define MSC_DSDSH_SH13_POS                   _MSC_UL(26)
#define MSC_DSDSH_SH13_LEN                   _MSC_UL(2)


#define MSC_DSDSH_SH14_POS                   _MSC_UL(28)
#define MSC_DSDSH_SH14_LEN                   _MSC_UL(2)


#define MSC_DSDSH_SH15_POS                   _MSC_UL(30)
#define MSC_DSDSH_SH15_LEN                   _MSC_UL(2)


#define MSC_ESR_ENL0_POS                     _MSC_UL(0)
#define MSC_ESR_ENL0_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL1_POS                     _MSC_UL(1)
#define MSC_ESR_ENL1_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL2_POS                     _MSC_UL(2)
#define MSC_ESR_ENL2_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL3_POS                     _MSC_UL(3)
#define MSC_ESR_ENL3_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL4_POS                     _MSC_UL(4)
#define MSC_ESR_ENL4_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL5_POS                     _MSC_UL(5)
#define MSC_ESR_ENL5_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL6_POS                     _MSC_UL(6)
#define MSC_ESR_ENL6_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL7_POS                     _MSC_UL(7)
#define MSC_ESR_ENL7_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL8_POS                     _MSC_UL(8)
#define MSC_ESR_ENL8_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL9_POS                     _MSC_UL(9)
#define MSC_ESR_ENL9_LEN                     _MSC_UL(1)


#define MSC_ESR_ENL10_POS                    _MSC_UL(10)
#define MSC_ESR_ENL10_LEN                    _MSC_UL(1)


#define MSC_ESR_ENL11_POS                    _MSC_UL(11)
#define MSC_ESR_ENL11_LEN                    _MSC_UL(1)


#define MSC_ESR_ENL12_POS                    _MSC_UL(12)
#define MSC_ESR_ENL12_LEN                    _MSC_UL(1)


#define MSC_ESR_ENL13_POS                    _MSC_UL(13)
#define MSC_ESR_ENL13_LEN                    _MSC_UL(1)


#define MSC_ESR_ENL14_POS                    _MSC_UL(14)
#define MSC_ESR_ENL14_LEN                    _MSC_UL(1)


#define MSC_ESR_ENL15_POS                    _MSC_UL(15)
#define MSC_ESR_ENL15_LEN                    _MSC_UL(1)


#define MSC_ESR_ENH0_POS                     _MSC_UL(16)
#define MSC_ESR_ENH0_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH1_POS                     _MSC_UL(17)
#define MSC_ESR_ENH1_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH2_POS                     _MSC_UL(18)
#define MSC_ESR_ENH2_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH3_POS                     _MSC_UL(19)
#define MSC_ESR_ENH3_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH4_POS                     _MSC_UL(20)
#define MSC_ESR_ENH4_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH5_POS                     _MSC_UL(21)
#define MSC_ESR_ENH5_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH6_POS                     _MSC_UL(22)
#define MSC_ESR_ENH6_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH7_POS                     _MSC_UL(23)
#define MSC_ESR_ENH7_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH8_POS                     _MSC_UL(24)
#define MSC_ESR_ENH8_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH9_POS                     _MSC_UL(25)
#define MSC_ESR_ENH9_LEN                     _MSC_UL(1)


#define MSC_ESR_ENH10_POS                    _MSC_UL(26)
#define MSC_ESR_ENH10_LEN                    _MSC_UL(1)


#define MSC_ESR_ENH11_POS                    _MSC_UL(27)
#define MSC_ESR_ENH11_LEN                    _MSC_UL(1)


#define MSC_ESR_ENH12_POS                    _MSC_UL(28)
#define MSC_ESR_ENH12_LEN                    _MSC_UL(1)


#define MSC_ESR_ENH13_POS                    _MSC_UL(29)
#define MSC_ESR_ENH13_LEN                    _MSC_UL(1)


#define MSC_ESR_ENH14_POS                    _MSC_UL(30)
#define MSC_ESR_ENH14_LEN                    _MSC_UL(1)


#define MSC_ESR_ENH15_POS                    _MSC_UL(31)
#define MSC_ESR_ENH15_LEN                    _MSC_UL(1)


#define MSC_UD_DATA_POS                      _MSC_UL(0)
#define MSC_UD_DATA_LEN                      _MSC_UL(8)


#define MSC_UD_V_POS                         _MSC_UL(16)
#define MSC_UD_V_LEN                         _MSC_UL(1)


#define MSC_UD_P_POS                         _MSC_UL(17)
#define MSC_UD_P_LEN                         _MSC_UL(1)


#define MSC_UD_C_POS                         _MSC_UL(18)
#define MSC_UD_C_LEN                         _MSC_UL(1)


#define MSC_UD_LABF_POS                      _MSC_UL(19)
#define MSC_UD_LABF_LEN                      _MSC_UL(2)


#define MSC_UD_IPF_POS                       _MSC_UL(21)
#define MSC_UD_IPF_LEN                       _MSC_UL(1)


#define MSC_UD_PERR_POS                      _MSC_UL(22)
#define MSC_UD_PERR_LEN                      _MSC_UL(1)


#define MSC_ICR_EDIP_POS                     _MSC_UL(0)
#define MSC_ICR_EDIP_LEN                     _MSC_UL(2)


#define MSC_ICR_EDIE_POS                     _MSC_UL(2)
#define MSC_ICR_EDIE_LEN                     _MSC_UL(2)


#define MSC_ICR_ECIP_POS                     _MSC_UL(4)
#define MSC_ICR_ECIP_LEN                     _MSC_UL(2)


#define MSC_ICR_ECIE_POS                     _MSC_UL(7)
#define MSC_ICR_ECIE_LEN                     _MSC_UL(1)


#define MSC_ICR_TFIP_POS                     _MSC_UL(8)
#define MSC_ICR_TFIP_LEN                     _MSC_UL(2)


#define MSC_ICR_TFIE_POS                     _MSC_UL(11)
#define MSC_ICR_TFIE_LEN                     _MSC_UL(1)


#define MSC_ICR_RDIP_POS                     _MSC_UL(12)
#define MSC_ICR_RDIP_LEN                     _MSC_UL(2)


#define MSC_ICR_RDIE_POS                     _MSC_UL(14)
#define MSC_ICR_RDIE_LEN                     _MSC_UL(2)


#define MSC_ISR_DEDI_POS                     _MSC_UL(0)
#define MSC_ISR_DEDI_LEN                     _MSC_UL(1)


#define MSC_ISR_DECI_POS                     _MSC_UL(1)
#define MSC_ISR_DECI_LEN                     _MSC_UL(1)


#define MSC_ISR_DTFI_POS                     _MSC_UL(2)
#define MSC_ISR_DTFI_LEN                     _MSC_UL(1)


#define MSC_ISR_URDI_POS                     _MSC_UL(3)
#define MSC_ISR_URDI_LEN                     _MSC_UL(1)


#define MSC_ISC_CDEDI_POS                    _MSC_UL(0)
#define MSC_ISC_CDEDI_LEN                    _MSC_UL(1)


#define MSC_ISC_CDECI_POS                    _MSC_UL(1)
#define MSC_ISC_CDECI_LEN                    _MSC_UL(1)


#define MSC_ISC_CDTFI_POS                    _MSC_UL(2)
#define MSC_ISC_CDTFI_LEN                    _MSC_UL(1)


#define MSC_ISC_CURDI_POS                    _MSC_UL(3)
#define MSC_ISC_CURDI_LEN                    _MSC_UL(1)


#define MSC_ISC_CDP_POS                      _MSC_UL(4)
#define MSC_ISC_CDP_LEN                      _MSC_UL(1)


#define MSC_ISC_CCP_POS                      _MSC_UL(5)
#define MSC_ISC_CCP_LEN                      _MSC_UL(1)


#define MSC_ISC_CDDIS_POS                    _MSC_UL(6)
#define MSC_ISC_CDDIS_LEN                    _MSC_UL(1)


#define MSC_ISC_SDEDI_POS                    _MSC_UL(16)
#define MSC_ISC_SDEDI_LEN                    _MSC_UL(1)


#define MSC_ISC_SDECI_POS                    _MSC_UL(17)
#define MSC_ISC_SDECI_LEN                    _MSC_UL(1)


#define MSC_ISC_SDTFI_POS                    _MSC_UL(18)
#define MSC_ISC_SDTFI_LEN                    _MSC_UL(1)


#define MSC_ISC_SURDI_POS                    _MSC_UL(19)
#define MSC_ISC_SURDI_LEN                    _MSC_UL(1)


#define MSC_ISC_SDP_POS                      _MSC_UL(20)
#define MSC_ISC_SDP_LEN                      _MSC_UL(1)


#define MSC_ISC_SCP_POS                      _MSC_UL(21)
#define MSC_ISC_SCP_LEN                      _MSC_UL(1)


#define MSC_ISC_SDDIS_POS                    _MSC_UL(22)
#define MSC_ISC_SDDIS_LEN                    _MSC_UL(1)


#define MSC_OCR_CLP_POS                      _MSC_UL(0)
#define MSC_OCR_CLP_LEN                      _MSC_UL(1)


#define MSC_OCR_SLP_POS                      _MSC_UL(1)
#define MSC_OCR_SLP_LEN                      _MSC_UL(1)


#define MSC_OCR_CSLP_POS                     _MSC_UL(2)
#define MSC_OCR_CSLP_LEN                     _MSC_UL(1)


#define MSC_OCR_ILP_POS                      _MSC_UL(3)
#define MSC_OCR_ILP_LEN                      _MSC_UL(1)


#define MSC_OCR_CLKCTRL_POS                  _MSC_UL(8)
#define MSC_OCR_CLKCTRL_LEN                  _MSC_UL(1)


#define MSC_OCR_CSL_POS                      _MSC_UL(9)
#define MSC_OCR_CSL_LEN                      _MSC_UL(2)


#define MSC_OCR_CSH_POS                      _MSC_UL(11)
#define MSC_OCR_CSH_LEN                      _MSC_UL(2)


#define MSC_OCR_CSC_POS                      _MSC_UL(13)
#define MSC_OCR_CSC_LEN                      _MSC_UL(2)


#define MSC_OCR_SDISEL_POS                   _MSC_UL(16)
#define MSC_OCR_SDISEL_LEN                   _MSC_UL(3)


#define MSC_SRC1_SRPN_POS                    _MSC_UL(0)
#define MSC_SRC1_SRPN_LEN                    _MSC_UL(8)


#define MSC_SRC1_TOS_POS                     _MSC_UL(10)
#define MSC_SRC1_TOS_LEN                     _MSC_UL(1)


#define MSC_SRC1_SRE_POS                     _MSC_UL(12)
#define MSC_SRC1_SRE_LEN                     _MSC_UL(1)


#define MSC_SRC1_SRR_POS                     _MSC_UL(13)
#define MSC_SRC1_SRR_LEN                     _MSC_UL(1)


#define MSC_SRC1_CLRR_POS                    _MSC_UL(14)
#define MSC_SRC1_CLRR_LEN                    _MSC_UL(1)


#define MSC_SRC1_SETR_POS                    _MSC_UL(15)
#define MSC_SRC1_SETR_LEN                    _MSC_UL(1)


#define MSC_SRC0_SRPN_POS                    _MSC_UL(0)
#define MSC_SRC0_SRPN_LEN                    _MSC_UL(8)


#define MSC_SRC0_TOS_POS                     _MSC_UL(10)
#define MSC_SRC0_TOS_LEN                     _MSC_UL(1)


#define MSC_SRC0_SRE_POS                     _MSC_UL(12)
#define MSC_SRC0_SRE_LEN                     _MSC_UL(1)


#define MSC_SRC0_SRR_POS                     _MSC_UL(13)
#define MSC_SRC0_SRR_LEN                     _MSC_UL(1)


#define MSC_SRC0_CLRR_POS                    _MSC_UL(14)
#define MSC_SRC0_CLRR_LEN                    _MSC_UL(1)


#define MSC_SRC0_SETR_POS                    _MSC_UL(15)
#define MSC_SRC0_SETR_LEN                    _MSC_UL(1)


#define MSC0_CLC_DISR_POS                    _MSC_UL(0)
#define MSC0_CLC_DISR_LEN                    _MSC_UL(1)


#define MSC0_CLC_DISS_POS                    _MSC_UL(1)
#define MSC0_CLC_DISS_LEN                    _MSC_UL(1)


#define MSC0_CLC_SPEN_POS                    _MSC_UL(2)
#define MSC0_CLC_SPEN_LEN                    _MSC_UL(1)


#define MSC0_CLC_EDIS_POS                    _MSC_UL(3)
#define MSC0_CLC_EDIS_LEN                    _MSC_UL(1)


#define MSC0_CLC_SBWE_POS                    _MSC_UL(4)
#define MSC0_CLC_SBWE_LEN                    _MSC_UL(1)


#define MSC0_CLC_FSOE_POS                    _MSC_UL(5)
#define MSC0_CLC_FSOE_LEN                    _MSC_UL(1)


#define MSC0_ID_MODREV_POS                   _MSC_UL(0)
#define MSC0_ID_MODREV_LEN                   _MSC_UL(8)


#define MSC0_ID_MODTYPE_POS                  _MSC_UL(8)
#define MSC0_ID_MODTYPE_LEN                  _MSC_UL(8)


#define MSC0_ID_MODNUM_POS                   _MSC_UL(16)
#define MSC0_ID_MODNUM_LEN                   _MSC_UL(16)


#define MSC0_FDR_STEP_POS                    _MSC_UL(0)
#define MSC0_FDR_STEP_LEN                    _MSC_UL(10)


#define MSC0_FDR_SM_POS                      _MSC_UL(11)
#define MSC0_FDR_SM_LEN                      _MSC_UL(1)


#define MSC0_FDR_SC_POS                      _MSC_UL(12)
#define MSC0_FDR_SC_LEN                      _MSC_UL(2)


#define MSC0_FDR_DM_POS                      _MSC_UL(14)
#define MSC0_FDR_DM_LEN                      _MSC_UL(2)


#define MSC0_FDR_RESULT_POS                  _MSC_UL(16)
#define MSC0_FDR_RESULT_LEN                  _MSC_UL(10)


#define MSC0_FDR_SUSACK_POS                  _MSC_UL(28)
#define MSC0_FDR_SUSACK_LEN                  _MSC_UL(1)


#define MSC0_FDR_SUSREQ_POS                  _MSC_UL(29)
#define MSC0_FDR_SUSREQ_LEN                  _MSC_UL(1)


#define MSC0_FDR_ENHW_POS                    _MSC_UL(30)
#define MSC0_FDR_ENHW_LEN                    _MSC_UL(1)


#define MSC0_FDR_DISCLK_POS                  _MSC_UL(31)
#define MSC0_FDR_DISCLK_LEN                  _MSC_UL(1)


#define MSC0_USR_UFT_POS                     _MSC_UL(0)
#define MSC0_USR_UFT_LEN                     _MSC_UL(1)


#define MSC0_USR_URR_POS                     _MSC_UL(1)
#define MSC0_USR_URR_LEN                     _MSC_UL(3)


#define MSC0_USR_PCTR_POS                    _MSC_UL(4)
#define MSC0_USR_PCTR_LEN                    _MSC_UL(1)


#define MSC0_USR_UC_POS                      _MSC_UL(16)
#define MSC0_USR_UC_LEN                      _MSC_UL(5)


#define MSC0_DSC_TM_POS                      _MSC_UL(0)
#define MSC0_DSC_TM_LEN                      _MSC_UL(1)


#define MSC0_DSC_CP_POS                      _MSC_UL(1)
#define MSC0_DSC_CP_LEN                      _MSC_UL(1)


#define MSC0_DSC_DP_POS                      _MSC_UL(2)
#define MSC0_DSC_DP_LEN                      _MSC_UL(1)


#define MSC0_DSC_NDBL_POS                    _MSC_UL(3)
#define MSC0_DSC_NDBL_LEN                    _MSC_UL(5)


#define MSC0_DSC_NDBH_POS                    _MSC_UL(8)
#define MSC0_DSC_NDBH_LEN                    _MSC_UL(5)


#define MSC0_DSC_ENSELL_POS                  _MSC_UL(13)
#define MSC0_DSC_ENSELL_LEN                  _MSC_UL(1)


#define MSC0_DSC_ENSELH_POS                  _MSC_UL(14)
#define MSC0_DSC_ENSELH_LEN                  _MSC_UL(1)


#define MSC0_DSC_DSDIS_POS                   _MSC_UL(15)
#define MSC0_DSC_DSDIS_LEN                   _MSC_UL(1)


#define MSC0_DSC_NBC_POS                     _MSC_UL(16)
#define MSC0_DSC_NBC_LEN                     _MSC_UL(6)


#define MSC0_DSC_PPD_POS                     _MSC_UL(24)
#define MSC0_DSC_PPD_LEN                     _MSC_UL(5)


#define MSC0_DSS_PFC_POS                     _MSC_UL(0)
#define MSC0_DSS_PFC_LEN                     _MSC_UL(4)


#define MSC0_DSS_NPTF_POS                    _MSC_UL(8)
#define MSC0_DSS_NPTF_LEN                    _MSC_UL(4)


#define MSC0_DSS_DC_POS                      _MSC_UL(16)
#define MSC0_DSS_DC_LEN                      _MSC_UL(7)


#define MSC0_DSS_DFA_POS                     _MSC_UL(24)
#define MSC0_DSS_DFA_LEN                     _MSC_UL(1)


#define MSC0_DSS_CFA_POS                     _MSC_UL(25)
#define MSC0_DSS_CFA_LEN                     _MSC_UL(1)


#define MSC0_DD_DDL_POS                      _MSC_UL(0)
#define MSC0_DD_DDL_LEN                      _MSC_UL(16)


#define MSC0_DD_DDH_POS                      _MSC_UL(16)
#define MSC0_DD_DDH_LEN                      _MSC_UL(16)


#define MSC0_DC_DCL_POS                      _MSC_UL(0)
#define MSC0_DC_DCL_LEN                      _MSC_UL(16)


#define MSC0_DC_DCH_POS                      _MSC_UL(16)
#define MSC0_DC_DCH_LEN                      _MSC_UL(16)


#define MSC0_DSDSL_SL0_POS                   _MSC_UL(0)
#define MSC0_DSDSL_SL0_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL1_POS                   _MSC_UL(2)
#define MSC0_DSDSL_SL1_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL2_POS                   _MSC_UL(4)
#define MSC0_DSDSL_SL2_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL3_POS                   _MSC_UL(6)
#define MSC0_DSDSL_SL3_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL4_POS                   _MSC_UL(8)
#define MSC0_DSDSL_SL4_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL5_POS                   _MSC_UL(10)
#define MSC0_DSDSL_SL5_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL6_POS                   _MSC_UL(12)
#define MSC0_DSDSL_SL6_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL7_POS                   _MSC_UL(14)
#define MSC0_DSDSL_SL7_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL8_POS                   _MSC_UL(16)
#define MSC0_DSDSL_SL8_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL9_POS                   _MSC_UL(18)
#define MSC0_DSDSL_SL9_LEN                   _MSC_UL(2)


#define MSC0_DSDSL_SL10_POS                  _MSC_UL(20)
#define MSC0_DSDSL_SL10_LEN                  _MSC_UL(2)


#define MSC0_DSDSL_SL11_POS                  _MSC_UL(22)
#define MSC0_DSDSL_SL11_LEN                  _MSC_UL(2)


#define MSC0_DSDSL_SL12_POS                  _MSC_UL(24)
#define MSC0_DSDSL_SL12_LEN                  _MSC_UL(2)


#define MSC0_DSDSL_SL13_POS                  _MSC_UL(26)
#define MSC0_DSDSL_SL13_LEN                  _MSC_UL(2)


#define MSC0_DSDSL_SL14_POS                  _MSC_UL(28)
#define MSC0_DSDSL_SL14_LEN                  _MSC_UL(2)


#define MSC0_DSDSL_SL15_POS                  _MSC_UL(30)
#define MSC0_DSDSL_SL15_LEN                  _MSC_UL(2)


#define MSC0_DSDSH_SH0_POS                   _MSC_UL(0)
#define MSC0_DSDSH_SH0_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH1_POS                   _MSC_UL(2)
#define MSC0_DSDSH_SH1_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH2_POS                   _MSC_UL(4)
#define MSC0_DSDSH_SH2_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH3_POS                   _MSC_UL(6)
#define MSC0_DSDSH_SH3_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH4_POS                   _MSC_UL(8)
#define MSC0_DSDSH_SH4_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH5_POS                   _MSC_UL(10)
#define MSC0_DSDSH_SH5_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH6_POS                   _MSC_UL(12)
#define MSC0_DSDSH_SH6_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH7_POS                   _MSC_UL(14)
#define MSC0_DSDSH_SH7_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH8_POS                   _MSC_UL(16)
#define MSC0_DSDSH_SH8_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH9_POS                   _MSC_UL(18)
#define MSC0_DSDSH_SH9_LEN                   _MSC_UL(2)


#define MSC0_DSDSH_SH10_POS                  _MSC_UL(20)
#define MSC0_DSDSH_SH10_LEN                  _MSC_UL(2)


#define MSC0_DSDSH_SH11_POS                  _MSC_UL(22)
#define MSC0_DSDSH_SH11_LEN                  _MSC_UL(2)


#define MSC0_DSDSH_SH12_POS                  _MSC_UL(24)
#define MSC0_DSDSH_SH12_LEN                  _MSC_UL(2)


#define MSC0_DSDSH_SH13_POS                  _MSC_UL(26)
#define MSC0_DSDSH_SH13_LEN                  _MSC_UL(2)


#define MSC0_DSDSH_SH14_POS                  _MSC_UL(28)
#define MSC0_DSDSH_SH14_LEN                  _MSC_UL(2)


#define MSC0_DSDSH_SH15_POS                  _MSC_UL(30)
#define MSC0_DSDSH_SH15_LEN                  _MSC_UL(2)


#define MSC0_ESR_ENL0_POS                    _MSC_UL(0)
#define MSC0_ESR_ENL0_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL1_POS                    _MSC_UL(1)
#define MSC0_ESR_ENL1_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL2_POS                    _MSC_UL(2)
#define MSC0_ESR_ENL2_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL3_POS                    _MSC_UL(3)
#define MSC0_ESR_ENL3_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL4_POS                    _MSC_UL(4)
#define MSC0_ESR_ENL4_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL5_POS                    _MSC_UL(5)
#define MSC0_ESR_ENL5_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL6_POS                    _MSC_UL(6)
#define MSC0_ESR_ENL6_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL7_POS                    _MSC_UL(7)
#define MSC0_ESR_ENL7_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL8_POS                    _MSC_UL(8)
#define MSC0_ESR_ENL8_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL9_POS                    _MSC_UL(9)
#define MSC0_ESR_ENL9_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENL10_POS                   _MSC_UL(10)
#define MSC0_ESR_ENL10_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENL11_POS                   _MSC_UL(11)
#define MSC0_ESR_ENL11_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENL12_POS                   _MSC_UL(12)
#define MSC0_ESR_ENL12_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENL13_POS                   _MSC_UL(13)
#define MSC0_ESR_ENL13_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENL14_POS                   _MSC_UL(14)
#define MSC0_ESR_ENL14_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENL15_POS                   _MSC_UL(15)
#define MSC0_ESR_ENL15_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENH0_POS                    _MSC_UL(16)
#define MSC0_ESR_ENH0_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH1_POS                    _MSC_UL(17)
#define MSC0_ESR_ENH1_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH2_POS                    _MSC_UL(18)
#define MSC0_ESR_ENH2_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH3_POS                    _MSC_UL(19)
#define MSC0_ESR_ENH3_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH4_POS                    _MSC_UL(20)
#define MSC0_ESR_ENH4_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH5_POS                    _MSC_UL(21)
#define MSC0_ESR_ENH5_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH6_POS                    _MSC_UL(22)
#define MSC0_ESR_ENH6_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH7_POS                    _MSC_UL(23)
#define MSC0_ESR_ENH7_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH8_POS                    _MSC_UL(24)
#define MSC0_ESR_ENH8_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH9_POS                    _MSC_UL(25)
#define MSC0_ESR_ENH9_LEN                    _MSC_UL(1)


#define MSC0_ESR_ENH10_POS                   _MSC_UL(26)
#define MSC0_ESR_ENH10_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENH11_POS                   _MSC_UL(27)
#define MSC0_ESR_ENH11_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENH12_POS                   _MSC_UL(28)
#define MSC0_ESR_ENH12_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENH13_POS                   _MSC_UL(29)
#define MSC0_ESR_ENH13_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENH14_POS                   _MSC_UL(30)
#define MSC0_ESR_ENH14_LEN                   _MSC_UL(1)


#define MSC0_ESR_ENH15_POS                   _MSC_UL(31)
#define MSC0_ESR_ENH15_LEN                   _MSC_UL(1)


#define MSC0_UD_DATA_POS                     _MSC_UL(0)
#define MSC0_UD_DATA_LEN                     _MSC_UL(8)


#define MSC0_UD_V_POS                        _MSC_UL(16)
#define MSC0_UD_V_LEN                        _MSC_UL(1)


#define MSC0_UD_P_POS                        _MSC_UL(17)
#define MSC0_UD_P_LEN                        _MSC_UL(1)


#define MSC0_UD_C_POS                        _MSC_UL(18)
#define MSC0_UD_C_LEN                        _MSC_UL(1)


#define MSC0_UD_LABF_POS                     _MSC_UL(19)
#define MSC0_UD_LABF_LEN                     _MSC_UL(2)


#define MSC0_UD_IPF_POS                      _MSC_UL(21)
#define MSC0_UD_IPF_LEN                      _MSC_UL(1)


#define MSC0_UD_PERR_POS                     _MSC_UL(22)
#define MSC0_UD_PERR_LEN                     _MSC_UL(1)


#define MSC0_ICR_EDIP_POS                    _MSC_UL(0)
#define MSC0_ICR_EDIP_LEN                    _MSC_UL(2)


#define MSC0_ICR_EDIE_POS                    _MSC_UL(2)
#define MSC0_ICR_EDIE_LEN                    _MSC_UL(2)


#define MSC0_ICR_ECIP_POS                    _MSC_UL(4)
#define MSC0_ICR_ECIP_LEN                    _MSC_UL(2)


#define MSC0_ICR_ECIE_POS                    _MSC_UL(7)
#define MSC0_ICR_ECIE_LEN                    _MSC_UL(1)


#define MSC0_ICR_TFIP_POS                    _MSC_UL(8)
#define MSC0_ICR_TFIP_LEN                    _MSC_UL(2)


#define MSC0_ICR_TFIE_POS                    _MSC_UL(11)
#define MSC0_ICR_TFIE_LEN                    _MSC_UL(1)


#define MSC0_ICR_RDIP_POS                    _MSC_UL(12)
#define MSC0_ICR_RDIP_LEN                    _MSC_UL(2)


#define MSC0_ICR_RDIE_POS                    _MSC_UL(14)
#define MSC0_ICR_RDIE_LEN                    _MSC_UL(2)


#define MSC0_ISR_DEDI_POS                    _MSC_UL(0)
#define MSC0_ISR_DEDI_LEN                    _MSC_UL(1)


#define MSC0_ISR_DECI_POS                    _MSC_UL(1)
#define MSC0_ISR_DECI_LEN                    _MSC_UL(1)


#define MSC0_ISR_DTFI_POS                    _MSC_UL(2)
#define MSC0_ISR_DTFI_LEN                    _MSC_UL(1)


#define MSC0_ISR_URDI_POS                    _MSC_UL(3)
#define MSC0_ISR_URDI_LEN                    _MSC_UL(1)


#define MSC0_ISC_CDEDI_POS                   _MSC_UL(0)
#define MSC0_ISC_CDEDI_LEN                   _MSC_UL(1)


#define MSC0_ISC_CDECI_POS                   _MSC_UL(1)
#define MSC0_ISC_CDECI_LEN                   _MSC_UL(1)


#define MSC0_ISC_CDTFI_POS                   _MSC_UL(2)
#define MSC0_ISC_CDTFI_LEN                   _MSC_UL(1)


#define MSC0_ISC_CURDI_POS                   _MSC_UL(3)
#define MSC0_ISC_CURDI_LEN                   _MSC_UL(1)


#define MSC0_ISC_CDP_POS                     _MSC_UL(4)
#define MSC0_ISC_CDP_LEN                     _MSC_UL(1)


#define MSC0_ISC_CCP_POS                     _MSC_UL(5)
#define MSC0_ISC_CCP_LEN                     _MSC_UL(1)


#define MSC0_ISC_CDDIS_POS                   _MSC_UL(6)
#define MSC0_ISC_CDDIS_LEN                   _MSC_UL(1)


#define MSC0_ISC_SDEDI_POS                   _MSC_UL(16)
#define MSC0_ISC_SDEDI_LEN                   _MSC_UL(1)


#define MSC0_ISC_SDECI_POS                   _MSC_UL(17)
#define MSC0_ISC_SDECI_LEN                   _MSC_UL(1)


#define MSC0_ISC_SDTFI_POS                   _MSC_UL(18)
#define MSC0_ISC_SDTFI_LEN                   _MSC_UL(1)


#define MSC0_ISC_SURDI_POS                   _MSC_UL(19)
#define MSC0_ISC_SURDI_LEN                   _MSC_UL(1)


#define MSC0_ISC_SDP_POS                     _MSC_UL(20)
#define MSC0_ISC_SDP_LEN                     _MSC_UL(1)


#define MSC0_ISC_SCP_POS                     _MSC_UL(21)
#define MSC0_ISC_SCP_LEN                     _MSC_UL(1)


#define MSC0_ISC_SDDIS_POS                   _MSC_UL(22)
#define MSC0_ISC_SDDIS_LEN                   _MSC_UL(1)


#define MSC0_OCR_CLP_POS                     _MSC_UL(0)
#define MSC0_OCR_CLP_LEN                     _MSC_UL(1)


#define MSC0_OCR_SLP_POS                     _MSC_UL(1)
#define MSC0_OCR_SLP_LEN                     _MSC_UL(1)


#define MSC0_OCR_CSLP_POS                    _MSC_UL(2)
#define MSC0_OCR_CSLP_LEN                    _MSC_UL(1)


#define MSC0_OCR_ILP_POS                     _MSC_UL(3)
#define MSC0_OCR_ILP_LEN                     _MSC_UL(1)


#define MSC0_OCR_CLKCTRL_POS                 _MSC_UL(8)
#define MSC0_OCR_CLKCTRL_LEN                 _MSC_UL(1)


#define MSC0_OCR_CSL_POS                     _MSC_UL(9)
#define MSC0_OCR_CSL_LEN                     _MSC_UL(2)


#define MSC0_OCR_CSH_POS                     _MSC_UL(11)
#define MSC0_OCR_CSH_LEN                     _MSC_UL(2)


#define MSC0_OCR_CSC_POS                     _MSC_UL(13)
#define MSC0_OCR_CSC_LEN                     _MSC_UL(2)


#define MSC0_OCR_SDISEL_POS                  _MSC_UL(16)
#define MSC0_OCR_SDISEL_LEN                  _MSC_UL(3)


#define MSC0_SRC1_SRPN_POS                   _MSC_UL(0)
#define MSC0_SRC1_SRPN_LEN                   _MSC_UL(8)


#define MSC0_SRC1_TOS_POS                    _MSC_UL(10)
#define MSC0_SRC1_TOS_LEN                    _MSC_UL(1)


#define MSC0_SRC1_SRE_POS                    _MSC_UL(12)
#define MSC0_SRC1_SRE_LEN                    _MSC_UL(1)


#define MSC0_SRC1_SRR_POS                    _MSC_UL(13)
#define MSC0_SRC1_SRR_LEN                    _MSC_UL(1)


#define MSC0_SRC1_CLRR_POS                   _MSC_UL(14)
#define MSC0_SRC1_CLRR_LEN                   _MSC_UL(1)


#define MSC0_SRC1_SETR_POS                   _MSC_UL(15)
#define MSC0_SRC1_SETR_LEN                   _MSC_UL(1)


#define MSC0_SRC0_SRPN_POS                   _MSC_UL(0)
#define MSC0_SRC0_SRPN_LEN                   _MSC_UL(8)


#define MSC0_SRC0_TOS_POS                    _MSC_UL(10)
#define MSC0_SRC0_TOS_LEN                    _MSC_UL(1)


#define MSC0_SRC0_SRE_POS                    _MSC_UL(12)
#define MSC0_SRC0_SRE_LEN                    _MSC_UL(1)


#define MSC0_SRC0_SRR_POS                    _MSC_UL(13)
#define MSC0_SRC0_SRR_LEN                    _MSC_UL(1)


#define MSC0_SRC0_CLRR_POS                   _MSC_UL(14)
#define MSC0_SRC0_CLRR_LEN                   _MSC_UL(1)


#define MSC0_SRC0_SETR_POS                   _MSC_UL(15)
#define MSC0_SRC0_SETR_LEN                   _MSC_UL(1)


#define MSC1_CLC_DISR_POS                    _MSC_UL(0)
#define MSC1_CLC_DISR_LEN                    _MSC_UL(1)


#define MSC1_CLC_DISS_POS                    _MSC_UL(1)
#define MSC1_CLC_DISS_LEN                    _MSC_UL(1)


#define MSC1_CLC_SPEN_POS                    _MSC_UL(2)
#define MSC1_CLC_SPEN_LEN                    _MSC_UL(1)


#define MSC1_CLC_EDIS_POS                    _MSC_UL(3)
#define MSC1_CLC_EDIS_LEN                    _MSC_UL(1)


#define MSC1_CLC_SBWE_POS                    _MSC_UL(4)
#define MSC1_CLC_SBWE_LEN                    _MSC_UL(1)


#define MSC1_CLC_FSOE_POS                    _MSC_UL(5)
#define MSC1_CLC_FSOE_LEN                    _MSC_UL(1)


#define MSC1_ID_MODREV_POS                   _MSC_UL(0)
#define MSC1_ID_MODREV_LEN                   _MSC_UL(8)


#define MSC1_ID_MODTYPE_POS                  _MSC_UL(8)
#define MSC1_ID_MODTYPE_LEN                  _MSC_UL(8)


#define MSC1_ID_MODNUM_POS                   _MSC_UL(16)
#define MSC1_ID_MODNUM_LEN                   _MSC_UL(16)


#define MSC1_FDR_STEP_POS                    _MSC_UL(0)
#define MSC1_FDR_STEP_LEN                    _MSC_UL(10)


#define MSC1_FDR_SM_POS                      _MSC_UL(11)
#define MSC1_FDR_SM_LEN                      _MSC_UL(1)


#define MSC1_FDR_SC_POS                      _MSC_UL(12)
#define MSC1_FDR_SC_LEN                      _MSC_UL(2)


#define MSC1_FDR_DM_POS                      _MSC_UL(14)
#define MSC1_FDR_DM_LEN                      _MSC_UL(2)


#define MSC1_FDR_RESULT_POS                  _MSC_UL(16)
#define MSC1_FDR_RESULT_LEN                  _MSC_UL(10)


#define MSC1_FDR_SUSACK_POS                  _MSC_UL(28)
#define MSC1_FDR_SUSACK_LEN                  _MSC_UL(1)


#define MSC1_FDR_SUSREQ_POS                  _MSC_UL(29)
#define MSC1_FDR_SUSREQ_LEN                  _MSC_UL(1)


#define MSC1_FDR_ENHW_POS                    _MSC_UL(30)
#define MSC1_FDR_ENHW_LEN                    _MSC_UL(1)


#define MSC1_FDR_DISCLK_POS                  _MSC_UL(31)
#define MSC1_FDR_DISCLK_LEN                  _MSC_UL(1)


#define MSC1_USR_UFT_POS                     _MSC_UL(0)
#define MSC1_USR_UFT_LEN                     _MSC_UL(1)


#define MSC1_USR_URR_POS                     _MSC_UL(1)
#define MSC1_USR_URR_LEN                     _MSC_UL(3)


#define MSC1_USR_PCTR_POS                    _MSC_UL(4)
#define MSC1_USR_PCTR_LEN                    _MSC_UL(1)


#define MSC1_USR_UC_POS                      _MSC_UL(16)
#define MSC1_USR_UC_LEN                      _MSC_UL(5)


#define MSC1_DSC_TM_POS                      _MSC_UL(0)
#define MSC1_DSC_TM_LEN                      _MSC_UL(1)


#define MSC1_DSC_CP_POS                      _MSC_UL(1)
#define MSC1_DSC_CP_LEN                      _MSC_UL(1)


#define MSC1_DSC_DP_POS                      _MSC_UL(2)
#define MSC1_DSC_DP_LEN                      _MSC_UL(1)


#define MSC1_DSC_NDBL_POS                    _MSC_UL(3)
#define MSC1_DSC_NDBL_LEN                    _MSC_UL(5)


#define MSC1_DSC_NDBH_POS                    _MSC_UL(8)
#define MSC1_DSC_NDBH_LEN                    _MSC_UL(5)


#define MSC1_DSC_ENSELL_POS                  _MSC_UL(13)
#define MSC1_DSC_ENSELL_LEN                  _MSC_UL(1)


#define MSC1_DSC_ENSELH_POS                  _MSC_UL(14)
#define MSC1_DSC_ENSELH_LEN                  _MSC_UL(1)


#define MSC1_DSC_DSDIS_POS                   _MSC_UL(15)
#define MSC1_DSC_DSDIS_LEN                   _MSC_UL(1)


#define MSC1_DSC_NBC_POS                     _MSC_UL(16)
#define MSC1_DSC_NBC_LEN                     _MSC_UL(6)


#define MSC1_DSC_PPD_POS                     _MSC_UL(24)
#define MSC1_DSC_PPD_LEN                     _MSC_UL(5)


#define MSC1_DSS_PFC_POS                     _MSC_UL(0)
#define MSC1_DSS_PFC_LEN                     _MSC_UL(4)


#define MSC1_DSS_NPTF_POS                    _MSC_UL(8)
#define MSC1_DSS_NPTF_LEN                    _MSC_UL(4)


#define MSC1_DSS_DC_POS                      _MSC_UL(16)
#define MSC1_DSS_DC_LEN                      _MSC_UL(7)


#define MSC1_DSS_DFA_POS                     _MSC_UL(24)
#define MSC1_DSS_DFA_LEN                     _MSC_UL(1)


#define MSC1_DSS_CFA_POS                     _MSC_UL(25)
#define MSC1_DSS_CFA_LEN                     _MSC_UL(1)


#define MSC1_DD_DDL_POS                      _MSC_UL(0)
#define MSC1_DD_DDL_LEN                      _MSC_UL(16)


#define MSC1_DD_DDH_POS                      _MSC_UL(16)
#define MSC1_DD_DDH_LEN                      _MSC_UL(16)


#define MSC1_DC_DCL_POS                      _MSC_UL(0)
#define MSC1_DC_DCL_LEN                      _MSC_UL(16)


#define MSC1_DC_DCH_POS                      _MSC_UL(16)
#define MSC1_DC_DCH_LEN                      _MSC_UL(16)


#define MSC1_DSDSL_SL0_POS                   _MSC_UL(0)
#define MSC1_DSDSL_SL0_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL1_POS                   _MSC_UL(2)
#define MSC1_DSDSL_SL1_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL2_POS                   _MSC_UL(4)
#define MSC1_DSDSL_SL2_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL3_POS                   _MSC_UL(6)
#define MSC1_DSDSL_SL3_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL4_POS                   _MSC_UL(8)
#define MSC1_DSDSL_SL4_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL5_POS                   _MSC_UL(10)
#define MSC1_DSDSL_SL5_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL6_POS                   _MSC_UL(12)
#define MSC1_DSDSL_SL6_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL7_POS                   _MSC_UL(14)
#define MSC1_DSDSL_SL7_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL8_POS                   _MSC_UL(16)
#define MSC1_DSDSL_SL8_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL9_POS                   _MSC_UL(18)
#define MSC1_DSDSL_SL9_LEN                   _MSC_UL(2)


#define MSC1_DSDSL_SL10_POS                  _MSC_UL(20)
#define MSC1_DSDSL_SL10_LEN                  _MSC_UL(2)


#define MSC1_DSDSL_SL11_POS                  _MSC_UL(22)
#define MSC1_DSDSL_SL11_LEN                  _MSC_UL(2)


#define MSC1_DSDSL_SL12_POS                  _MSC_UL(24)
#define MSC1_DSDSL_SL12_LEN                  _MSC_UL(2)


#define MSC1_DSDSL_SL13_POS                  _MSC_UL(26)
#define MSC1_DSDSL_SL13_LEN                  _MSC_UL(2)


#define MSC1_DSDSL_SL14_POS                  _MSC_UL(28)
#define MSC1_DSDSL_SL14_LEN                  _MSC_UL(2)


#define MSC1_DSDSL_SL15_POS                  _MSC_UL(30)
#define MSC1_DSDSL_SL15_LEN                  _MSC_UL(2)


#define MSC1_DSDSH_SH0_POS                   _MSC_UL(0)
#define MSC1_DSDSH_SH0_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH1_POS                   _MSC_UL(2)
#define MSC1_DSDSH_SH1_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH2_POS                   _MSC_UL(4)
#define MSC1_DSDSH_SH2_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH3_POS                   _MSC_UL(6)
#define MSC1_DSDSH_SH3_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH4_POS                   _MSC_UL(8)
#define MSC1_DSDSH_SH4_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH5_POS                   _MSC_UL(10)
#define MSC1_DSDSH_SH5_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH6_POS                   _MSC_UL(12)
#define MSC1_DSDSH_SH6_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH7_POS                   _MSC_UL(14)
#define MSC1_DSDSH_SH7_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH8_POS                   _MSC_UL(16)
#define MSC1_DSDSH_SH8_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH9_POS                   _MSC_UL(18)
#define MSC1_DSDSH_SH9_LEN                   _MSC_UL(2)


#define MSC1_DSDSH_SH10_POS                  _MSC_UL(20)
#define MSC1_DSDSH_SH10_LEN                  _MSC_UL(2)


#define MSC1_DSDSH_SH11_POS                  _MSC_UL(22)
#define MSC1_DSDSH_SH11_LEN                  _MSC_UL(2)


#define MSC1_DSDSH_SH12_POS                  _MSC_UL(24)
#define MSC1_DSDSH_SH12_LEN                  _MSC_UL(2)


#define MSC1_DSDSH_SH13_POS                  _MSC_UL(26)
#define MSC1_DSDSH_SH13_LEN                  _MSC_UL(2)


#define MSC1_DSDSH_SH14_POS                  _MSC_UL(28)
#define MSC1_DSDSH_SH14_LEN                  _MSC_UL(2)


#define MSC1_DSDSH_SH15_POS                  _MSC_UL(30)
#define MSC1_DSDSH_SH15_LEN                  _MSC_UL(2)


#define MSC1_ESR_ENL0_POS                    _MSC_UL(0)
#define MSC1_ESR_ENL0_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL1_POS                    _MSC_UL(1)
#define MSC1_ESR_ENL1_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL2_POS                    _MSC_UL(2)
#define MSC1_ESR_ENL2_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL3_POS                    _MSC_UL(3)
#define MSC1_ESR_ENL3_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL4_POS                    _MSC_UL(4)
#define MSC1_ESR_ENL4_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL5_POS                    _MSC_UL(5)
#define MSC1_ESR_ENL5_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL6_POS                    _MSC_UL(6)
#define MSC1_ESR_ENL6_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL7_POS                    _MSC_UL(7)
#define MSC1_ESR_ENL7_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL8_POS                    _MSC_UL(8)
#define MSC1_ESR_ENL8_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL9_POS                    _MSC_UL(9)
#define MSC1_ESR_ENL9_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENL10_POS                   _MSC_UL(10)
#define MSC1_ESR_ENL10_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENL11_POS                   _MSC_UL(11)
#define MSC1_ESR_ENL11_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENL12_POS                   _MSC_UL(12)
#define MSC1_ESR_ENL12_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENL13_POS                   _MSC_UL(13)
#define MSC1_ESR_ENL13_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENL14_POS                   _MSC_UL(14)
#define MSC1_ESR_ENL14_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENL15_POS                   _MSC_UL(15)
#define MSC1_ESR_ENL15_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENH0_POS                    _MSC_UL(16)
#define MSC1_ESR_ENH0_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH1_POS                    _MSC_UL(17)
#define MSC1_ESR_ENH1_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH2_POS                    _MSC_UL(18)
#define MSC1_ESR_ENH2_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH3_POS                    _MSC_UL(19)
#define MSC1_ESR_ENH3_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH4_POS                    _MSC_UL(20)
#define MSC1_ESR_ENH4_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH5_POS                    _MSC_UL(21)
#define MSC1_ESR_ENH5_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH6_POS                    _MSC_UL(22)
#define MSC1_ESR_ENH6_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH7_POS                    _MSC_UL(23)
#define MSC1_ESR_ENH7_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH8_POS                    _MSC_UL(24)
#define MSC1_ESR_ENH8_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH9_POS                    _MSC_UL(25)
#define MSC1_ESR_ENH9_LEN                    _MSC_UL(1)


#define MSC1_ESR_ENH10_POS                   _MSC_UL(26)
#define MSC1_ESR_ENH10_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENH11_POS                   _MSC_UL(27)
#define MSC1_ESR_ENH11_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENH12_POS                   _MSC_UL(28)
#define MSC1_ESR_ENH12_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENH13_POS                   _MSC_UL(29)
#define MSC1_ESR_ENH13_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENH14_POS                   _MSC_UL(30)
#define MSC1_ESR_ENH14_LEN                   _MSC_UL(1)


#define MSC1_ESR_ENH15_POS                   _MSC_UL(31)
#define MSC1_ESR_ENH15_LEN                   _MSC_UL(1)


#define MSC1_UD_DATA_POS                     _MSC_UL(0)
#define MSC1_UD_DATA_LEN                     _MSC_UL(8)


#define MSC1_UD_V_POS                        _MSC_UL(16)
#define MSC1_UD_V_LEN                        _MSC_UL(1)


#define MSC1_UD_P_POS                        _MSC_UL(17)
#define MSC1_UD_P_LEN                        _MSC_UL(1)


#define MSC1_UD_C_POS                        _MSC_UL(18)
#define MSC1_UD_C_LEN                        _MSC_UL(1)


#define MSC1_UD_LABF_POS                     _MSC_UL(19)
#define MSC1_UD_LABF_LEN                     _MSC_UL(2)


#define MSC1_UD_IPF_POS                      _MSC_UL(21)
#define MSC1_UD_IPF_LEN                      _MSC_UL(1)


#define MSC1_UD_PERR_POS                     _MSC_UL(22)
#define MSC1_UD_PERR_LEN                     _MSC_UL(1)


#define MSC1_ICR_EDIP_POS                    _MSC_UL(0)
#define MSC1_ICR_EDIP_LEN                    _MSC_UL(2)


#define MSC1_ICR_EDIE_POS                    _MSC_UL(2)
#define MSC1_ICR_EDIE_LEN                    _MSC_UL(2)


#define MSC1_ICR_ECIP_POS                    _MSC_UL(4)
#define MSC1_ICR_ECIP_LEN                    _MSC_UL(2)


#define MSC1_ICR_ECIE_POS                    _MSC_UL(7)
#define MSC1_ICR_ECIE_LEN                    _MSC_UL(1)


#define MSC1_ICR_TFIP_POS                    _MSC_UL(8)
#define MSC1_ICR_TFIP_LEN                    _MSC_UL(2)


#define MSC1_ICR_TFIE_POS                    _MSC_UL(11)
#define MSC1_ICR_TFIE_LEN                    _MSC_UL(1)


#define MSC1_ICR_RDIP_POS                    _MSC_UL(12)
#define MSC1_ICR_RDIP_LEN                    _MSC_UL(2)


#define MSC1_ICR_RDIE_POS                    _MSC_UL(14)
#define MSC1_ICR_RDIE_LEN                    _MSC_UL(2)


#define MSC1_ISR_DEDI_POS                    _MSC_UL(0)
#define MSC1_ISR_DEDI_LEN                    _MSC_UL(1)


#define MSC1_ISR_DECI_POS                    _MSC_UL(1)
#define MSC1_ISR_DECI_LEN                    _MSC_UL(1)


#define MSC1_ISR_DTFI_POS                    _MSC_UL(2)
#define MSC1_ISR_DTFI_LEN                    _MSC_UL(1)


#define MSC1_ISR_URDI_POS                    _MSC_UL(3)
#define MSC1_ISR_URDI_LEN                    _MSC_UL(1)


#define MSC1_ISC_CDEDI_POS                   _MSC_UL(0)
#define MSC1_ISC_CDEDI_LEN                   _MSC_UL(1)


#define MSC1_ISC_CDECI_POS                   _MSC_UL(1)
#define MSC1_ISC_CDECI_LEN                   _MSC_UL(1)


#define MSC1_ISC_CDTFI_POS                   _MSC_UL(2)
#define MSC1_ISC_CDTFI_LEN                   _MSC_UL(1)


#define MSC1_ISC_CURDI_POS                   _MSC_UL(3)
#define MSC1_ISC_CURDI_LEN                   _MSC_UL(1)


#define MSC1_ISC_CDP_POS                     _MSC_UL(4)
#define MSC1_ISC_CDP_LEN                     _MSC_UL(1)


#define MSC1_ISC_CCP_POS                     _MSC_UL(5)
#define MSC1_ISC_CCP_LEN                     _MSC_UL(1)


#define MSC1_ISC_CDDIS_POS                   _MSC_UL(6)
#define MSC1_ISC_CDDIS_LEN                   _MSC_UL(1)


#define MSC1_ISC_SDEDI_POS                   _MSC_UL(16)
#define MSC1_ISC_SDEDI_LEN                   _MSC_UL(1)


#define MSC1_ISC_SDECI_POS                   _MSC_UL(17)
#define MSC1_ISC_SDECI_LEN                   _MSC_UL(1)


#define MSC1_ISC_SDTFI_POS                   _MSC_UL(18)
#define MSC1_ISC_SDTFI_LEN                   _MSC_UL(1)


#define MSC1_ISC_SURDI_POS                   _MSC_UL(19)
#define MSC1_ISC_SURDI_LEN                   _MSC_UL(1)


#define MSC1_ISC_SDP_POS                     _MSC_UL(20)
#define MSC1_ISC_SDP_LEN                     _MSC_UL(1)


#define MSC1_ISC_SCP_POS                     _MSC_UL(21)
#define MSC1_ISC_SCP_LEN                     _MSC_UL(1)


#define MSC1_ISC_SDDIS_POS                   _MSC_UL(22)
#define MSC1_ISC_SDDIS_LEN                   _MSC_UL(1)


#define MSC1_OCR_CLP_POS                     _MSC_UL(0)
#define MSC1_OCR_CLP_LEN                     _MSC_UL(1)


#define MSC1_OCR_SLP_POS                     _MSC_UL(1)
#define MSC1_OCR_SLP_LEN                     _MSC_UL(1)


#define MSC1_OCR_CSLP_POS                    _MSC_UL(2)
#define MSC1_OCR_CSLP_LEN                    _MSC_UL(1)


#define MSC1_OCR_ILP_POS                     _MSC_UL(3)
#define MSC1_OCR_ILP_LEN                     _MSC_UL(1)


#define MSC1_OCR_CLKCTRL_POS                 _MSC_UL(8)
#define MSC1_OCR_CLKCTRL_LEN                 _MSC_UL(1)


#define MSC1_OCR_CSL_POS                     _MSC_UL(9)
#define MSC1_OCR_CSL_LEN                     _MSC_UL(2)


#define MSC1_OCR_CSH_POS                     _MSC_UL(11)
#define MSC1_OCR_CSH_LEN                     _MSC_UL(2)


#define MSC1_OCR_CSC_POS                     _MSC_UL(13)
#define MSC1_OCR_CSC_LEN                     _MSC_UL(2)


#define MSC1_OCR_SDISEL_POS                  _MSC_UL(16)
#define MSC1_OCR_SDISEL_LEN                  _MSC_UL(3)


#define MSC1_SRC1_SRPN_POS                   _MSC_UL(0)
#define MSC1_SRC1_SRPN_LEN                   _MSC_UL(8)


#define MSC1_SRC1_TOS_POS                    _MSC_UL(10)
#define MSC1_SRC1_TOS_LEN                    _MSC_UL(1)


#define MSC1_SRC1_SRE_POS                    _MSC_UL(12)
#define MSC1_SRC1_SRE_LEN                    _MSC_UL(1)


#define MSC1_SRC1_SRR_POS                    _MSC_UL(13)
#define MSC1_SRC1_SRR_LEN                    _MSC_UL(1)


#define MSC1_SRC1_CLRR_POS                   _MSC_UL(14)
#define MSC1_SRC1_CLRR_LEN                   _MSC_UL(1)


#define MSC1_SRC1_SETR_POS                   _MSC_UL(15)
#define MSC1_SRC1_SETR_LEN                   _MSC_UL(1)


#define MSC1_SRC0_SRPN_POS                   _MSC_UL(0)
#define MSC1_SRC0_SRPN_LEN                   _MSC_UL(8)


#define MSC1_SRC0_TOS_POS                    _MSC_UL(10)
#define MSC1_SRC0_TOS_LEN                    _MSC_UL(1)


#define MSC1_SRC0_SRE_POS                    _MSC_UL(12)
#define MSC1_SRC0_SRE_LEN                    _MSC_UL(1)


#define MSC1_SRC0_SRR_POS                    _MSC_UL(13)
#define MSC1_SRC0_SRR_LEN                    _MSC_UL(1)


#define MSC1_SRC0_CLRR_POS                   _MSC_UL(14)
#define MSC1_SRC0_CLRR_LEN                   _MSC_UL(1)


#define MSC1_SRC0_SETR_POS                   _MSC_UL(15)
#define MSC1_SRC0_SETR_LEN                   _MSC_UL(1)

#endif
