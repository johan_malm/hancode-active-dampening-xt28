

#ifndef _PCP_PUB_H
#define _PCP_PUB_H







#define PCP_R0_OFFSET                   0x1C
#define PCP_R1_OFFSET                   0x18
#define PCP_R2_OFFSET                   0x14
#define PCP_R3_OFFSET                   0x10
#define PCP_R4_OFFSET                   0x0C
#define PCP_R5_OFFSET                   0x08
#define PCP_R6_OFFSET                   0x04
#define PCP_R7_OFFSET                   0x00

#define PCP_MAX_ARBTR_CYCLES            0x04UL          


#define Pcp_GetState()                  (Pcp_stPcp_u32)


#define PCP_ST_INACTIVE                 0x01UL
#define PCP_ST_ACTIVE                   0x02UL
#define PCP_ST_ERROR                    0x04UL


#define PCP_SHUTDOWN_DEF                1




#define Pcp_RtmPrologue(rega, regb)                                                                \
                                   ld_pi   (rega, adSysTicks)       \
                                   ld_f    (regb, rega, 32)         \
                                   st_pi   (regb, tiRunTimeTemp)   

#define Pcp_RtmEpilogue(rega, regb)                                                                \
                                   ld_pi   (rega, adSysTicks)       \
                                   ld_f    (regb, rega, 32)         \
                                   ld_pi   (rega, tiRunTimeTemp)    \
                                   sub     (regb, rega, uc)         \
                                   st_pi   (regb, tiRunTime)        \
                                   ld_i    (rega, tiRunTimePeak)    \
                                   comp_pi (regb, tiRunTimePeak)    \
                                   st_p    (regb, rega, sgt)        \
                                   ld_i    (rega, tiRunTimeMin)     \
                                   comp_pi (regb, tiRunTimeMin)     \
                                   st_p    (regb, rega, slt)       

#define Pcp_RtmDisableInt          clr     (r7, PCP_R7_IEN_POS)    
#define Pcp_RtmEnableInt           set     (r7, PCP_R7_IEN_POS)    




#define Pcp_RtmPrologueAs(rega, regb)                                                              \
                                   ld.pi   rega, adSysTicks;                                       \
                                   ld.f    regb, rega, size=32;                                    \
                                   st.pi   regb, tiRunTimeTemp

#define Pcp_RtmEpilogueAs(rega, regb)                                                              \
                                   ld.pi   rega, adSysTicks;                                       \
                                   ld.f    regb, rega, size=32;                                    \
                                   ld.pi   rega, tiRunTimeTemp;                                    \
                                   sub     regb, rega, cc_uc;                                      \
                                   st.pi   regb, tiRunTime;                                        \
                                   ld.i    rega, tiRunTimePeak;                                    \
                                   comp.pi regb, tiRunTimePeak;                                    \
                                   st.p    regb, rega, cc_sgt;                                     \
                                   ld.i    rega, tiRunTimeMin;                                     \
                                   comp.pi regb, tiRunTimeMin;                                     \
                                   st.p    regb, rega, cc_slt

#define Pcp_RtmDisableIntAs        clr     r7, PCP_R7_IEN_POS_NDT
#define Pcp_RtmEnableIntAs         set     r7, PCP_R7_IEN_POS_NDT






typedef struct
{
    uint32 xQuestion_u32;       
    uint32 xAnswer_u32;         
    uint32 xBuffer_u32;         
} Pcp_MonData_t;




__PRAGMA_USE__hwe__1_5ms__InitRAM__x32__START
extern uint32 Pcp_stPcp_u32;
__PRAGMA_USE__hwe__1_5ms__InitRAM__x32__END

__PRAGMA_USE__hwe__1_5ms__InitRAM__s32__START
extern volatile Pcp_MonData_t Pcp_Mon_Data_s;
__PRAGMA_USE__hwe__1_5ms__InitRAM__s32__END

__PRAGMA_USE__CODE__hwe__HighSpeed__START
extern void Pcp_ErrorHandler_Proc(void);
__PRAGMA_USE__CODE__hwe__HighSpeed__END

__PRAGMA_USE__CODE__hwe__LowSpeed__START
extern void Pcp_Proc_Ini(void);
extern void Pcp_InitCsaR7(void);
extern void Pcp_Enable_Proc_IniEnd(void);
extern void Pcp_Load_Proc_IniEnd(void);
extern void Pcp_Load_Proc(void);
extern void Pcp_Mon_Ini(void);
extern void Pcp_Shutdown(void);
__PRAGMA_USE__CODE__hwe__LowSpeed__END



#endif
