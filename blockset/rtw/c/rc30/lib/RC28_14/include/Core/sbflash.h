

#ifndef _SBFLASH_H
#define _SBFLASH_H






#define SBFLASH_MAX_DATA_WIDTH   256

#define SBFLASH_SIZE_RAM_ERASE   72/4

#define SBFLASH_SIZE_RAM_PROG    (72 + SBFLASH_MAX_DATA_WIDTH)/4


typedef enum
{
    SBFLASH_INIT_E,
    SBFLASH_ERASE_START_E,
    SBFLASH_ERASE_DO_E,
    SBFLASH_ERASE_END_E,
    SBFLASH_PROG_START_E,
    SBFLASH_PROG_DO_E,
    SBFLASH_PROG_END_E,
    SBFLASH_GETSTATUS_E
} SBFlash_Action_t;



typedef enum
{
    SBFLASH_RB_PROG_E   = 0,                       
    SBFLASH_CUST_PROG_E = 1                        
} SBFlash_SessionId_t;



typedef enum
{
    SBFLASH_ST_OK_E,                      
    SBFLASH_E_INVALID_ADD_E,              
    SBFLASH_E_AREA_LOCKED_E,              
    SBFLASH_E_SEQUENCE_ERR_E,             
    SBFLASH_ST_ERASING_E,                 
    SBFLASH_E_ERASE_ERR_E,                
    SBFLASH_ST_PROGRAMMING_E,             
    SBFLASH_E_PROG_ERR_E,                 
    SBFLASH_E_NOT_ERASED_E,               
    SBFLASH_E_TOO_MANY_DATA_E,            
    SBFLASH_E_SBRBPROG_E,                 
    SBFLASH_ST_ERROR_E                    
} SBFlash_Status_t;


typedef struct
{
    SBFlash_Action_t xAction_s;        
    SBFlash_SessionId_t xSessionId_s;  
    uint32 adStart_u32;                
    uint32 adEnd_u32;                  
    uint8 *dBuffer_pu8;                
    uint32 xDataLen_u32;               
    uint32 *xWorkspace_pu32;           
} SBFlash_Order_t;


extern SBFlash_Status_t SBFlash_Do(SBFlash_Order_t *xOrder_ps) __attribute__ ((section (".sb_flashdo,\"aw\",@progbits")));

#endif
