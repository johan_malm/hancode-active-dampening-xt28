


#ifndef _RESET_PUB_H
#define _RESET_PUB_H






#define RESET_SHUTDOWN  1


  
#define RESET_HISTBUF_VISIBLE       0x00                         
#define RESET_HISTBUF_SUPPRESSED    0x01                     



#define SWRESET_SOFTRESET_E     SWRESET_SOFTRESET_5VUNDERVOLTAGE_E


#define RESET_HISTBUF_SIZE 8




#define Reset_StoreErrorIntrInFlash(a, b, c, d, e);
#define Reset_InitErrorIntr();
#define Reset_IsrFlashError();
#define Reset_IsrMarginReadError();
#define Reset_DFlashSBitError();



#if ( (MACHINE_TYPE == TC_1796)||(MACHINE_TYPE == TC_1766) )
  
#define RESET_TEST_NOTHING  0x00                                       
#define RESET_TEST_RAM      0x01                                                 
#define RESET_TEST_ROM      0x02                                                 
#define RESET_TEST_RAMROM   0x03                                              


#define RESET_DSM_MAXVISIBILITIES   0x03ul
#define RESET_DSM_VISIBLE           0x00
#define RESET_DSM_LOCKED            0x01
#define RESET_DSM_SUPPRESSED        0x02


#define RESET_DUMMY_CONF_MASK   0x8000


enum
{
    SWRESET_GRP_POWERON_E = 0x00,
    SWRESET_GRP_HWRESET_E,
    SWRESET_GRP_WDT_E,
    SWRESET_GRP_WAKEUP_E,
    SWRESET_GRP_TRAP_E,
    SWRESET_GRP_SB_E,
    SWRESET_GRP_CB_E,
    SWRESET_SOFTRESETGRP_E,
    SWRESET_GRP_DUMMY_01,
    SWRESET_GRP_DUMMY_02,
    SWRESET_GRP_DUMMY_03,
    SWRESET_GRP_NUM_E
};


enum
{
    
    SWRESET_POWERON_E = 0x0000,
    SWRESET_POWERON_WDT_E,
    SWRESET_POWERON_KL15_E,
    SWRESET_HW_E,
    SWRESET_WDT_E,
    SWRESET_WAKEUP_E,
    SWRESET_HWR_NUM_E,
    
    
                                                                       
    TRAP_MMU_VAF_E = SWRESET_HWR_NUM_E,                                  
    TRAP_MMU_VAP_E,                                                 
                                                   
    TRAP_INTPROT_PRIV_E,                                       
    TRAP_INTPROT_MPR_E,                                
    TRAP_INTPROT_MPW_E,                               
    TRAP_INTPROT_MPX_E,                           
    TRAP_INTPROT_MPP_E,                          
    TRAP_INTPROT_MPN_E,                               
    TRAP_INTPROT_GRWP_E,                             
                                                          
    TRAP_INSTRERR_IOPC_E,                                              
    TRAP_INSTRERR_UOPC_E,                                        
    TRAP_INSTRERR_OPD_E,                                
    TRAP_INSTRERR_ALN_E,                                 
    TRAP_INSTRERR_MEM_E,                                 
                                                          
    TRAP_CONTMANA_FCD_E,                                   
    TRAP_CONTMANA_CDO_E,                                          
    TRAP_CONTMANA_CDU_E,                                         
    TRAP_CONTMANA_FCU_E,                                  
    TRAP_CONTMANA_CSU_E,                                       
    TRAP_CONTMANA_CTYP_E,                                          
    TRAP_CONTMANA_NEST_E,                                               
                                       
    TRAP_SYSBUSERR_PSE_E,                                     
    TRAP_SYSBUSERR_DSE_E,                                       
    TRAP_SYSBUSERR_DAE_E,                                       
                                                             
    TRAP_ASSTRAP_OVF_E,                                           
    TRAP_ASSTRAP_SOVF_E,                                   
                                                                 
    TRAP_SYSCALL_SYS_E,                                                 
                                                      
    TRAP_NMI_EXT_E,                                                 
    TRAP_NMI_PLL_E,                                                      
    TRAP_NMI_WDT_E,                                           
    TRAP_NMI_PER_E,                                             
    TRAP_RES1_E,                                                                     
    TRAP_RES2_E,                                                                     
    TRAP_NUM_E,

    
    
    SWRESET_POWERON_SIMU_E = TRAP_NUM_E,
    SWRESET_HRESET_SIMU_E,
    SWRESET_RB_PROG_E,
    SWRESET_SOFTRESET_5VUNDERVOLTAGE_E,
    SWRESET_SOFTRESET_POSTDRV2PREDRV_E,
    SWRESET_CBPROG_E,
    SWRESET_CBCPU_E,
    
    SWRESET_SBDUMMY_1_E,
    SWRESET_SBDUMMY_2_E,
    SWRESET_SBDUMMY_3_E,
    SWRESET_SBDUMMY_4_E,
    SWRESET_SBDUMMY_5_E,
    SWRESET_SBDUMMY_6_E,
    SWRESET_SBDUMMY_7_E,
    SWRESET_SBDUMMY_8_E,
    SWRESET_NUM_SB_E
};
#endif




#ifdef MEMLAY_USE_EARLY_CLEARED_RAM
MEMLAY_USE_EARLY_CLEARED_RAM(extern SBReset_EnvSWReset_t, *Reset_dActEnvSWReset_ps);
#else
__PRAGMA_USE__eos__1_5ms__RAM__x32__START
extern SBReset_EnvSWReset_t *Reset_dActEnvSWReset_ps;
__PRAGMA_USE__eos__1_5ms__RAM__x32__END
#endif

struct Reset_Environment
{
    uint32 adLoc;
    uint32 xUserValue;
    uint16 xId;
    uint8  xGrp;
    uint8  ctRst;
};
__PRAGMA_USE__eos__1_5ms__RAM__s32__START
extern struct Reset_Environment Reset_Env;   
                                             
                                             
__PRAGMA_USE__eos__1_5ms__RAM__s32__END



#define RESET_TRAPERRINTRBUFSIZE    4

#define RESET_FLMRERRBUFSIZE   4


typedef enum
{
    RESET_ERRINTRSTATE_READY,             
    RESET_ERRINTRSTATE_BUSY,              
    RESET_ERRINTRSTATE_START              
}Reset_ErrIntrState_t;


typedef enum
{
    RESET_PETSR_BIT0 = TRAP_NUM_E,              
    RESET_PETSR_BIT1,                           
    RESET_PETSR_BIT2,                           
    RESET_PETSR_BIT3,                           
    RESET_PETSR_BIT4,                           
    RESET_PETSR_BIT5,                           
    RESET_PETSR_BIT6,                           
    RESET_PETSR_BIT7,                           
    RESET_PETSR_BIT8,                           
    RESET_FLASH_CMD_SEQ_ERROR,                  
    RESET_FLASH_PROT_ERROR,                     
    RESET_FLASH_PFLASH_SBIT,                    
    RESET_FLASH_PFLASH_DBIT,                    
    RESET_FLASH1_PFLASH_SBIT,                   
    RESET_FLASH1_PFLASH_DBIT,                   
    RESET_FLASH_DFLASH_SBIT,                    
    RESET_FLASH_DFLASH_DBIT,                    
    RESET_FLASH_END_OF_BUSY,                    
    RESET_MLI_TX_PARITY_ERROR,                  
    RESET_MLI_TX_TIMEOUT_ERROR,                 
    RESET_MLI_RX_PARITY_ERROR,                  
    RESET_MLI_RX_MEM_PROT,                      
    RESET_PCPERR_BER,                           
    RESET_PCPERR_IOP,                           
    RESET_PCPERR_DCR,                           
    RESET_PCPERR_IAE,                           
    RESET_BUSERR_DBCU,                          
    RESET_BUSERR_PBCU,                          
    RESET_BUSERR_RBCU,                          
    RESET_BUSERR_SBCU,                          
    RESET_BUSERR_LBCU,                          
    RESET_CALCERR_FPU                           
}Reset_ErrIntrID_t;



#define RESET_PRTYERR_LDRAM_DPRAM   RESET_PETSR_BIT0    
#define RESET_PRTYERR_SPRAM_ICACHE  RESET_PETSR_BIT1    
#define RESET_PRTYERR_PCTRAM        RESET_PETSR_BIT2    
#define RESET_PRTYERR_SBRAM_SRAM    RESET_PETSR_BIT3    
#define RESET_PRTYERR_PRAM          RESET_PETSR_BIT4    
#define RESET_PRTYERR_CMEM          RESET_PETSR_BIT5    
#define RESET_PRTYERR_CAN           RESET_PETSR_BIT6    




typedef struct
{
    Reset_ErrIntrState_t state;            
    Reset_ErrIntrID_t id;                  
    uint32 usrval;                         
}Reset_ErrIntrInfo_t;


MEMLAY_USE_PROTRAM(extern Reset_ErrIntrInfo_t, Reset_ErrIntrInfo_s);  
__PRAGMA_USE__eos__1_5ms__RAM__arr8__START
extern uint8 Reset_TrapErrIntrBuf[RESET_TRAPERRINTRBUFSIZE]; 
__PRAGMA_USE__eos__1_5ms__RAM__arr8__END


MEMLAY_USE_EARLY_CLEARED_RAM(extern uint16, Reset_xHistBuf[RESET_HISTBUF_SIZE]);



__PRAGMA_USE__CODE__eos__HighSpeed__START
extern void Reset_SWResetRequest(uint8 dResetGrp_u8,
                                 uint16 dResetIdentifier_u16,
                                 uint32 dUserDefined_u32);
extern void Reset_SoftReset(uint8, uint16, uint32 );
extern void Reset_SWResetHandler(void);
extern void Reset_SWResetInit(void);
extern void Reset_ProtRamInit(void);
extern void Reset_DSMHandling_Proc_Ini(void);
extern void Reset_SWResetMonitor_Proc(void);
extern void Reset_SWResetMonitor_Ini(void);
extern void Reset_SWResetMonitor_Shutdown(void);
extern bool Reset_EcuWasOff(void);
extern bool Reset_ProtRAMCleared(void);
extern bool Reset_ProtRAMTest(void);
extern void Reset_ProtRAMCheckIO(void);
extern void Reset_SetWdogMarker(void);

extern void Reset_ErrIntr(bool rst, Reset_ErrIntrID_t id, uint32 usrval);
extern void Reset_TrapErrIntr_Proc_Ini(void);
extern void Reset_TrapErrIntr_Proc(void);
__PRAGMA_USE__CODE__eos__HighSpeed__END



__PRAGMA_USE__eos__1_5ms__RAM__arr8__START
extern uint8 Reset_FlMRErrBuf[RESET_FLMRERRBUFSIZE]; 
__PRAGMA_USE__eos__1_5ms__RAM__arr8__END

__PRAGMA_USE__CODE__eos__HighSpeed__START
extern void Reset_FlMarginReadErr(uint8 const *id);
__PRAGMA_USE__CODE__eos__HighSpeed__END


#endif
