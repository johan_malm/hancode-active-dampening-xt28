

#ifndef _CPU_PUB_H
#define _CPU_PUB_H






#ifndef CPU_DEBUG_INTSUSP
#define CPU_DEBUG_INTSUSP   (0)
#endif


#ifndef CPU_USE_DAMOS
#define CPU_USE_DAMOS       (TRUE)
#endif





#define CPU_INLINE              static __inline__


#define CPU_INLINE_ALWAYS       __attribute__ (( always_inline ))
#define CPU_INLINE_PROTOTYPE    CPU_INLINE  CPU_INLINE_ALWAYS


#define CPU_TICKS_PER_US_U32   (MACHINE_TICKS_PER_US)
#define CPU_TICKS_PER_10US_U32 ((MACHINE_TICKS_PER_US) * 10UL)
#define CPU_TICKS_PER_MS_U32   ((MACHINE_TICKS_PER_US) * 1000UL)
#define CPU_TICKS_PER_S_U32    ((MACHINE_TICKS_PER_US) * 1000000UL)



#define CPU_SET_SEQUENCE_POINT() asm volatile ("" ::: "memory")








#define CPU_US_TO_TIM0TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 0ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 0ULL))
#define CPU_US_TO_TIM1TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 4ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 4ULL))
#define CPU_US_TO_TIM2TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 8ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 8ULL))
#define CPU_US_TO_TIM3TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 12ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 12ULL))
#define CPU_US_TO_TIM4TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 16ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 16ULL))
#define CPU_US_TO_TIM5TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 20ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 20ULL))
#define CPU_US_TO_TIM6TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 32ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_US_U32) >> 32ULL))


#define CPU_MS_TO_TIM0TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 0ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 0ULL))
#define CPU_MS_TO_TIM1TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 4ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 4ULL))
#define CPU_MS_TO_TIM2TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 8ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 8ULL))
#define CPU_MS_TO_TIM3TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 12ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 12ULL))
#define CPU_MS_TO_TIM4TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 16ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 16ULL))
#define CPU_MS_TO_TIM5TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 20ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 20ULL))
#define CPU_MS_TO_TIM6TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 32ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_MS_U32) >> 32ULL))


#define CPU_S_TO_TIM0TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 0ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 0ULL))
#define CPU_S_TO_TIM1TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 4ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 4ULL))
#define CPU_S_TO_TIM2TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 8ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 8ULL))
#define CPU_S_TO_TIM3TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 12ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 12ULL))
#define CPU_S_TO_TIM4TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 16ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 16ULL))
#define CPU_S_TO_TIM5TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 20ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 20ULL))
#define CPU_S_TO_TIM6TICKS(time)\
  (uint32)(((((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 32ULL) >= 0xFFFFFFFFULL) ? \
  0xFFFFFFFFULL \
  : (((uint64)(time) * (uint64)CPU_TICKS_PER_S_U32) >> 32ULL))


#define CPU_FDIV(a,b) ( (((a)/(b))  > (real32)MAXUINT32)  ? MAXUINT32 : (uint32)((a)/(b)) )


#define CPU_TIM0TICKS_TO_US(ticks)\
  (((ticks) / CPU_TICKS_PER_US_U32))
#define CPU_TIM1TICKS_TO_US(ticks)\
  (CPU_FDIV((real32)(ticks) * 16.0F, (real32)CPU_TICKS_PER_US_U32))
#define CPU_TIM2TICKS_TO_US(ticks)\
  (CPU_FDIV((real32)(ticks) * 256.0F, (real32)CPU_TICKS_PER_US_U32))
#define CPU_TIM3TICKS_TO_US(ticks)\
  (CPU_FDIV((real32)(ticks) * 4096.0F, (real32)CPU_TICKS_PER_US_U32))
#define CPU_TIM4TICKS_TO_US(ticks)\
  (CPU_FDIV((real32)(ticks) * 65536.0F, (real32)CPU_TICKS_PER_US_U32))
#define CPU_TIM5TICKS_TO_US(ticks)\
  (CPU_FDIV((real32)(ticks) * 1048576.0F, (real32)CPU_TICKS_PER_US_U32))
#define CPU_TIM6TICKS_TO_US(ticks)\
  (CPU_FDIV((real32)(ticks) * 4294967296.0F, (real32)CPU_TICKS_PER_US_U32))


#define CPU_TIM0TICKS_TO_MS(ticks)\
  (((ticks) / CPU_TICKS_PER_MS_U32))
#define CPU_TIM1TICKS_TO_MS(ticks)\
  (CPU_FDIV((real32)(ticks) * 16.0F, (real32)CPU_TICKS_PER_MS_U32))
#define CPU_TIM2TICKS_TO_MS(ticks)\
  (CPU_FDIV((real32)(ticks) * 256.0F, (real32)CPU_TICKS_PER_MS_U32))
#define CPU_TIM3TICKS_TO_MS(ticks)\
  (CPU_FDIV((real32)(ticks) * 4096.0F, (real32)CPU_TICKS_PER_MS_U32))
#define CPU_TIM4TICKS_TO_MS(ticks)\
  (CPU_FDIV((real32)(ticks) * 65536.0F, (real32)CPU_TICKS_PER_MS_U32))
#define CPU_TIM5TICKS_TO_MS(ticks)\
  (CPU_FDIV((real32)(ticks) * 1048576.0F, (real32)CPU_TICKS_PER_MS_U32))
#define CPU_TIM6TICKS_TO_MS(ticks)\
  (CPU_FDIV((real32)(ticks) * 4294967296.0F, (real32)CPU_TICKS_PER_MS_U32))


#define CPU_TIM0TICKS_TO_S(ticks)\
  (((ticks) / CPU_TICKS_PER_S_U32))
#define CPU_TIM1TICKS_TO_S(ticks)\
  (CPU_FDIV((real32)(ticks) * 16.0F, (real32)CPU_TICKS_PER_S_U32))
#define CPU_TIM2TICKS_TO_S(ticks)\
  (CPU_FDIV((real32)(ticks) * 256.0F, (real32)CPU_TICKS_PER_S_U32))
#define CPU_TIM3TICKS_TO_S(ticks)\
  (CPU_FDIV((real32)(ticks) * 4096.0F, (real32)CPU_TICKS_PER_S_U32))
#define CPU_TIM4TICKS_TO_S(ticks)\
  (CPU_FDIV((real32)(ticks) * 65536.0F, (real32)CPU_TICKS_PER_S_U32))
#define CPU_TIM5TICKS_TO_S(ticks)\
  (CPU_FDIV((real32)(ticks) * 1048576.0F, (real32)CPU_TICKS_PER_S_U32))
#define CPU_TIM6TICKS_TO_S(ticks)\
  (CPU_FDIV((real32)(ticks) * 4294967296.0F, (real32)CPU_TICKS_PER_S_U32))



#define CPU_US_TO_TICKS CPU_US_TO_TIM0TICKS
#define CPU_MS_TO_TICKS CPU_MS_TO_TIM0TICKS
#define CPU_S_TO_TICKS  CPU_S_TO_TIM0TICKS
#define CPU_TICKS_TO_US CPU_TIM0TICKS_TO_US
#define CPU_TICKS_TO_MS CPU_TIM0TICKS_TO_MS
#define CPU_TICKS_TO_S  CPU_TIM0TICKS_TO_S


#define Cpu_ServeWatchdog      Cpu_Watchdog_Proc




typedef struct
{
    uint32 dCRCVal_u32;  
    bool   stDMA_b;      
} Cpu_CRCState_t;


typedef struct
{
    uint32 tiMaxIntDis_u32;
    uint32 adMaxIntDis_u32;
} Cpu_DebugInfo_t;



__PRAGMA_USE__hwe__1_5ms__RAM__s32__START
extern Cpu_DebugInfo_t Cpu_DebugInfo_s;
__PRAGMA_USE__hwe__1_5ms__RAM__s32__END

__PRAGMA_USE__hwe__1_5ms__RAM__x8__START
extern uint8 Cpu_ctIntSuspLvl_u8;
__PRAGMA_USE__hwe__1_5ms__RAM__x8__END




__PRAGMA_USE__CODE__hwe__LowSpeed__START
extern void Cpu_InitRAM(uint32* , const uint32*, uint32, uint32, uint32);
__PRAGMA_USE__CODE__hwe__LowSpeed__END


__PRAGMA_USE__CODE__hwe__NormalSpeed__START
extern void Cpu_ResetEndinit(void);
extern void Cpu_SetEndinit(void);
__PRAGMA_USE__CODE__hwe__NormalSpeed__END


__PRAGMA_USE__CODE__hwe__HighSpeed__START
extern void   Cpu_CRCTaskStart(Cpu_CRCState_t *);
extern void   Cpu_CRCTaskEnd(const Cpu_CRCState_t *);
extern uint32 Cpu_CalcCRC(uint32*, uint32, uint32);
__PRAGMA_USE__CODE__hwe__HighSpeed__END


__PRAGMA_USE__CODE__hwe__NormalSpeed__START
extern void Cpu_SetWatchdogTimeout(uint32);


extern uint32 Cpu_GetWatchdogTimeout(void);
__PRAGMA_USE__CODE__hwe__NormalSpeed__END


__PRAGMA_USE__CODE__hwe__HighSpeed__START
extern void Cpu_SwapByteBuf16(uint8*, uint32);
extern void Cpu_SwapByteBuf32(uint8*, uint32);
__PRAGMA_USE__CODE__hwe__HighSpeed__END


__PRAGMA_USE__CODE__hwe__NormalSpeed__START
extern bool Cpu_DetectDebugger(void);
__PRAGMA_USE__CODE__hwe__NormalSpeed__END




__PRAGMA_USE__CODE__hwe__LowSpeed__START
extern void Cpu_Proc_Ini(void);
extern void Cpu_Proc_IniEnd(void);
extern void Cpu_Mon_Proc_Ini(void);


extern void Cpu_Mon_Proc(void);


void Cpu_ErrorInt_Proc_Ini(void);
void Cpu_ErrorInt_Proc(void);
__PRAGMA_USE__CODE__hwe__LowSpeed__END


__PRAGMA_USE__CODE__hwe__NormalSpeed__START
extern void Cpu_Watchdog_Proc(void);
__PRAGMA_USE__CODE__hwe__NormalSpeed__END



#define Cpu_GetSysTimePart(TIMER) Cpu_Get##TIMER

#define Cpu_GetTIM0 STM.TIM0
#define Cpu_GetTIM1 STM.TIM1
#define Cpu_GetTIM2 STM.TIM2
#define Cpu_GetTIM3 STM.TIM3
#define Cpu_GetTIM4 STM.TIM4
#define Cpu_GetTIM5 STM.TIM5
#define Cpu_GetTIM6 STM.TIM6



#define Cpu_GetSysTicks() Cpu_GetTIM0



#define Cpu_DisableInt()    Cpu_SuspendInt()


#define Cpu_EnableInt()     Cpu_ResumeInt()



#define STD_SUSPEND_INTERRUPTS    Cpu_SuspendInt();



#define STD_RESUME_INTERRUPTS     Cpu_ResumeInt();



#if (CPU_DEBUG_INTSUSP == 0)
#define Cpu_SuspendInt()                                                                           \
{                                                                                                  \
           \
           \
    uint32 stPreviousStateOfCpuIntSystem_u32;                                                      \
                       \
    asm volatile ("mfcr %0, $icr ; disable":"=d" (stPreviousStateOfCpuIntSystem_u32)::"memory");   \
    do { } while(0) 
#else
#define Cpu_SuspendInt()                                                                           \
{                                                                                                  \
                                              \
    uint32 tiCurrIntDis_u32;                                                                       \
           \
           \
    uint32 stPreviousStateOfCpuIntSystem_u32;                                                      \
    asm volatile ("mfcr %0, $icr ; disable":"=d" (stPreviousStateOfCpuIntSystem_u32)::"memory");   \
    tiCurrIntDis_u32 = Cpu_GetSysTimePart(TIM0);                                                   \
                                                    \
    Cpu_ctIntSuspLvl_u8++;                                                                         \
    CPU_SET_SEQUENCE_POINT();                                                                      \
    do { } while(0) 
#endif



#if (CPU_DEBUG_INTSUSP == 0)
#define Cpu_ResumeInt()                                                                            \
            \
    asm volatile ("jz.t %0,8,0f; enable; 0:"::"d" (stPreviousStateOfCpuIntSystem_u32):"memory");   \
} do { } while(0) 
#else
#define Cpu_ResumeInt()                                                                            \
    CPU_SET_SEQUENCE_POINT();                                                                      \
                                                                 \
    tiCurrIntDis_u32 = Cpu_GetSysTimePart(TIM0) - tiCurrIntDis_u32;                                \
                                                    \
    Cpu_ctIntSuspLvl_u8--;                                                                         \
                             \
    if (tiCurrIntDis_u32 < CPU_S_TO_TICKS(1UL))                                                    \
    {                                                                                              \
                                                        \
        if (tiCurrIntDis_u32 > Cpu_DebugInfo_s.tiMaxIntDis_u32)                                    \
        {                                                                                          \
                                                                           \
            Cpu_DebugInfo_s.tiMaxIntDis_u32 = tiCurrIntDis_u32;                                    \
                                        \
            asm volatile ("mfcr %0, $pc":"=d" (Cpu_DebugInfo_s.adMaxIntDis_u32) : : "memory" );    \
        }                                                                                          \
    }                                                                                              \
            \
    asm volatile ("jz.t %0,8,0f; enable; 0:"::"d" (stPreviousStateOfCpuIntSystem_u32):"memory");   \
} do { } while(0) 
#endif



#define Cpu_EnableSRN(SRC_REGISTER)     asm volatile ("":::"memory");   \
                                        ((SRC_REGISTER) |= SRE);        \
                                        asm volatile ("":::"memory")



#define Cpu_DisableSRN(SRC_REGISTER)    asm volatile ("":::"memory");   \
                                        ((SRC_REGISTER) &= ~SRE);       \
                                        asm volatile ("":::"memory")



#define Cpu_SetIntReq(SRC_REGISTER)     asm volatile ("":::"memory");   \
                                        ((SRC_REGISTER) |= SETR);       \
                                        asm volatile ("":::"memory")



#define Cpu_ClrIntReq(SRC_REGISTER)     asm volatile ("":::"memory");   \
                                        ((SRC_REGISTER) |= CLRR);       \
                                        asm volatile ("":::"memory")



#define Cpu_IsIntPending(SRC_REGISTER) ((bool)(((SRC_REGISTER) & SRR) != 0UL))



#define Cpu_SetSRNPrio(SRC_REGISTER,PRIO)                                     \
{                                                                             \
    uint32 xTempSrcReg_u32 = (SRC_REGISTER);                                  \
    (SRC_REGISTER) = ( (xTempSrcReg_u32 & ~SRPN_MSK) | ((PRIO) & SRPN_MSK) ); \
} do {} while(0)



#define CPU_PRIO_TOS_MSK (SRPN_MSK | TOS_MSK)

#define Cpu_SetSRNPrioTOS(SRC_REGISTER,PRIO,TOS)                    \
{                                                                   \
    uint32 xTempSrcReg_u32 = (SRC_REGISTER);                        \
    (SRC_REGISTER) = ( (xTempSrcReg_u32 & ~CPU_PRIO_TOS_MSK)        \
                    | ((PRIO) & SRPN_MSK)                   \
                    | ( ((TOS) << 10ul) & TOS_MSK ) );      \
} do {} while(0)



#define Cpu_GetRev() ((uint8)(SCU.CHIPID & REV_MSK))



#define Cpu_GetID()  ((uint8)((SCU.CHIPID & MOD_MSK) >> 8UL))



#define Cpu_SetCRCStart(VALUE) (MCHK.RR = (VALUE))



#define Cpu_DoCRC(VALUE) (MCHK.IR = (VALUE))



#define Cpu_GetCRC() (MCHK.RR)


#if ((MACHINE_TYPE == TC_1796) || (MACHINE_TYPE == TC_1766))
  #define Cpu_IsED() (Reg_GetBit(SCU, STAT, STAT_EEA))
#elif ((MACHINE_TYPE == TC_1797) || (MACHINE_TYPE == TC_1767))
  #define Cpu_IsED() (Reg_GetBit(SCU, CHIPID, CHIPID_EEA))
#else
  #error "MACHINE_TYPE not supported"
#endif



#if ((MACHINE_TYPE == TC_1796) || (MACHINE_TYPE == TC_1766))
  #define Cpu_GetNMIENState() (Reg_GetBit(SCU, CON, CON_NMIEN))
  #define Cpu_GetWDTENState() (Reg_GetBit(SCU, CON, CON_NMIEN))
#elif ((MACHINE_TYPE == TC_1797) || (MACHINE_TYPE == TC_1767))
  
  
  
  
  #define Cpu_GetNMIENState() (!Reg_GetBit(SCU, TRAPDIS, TRAPDIS_WDTT))
  #define Cpu_GetWDTENState() (!Reg_GetBit(SCU, TRAPDIS, TRAPDIS_WDTT))
#else
  #error "MACHINE_TYPE not supported"
#endif




#define Cpu_GetDebugActState() (Cpu_DetectDebugger())


#endif
