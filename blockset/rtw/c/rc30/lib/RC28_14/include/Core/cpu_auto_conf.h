

#ifndef _CPU_AUTO_CONF_H
#define _CPU_AUTO_CONF_H
#ifndef _LIBRARYABILITY_H






#include "adc_1797.h"
#include "can_1797.h"
#include "cps_1797.h"
#include "dma_1797.h"
#include "gpta0_1797.h"
#include "gpta1_1797.h"
#include "lbcu_1797.h"
#include "msc_1797.h"
#include "pcp.h"
#include "pcp_1797.h"
#include "sbcu_1797.h"
#include "scu_1797.h"
#include "ssc_1797.h"
#include "stm_1797.h"





#define CPU_MASTER_SLAVE_SYSTEM        FALSE



#define CPU_SINGLE_ECU                 0xFF  
#define CPU_MASTER_ECU                 0x00  
#define CPU_SLAVE_ECU                  0x01  


#define CPU_WATCHDOG_ENABLE            TRUE

#define CPU_WATCHDOG_TIMEOUT           500UL


#define CPU_ENABLE_ERROR_INTERRUPTS    TRUE


#define Cpu_GetSCLKFreq()              (0UL)
#define Cpu_GetSSC0Freq()              (20039UL) 	
#define Cpu_GetFADCFreq()              (0UL)
#define Cpu_GetERAYFreq()              (0UL)
#define Cpu_GetMLI0Freq()              (0UL)
#define Cpu_GetADCFreq()               (90000UL) 	
#define Cpu_GetSSC1Freq()              (0UL)
#define Cpu_GetMLI1Freq()              (0UL)
#define Cpu_GetMSC0Freq()              (45000UL) 	
#define Cpu_GetASCFreq()               (0UL)
#define Cpu_GetGPTAFreq()              (20039UL) 	
#define Cpu_GetCANFreq()               (74971UL) 	
#define Cpu_GetMSC1Freq()              (0UL)



#define Cpu_GetEnableTime(MODULE)      Cpu_GetEnableTime##MODULE
#define Cpu_GetEnableTimeADC           Cpu_tiEnaModAdc_u32
#define Cpu_GetEnableTimeCAN           Cpu_tiEnaModCan_u32



#define ADC0_CONV_FIN_SRN              ADC0.SRC1
#define ADCI_SRN_EVENT_0               ADC0.SRC0
#define ADCI_SRN_EVENT_1               ADC0.SRC3
#define ADCI_SRN_EVENT_2               ADC0.SRC6
#define API_SYSTEM_TIMER_1             STM.SRC0
#define API_SYSTEM_TIMER_2             STM.SRC1
#define API_TASK_PRIO_1                CPS.CPU_SRC0
#define API_TASK_PRIO_2                CPS.CPU_SRC1
#define API_TASK_PRIO_3                CPS.CPU_SRC2
#define API_TASK_PRIO_4                CPS.CPU_SRC3
#define CAN_ERROR_SRN                  CAN.SRC5
#define CAN_RX_SRN                     CAN.SRC2
#define CAN_TX_SRN                     CAN.SRC1
#define CPU_LBCU_SRN                   LBCU.SRC
#define CPU_SBCU_SRN                   SBCU.SRC
#define DMA_ERROR_SRN                  DMA.SRC7
#define DUMMY_CAN_SRC0                 CAN.SRC0
#define FLASH_ERROR_SRN                SCU.SRC0
#define PCP_ESR                        Cpu_SrnVar_PCP_ESR_u32
#define PCP_LOAD_SRN                   ADC0.SRC2
#define PCP_MON_SRN                    ADC0.SRC4
#define PCP_SRN_0                      PCP.SRC0
#define PCP_SRN_1                      PCP.SRC1
#define PCP_SRN_10                     PCP.SRC10
#define PCP_SRN_11                     PCP.SRC11
#define PCP_SRN_2                      PCP.SRC2
#define PCP_SRN_3                      PCP.SRC3
#define PCP_SRN_4                      PCP.SRC4
#define PCP_SRN_5                      PCP.SRC5
#define PCP_SRN_6                      PCP.SRC6
#define PCP_SRN_7                      PCP.SRC7
#define PCP_SRN_8                      PCP.SRC8
#define PCP_SRN_9                      PCP.SRC9
#define PWMIN_P_DFI_01_SRN             GPTA0.SRC00
#define PWMIN_P_DFI_02_SRN             GPTA1.SRC01
#define PWMIN_P_DFI_03_SRN             GPTA0.SRC01
#define PWMIN_P_DFI_04_SRN             GPTA0.SRC02
#define PWMIN_P_DFI_05_SRN             GPTA1.SRC00
#define PWMIN_P_DFI_06_SRN             GPTA0.SRC03
#define PWMIN_P_DFI_07_SRN             GPTA0.SRC11
#define PWMIN_P_DFI_08_SRN             GPTA0.SRC10
#define PWMIN_P_DFI_09_SRN             GPTA1.SRC02
#define PWMIN_P_DFI_10_SRN             GPTA1.SRC03
#define P_DFI_01_CPUSRN                ADC0.SRC5
#define P_DFI_02_CPUSRN                ADC0.SRC7
#define P_DFI_03_CPUSRN                ADC0.SRC8
#define P_DFI_04_CPUSRN                CAN.SRC10
#define P_DFI_05_CPUSRN                CAN.SRC11
#define P_DFI_06_CPUSRN                CAN.SRC12
#define P_DFI_07_CPUSRN                CAN.SRC13
#define P_DFI_08_CPUSRN                CAN.SRC14
#define P_DFI_09_CPUSRN                CAN.SRC15
#define P_DFI_10_CPUSRN                CAN.SRC3
#define P_PDOH_10_SRN                  GPTA0.SRC16
#define P_PDOH_11_SRN                  GPTA0.SRC15
#define P_PDOH_12_SRN                  GPTA0.SRC17
#define P_PDOH_1_SRN                   GPTA1.SRC18
#define P_PDOH_9_SRN                   GPTA0.SRC14
#define P_POH_11_SRN                   GPTA0.SRC06
#define P_POH_13_SRN                   GPTA0.SRC07
#define RST_CPLD_INTR                  GPTA0.SRC09
#define SPI_SSC0_GPTA_TIMER_SRN        GPTA0.SRC37
#define SPI_SSC0_QH_SRN                SSC0.ESRC
#define SPI_SSC0_TSRC                  SSC0.TSRC
#define SPI_SSC0_TXRX_SRN              SSC0.RSRC
#define ST_INH_H_INTR                  GPTA1.SRC06
#define ST_OE_INTR                     GPTA0.SRC12
#define SW_INT_01                      CAN.SRC4
#define SW_INT_02                      CAN.SRC6
#define SW_INT_03                      CAN.SRC7
#define SW_INT_04                      CAN.SRC8
#define V_DO_INTR                      GPTA1.SRC08








__PRAGMA_USE__hwe__1_5ms__RAM__x32__START
extern uint32               Cpu_SrnVar_PCP_ESR_u32;
__PRAGMA_USE__hwe__1_5ms__RAM__x32__END
__PRAGMA_USE__hwe__1_5ms__RAM__x32__START
extern uint32               Cpu_tiEnaModAdc_u32;
__PRAGMA_USE__hwe__1_5ms__RAM__x32__END
__PRAGMA_USE__hwe__1_5ms__RAM__x32__START
extern uint32               Cpu_tiEnaModCan_u32;
__PRAGMA_USE__hwe__1_5ms__RAM__x32__END




CPU_INLINE_PROTOTYPE uint8 Cpu_GetECUType(void);
CPU_INLINE uint8 Cpu_GetECUType(void)
{
    return ((uint8)CPU_SINGLE_ECU);
}



#endif


#endif
