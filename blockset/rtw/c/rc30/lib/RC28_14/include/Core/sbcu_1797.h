

#ifndef _SBCU_1797_H
#define _SBCU_1797_H


#ifdef REGDEF_FOR_PCP
    #define _SBCU_UL(x) x
#else
    #define _SBCU_UL(x) x##UL
#endif



typedef struct {
    volatile uint32 ID;                  
    volatile uint32 RESERVED0[1];        
    volatile uint32 CON;                 
    volatile uint32 RESERVED1[3];        
    volatile uint32 ECON;                
    volatile uint32 EADD;                
    volatile uint32 EDAT;                
    volatile uint32 RESERVED2[1];        
    volatile uint32 DBCNTL;              
    volatile uint32 DBGRNT;              
    volatile uint32 DBADR1;              
    volatile uint32 DBADR2;              
    volatile uint32 DBBOS;               
    volatile uint32 DBGNTT;              
    volatile uint32 DBADRT;              
    volatile uint32 DBBOST;              
    volatile uint32 DBDAT;               
    volatile uint32 RESERVED3[42];       
    volatile uint32 SRC;                 
} SBCU_RegMap_t;




extern SBCU_RegMap_t SBCU __attribute__ ((asection (".zbss.label_only","f=awz")));





#define SBCU_ID_MOD_NUMBER_POS               _SBCU_UL(16)
#define SBCU_ID_MOD_NUMBER_LEN               _SBCU_UL(16)


#define SBCU_ID_MOD_REV_POS                  _SBCU_UL(0)
#define SBCU_ID_MOD_REV_LEN                  _SBCU_UL(8)


#define SBCU_ID_MOD_TYPE_POS                 _SBCU_UL(8)
#define SBCU_ID_MOD_TYPE_LEN                 _SBCU_UL(8)


#define SBCU_ID_MOD_REV_POS                  _SBCU_UL(0)
#define SBCU_ID_MOD_REV_LEN                  _SBCU_UL(8)






#define SBCU_CON_TOUT_POS                    _SBCU_UL(0)
#define SBCU_CON_TOUT_LEN                    _SBCU_UL(16)


#define SBCU_CON_DBG_POS                     _SBCU_UL(16)
#define SBCU_CON_DBG_LEN                     _SBCU_UL(1)


#define SBCU_CON_SPE_POS                     _SBCU_UL(19)
#define SBCU_CON_SPE_LEN                     _SBCU_UL(1)


#define SBCU_CON_SPC_POS                     _SBCU_UL(24)
#define SBCU_CON_SPC_LEN                     _SBCU_UL(8)


#define SBCU_ECON_ERRCNT_POS                 _SBCU_UL(0)
#define SBCU_ECON_ERRCNT_LEN                 _SBCU_UL(16)


#define SBCU_ECON_TOUT_POS                   _SBCU_UL(16)
#define SBCU_ECON_TOUT_LEN                   _SBCU_UL(1)


#define SBCU_ECON_RDY_POS                    _SBCU_UL(17)
#define SBCU_ECON_RDY_LEN                    _SBCU_UL(1)


#define SBCU_ECON_ABT_POS                    _SBCU_UL(18)
#define SBCU_ECON_ABT_LEN                    _SBCU_UL(1)


#define SBCU_ECON_ACK_POS                    _SBCU_UL(19)
#define SBCU_ECON_ACK_LEN                    _SBCU_UL(2)


#define SBCU_ECON_SVM_POS                    _SBCU_UL(21)
#define SBCU_ECON_SVM_LEN                    _SBCU_UL(1)


#define SBCU_ECON_WRN_POS                    _SBCU_UL(22)
#define SBCU_ECON_WRN_LEN                    _SBCU_UL(1)


#define SBCU_ECON_RDN_POS                    _SBCU_UL(23)
#define SBCU_ECON_RDN_LEN                    _SBCU_UL(1)


#define SBCU_ECON_TAG_POS                    _SBCU_UL(24)
#define SBCU_ECON_TAG_LEN                    _SBCU_UL(4)


#define SBCU_ECON_OPC_POS                    _SBCU_UL(28)
#define SBCU_ECON_OPC_LEN                    _SBCU_UL(4)


#define SBCU_EADD_FPIADR_POS                 _SBCU_UL(0)
#define SBCU_EADD_FPIADR_LEN                 _SBCU_UL(32)


#define SBCU_EDAT_FPIDAT_POS                 _SBCU_UL(0)
#define SBCU_EDAT_FPIDAT_LEN                 _SBCU_UL(32)


#define SBCU_DBCNTL_EO_POS                   _SBCU_UL(0)
#define SBCU_DBCNTL_EO_LEN                   _SBCU_UL(1)


#define SBCU_DBCNTL_OA_POS                   _SBCU_UL(1)
#define SBCU_DBCNTL_OA_LEN                   _SBCU_UL(1)


#define SBCU_DBCNTL_RA_POS                   _SBCU_UL(4)
#define SBCU_DBCNTL_RA_LEN                   _SBCU_UL(1)


#define SBCU_DBCNTL_CONCOM0_POS              _SBCU_UL(12)
#define SBCU_DBCNTL_CONCOM0_LEN              _SBCU_UL(1)


#define SBCU_DBCNTL_CONCOM1_POS              _SBCU_UL(13)
#define SBCU_DBCNTL_CONCOM1_LEN              _SBCU_UL(1)


#define SBCU_DBCNTL_CONCOM2_POS              _SBCU_UL(14)
#define SBCU_DBCNTL_CONCOM2_LEN              _SBCU_UL(1)


#define SBCU_DBCNTL_ONG_POS                  _SBCU_UL(16)
#define SBCU_DBCNTL_ONG_LEN                  _SBCU_UL(1)


#define SBCU_DBCNTL_ONA1_POS                 _SBCU_UL(20)
#define SBCU_DBCNTL_ONA1_LEN                 _SBCU_UL(2)


#define SBCU_DBCNTL_ONA2_POS                 _SBCU_UL(24)
#define SBCU_DBCNTL_ONA2_LEN                 _SBCU_UL(2)


#define SBCU_DBCNTL_ONBOS0_POS               _SBCU_UL(28)
#define SBCU_DBCNTL_ONBOS0_LEN               _SBCU_UL(1)


#define SBCU_DBCNTL_ONBOS1_POS               _SBCU_UL(29)
#define SBCU_DBCNTL_ONBOS1_LEN               _SBCU_UL(1)


#define SBCU_DBCNTL_ONBOS2_POS               _SBCU_UL(30)
#define SBCU_DBCNTL_ONBOS2_LEN               _SBCU_UL(1)


#define SBCU_DBCNTL_ONBOS3_POS               _SBCU_UL(31)
#define SBCU_DBCNTL_ONBOS3_LEN               _SBCU_UL(1)


#define SBCU_DBGRNT_DMAH_POS                 _SBCU_UL(0)
#define SBCU_DBGRNT_DMAH_LEN                 _SBCU_UL(1)


#define SBCU_DBGRNT_ONE_POS                  _SBCU_UL(1)
#define SBCU_DBGRNT_ONE_LEN                  _SBCU_UL(2)


#define SBCU_DBGRNT_ONES_POS                 _SBCU_UL(7)
#define SBCU_DBGRNT_ONES_LEN                 _SBCU_UL(9)


#define SBCU_DBGRNT_PCP_POS                  _SBCU_UL(3)
#define SBCU_DBGRNT_PCP_LEN                  _SBCU_UL(1)


#define SBCU_DBGRNT_DMAM_POS                 _SBCU_UL(4)
#define SBCU_DBGRNT_DMAM_LEN                 _SBCU_UL(1)


#define SBCU_DBGRNT_LFI_POS                  _SBCU_UL(5)
#define SBCU_DBGRNT_LFI_LEN                  _SBCU_UL(1)


#define SBCU_DBGRNT_DMAL_POS                 _SBCU_UL(6)
#define SBCU_DBGRNT_DMAL_LEN                 _SBCU_UL(1)


#define SBCU_DBADR1_ADR1_POS                 _SBCU_UL(0)
#define SBCU_DBADR1_ADR1_LEN                 _SBCU_UL(32)


#define SBCU_DBADR2_ADR2_POS                 _SBCU_UL(0)
#define SBCU_DBADR2_ADR2_LEN                 _SBCU_UL(32)


#define SBCU_DBBOS_OPC_POS                   _SBCU_UL(0)
#define SBCU_DBBOS_OPC_LEN                   _SBCU_UL(4)


#define SBCU_DBBOS_SVM_POS                   _SBCU_UL(4)
#define SBCU_DBBOS_SVM_LEN                   _SBCU_UL(1)


#define SBCU_DBBOS_WR_POS                    _SBCU_UL(8)
#define SBCU_DBBOS_WR_LEN                    _SBCU_UL(1)


#define SBCU_DBBOS_RD_POS                    _SBCU_UL(12)
#define SBCU_DBBOS_RD_LEN                    _SBCU_UL(1)


#define SBCU_DBGNTT_DMAH_POS                 _SBCU_UL(0)
#define SBCU_DBGNTT_DMAH_LEN                 _SBCU_UL(1)


#define SBCU_DBGNTT_PCP_POS                  _SBCU_UL(3)
#define SBCU_DBGNTT_PCP_LEN                  _SBCU_UL(1)


#define SBCU_DBGNTT_DMAM_POS                 _SBCU_UL(4)
#define SBCU_DBGNTT_DMAM_LEN                 _SBCU_UL(1)


#define SBCU_DBGNTT_LFI_POS                  _SBCU_UL(5)
#define SBCU_DBGNTT_LFI_LEN                  _SBCU_UL(1)


#define SBCU_DBGNTT_DMAL_POS                 _SBCU_UL(6)
#define SBCU_DBGNTT_DMAL_LEN                 _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR00_POS               _SBCU_UL(16)
#define SBCU_DBGNTT_CHNR00_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR01_POS               _SBCU_UL(17)
#define SBCU_DBGNTT_CHNR01_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR02_POS               _SBCU_UL(18)
#define SBCU_DBGNTT_CHNR02_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR03_POS               _SBCU_UL(19)
#define SBCU_DBGNTT_CHNR03_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR04_POS               _SBCU_UL(20)
#define SBCU_DBGNTT_CHNR04_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR05_POS               _SBCU_UL(21)
#define SBCU_DBGNTT_CHNR05_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR06_POS               _SBCU_UL(22)
#define SBCU_DBGNTT_CHNR06_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_CHNR07_POS               _SBCU_UL(23)
#define SBCU_DBGNTT_CHNR07_LEN               _SBCU_UL(1)


#define SBCU_DBGNTT_ONE_POS                  _SBCU_UL(7)
#define SBCU_DBGNTT_ONE_LEN                  _SBCU_UL(9)


#define SBCU_DBGNTT_ONES_POS                 _SBCU_UL(1)
#define SBCU_DBGNTT_ONES_LEN                 _SBCU_UL(2)


#define SBCU_DBADRT_FPIADR_POS               _SBCU_UL(0)
#define SBCU_DBADRT_FPIADR_LEN               _SBCU_UL(32)


#define SBCU_DBBOST_FPIOPC_POS               _SBCU_UL(0)
#define SBCU_DBBOST_FPIOPC_LEN               _SBCU_UL(4)


#define SBCU_DBBOST_FPISVM_POS               _SBCU_UL(4)
#define SBCU_DBBOST_FPISVM_LEN               _SBCU_UL(1)


#define SBCU_DBBOST_FPIACK_POS               _SBCU_UL(5)
#define SBCU_DBBOST_FPIACK_LEN               _SBCU_UL(2)


#define SBCU_DBBOST_FPIRDY_POS               _SBCU_UL(7)
#define SBCU_DBBOST_FPIRDY_LEN               _SBCU_UL(1)


#define SBCU_DBBOST_FPIWR_POS                _SBCU_UL(8)
#define SBCU_DBBOST_FPIWR_LEN                _SBCU_UL(1)


#define SBCU_DBBOST_FPIRST_POS               _SBCU_UL(9)
#define SBCU_DBBOST_FPIRST_LEN               _SBCU_UL(2)


#define SBCU_DBBOST_FPIOPS_POS               _SBCU_UL(11)
#define SBCU_DBBOST_FPIOPS_LEN               _SBCU_UL(1)


#define SBCU_DBBOST_FPIRD_POS                _SBCU_UL(12)
#define SBCU_DBBOST_FPIRD_LEN                _SBCU_UL(1)


#define SBCU_DBBOST_FPIABORT_POS             _SBCU_UL(13)
#define SBCU_DBBOST_FPIABORT_LEN             _SBCU_UL(1)


#define SBCU_DBBOST_FPITOUT_POS              _SBCU_UL(14)
#define SBCU_DBBOST_FPITOUT_LEN              _SBCU_UL(1)


#define SBCU_DBBOST_FPITAG_POS               _SBCU_UL(16)
#define SBCU_DBBOST_FPITAG_LEN               _SBCU_UL(4)


#define SBCU_DBDAT_FPIDATA_POS               _SBCU_UL(0)
#define SBCU_DBDAT_FPIDATA_LEN               _SBCU_UL(32)


#define SBCU_SRC_SRPN_POS                    _SBCU_UL(0)
#define SBCU_SRC_SRPN_LEN                    _SBCU_UL(8)


#define SBCU_SRC_TOS_POS                     _SBCU_UL(10)
#define SBCU_SRC_TOS_LEN                     _SBCU_UL(1)


#define SBCU_SRC_SRE_POS                     _SBCU_UL(12)
#define SBCU_SRC_SRE_LEN                     _SBCU_UL(1)


#define SBCU_SRC_SRR_POS                     _SBCU_UL(13)
#define SBCU_SRC_SRR_LEN                     _SBCU_UL(1)


#define SBCU_SRC_CLRR_POS                    _SBCU_UL(14)
#define SBCU_SRC_CLRR_LEN                    _SBCU_UL(1)


#define SBCU_SRC_SETR_POS                    _SBCU_UL(15)
#define SBCU_SRC_SETR_LEN                    _SBCU_UL(1)

#endif
