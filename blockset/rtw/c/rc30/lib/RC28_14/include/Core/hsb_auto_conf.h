

#ifndef _HSB_CONF_H
#define _HSB_CONF_H

 



#define HSB_PATTERN_START 0xA5A5A5A5ul
#define HSB_PATTERN_END 0xC3C3C3C3ul



#define HSB_START_HSB 0x8001FF00ul


#define SB_FLASHHNDL_SIZE 0xEDC


#define HSB_CB_START 0x80004000ul


#define HSB_CB2_START 0x80040000ul


typedef struct
{
	uint32	adExtRamStart;	
	uint32	dExtRamSize;	
	uint32	xEBUMask_u32;	
	uint32	xSlavePort_32;	
	uint32	xSlavePin_32;	
	uint32	xActiveMaster_32;	
	uint32	adrPort1_u32;	
	uint32	xPort1Val_u32;	
	uint32	xPort1Msk_u32;	
	uint32	adrPort2_u32;	
	uint32	xPort2Val_u32;	
	uint32	xPort2Msk_u32;	
	uint32	adrPort3_u32;	
	uint32	xPort3Val_u32;	
	uint32	xPort3Msk_u32;	
	uint32	adCanPortAdr_u32;	
	uint32	xCanPortMsk_u32;	
	uint32	xCanPortVal_u32;	
	uint32	xTicks2Us_u32;	
	uint8	xPllK2Value_u8;	
	uint8	xPllK3Value_u8;	
	uint8	xPllVcoSelValue_u8;	
	uint8	xPllNValue_u8;	
	uint8	xWSIntFlash_u8;	
	uint8   dReserved[19];
} HWDef_t;


typedef struct
{
	uint32	adCB;	
	uint32	adCBCopyPage;	
	uint32	dSlaveRxMsgId_u32;	
	uint32	adrRST5IOCR_u32;	
	uint32	xMskRST5IOCR_u32;	
	uint32	xValRST5IOCR_u32;	
	uint32	adrRST5IN_u32;	
	uint32	xMskRST5IN_u32;	
	uint32	adrSPIBase_u32;	
	uint32	SPIPisel_u32;	
	uint32	xSPISsoc_u32;	
	uint32	xSPISsotc_u32;	
	uint32	xSPICon_u32;	
	uint32	xSPIFdr_u32;	
	uint32	xCanFDRValue_u32;	
	uint32	dSlaveTxMsgId_u32;	
	uint32	dTxMsgId;	
	uint8	dTxMsgObj;	
	uint8	dRxMsgObj;	
	uint8	dSWResetStayRBProg;	
	uint8	xCanNode;	
	uint16	xCanBdr;	
	uint8	stDisableRBProg_u8;	
	uint8	xStabi_u8;	
	uint16	xESBScan_Msk0;	
	uint16	xESBScan_Msk1;	
	uint16	xESBScan_Msk2;	
	uint16	xESBScan_Msk3;	
	uint16	xESBScan_Msk4;	
	uint16	dummy2;	
	uint32	xMain_u32;	
	uint8	xPrjVersion0_u8;	
	uint8	xPrjVersion1_u8;	
	uint8	xPrjVersion2_u8;	
	uint8	xPrjVersion3_u8;	
	uint32	dRxMsgId;	
	uint8	dReserved[0];
} PrjDef_t;


typedef struct
{
	uint32    dPatternStart;         
	uint32    xSpeedGrade_u32;       
	uint32    dPrjId_u32;            
	uint32    dPrjComp_u32;          
	HWDef_t   HWDef_s;               
	PrjDef_t  PrjDef_s;              
	uint32    dPatternEnd;           
	uint32    dCheckSum_u32;         
} HSB_Conf_t;

extern const HSB_Conf_t HSB_Conf_cs;

#endif
