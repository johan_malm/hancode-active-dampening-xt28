/***************************************************************************************************
*                                                                                                  *
*  FILENAME:                                                                                      */
/**\file        common_api.h
*
*  \brief       common definitions; based on common.h.
*
*/
/***************************************************************************************************
* (c) Copyright by BOSCH REXROTH AG, all rights reserved                                           *
****************************************************************************************************
*
* PROJECT:                   API RC30 platform
*
* FILE VERSION INFORMATION:  $Revision: 259 $
*                            $Date: 2015-04-16 12:02:47 +0200 (do, 16 apr 2015) $
*                            $Author: jasonvankolfschoten $
*
* REVISION HISTORY:
*
***************************************************************************************************/

#ifndef _COMMON_API_H
#define _COMMON_API_H

#ifndef __use64integers__
#define __use64integers__
#endif

#include "std_type.h"


#endif
