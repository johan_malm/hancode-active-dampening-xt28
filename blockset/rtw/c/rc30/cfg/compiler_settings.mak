####################################################################################################
#
# Makefile for TriCore Projects
# (c) Copyright by BOSCH REXROTH AG, all rights reserved
#
# Compiler settings for make of the RC30 platform.
#
# Sub dir: Project
#
# FILENAME: compiler_settings.mak
#
# FILE VERSION INFORMATION:
#   $Revision: 259 $
#   $Date: 2015-04-16 12:02:47 +0200 (do, 16 apr 2015) $
#   $Author: jasonvankolfschoten $
#
####################################################################################################

####################################################################################################
# compiler path
####################################################################################################

CC_PATH        = C:/HighTec/TriCore
CC_PATH_BIN    = $(CC_PATH)/bin

####################################################################################################
# Tools for the complete projekt
####################################################################################################

export CROSS     = $(CC_PATH_BIN)/tricore-
export CC        = $(CROSS)gcc
export AS        = $(CROSS)as
export AR        = $(CROSS)ar
export LD        = $(CROSS)gcc
export OC        = $(CROSS)objcopy
export OD        = $(CROSS)objdump
export CP        = $(CC_PATH_BIN)/cp
export RM        = $(CC_PATH_BIN)/rm

####################################################################################################
# switch for debug and release for the complete projekt
####################################################################################################

DEBUG     = -O0 -g2
RELEASE   = -O2
#BUILD     = $(DEBUG)
BUILD    = $(RELEASE)

####################################################################################################
# Flags for Assembler, Compiler and Linker for the complete projekt
# For verification if the linker flags are really passed to the linker use the linker option -v and
# compare the specified flags.
####################################################################################################

export WARNINGS     = -Wall -W -Wmissing-prototypes -Wundef -Wpointer-arith -Wbad-function-cast\
										  -Wcast-qual -Wcast-align -Wstrict-prototypes -Wnested-externs -Wfloat-equal\
										  -Wsign-compare
export AS_FLAGS     =
export CC_FLAGS     = $(BUILD) $(WARNINGS) -mtc13 -finline -mcpu=tc1796 -mhard-float -DGNU\
											-fmessage-length=0
export LINKER_FLAGS = -Wl,-relax -nostartfiles -Wl,--pcpmap -Wl,--extmap=a, -lm
