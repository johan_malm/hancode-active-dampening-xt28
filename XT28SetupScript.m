%% Configurationfile for all parameters used by the model
clear all, close all, clc

% Output config
cfg_minLoad = 5000;
cfg_maxLoad	= 30000;

% Saturation of output
currentSaturation_mA = 100; %[mA]

% Pressure sensor filter
tau_pressure = 1.0 / (2.0 * 3.1415);
Ts_sensor = 0.001;
alpha = tau_pressure/(tau_pressure + 0.001);

% Area of cylinders A and B side.
area_A = 0.00785; %[m^2]
area_B = 0.00589; %[m^2]

maximumFlowQ = 100/1000/60;
deltaPressure8Bar = 800000;
Kt = -0.000077;

% Pos sensor limits
minPos = [1061, 1199, 1115, 1108, 1105, 1068];
maxPos = [3864, 4200, 4126, 4039, 4096, 4046];

% Sliding mode
U = 1;

% Running libsetup
run librarySetup.m





