clc

hanCodeMainID = fopen('activeDampening_ert_rtw/activeDampening.c', 'r+');

tline = fgets(hanCodeMainID);
textArray = [tline];

abort = false;
while ischar(tline)
    
    if strcmp(strtrim(char(tline)), '/* ADDED COSTUME CODE FROM SCRIPT!!! */') == true
        disp('-------------------------------------------------------')
        disp('Abort! Code allready included')
        disp('-------------------------------------------------------')
        abort = true;
        break
    end
    
    tline = fgets(hanCodeMainID);
    if ischar(tline)
        textArray = [textArray, tline];
    else
        break
    end
    
    if strcmp(strtrim(char(tline)), 'void activeDampening_initialize(void)') == true
        disp('Adding init script after init...')
        textArray = [textArray, fgets(hanCodeMainID)];
        textArray = [textArray, char(10) '/* ADDED COSTUME CODE FROM SCRIPT!!! */' char(10)];
        
        configAddID = fopen('XT28CostumeCodeToAddToGeneretedFile/activeDampeningCostumeInit.c', 'r+');
        configAdd = fgets(configAddID);
        while ischar(configAdd)
            textArray = [textArray, configAdd];
            configAdd = fgets(configAddID);
        end
        textArray = [textArray, char(10) '/* END ADDED COSTUME CODE */' char(10) char(10)];
    end
    
    if strcmp(strtrim(char(tline)), 'void activeDampening_step(void)') == true
        disp('Adding step functions after step.. ')
        textArray = [textArray, fgets(hanCodeMainID)];
        textArray = [textArray, char(10) '/* ADDED COSTUME CODE FROM SCRIPT!!! */' char(10)];
        
        configAddID = fopen('XT28CostumeCodeToAddToGeneretedFile/activeDampeningCurrentDebuggSend.c', 'r+');
        configAdd = fgets(configAddID);
        while ischar(configAdd)
            textArray = [textArray, configAdd];
            configAdd = fgets(configAddID);
        end
        textArray = [textArray, char(10) '/* END ADDED COSTUME CODE */' char(10) char(10)];
        
    end
end

textArray = [textArray, char(10)]; % add newline at end
closeStatus = fclose('all');
if abort == false
    disp('Writing new file...')
    tempFile = fopen('activeDampening_ert_rtw/activeDampening.c', 'r+');
    fwrite(tempFile, textArray);
    closeStatus = fclose('all');
    
    disp(char(10))
    disp('-------------------------------------------------------')
    disp('Recompiling...')
    disp('-------------------------------------------------------' )
    disp(char(10))
    
    cd('activeDampening_ert_rtw/')
    system('make -f activeDampening.mk');
    cd('../')
    
    c = clock;
    disp([char(10) '-----------------------------------------------------------------'])
    disp(['Compleated added costume code to the generedted C code at: '  num2str(c(4))  ':'  num2str(c(5)) ':' num2str(round(c(6)))])
    disp(['-----------------------------------------------------------------' char(10)])
end
