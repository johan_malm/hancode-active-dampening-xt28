{
    out_ts out_s1;
    out_ts out_s2;
    out_ts out_s3;
    out_ts out_s4;
    uint16 i_mA_LeftA;
    uint16 i_mA_LeftB;
    uint16 i_mA_RightA;
    uint16 i_mA_RightB;
    
    //FRONT
    out_getStatusxt(OUT_3_POH, &out_s1); // obtain the data in output port 11 and save it to pointer out_s1
    out_getStatusxt(OUT_4_POH, &out_s2);
    i_mA_LeftA = out_s1.out_po_s.i_mA_u16;  //actual current in mA
    i_mA_LeftB = out_s2.out_po_s.i_mA_u16; //actual current in mA
    
    out_getStatusxt(OUT_1_POH, &out_s3); // obtain the data in output port 11 and save it to pointer out_s1
    out_getStatusxt(OUT_2_POH, &out_s4);
    i_mA_RightA = out_s3.out_po_s.i_mA_u16;  //actual current in mA
    i_mA_RightB = out_s4.out_po_s.i_mA_u16; //actual current in mA
    uint8 data_au8_currentCylinders[8];
    
    data_au8_currentCylinders[0] = i_mA_LeftA;
    data_au8_currentCylinders[1] = i_mA_LeftA>>8;
    data_au8_currentCylinders[2] = i_mA_LeftB;
    data_au8_currentCylinders[3] = i_mA_LeftB>>8;
    data_au8_currentCylinders[4] = i_mA_RightA;
    data_au8_currentCylinders[5] = i_mA_RightA>>8;
    data_au8_currentCylinders[6] = i_mA_RightB;
    data_au8_currentCylinders[7] = i_mA_RightB>>8;
    
    sys_enterCriticalSection();
    if (0 == can_sendData(CAN_1, 0x17FE0006, CAN_EXD_DU8, 8, data_au8_currentCylinders)) {}
    sys_leaveCriticalSection();
    
    //MID
    out_getStatusxt(OUT_7_POH, &out_s1); // obtain the data in output port 11 and save it to pointer out_s1
    out_getStatusxt(OUT_8_POH, &out_s2);
    i_mA_LeftA = out_s1.out_po_s.i_mA_u16;  //actual current in mA
    i_mA_LeftB = out_s2.out_po_s.i_mA_u16; //actual current in mA
    
    out_getStatusxt(OUT_5_POH, &out_s3); // obtain the data in output port 11 and save it to pointer out_s1
    out_getStatusxt(OUT_6_POH, &out_s4);
    i_mA_RightA = out_s3.out_po_s.i_mA_u16;  //actual current in mA
    i_mA_RightB = out_s4.out_po_s.i_mA_u16; //actual current in mA
    // uint8 data_au8_currentCylinders[8];
    
    data_au8_currentCylinders[0] = i_mA_LeftA;
    data_au8_currentCylinders[1] = i_mA_LeftA>>8;
    data_au8_currentCylinders[2] = i_mA_LeftB;
    data_au8_currentCylinders[3] = i_mA_LeftB>>8;
    data_au8_currentCylinders[4] = i_mA_RightA;
    data_au8_currentCylinders[5] = i_mA_RightA>>8;
    data_au8_currentCylinders[6] = i_mA_RightB;
    data_au8_currentCylinders[7] = i_mA_RightB>>8;
    
    sys_enterCriticalSection();
    if (0 == can_sendData(CAN_1, 0x17FE0007, CAN_EXD_DU8, 8, data_au8_currentCylinders)) {}
    sys_leaveCriticalSection();
    
    //BACK
    out_getStatusxt(OUT_11_POH, &out_s1); // obtain the data in output port 11 and save it to pointer out_s1
    out_getStatusxt(OUT_12_POH, &out_s2);
    i_mA_LeftA = out_s1.out_po_s.i_mA_u16;  //actual current in mA
    i_mA_LeftB = out_s2.out_po_s.i_mA_u16; //actual current in mA
    
    out_getStatusxt(OUT_9_POH, &out_s3); // obtain the data in output port 11 and save it to pointer out_s1
    out_getStatusxt(OUT_10_POH, &out_s4);
    i_mA_RightA = out_s3.out_po_s.i_mA_u16;  //actual current in mA
    i_mA_RightB = out_s4.out_po_s.i_mA_u16; //actual current in mA
    // uint8 data_au8_currentCylinders[8];
    
    data_au8_currentCylinders[0] = i_mA_LeftA;
    data_au8_currentCylinders[1] = i_mA_LeftA>>8;
    data_au8_currentCylinders[2] = i_mA_LeftB;
    data_au8_currentCylinders[3] = i_mA_LeftB>>8;
    data_au8_currentCylinders[4] = i_mA_RightA;
    data_au8_currentCylinders[5] = i_mA_RightA>>8;
    data_au8_currentCylinders[6] = i_mA_RightB;
    data_au8_currentCylinders[7] = i_mA_RightB>>8;
    
    sys_enterCriticalSection();
    if (0 == can_sendData( CAN_1, 0x17FE0008,
            CAN_EXD_DU8,
            8,
            data_au8_currentCylinders)
            ) {
    }
    sys_leaveCriticalSection();
}